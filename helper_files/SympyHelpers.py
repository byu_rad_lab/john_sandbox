"""
This module contains custom wrappers to simplify common sympy operations
"""

import sympy as sp


def ppt(expr, prec=4, **kwargs):

    # Precede with blank line because that's how I roll
    print()

    # Do not wrap lines by default, let my soul rejoice in largeness
    if not 'wrap_line' in kwargs:
        kwargs['wrap_line'] = False

    if isinstance(expr, (float, int)):
        if not kwargs == {}:
            sp.pprint(expr, **kwargs)
        else:
            sp.pprint(expr)
        return
    else:
        if not kwargs == {}:
            sp.pprint(sp.N(expr, prec), **kwargs)
        else:
            sp.pprint(sp.N(expr, prec))

