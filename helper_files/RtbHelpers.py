'''RTB Helpers:
Collection of useful functions that expedite and clean up
the robotics toolkit frontend.'''

import roboticstoolbox as rtb
import numpy as np


def dh2rob(dh):
    '''robot = dh2rob(dh)
    Takes in a single list of DH parameters and returns an rtb robot arm
    dh is a list or sequence of floats representing dh parameters, in the
    order [theta1, d1, a1, alpha1, theta2, ... alphaN]
    dh must be of length n * 4'''
    n = len(dh)
    if n % 4 != 0:
        print("DH is not in sets of 4, check DH")
        return None
    links = []
    for i in range(n // 4):
        j = 4 * i
        links.append(rtb.DHLink(theta=dh[j],
                                d=dh[j + 1],
                                a=dh[j + 2],
                                alpha=dh[j + 3]))
    return rtb.DHRobot(links)

def mask2dh(x, dh, mask):
    dh_out = dh
    j = 0
    for i in range(len(dh)):
        if mask[i]:
            dh[i] = x[j]
            j += 1
    return dh_out

# maybe leave these ones in the optimization folder
# def dh2b(dh, lt=[], ut=[], mask=[])