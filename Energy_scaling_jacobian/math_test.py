import numpy as np
import RoboPy as rp

dh = [[0, 0, 1, 0], [0, 0, 1, 0], [0, 0, 1, 0], [0, 0, 1, 0]]
n = len(dh)
link_inertia = [np.eye(3)] * n
mass = [1.0] * n
coms = [np.array([-0.5, 0, 0])] * n

arm = rp.SerialArmDyn(dh, link_inertia=link_inertia, mass=mass, r_com=coms)

q = np.array([i for i in range(n)])
qd = np.array([i for i in range(n)])

J = arm.jacob(q)
xd = J @ qd
Jdag = np.linalg.pinv(J)
# Jdag = np.linalg.inv(J.T @ J) @ J.T
M = arm.get_M(q)

print("J")
rp.mprint(J)

Jmin = J[[0, 1, 5], :]
JminDag = np.linalg.pinv(Jmin)

print("Jdag")
rp.mprint(Jdag)
print("M")
rp.mprint(M)
Mchol = np.linalg.cholesky(M).T
print("M^(1/2)")
rp.mprint(Mchol)
print("Jd")
JD = Mchol @ Jdag
rp.mprint(JD)
print("Jd inv")
JDinv = np.linalg.pinv(JD)
print(JDinv)
print(J)