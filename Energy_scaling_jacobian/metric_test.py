import RoboPy as rp
import numpy as np
from Jacobian_Orthogonality.source import AIM, yoshikawa, rcond
from scipy.optimize import differential_evolution

n = 4
dh = [[0, 0, 1, 0]] * n
mass = [1] * n
r_com = [np.array([-0.5, 0, 0])] * n
link_inertias = [np.eye(3)] * n
arm = rp.SerialArmDyn(dh, mass=mass, link_inertia=link_inertias, r_com=r_com)

def energy_metric(J, M):
    # check for zero rows
    mask = []
    for i in range(6):
        if not np.linalg.norm(J[i, :]) == 0:
            mask.append(i)
    Jmin = J[np.ix_(mask)]
    Jdag = np.linalg.pinv(Jmin)
    Fdag = rp.clean_rotation_matrix(Jdag.T @ M @ Jdag)
    D = np.linalg.cholesky(Fdag)
    Jd = D.T @ Jmin

    yoshikawa = np.sqrt(np.linalg.det(Jd @ Jd.T))
    condition = 1 / np.linalg.cond(Jd @ Jd.T)
    a = AIM(Jd, 'min')

    return yoshikawa, condition, a

def metric(J):
    mask = []
    for i in range(6):
        if not np.linalg.norm(J[i, :]) == 0:
            mask.append(i)
    Jmin = J[np.ix_(mask)]

    yoshikawa = np.sqrt(np.linalg.det(Jmin @ Jmin.T))
    condition = 1 / np.linalg.cond(Jmin @ Jmin.T)
    a = AIM(Jmin, 'min')

    return yoshikawa, condition, a

q0 = [0] * n

def cost(x):
    q = np.hstack((0, x))
    M = arm.get_M(q)
    J = arm.jacob(q)
    mask = []
    for i in range(6):
        if not np.linalg.norm(J[i, :]) == 0:
            mask.append(i)
    Jmin = J[np.ix_(mask)]
    Jdag = np.linalg.pinv(Jmin)
    Fdag = Jdag.T @ M @ Jdag
    D = np.linalg.cholesky(Fdag)
    Jd = D.T @ Jmin

    # # Energy Based
    # # Yoshikawa
    # w = np.sqrt(np.linalg.det(Jd @ Jd.T))
    # if np.isnan(w):
    #     w = 0

    # # Condition Number
    # w = 1 / np.linalg.cond(Jd @ Jd.T)

    # # AIM Metric
    # w = AIM(Jd, 'w')

    # # Non-energy Based
    # # Yoshikawa
    # w = np.sqrt(np.linalg.det(Jmin @ Jmin.T))
    # if np.isnan(w):
    #     w = 0

    # # Condition Number
    # w = 1 / np.linalg.cond(Jmin @ Jmin.T)

    # # AIM Metric
    w = AIM(Jmin, 'w')

    print(w)

    return -w

# q = [1,2,3,4,5]
# qd = np.array([1,2,3,4,5])
#
# M = arm.get_M(q)
# J = arm.jacob(q)
#
# mask = []
# for i in range(6):
#     if not np.linalg.norm(J[i, :]) == 0:
#         mask.append(i)
# J = J[np.ix_(mask)]
# Jdag = np.linalg.pinv(J)
# Fdag = Jdag.T @ M @ Jdag
# D = np.linalg.cholesky(Fdag)
# Jd = D.T @ J
#
# print(J)
# print(M)
# print(Jd)
#
# E = qd.T @ M @ qd
# K = qd.T @ Jd.T @ Jd @ qd
#
# print(E)
# print(K)

bounds = [(-np.pi, np.pi)] * (n - 1)
sol = differential_evolution(cost, bounds, maxiter=50)
print(sol)
qf = np.hstack([0, sol.x])

J = arm.jacob(qf)
M = arm.get_M(qf)
mask = []
for i in range(6):
    if not np.linalg.norm(J[i, :]) == 0:
        mask.append(i)
Jmin = J[np.ix_(mask)]
Jdag = np.linalg.pinv(Jmin)
Fdag = rp.clean_rotation_matrix(Jdag.T @ M @ Jdag)
D = np.linalg.cholesky(Fdag)
Jd = D.T @ Jmin

print(f"Jacobian: \n{Jmin}\n")
print(f"Jd: \n{Jd}")

viz = rp.VizScene()
viz.add_frame(rp.transl([0, 0, 0.25]))
viz.add_arm(arm)
viz.update(qf)
viz.hold()