import RoboPy as rp
import numpy as np

L = 1
arm = rp.PlanarDyn(n=3, L=L)

q = [0, 1, -1]

M = arm.get_M(q)
J = arm.jacob(q)[[0, 1, 5]]
Em, Vm = np.linalg.eig(M)


N = np.linalg.inv(J.T) @ M @ np.linalg.inv(J)
En, Vn = np.linalg.eig(N)

Ev, Vv = np.linalg.eig(N[0:2, 0:2])

print(f"M:\n{M}\nEigenvalues of M:\n{Em}\nEigenvectors of M:\n{Vm}\nN:\n{N}\nEigenvalues of N:\n{En}\nEigenvectors of N:\n{Vn}")
print(f"Eigenvalues of Nv:\n{Ev}\nEigenvectors of Nv:\n{Vv}")


