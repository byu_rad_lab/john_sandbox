import RoboPy as rp
import numpy as np

np.set_printoptions(precision=3)


UNIT_SCALE = 1
dh = [[0, 0, UNIT_SCALE, 0]] * 3
linear_density = 1 / UNIT_SCALE
# mass = [linear_density * x[2] for x in dh]
# r_com = [np.array([x[2] / 2, 0, 0]) for x in dh]
# inertia_tensor = [np.diag([0, m * l[2]**2 / 12, m * l[2]**2 / 12]) for m, l in zip(mass, dh)]
motor_mass = 0.0
arm = rp.SimpleDynArm(dh, linear_density=linear_density, motor_mass=motor_mass)

# print(arm.link_inertia)

q = [0, 0.1, 0.2]
J = arm.jacob(q)[[0, 1, 5]]
Jinv = np.linalg.inv(J)

print(J)

M = arm.get_M(q)
N = Jinv.T @ M @ Jinv / UNIT_SCALE**2

nv = np.linalg.norm(N[0:2, 0:2])
nw = np.linalg.norm(N[2:3, 2:3])

Js = np.copy(J)
Js[0:2] = Js[0:2] * np.sqrt(nv)
Js[2:3] = Js[2:3] * np.sqrt(nw)

print(Js)


"""
unscaled, S = 1
[[-0.70317543  0.13829553  0.27941549]
 [ 0.51048005 -0.02982223  0.96017021]
 [ 1.          1.          1.        ]]
unscaled, S = 100
[[-70.31755066  13.82954407  27.94154358]
 [ 51.04801559  -2.98221588  96.01702881]
 [  1.           1.           1.        ]]
scaled, S = 1
[[-1.17906806  0.2318907   0.46851734]
 [ 0.85596096 -0.05000522  1.60999088]
 [ 0.81734293  0.81734293  0.81734293]]
scaled, S = 100
[[-1.17906806  0.2318907   0.46851734]
 [ 0.85596096 -0.05000522  1.60999088]
 [ 0.81734293  0.81734293  0.81734293]]
"""