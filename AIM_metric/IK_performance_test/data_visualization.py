import numpy as np
import matplotlib.pyplot as plt
from metric_calculation import metrics


def plot_ik_survey(test_data, scale=False):

    fig, axs = plt.subplots(1, len(metrics))
    y = np.asarray(test_data['ik'])
    if scale:
        y = y / max(y)

    counter = 0
    for key in metrics:
        x = np.asarray(test_data[key])
        if scale:
            x = x / max(max(x), 0.01)
        axs[counter].scatter(x, y)
        axs[counter].set_xlabel(key)
        counter += 1

    return fig, axs
