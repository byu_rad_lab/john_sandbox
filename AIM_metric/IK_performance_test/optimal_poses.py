import RoboPy as rp
import numpy as np
import metric_calculation
from scipy import optimize

# arm1 = rp.panda()
# arm2 = rp.panda()

dh = [[0, 0, 1, 0]] * 3
arm1 = rp.SerialArm(dh=dh)
arm2 = rp.SerialArm(dh=dh)

arm2.base = rp.transl([0, 3, 0])
viz = rp.VizScene()
viz.add_arm(arm1)
viz.add_arm(arm2)

score1 = lambda q: -metric_calculation.rcond(arm1.jacob(np.hstack([0.0, q])), planar=True)
score2 = lambda q: -metric_calculation.yoshikawa(arm2.jacob(np.hstack([0.0, q])), planar=True)

# def jacobian(q, func):
#     q0 = np.copy(q)
#     n = len(q)
#     y = np.zeros((n,))
#     eps = 1e-1
#     for i in range(n):
#         q1 = np.asarray([eps if j == i else 0 for j in range(n)], dtype=np.float64) + q0
#         y[i] = (func(q1) - func(q0)) / eps
#
#     return y

bounds = [(-np.pi, np.pi)] * (arm1.n - 1)

sol1 = optimize.brute(score1, ranges=bounds, Ns=435)
x1 = np.hstack([0.0, sol1])

sol2 = optimize.brute(score2, ranges=bounds, Ns=435)
x2 = np.hstack([0.0, sol2])

# sol1 = optimize.shgo(score1, bounds, iters=5, n=100,
#                      minimizer_kwargs={'method': 'Nelder-Mead', 'callback': lambda xk: viz.update([np.hstack([0.0, xk]), np.zeros((arm2.n,))])},
#                      options={'minimize_every_iter': True})

# sol1 = optimize.minimize(score1, x0=np.ones(arm1.n - 1), method='Nelder-Mead', callback=lambda xk: viz.update([np.hstack([0.0, xk]), np.zeros((arm2.n,))]))
# # sol1 = optimize.differential_evolution(score1, bounds, callback=lambda xk, convergence: viz.update([np.hstack([0.0, xk]), np.zeros((arm2.n,))]), polish=True, popsize=1000, maxiter=200)
# print(sol1)
# x1 = np.hstack([0.0, sol1.x])
# print()
#
# sol2 = optimize.minimize(score2, x0=np.ones(arm2.n - 1), method='Nelder-Mead', callback=lambda xk: viz.update([x1, np.hstack([0.0, xk])]))
# # sol2 = optimize.differential_evolution(score2, bounds, callback=lambda xk, convergence: viz.update([x1, np.hstack([0.0, xk])]), polish=True, popsize=1000, maxiter=200)
# print(sol2)
# x2 = np.hstack([0.0, sol2.x])
# print()

J1 = arm1.jacob(x1)
J2 = arm1.jacob(x2)

rp.mprint(J1, eps=1e-4)
print()
rp.mprint(J2, eps=1e-4)
print()

rp.mprint(metric_calculation.findY(J1))
print()
rp.mprint(metric_calculation.findY(J2))

viz.update([x1, x2])
viz.hold()

