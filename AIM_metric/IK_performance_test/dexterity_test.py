import RoboPy as rp
import numpy as np
import metric_calculation
import arm_generation
from multiprocessing import Pool
from tqdm import tqdm


def get_random_pose(reach, rng=np.random.default_rng(None)):
    p = rng.random((3,))
    p = p / np.linalg.norm(p) * reach * rng.random()
    random_quaternion = rng.random((4,))
    random_quaternion = random_quaternion / np.linalg.norm(random_quaternion)
    R = rp.q2R(random_quaternion)
    A = rp.se3(R, p)
    return A


def ik_test_standard(arm=rp.SerialArm(dh=[]), p=100, seed=None):
    n_successful_trial = 0
    rng = np.random.default_rng(seed)
    for i in range(p):
        # print(f"{i + 1} / {p}")
        # get random pose
        A = get_random_pose(arm.reach, rng)

        # get random starting point
        q = rng.random((arm.n,)) * 2 * np.pi - np.pi

        ik_sol = arm.ik(A, q, method='pinv', rep='q', mit=50, tol=1e-2, mindel=1e-5)

        if ik_sol.status:
            n_successful_trial += 1

    return n_successful_trial / p


def ik_test_jointspace(arm=rp.SerialArm(dh=[]), p=100, seed=None):
    rng = np.random.default_rng(seed)
    n_successful_trial = 0
    for i in range(p):
        qA = rng.random((arm.n,)) * 2 * np.pi - np.pi
        A = arm.fk(qA)
        q = rng.random((arm.n,)) * 2 * np.pi - np.pi

        ik_sol = arm.ik(A, q, method='pinv', rep='q', mit=10, tol=1e-2, mindel=1e-5, retry=1)

        if ik_sol.status:
            n_successful_trial += 1

    return n_successful_trial / p


def conduct_ik_survey(k=10, p_metric=100, p_ik=100, n=7, L=2.0, ik_test='standard'):

    test_data = {'w': [], 'min': [], 'yosh': [], 'rcond': [], 'basis': [], 'ik': []}
    for i in tqdm(range(k)):
        print(f"{i + 1} / {k}")
        arm = arm_generation.generate_random_arm(n=n, L=L, seed=i)
        # test_data['arm'].append(arm)
        test_data['w'].append(
            metric_calculation.get_global_measure(arm, metric_calculation.aim, p=p_metric, opt='w', seed=i))
        test_data['min'].append(
            metric_calculation.get_global_measure(arm, metric_calculation.aim, p=p_metric, opt='min', seed=i))
        test_data['yosh'].append(
            metric_calculation.get_global_measure(arm, metric_calculation.yoshikawa, p=p_metric, seed=i))
        test_data['rcond'].append(
            metric_calculation.get_global_measure(arm, metric_calculation.rcond, p=p_metric, seed=i))
        test_data['basis'].append(
            metric_calculation.get_global_measure(arm, metric_calculation.basis, p=p_metric, seed=i))
        if ik_test == 'standard':
            test_data['ik'].append(ik_test_standard(arm, p_ik, seed=i))
        elif ik_test == 'jointspace':
            test_data['ik'].append(ik_test_jointspace(arm, p_ik, seed=i))

    return test_data


def f(x, k, n, L, p_metric, p_ik, ik_test):
    print(f"{ik_test}: [{x + 1}] / [{k}]")
    arm = arm_generation.generate_random_arm(n=n, L=L, seed=x)
    output = {}
    output['w'] = metric_calculation.get_global_measure(arm, metric_calculation.aim, p=p_metric, opt='w', seed=x)
    output['min'] = metric_calculation.get_global_measure(arm, metric_calculation.aim, p=p_metric, opt='min', seed=x)
    # output['fro'] = metric_calculation.get_global_measure(arm, metric_calculation.aim, p=p_metric, opt='fro', seed=x)
    # output['sum'] = metric_calculation.get_global_measure(arm, metric_calculation.aim, p=p_metric, opt='sum', seed=x)
    output['yosh'] = metric_calculation.get_global_measure(arm, metric_calculation.yoshikawa, p=p_metric, seed=x)
    output['rcond'] = metric_calculation.get_global_measure(arm, metric_calculation.rcond, p=p_metric, seed=x)
    # output['basis'] = metric_calculation.get_global_measure(arm, metric_calculation.basis, p=p_metric, seed=x)
    if ik_test == 'standard':
        output['ik'] = ik_test_standard(arm, p_ik, seed=x)
    elif ik_test == 'jointspace':
        output['ik'] = ik_test_jointspace(arm, p_ik, seed=x)
    else:
        print("warning! invalid IK test option!", ik_test)

    return output


def conduct_ik_survey_parallel(k=10, p_metric=100, p_ik=100, n=7, L=2.0, ik_test='standard'):

    with Pool() as p:
        pool_output = p.starmap(f, [(i, k, n, L, p_metric, p_ik, ik_test) for i in range(k)])

    final_output = {}
    for key in pool_output[0].keys():
        final_output[key] = []

    for key in final_output.keys():
        for d in pool_output:
            final_output[key].append(d[key])

    return final_output


def conduct_aim_survey(k=10, p_metric=100, p_ik=100, n=7, L=2.0, ik_test='standard'):

    test_data = {'arm': [], 'w': [], 'min': [], 'fro': [], 'sum': [], 'basis': [], 'ik': []}
    for i in tqdm(range(k)):
        print(f"{i + 1} / {k}")
        arm = arm_generation.generate_random_arm(n=n, L=L)
        test_data['arm'].append(arm)
        test_data['w'].append(metric_calculation.get_global_measure(arm, metric_calculation.aim, p=p_metric, opt='w'))
        test_data['min'].append(
            metric_calculation.get_global_measure(arm, metric_calculation.aim, p=p_metric, opt='min'))
        test_data['fro'].append(
            metric_calculation.get_global_measure(arm, metric_calculation.aim, p=p_metric, opt='fro'))
        test_data['sum'].append(
            metric_calculation.get_global_measure(arm, metric_calculation.aim, p=p_metric, opt='sum'))
        test_data['basis'].append(
            metric_calculation.get_global_measure(arm, metric_calculation.aim, p=p_metric))
        if ik_test == 'standard':
            test_data['ik'].append(ik_test_standard(arm, p_ik))
        elif ik_test == 'jointspace':
            test_data['ik'].append(ik_test_jointspace(arm, p_ik))

    return test_data

