import scipy.io
import scipy.stats
import data_visualization
import matplotlib.pyplot as plt
import numpy as np
import perform_metric_test
from metric_calculation import metrics
import matplotlib
matplotlib.rcParams.update({'font.size': 19})


k = str(perform_metric_test.k)
L = str(perform_metric_test.L)

# standard_ik_test_data = scipy.io.loadmat(f'test_data_k={k}_L={L}_method=standard.mat', squeeze_me=True)
# y = standard_ik_test_data['ik']
# spearman_scores_standard = {}
# p_scores_standard = {}
# for key in standard_ik_test_data.keys():
#     if key in metrics:
#         x = standard_ik_test_data[key]
#         r, p = scipy.stats.spearmanr(x, y, alternative='greater')
#         spearman_scores_standard[key] = r
#         p_scores_standard[key] = p

jointspace_ik_test_data = scipy.io.loadmat(f'test_data_k={k}_L={L}_method=jointspace.mat', squeeze_me=True)

y = jointspace_ik_test_data['ik']
spearman_scores_jointspace = {}
p_scores_jointspace = {}
for key in jointspace_ik_test_data.keys():
    if key in metrics:
        x = jointspace_ik_test_data[key]
        r, p = scipy.stats.spearmanr(x, y, alternative='greater')
        spearman_scores_jointspace[key] = r
        p_scores_jointspace[key] = p

# print(spearman_scores_standard)
print(spearman_scores_jointspace)

print()
# print(p_scores_standard)
print(p_scores_jointspace)

fig1, axs1 = plt.subplots(1, 2)
axs1 = np.ravel(axs1)

fig2, axs2 = plt.subplots(1, 2)
axs2 = np.ravel(axs2)
# y1 = standard_ik_test_data['ik']
y2 = jointspace_ik_test_data['ik']

labels = metrics

# for i, key in enumerate(metrics):
#     x = jointspace_ik_test_data[key]
#     x = x / max(max(x), 0.01)
#     axs1[i].scatter(x, y1, c='k', s=1)
#     axs1[i].set_xlabel(labels[i])
#     axs1[i].set_ylabel('IK Test Score')
#
#     axs2[i].scatter(x, y2, c='k', s=1)
#     axs2[i].set_xlabel(labels[i])
#     axs2[i].set_ylabel('IK Test Score')

axs1[0].scatter(jointspace_ik_test_data['min'], y2, c='k', s=1)
axs1[1].scatter(jointspace_ik_test_data['w'], y2, c='k', s=1)
axs1[0].set_ylabel('IK Test Score')
axs1[0].set_xlabel('Minimum angle AIM')
axs1[1].set_xlabel('Weighted sum AIM')

axs2[0].scatter(jointspace_ik_test_data['yosh'], y2, c='k', s=1)
axs2[1].scatter(jointspace_ik_test_data['rcond'], y2, c='k', s=1)
axs2[0].set_ylabel('IK Test Score')
axs2[0].set_xlabel('Yoshikawa Metric')
axs2[1].set_xlabel('Inverse Condition #')

plt.show()
