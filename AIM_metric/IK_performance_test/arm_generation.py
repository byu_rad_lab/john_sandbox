import RoboPy as rp
import numpy as np
from numpy.random import default_rng


panda = rp.Panda()
human = rp.HumanArm()


def generate_random_arm(n=7, L=2.0, seed=None):
    rng = default_rng(seed)
    dh = rng.random((n, 4)) * 2 - 1
    length = np.sum(np.linalg.norm(dh[:, [0, 2]], axis=1))
    dh[:, [0, 2]] *= L / length
    dh[:, [1, 3]] *= np.pi
    arm = rp.SerialArm(dh)
    arm.set_qlim_warnings(False)
    return arm
