import numpy as np
import RoboPy as rp
from itertools import combinations
import warnings
warnings.filterwarnings("error")


def findY(J):
    n = J.shape[1]
    vN = np.linalg.norm(J[0:3, :], axis=0)
    J[0:3, :] = J[0:3, :] / np.max(vN)
    N = np.linalg.norm(J, axis=0)
    Jn = J / N
    Y = np.minimum(np.abs(Jn.T @ Jn), 1)
    Y = np.arccos(Y) / np.pi * 2
    return Y


def aim(J, method='min', planar=False):
    if planar:
        J = J[[0, 1, 5]]
    # Calculate Y
    n = J.shape[1]
    vN = np.linalg.norm(J[0:3, :], axis=0)
    J[0:3, :] = J[0:3, :] / np.max(vN)
    N = np.linalg.norm(J, axis=0)
    Jn = J / N
    Y = np.minimum(np.abs(Jn.T @ Jn), 1) - np.eye(n)

    # Find metric
    if method == 'w':
        N = int((n ** 2 - n) / 2)
        angles = np.arccos(Y[np.tril_indices_from(Y, k=-1)]) / np.pi * 2
        angles.sort()
        W = np.asarray([a / (i + 1)**2 for i, a in enumerate(angles)])
        y = np.sum(W)
    elif method == 'min':
        y = np.arccos(np.max(Y)) / np.pi * 2
    elif method == 'fro':
        y = np.linalg.norm(np.arccos(Y) / np.pi * 2, 'fro') / (n ** 2 - n)
    elif method == 'sum':
        y = np.sum(np.arccos(Y) / np.pi * 2) / (n ** 2 - n)
    else:
        print(method, " is not a valid metric method!")
        return None

    return y


def basis(A: np.ndarray, planar=False):
    m = A.shape[0]
    n = A.shape[1]
    # r = np.linalg.matrix_rank(A)
    r = min(m, n)
    # print("Rank: ", r)
    p = n - r + 1
    gamma = []
    for i in range(n):
        g = []
        Acols = [a for a in A.T]
        a = Acols.pop(i)
        if np.linalg.norm(a) == 0:
            g = [0] * p
        else:
            Acol_combinations = combinations(Acols, r - 1)
            for Abar in Acol_combinations:
                Abar = np.asarray(Abar).T
                # Adag = np.linalg.inv(Abar.T @ Abar) @ Abar.T
                Adag = np.linalg.pinv(Abar)
                ap = Abar @ Adag @ a
                if np.linalg.norm(ap) == 0:
                    g.append(1)
                else:
                    y = a @ ap / (np.linalg.norm(a) * np.linalg.norm(ap))
                    y = np.clip(y, 0, 1)
                    try:
                        g.append(2 / np.pi * np.arccos(y))
                    except:
                        print("Exception:\n", f"arccos({y})")
                        g.append(np.nan)
        gamma.append(g)
    Y = np.asarray(gamma)
    return np.min(Y)



def yoshikawa(J, planar=False):
    if planar:
        J = J[[0, 1, 5]]
    y = np.sqrt(np.max(np.linalg.det(J @ J.T), 0))
    if np.isnan(y):
        y = 0.0
    return y


def rcond(J, planar=False):
    if planar:
        J = J[[0, 1, 5]]
    return 1 / np.linalg.cond(J @ J.T)


def get_global_measure(arm=rp.Panda(), metric=aim, p=100, opt=None, seed=None):
    n = arm.n
    s = 0.0
    rng = np.random.default_rng(seed)
    qs = rng.random((p, n)) * 2 * np.pi - np.pi
    for q in qs:
        J = arm.jacob(q)

        # vN = np.linalg.norm(J[0:3, :], axis=0)
        # J[0:3, :] = J[0:3, :] / np.max(vN)

        if opt is None:
            s += metric(J)
        else:
            s += metric(J, opt)

    return s / p


metrics = ('w', 'min', 'yosh', 'rcond')