import scipy.io
import time

import dexterity_test
import data_visualization

k = 1000
n = 7
L = 1.0
p_metric = 200
p_ik = 150

if __name__ == '__main__':

    tick = time.perf_counter()

    # data = dexterity_test.conduct_ik_survey_parallel(k, p_metric, p_ik, n, L, ik_test='standard')
    # data_visualization.plot_ik_survey(data, scale=True)
    # scipy.io.savemat(f"test_data_k={k}_L={L}_method=standard.mat",
    #                  mdict=data)

    data = dexterity_test.conduct_ik_survey_parallel(k, p_metric, p_ik, n, L, ik_test='jointspace')
    data_visualization.plot_ik_survey(data, scale=True)
    scipy.io.savemat(f"test_data_k={k}_L={L}_method=jointspace.mat",
                     mdict=data)

    tock = time.perf_counter()
    print(tock - tick)
