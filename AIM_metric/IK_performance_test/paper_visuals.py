import RoboPy as rp
from numpy import pi
from metric_calculation import findY


viz = rp.VizScene()
arm = rp.SerialArm(dh=[[0, 0, 1, 0]] * 3)
viz.add_arm(arm)
q = [0, pi/2, pi]
viz.update(q)
rp.mprint(arm.jacob(q))
rp.mprint(findY(arm.jacob(q)))
viz.hold()

