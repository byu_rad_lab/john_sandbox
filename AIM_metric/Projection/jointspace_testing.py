import numpy as np
import RoboPy as rp
import matplotlib.pyplot as plt
from source import aim, yoshikawa, rcond


dh = [[0, 0, 1, 0], [0, 0, 1, 0], [0, 0, 1, 0]]
L = 3
arm = rp.SerialArm(dh)
m = 10000
# qs = np.asarray([[[i, j] for i in np.linspace(0, 2 * np.pi, m)] for j in np.linspace(0, 2 * np.pi, m)])
qs = (np.random.random((m, len(dh))) * 2 - 1) * np.pi

k = 70
# metrics_js = np.zeros((m, m, 3))
metrics_xs = np.zeros((k, k, 3))

# for i in range(m):
#     for j in range(m):
#     q = qs[i, j]

for q in qs:
    x = arm.fk(q, rep='cart')[0:2]
    a = min(int(((x[0] + L) / (2 * L)) * k), k - 1)
    b = min(int(((x[1] + L) / (2 * L)) * k), k - 1)
    J = arm.jacob(q)[[0, 1, 5]]

    y, r, c = yoshikawa(J), rcond(J), aim(J)
    # metrics_js[i, j, 0] = y
    # metrics_js[i, j, 1] = r
    # metrics_js[i, j, 2] = c

    ap = k - 1 - a
    bp = k - 1 - b

    metrics_xs[a, b, 0] = max(y, metrics_xs[a, b, 0])
    metrics_xs[a, b, 1] = max(r, metrics_xs[a, b, 1])
    metrics_xs[a, b, 2] = max(c, metrics_xs[a, b, 2])

    metrics_xs[ap, b, 0] = max(y, metrics_xs[ap, b, 0])
    metrics_xs[ap, b, 1] = max(r, metrics_xs[ap, b, 1])
    metrics_xs[ap, b, 2] = max(c, metrics_xs[ap, b, 2])

    metrics_xs[a, bp, 0] = max(y, metrics_xs[a, bp, 0])
    metrics_xs[a, bp, 1] = max(r, metrics_xs[a, bp, 1])
    metrics_xs[a, bp, 2] = max(c, metrics_xs[a, bp, 2])

    metrics_xs[ap, bp, 0] = max(y, metrics_xs[ap, bp, 0])
    metrics_xs[ap, bp, 1] = max(r, metrics_xs[ap, bp, 1])
    metrics_xs[ap, bp, 2] = max(c, metrics_xs[ap, bp, 2])

    metrics_xs[b, a, 0] = max(y, metrics_xs[b, a, 0])
    metrics_xs[b, a, 1] = max(r, metrics_xs[b, a, 1])
    metrics_xs[b, a, 2] = max(c, metrics_xs[b, a, 2])

    metrics_xs[bp, a, 0] = max(y, metrics_xs[bp, a, 0])
    metrics_xs[bp, a, 1] = max(r, metrics_xs[bp, a, 1])
    metrics_xs[bp, a, 2] = max(c, metrics_xs[bp, a, 2])

    metrics_xs[b, ap, 0] = max(y, metrics_xs[b, ap, 0])
    metrics_xs[b, ap, 1] = max(r, metrics_xs[b, ap, 1])
    metrics_xs[b, ap, 2] = max(c, metrics_xs[b, ap, 2])

    metrics_xs[bp, ap, 0] = max(y, metrics_xs[bp, ap, 0])
    metrics_xs[bp, ap, 1] = max(r, metrics_xs[bp, ap, 1])
    metrics_xs[bp, ap, 2] = max(c, metrics_xs[bp, ap, 2])


fig, axs = plt.subplots(1, 3)

for i in range(3):
    axs[i].matshow(metrics_xs[:, :, i], cmap='gist_heat')

plt.show()
