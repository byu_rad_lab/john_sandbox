import numpy as np


def yoshikawa(J):
    d = np.linalg.det(J @ J.T)
    y = np.sqrt(max(0.0, d))
    return y


def rcond(J):
    r = 1 / np.linalg.cond(J @ J.T)
    return r


def aim(J):
    # Calculate Y
    n = J.shape[1]
    N = np.linalg.norm(J, axis=0)
    Jn = J / N
    Y = np.minimum(np.abs(Jn.T @ Jn), 1) - np.eye(n)
    y = np.arccos(np.max(Y)) / np.pi * 2
    return y
