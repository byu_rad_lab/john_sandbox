import RoboPy as rp
import numpy as np
from PyQt5 import QtCore, QtWidgets
import pyqtgraph as pg
from pyqtgraph import opengl as gl


app = QtWidgets.QApplication([])
mainwindow = QtWidgets.QMainWindow()
mainwindow.setGeometry(200, 200, 1400, 800)
mainwindow.show()

w = gl.GLViewWidget()
mainwindow.setCentralWidget(w)

w.addItem(gl.GLLinePlotItem(pos=np.array([[0, 0, 0],
                                          [1, 0, 0],
                                          [0, 0, 0],
                                          [0, 1, 0],
                                          [0, 0, 0],
                                          [0, 0, 1]]),
                            color=np.array([[1, 0, 0, 1],
                                            [1, 0, 0, 1],
                                            [0, 1, 0, 1],
                                            [0, 1, 0, 1],
                                            [0, 0, 1, 1],
                                            [0, 0, 1, 1]]),
                            mode='lines'))

w.addItem(gl.GLGridItem())

A = np.array([0.57559515, 0.40471938, 0.01062274])
dh = [[0, 0, a, 0] for a in A]
arm = rp.SerialArm(dh)

q0 = np.array([0, 0, 0])
sol = arm.ik([0.5, 0.5, np.pi / 2], q0=q0, method='pinv', rep='planar', maxdel=0.01)
qf = sol.qf
qs = sol.qs

L0 = np.linalg.norm(qf - q0)
L1 = 0
for i in range(1, len(qs)):
    L1 += np.linalg.norm(qs[i] - qs[i - 1])

print(f"Minimum length: {L0}\nReal length: {L1}\nRatio: {L1 / L0}\nDifference: {L1 - L0}")

minLine = gl.GLLinePlotItem(pos=np.vstack([q0, qf]), color=[1, 1, 1, 1])
trueLine = gl.GLLinePlotItem(pos=qs, color=[1, 0.5, 0.5, 1])

w.addItem(minLine)
w.addItem(trueLine)

from scipy.optimize import minimize

def cost(x):
    dh = [[0, 0, a, 0] for a in x]
    arm = rp.SerialArm(dh)

    q0 = np.array([0, 0, 0])
    sol = arm.ik([0.5, 0.5, np.pi / 2], q0=q0, method='pinv', rep='planar', maxdel=0.01)
    qf = sol.qf
    qs = sol.qs

    L0 = np.linalg.norm(qf - q0)
    L1 = 0
    for i in range(1, len(qs)):
        L1 += np.linalg.norm(qs[i] - qs[i - 1])

    return L1 / L0

def constr(x):
    dh = [[0, 0, a, 0] for a in x]
    arm = rp.SerialArm(dh)

    q0 = np.array([0, 0, 0])
    sol = arm.ik([0.5, 0.5, np.pi / 2], q0=q0, method='pinv', rep='planar', maxdel=0.01)

    return np.linalg.norm(sol.ef) - 0.01

def callback(x):
    dh = [[0, 0, a, 0] for a in x]
    arm = rp.SerialArm(dh)

    q0 = np.array([0, 0, 0])
    sol = arm.ik([0.5, 0.5, np.pi / 2], q0=q0, method='pinv', rep='planar', maxdel=0.01)
    qf = sol.qf
    qs = sol.qs

    minLine.setData(pos=np.vstack([q0, qf]))
    trueLine.setData(pos=qs)

    L0 = np.linalg.norm(qf - q0)
    L1 = 0
    for i in range(1, len(qs)):
        L1 += np.linalg.norm(qs[i] - qs[i - 1])

    print(x, L1 / L0, np.linalg.norm(sol.ef) - 0.01)
    app.processEvents()

sol = minimize(cost, x0=A, callback=callback, constraints={'type':'ineq', 'fun':constr}, method='Nelder-mead', bounds=[(0, 5), (0, 5), (0, 5)])

x = sol.x
dh = [[0, 0, a, 0] for a in x]
arm = rp.SerialArm(dh)

q0 = np.array([0, 0, 0])
sol = arm.ik([0.5, 0.5, np.pi / 2], q0=q0, method='pinv', rep='planar', maxdel=0.01)
qf = sol.qf
qs = sol.qs

minLine.setData(pos=np.vstack([q0, qf]))
trueLine.setData(pos=qs)
app.processEvents()

viz = rp.VizScene()
viz.add_arm(arm)
viz.add_marker(pos=arm.fk(q0, rep='cart'), color=[0, 1, 0, 1])
viz.add_marker(pos=arm.fk(qf, rep='cart'), color=[1, 0, 0, 1])

while True:
    for q in qs:
        viz.update(q)
        for i in range(20):
            app.processEvents()
    for q in np.flip(qs, axis=0):
        viz.update(q)
        for i in range(20):
            app.processEvents()

app.exec()
