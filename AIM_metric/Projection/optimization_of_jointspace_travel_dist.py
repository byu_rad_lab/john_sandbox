import RoboPy as rp
import numpy as np
from pymoo.core.problem import ElementwiseProblem
from pymoo.algorithms.moo.nsga2 import NSGA2
from pymoo.operators.crossover.sbx import SBX
from pymoo.operators.mutation.pm import PM
from pymoo.operators.sampling.rnd import FloatRandomSampling
from pymoo.termination import get_termination
from pymoo.optimize import minimize


class MyProblem(ElementwiseProblem):

    def __init__(self):
        super().__init__(n_var=3,
                         n_obj=1,
                         n_ieq_constr=1,
                         xl=np.array([0, 0, 0], dtype=float),
                         xu=np.array([2, 2, 2], dtype=float))

    def _evaluate(self, x : np.ndarray, out : dict, *args, **kwargs):
        dh = [[0, 0, a, 0] for a in x]
        arm = rp.SerialArm(dh)

        q0 = [0, 0, 0]
        sol = arm.ik([0.5, 0.5, np.pi / 2], q0=q0, method='pinv', rep='planar', maxdel=0.01, mit=200)
        if sol.status:
            g = 0  # constraint fulfilled

            qf = sol.qf
            qs = sol.qs

            L0 = np.linalg.norm(qf - q0)
            L1 = 0
            for i in range(1, len(qs)):
                L1 += np.linalg.norm(qs[i] - qs[i - 1])

            f = L1 / L0

        else:
            g = np.linalg.norm(sol.ef)
            f = np.inf

        out['F'] = f
        out['G'] = g


problem = MyProblem()
termination = get_termination("n_gen", 20)
algorithm = NSGA2(
    pop_size=50,
    n_offsprings=15,
    sampling=FloatRandomSampling(),
    crossover=SBX(prob=0.9, eta=15),
    mutation=PM(eta=20),
    eliminate_duplicates=True
)

sol = minimize(problem,
               algorithm,
               termination,
               seed=1,
               save_history=True,
               verbose=True)

X = sol.X
F = sol.F

print(X)
print(F)
