import numpy as np
import RoboPy as rp
import warnings
warnings.filterwarnings("error")
from itertools import combinations
import tqdm

import matplotlib.pyplot as plt


def measure_orthogonality(A: np.ndarray):
    m = A.shape[0]
    n = A.shape[1]
    # r = np.linalg.matrix_rank(A)
    r = min(m, n)
    # print("Rank: ", r)
    p = n - r + 1
    gamma = []
    for i in range(n):
        g = []
        Acols = [a for a in A.T]
        a = Acols.pop(i)
        if np.linalg.norm(a) == 0:
            g = [0] * p
        else:
            Acol_combinations = combinations(Acols, r - 1)
            for Abar in Acol_combinations:
                Abar = np.asarray(Abar).T
                # Adag = np.linalg.inv(Abar.T @ Abar) @ Abar.T
                Adag = np.linalg.pinv(Abar)
                ap = Abar @ Adag @ a
                if np.linalg.norm(ap) == 0:
                    g.append(1)
                else:
                    y = a @ ap / (np.linalg.norm(a) * np.linalg.norm(ap))
                    y = np.clip(y, 0, 1)
                    try:
                        g.append(2 / np.pi * np.arccos(y))
                    except:
                        print("Exception:\n", f"arccos({y})")
                        g.append(np.nan)
        gamma.append(g)
    return np.asarray(gamma)


def yoshikawa(A):
    y = np.linalg.det(A @ A.T)
    if y <= 0:
        return np.sqrt(np.abs(y))
    else:
        return y


n = 3
dh = [[0, 0, 0.5, 0], [0, 0, 0.5, 0], [0, 0, 0.5, 0], [0, 0, 0.5, 0]]
arm = rp.SerialArm(dh)

p = 1000
qs = arm.randq(p)

gammas = np.zeros((p, n))
yosh = np.zeros((p,))

for i, q in enumerate(qs):
    J = arm.jacob(q)[[0, 1, 5]]
    yosh[i] = yoshikawa(J)
    gammas[i] = np.squeeze(measure_orthogonality(J))

fig, axs = plt.subplots(2, 1)
axs[0].scatter(np.linspace(0, 1, p), yosh, c='k')
axs[1].scatter(np.linspace(0, 1, p), gammas[:, 0], c='r')
axs[1].scatter(np.linspace(0, 1, p), gammas[:, 1], c='b')
axs[1].scatter(np.linspace(0, 1, p), gammas[:, 2], c='g')

print(np.mean(yosh), np.mean(gammas, axis=0))

plt.show()
