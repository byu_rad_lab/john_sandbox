import numpy as np
import RoboPy as rp
import matplotlib.pyplot as plt
import multiprocessing
from scipy.linalg import null_space


dh_yosh = [[0, 0, x, 0] for x in [1.23807624e-04, 9.17323921e-01, 1.08253541e+00, 1.68651303e-05]]
dh_rcond = [[0, 0, x, 0] for x in [1.06008034, 0.9363885,  0.00124693, 0.00228423]]
dh_aim = [[0, 0, x, 0] for x in [9.03965134e-01, 6.51821144e-01, 4.44030365e-01, 1.83357817e-04]]
dh_default = [[0, 0, x, 0] for x in [0.5, 0.5, 0.5, 0.5]]
dh_basis = [[0, 0, x, 0] for x in [0.7699714726086322, 0.6717047844196011, 0.23531477449062904, 0.32300896848113764]]

arm_yosh = rp.SerialArm(dh_yosh, base=rp.transl([0, 1, 0]))
arm_rcond = rp.SerialArm(dh_rcond, base=rp.transl([0, 2, 0]))
arm_aim = rp.SerialArm(dh_aim, base=rp.transl([0, 3, 0]))
arm_default = rp.SerialArm(dh_default, base=rp.transl([0, 0, 0]))
arm_basis = rp.SerialArm(dh_basis, base=rp.transl([0, 0, 4]))

arm = arm_aim

p = 100000
dims = (30, 30)
# binned_workspace = np.zeros(dims)
bounds = [(-2, 2), (-2, 2)]
bound_lengths = [b[1] - b[0] for b in bounds]


def pose2index(pose):
    index = [0] * len(dims)
    for i, x in enumerate(pose):
        index[i] = min(int((pose[i] - bounds[i][0]) / bound_lengths[i] * dims[i]), dims[i] - 1)
    return tuple(index)


def search_nullspace(qf, target):
    q_prev = qf
    qs = []
    dq = 0.5
    Nprev = np.array([0, 0, 0, 0])

    while True:
        J = arm.jacob(q_prev)
        N = np.squeeze(null_space(J))
        index = np.argmax(np.abs(N))
        if N[index] * Nprev[index] < 0:
            N = N * -1
        q_cur = q_prev - N * dq
        while np.linalg.norm(arm.fk(q_cur, rep='cart') - target) > 1e-4:
            Jdag = np.linalg.pinv(arm.jacob(q_cur)[[0, 1, 5]])
            q_cur = q_cur + Jdag @ (target - arm.fk(q_cur, rep='cart'))
        Nprev = N
        qn_cur = rp.wrap_relative(q_cur, qf)
        qs.append(qn_cur)
        if (qn_cur - q_prev) @ (qf - q_prev) > 0 and np.linalg.norm(qf - q_cur) < 1.5 * dq:
            break
        q_prev = q_cur

    qs = [qf] + qs
    qs = np.asarray(qs)

    return qs


def init_worker(shared_array, shape):
    global arr
    arr = np.ctypeslib.as_array(shared_array)
    arr = arr.reshape(shape)


def worker_fun(q):
    x = arm.fk(q, rep='cart')
    pose = x[0:2]
    index = pose2index(pose)
    index = index + pose2index(arm.fk_com(q)[0:2])
    arr[index] = 1
    # if arr[index] == 0:
    #     qs = search_nullspace(q, x)
    #     xs = arm.fk_com(qs)
    #     dist = 0
    #     for i in range(1, xs.shape[0]):
    #         dist += np.linalg.norm(xs[i] - xs[i - 1])
    #     arr[index] = dist


def main():
    array_length = 1
    for d in dims:
        array_length *= d
    shared_array = multiprocessing.Array('i', array_length * array_length, lock=False)
    arr = np.ctypeslib.as_array(shared_array)
    arr = arr.reshape(dims + dims)

    pool = multiprocessing.Pool(initializer=init_worker, initargs=(shared_array, dims + dims))
    pool.map(worker_fun, arm.randq(p))

    dextrous_workspace = np.sum(arr, axis=(2, 3))

    score = np.sum(arr) / min(array_length * array_length, p)
    print(score)

    fig, ax = plt.subplots()
    ax.matshow(dextrous_workspace, cmap='gist_heat')
    plt.show()


if __name__ == '__main__':
    multiprocessing.freeze_support()
    main()


"""
default: 16411
rcond: 3636
yosh: 3686
aim: 11929
"""
