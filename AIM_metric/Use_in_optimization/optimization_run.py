from pymoo.algorithms.soo.nonconvex.de import DE
from pymoo.core.problem import ElementwiseProblem
from pymoo.operators.sampling.lhs import LHS
from pymoo.optimize import minimize
from pymoo.termination import get_termination
from pymoo.termination.default import DefaultSingleObjectiveTermination
from pymoo.core.problem import StarmapParallelization
import numpy as np
import RoboPy as rp
import multiprocessing


def get_arm(x):
    x = x / np.sum(x) * 2.0
    dh = [[0, 0, a, 0] for a in x]
    arm = rp.SerialArm(dh)
    return arm


def yoshikawa(J):
    d = np.linalg.det(J @ J.T)
    y = np.sqrt(max(0.0, d))
    return y


def rcond(J):
    r = 1 / np.linalg.cond(J @ J.T)
    return r


def aim(J):
    # Calculate Y
    n = J.shape[1]
    N = np.linalg.norm(J, axis=0)
    Jn = J / N
    Y = np.minimum(np.abs(Jn.T @ Jn), 1) - np.eye(n)
    y = np.arccos(np.max(Y)) / np.pi * 2
    return y


def aim_sum(J):
    # Calculate Y
    n = J.shape[1]
    N = np.linalg.norm(J, axis=0)
    Jn = J / N
    Y = np.minimum(np.abs(Jn.T @ Jn), 1) - np.eye(n)
    y = np.arccos(Y) / np.pi * 2
    y = np.sort(np.ravel(y))
    return y @ np.asarray([1 / (1 + i**2) for i in range(len(y))])


def basis(A: np.ndarray):
    m = 3
    n = 4
    p = 3
    gamma = np.zeros((n,))

    for i in range(n):
        Acols = [a for a in A.T]
        a = Acols.pop(i)
        if np.linalg.norm(a) == 0:
            g = [0] * p
        else:
            Abar = np.asarray(Acols).T
            # Adag = np.linalg.inv(Abar.T @ Abar) @ Abar.T
            Adag = np.linalg.pinv(Abar)
            ap = Abar @ Adag @ a
            if np.linalg.norm(ap) == 0:
                gamma[i] = 1
            else:
                y = a @ ap / (np.linalg.norm(a) * np.linalg.norm(ap))
                y = np.clip(y, 0, 1)
                gamma[i] = (2 / np.pi * np.arccos(y))
    return np.min(gamma)


metric_functions = {'yoshikawa': yoshikawa, 'rcond': rcond, 'aim': aim, 'basis': basis, 'aim_sum': aim_sum}


def get_metric_score(arm: rp.SerialArm, metric='yoshikawa'):
    qs = arm.randq(500)

    get_metric = metric_functions[metric]

    score = 0

    for q in qs:
        J = arm.jacob(q)[[0, 1, 5]]
        score += get_metric(J)

    return -score


class MyProblem(ElementwiseProblem):
    def __init__(self, metric='yoshikawa', **kwargs):
        super().__init__(n_var=4,
                         n_obj=1,
                         n_ieq_constr=0,
                         xl=np.array([0, 0, 0, 0], dtype=float),
                         xu=np.array([1, 1, 1, 1], dtype=float),
                         **kwargs)
        self.metric = metric

    def _evaluate(self, x: np.ndarray, out: dict, *args, **kwargs):
        arm = get_arm(x)
        f = get_metric_score(arm, self.metric)

        out['F'] = f


algorithm = DE(
    pop_size=100,
    sampling=LHS(),
    variant="DE/rand/1/bin",
    CR=0.3,
    dither="vector",
    jitter=False
)

termination = DefaultSingleObjectiveTermination(xtol=1e-4, ftol=1e-2, n_max_gen=50)


def optimize(metric, offset=0):
    pool = multiprocessing.Pool()
    problem = MyProblem(metric, elementwise_runner=StarmapParallelization(pool.starmap))
    res = minimize(problem,
                   algorithm,
                   seed=1,
                   verbose=True,
                   termination=termination)

    print("Best solution found: \nX = %s\nF = %s" % (res.X, res.F))
    arm = get_arm(res.X)
    arm.base = rp.transl([0, offset, 0])

    with open(f'opt_results_{metric}.txt', 'w') as f:
        X = res.X / np.sum(res.X) * 2.0
        f.write(f"[{X[0]}, {X[1]}, {X[2]}, {X[3]}]\n")
        f.write(str(res.F))
        f.write('\n')

    return arm


def main():
    # arms = [optimize(metric, i) for i, metric in enumerate(metric_functions.keys())]
    arms = [optimize('aim_sum', 0)]
    viz = rp.VizScene()
    for arm in arms:
        viz.add_arm(arm)
    viz.wander()
    viz.hold()


if __name__ == '__main__':
    multiprocessing.freeze_support()
    main()
