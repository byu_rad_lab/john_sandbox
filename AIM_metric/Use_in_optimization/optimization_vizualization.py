import RoboPy as rp
import numpy as np
from PyQt5 import QtWidgets, QtCore
import pyqtgraph as pg
import tqdm
from scipy.linalg import null_space


dh_yosh = [[0, 0, x, 0] for x in [1.23807624e-04, 9.17323921e-01, 1.08253541e+00, 1.68651303e-05]]
dh_rcond = [[0, 0, x, 0] for x in [1.06008034, 0.9363885,  0.00124693, 0.00228423]]
dh_aim = [[0, 0, x, 0] for x in [9.03965134e-01, 6.51821144e-01, 4.44030365e-01, 1.83357817e-04]]
dh_default = [[0, 0, x, 0] for x in [0.5, 0.5, 0.5, 0.5]]
dh_basis = [[0, 0, x, 0] for x in [0.7699714726086322, 0.6717047844196011, 0.23531477449062904, 0.32300896848113764]]

arm_yosh = rp.SerialArm(dh_yosh, base=rp.transl([0, 0, 0]))
arm_rcond = rp.SerialArm(dh_rcond, base=rp.transl([0, 0, 0]))
arm_aim = rp.SerialArm(dh_aim, base=rp.transl([0, 0, 0]))
arm_default = rp.SerialArm(dh_default, base=rp.transl([0, 0, 0]))
arm_basis = rp.SerialArm(dh_basis, base=rp.transl([0, 0, 0]))
arm = arm_rcond
dh = arm.dh

target = np.array([1.5, 0.2, 0.0])

sol = arm.ik(target=target, rep='planar', maxdel=0.1, mindel=1e-8, retry=5)
qf = sol.qf

viz = rp.VizScene()
viz.window.setBackgroundColor(255, 255, 255, 0)
viz.window.removeItem(viz.grid)
viz.add_arm(arm, link_colors=[0.5, 0.1, 0.1, 1], joint_colors=[0.1, 0.1, 0.5, 1], ee_color=[0.5, 0.1, 0.1, 1])
viz.update(qf)

q_prev = qf
qs = []
dq = 0.15
Nprev = np.array([0, 0, 0, 0])

while True:
    J = arm.jacob(q_prev)
    N = np.squeeze(null_space(J))
    index = np.argmax(np.abs(N))
    if N[index] * Nprev[index] < 0:
        N = N * -1
    q_cur = q_prev - N * dq
    while np.linalg.norm(arm.fk(q_cur, rep='planar') - target) > 1e-4:
        Jdag = np.linalg.pinv(arm.jacob(q_cur)[[0, 1, 5]])
        q_cur = q_cur + Jdag @ (target - arm.fk(q_cur, rep='planar'))
    Nprev = N
    qn_cur = rp.wrap_relative(q_cur, qf)
    qs.append(qn_cur)
    if (qn_cur - q_prev) @ (qf - q_prev) > 0 and np.linalg.norm(qf - q_cur) < 1.5 * dq:
        print("negative exit condition 2")
        break
    q_prev = q_cur

qs = [qf] + qs
qs = np.asarray(qs)

# app = QtWidgets.QApplication([])
# w = pg.PlotWidget()
# w.show()
# scatter = pg.ScatterPlotItem(pos=qs[:, 0:2])
# w.addItem(scatter)

# xs_com = []
# for q in qs:
#     pos = arm.fk_com(q)
#     viz.add_marker(pos)
#     xs_com.append(pos)
#
# xs_com = np.asarray(xs_com)
# print(np.cov(xs_com, rowvar=False))

for i, q in enumerate(qs):
    if i % 1 == 0:
        pos = arm.fk_com(q)
        pos[2] = 0.25
        viz.add_marker(pos, color=[0.0, 0.0, 0.0, 1], size=15)

for scatter in viz.markers:
    scatter.setGLOptions('translucent')

# while True:
#     for q in qs:
#         viz.update(q)
#         viz.hold(0.01)

viz.hold()
