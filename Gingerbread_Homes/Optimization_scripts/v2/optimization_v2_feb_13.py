from pymoo.core.problem import ElementwiseProblem
from pymoo.algorithms.moo.nsga2 import NSGA2
from pymoo.termination import get_termination
from pymoo.optimize import minimize
from pymoo.core.variable import Real, Integer
from pymoo.core.mixed import MixedVariableSampling, MixedVariableMating, MixedVariableDuplicateElimination
import numpy as np
from source_v2_feb_13 import cost_con
from Gingerbread_Homes.Optimization_scripts.v1 import optimization_settings_v1_nov_14 as params
import RoboPy as rp


class ArmOptimization(ElementwiseProblem):

    def __init__(self):
        variables = {}

        for i in range(params.n_links):
            variables[f"x{i * 4}"] = Real(bounds=(0.0, 1.0))
            variables[f"x{i * 4 + 1}"] = Real(bounds=(-np.pi, np.pi))
            variables[f"x{i * 4 + 2}"] = Real(bounds=(0.0, 1.0))
            variables[f"x{i * 4 + 3}"] = Real(bounds=(-np.pi, np.pi))

            variables[f"m{i}"] = Integer(bounds=(1, 3))

        super().__init__(vars=variables,
                         n_obj=3,
                         n_ieq_constr=params.target_points.shape[0])

    def _evaluate(self, vars : np.ndarray, out : dict, *args, **kwargs):

        x = np.asarray([vars[f'x{i}'] for i in range(4 * params.n_links)]
                       + [vars[f'm{i}'] for i in range(params.n_links)])

        scores, cons = cost_con(x)

        out['F'] = scores
        out['G'] = cons


problem = ArmOptimization()
termination = get_termination("time", "00:05:00")
algorithm = NSGA2(
    pop_size=20,
    n_offsprings=5,
    sampling=MixedVariableSampling(),
    mating=MixedVariableMating(eliminate_duplicates=MixedVariableDuplicateElimination()),
    eliminate_duplicates=MixedVariableDuplicateElimination()
)
sol = minimize(problem,
               algorithm,
               termination,
               seed=1,
               save_history=True,
               verbose=True)

print(sol)
print(sol.X)
F = sol.F
F[:, 0] = F[:, 0] / np.min(F[:, 0]) - 1
F[:, 1] = F[:, 1] / np.min(F[:, 1]) - 1
F[:, 2] = F[:, 2] / np.min(F[:, 2]) - 1
print(F)

with open("opt_results.txt", 'w') as f:
    for x in sol.X:
        f.write(str(x))
        f.write('\n')

    f.write('\n')
    for x in sol.F:
        f.write(str(x))
        f.write('\n')


viz = rp.VizScene()

for j, d in enumerate(sol.X):
    dh = np.reshape(np.asarray([d[f'x{i}'] for i in range(params.n_links * 4)]), (params.n_links, 4))
    arm = rp.SerialArm(dh, base=rp.transl([2 * j, 2 * j, 0]))
    viz.add_arm(arm)

viz.wander()
