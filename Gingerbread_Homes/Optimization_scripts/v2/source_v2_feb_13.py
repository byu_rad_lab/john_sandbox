import RoboPy as rp
import numpy as np
from Gingerbread_Homes.Optimization_scripts.v1 import optimization_settings_v1_nov_14 as params

n = params.n_links
p = params.p
k = params.target_points.shape[0]
qs = params.qs


def pos_2_index(pos: np.ndarray, bounds: list, dims: tuple):
    x = pos[0]
    y = pos[1]
    z = pos[2]
    index = (0, 0, 0)
    i = min(int((x - bounds[0][0]) / (bounds[0][1] - bounds[0][0]) * dims[0]), dims[0] - 1)
    j = min(int((y - bounds[1][0]) / (bounds[1][1] - bounds[1][0]) * dims[1]), dims[1] - 1)
    k = min(int((z - bounds[2][0]) / (bounds[2][1] - bounds[2][0]) * dims[2]), dims[2] - 1)
    index = (i, j, k)
    return index


def cost_con(x):

    dh = np.reshape(x[0:n * 4], (n, 4))
    x_motor = x[n * 4:None]
    motor_mass = [params.motor_dict[i]['mass'] for i in x_motor]
    arm = rp.SimpleDynArm(dh, motor_mass=motor_mass, linear_density=params.linear_density)
    torque_constraints = np.asarray([np.abs(params.motor_dict[i]['tau']) for i in x_motor])

    total_length = 0
    for d in dh:
        total_length += np.sqrt(d[0]**2 + d[2]**2)
    dollar_cost = total_length * params.linear_cost
    for i in x_motor:
        dollar_cost += params.motor_dict[i]['cost']

    manip_scores = np.zeros((p,))
    dynamic_scores = np.zeros((p,))

    discrete_bins = np.zeros((params.m, params.n, params.l))
    xs = np.zeros((p, 3))
    taus = np.zeros((p, params.n_links))

    feasible_sample_count = 0

    for i, q in enumerate(qs):
        xs[i] = arm.fk(q, rep='cart')
        J = arm.jacob(q)
        M = arm.get_M(q)
        G = arm.get_G(q, g=np.array([0, 0, -9.81]))
        taus[i] = G

        Y = np.sqrt(np.abs(np.linalg.det(J @ J.T)))
        D = np.sqrt(np.abs(np.linalg.det(J @ np.linalg.inv(M.T @ M) @ J.T)))

        manip_scores[i] = Y
        dynamic_scores[i] = D

    bounds = [(np.min(xs[:, 0]), np.max(xs[:, 0])),
              (np.min(xs[:, 1]), np.max(xs[:, 1])),
              (np.min(xs[:, 2]), np.max(xs[:, 2]))]

    for i, x in enumerate(xs):
        if np.min(torque_constraints - np.abs(taus[i])) < 0.0:
            # print(taus[i])
            pass
        else:
            feasible_sample_count += 1
            index = pos_2_index(x, bounds, (params.m, params.n, params.l))
            discrete_bins[index] += 1

    pd_at_points = np.zeros((k,))
    # print(feasible_sample_count)
    for i, t in enumerate(params.target_points):
        x, y, z = t
        x = (x - bounds[0][0]) / (bounds[0][1] - bounds[0][0])
        y = (y - bounds[1][0]) / (bounds[1][1] - bounds[1][0])
        z = (z - bounds[2][0]) / (bounds[2][1] - bounds[2][0])

        if 0 < x < 1 and 0 < y < 1 and 0 < z < 1:
            index = pos_2_index(t, bounds, (params.m, params.n, params.l))
            s = discrete_bins[index] / (feasible_sample_count + 1) * params.m * params.n * params.l
        else:
            s = 0

        pd_at_points[i] = s

    manip_scores.sort()
    dynamic_scores.sort()

    manip = np.mean(manip_scores[int(0.1 * p):int(0.9 * p)])
    dyna = np.mean(dynamic_scores[int(0.1 * p):int(0.9 * p)])

    return np.array([dollar_cost, -manip, -dyna]), 1 - pd_at_points
