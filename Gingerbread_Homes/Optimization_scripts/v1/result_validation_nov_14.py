import RoboPy as rp
import numpy as np
from Gingerbread_Homes.Optimization_scripts.v1.optimization_settings_v1_nov_14 import target_points

"""
Optimized for $ cost: this arm is smaller (note nearly collocated joints near beginning of robot) but has a hard time
solving IK for all points in the space.
"""
# dh = [[ 0.19392996,  1.294865,    0.48565982, -0.68545655],
#         [0.07476455,  1.57789563, 0.11441654,  2.07901038],
#         [0.10895666, -1.16169907,  0.09062814, -1.22513244],
#         [0.31785672,  2.74114318,  0.54940374,  0.22915354],
#         [0.42419155,  1.31418257, 0.61809311,  0.59959596],
#         [0.52525828,  1.93180456,  0.48829713, -3.]]



# # Panda robot (for comparison)
# dh = rp.Panda(2.5).dh

"""
Optimized with respect to manipulability (pairwise AIM score): This arm (compared with the $ cost arm) shows much better
freedom of motion, solving all points very easily.
"""
dh = [[ 0.12079527,  0.8267963,   0.97184539, -1.74311953],
        [0.31023712, -1.60866373, 0.31618834,  2.38210589],
        [0.17762674, -0.96269747,  0.82595724, -1.517136],
        [0.89850621,  2.70593359,  0.37206352,  1.94774582],
        [0.39235097,  0.67242595, 0.75855145,  1.40536782],
        [0.32333157, -0.63218557,  0.26281963, -1.]]

"""
Optimized with respect to dynamic manipulability (original Yoshikawa formulation)
"""

# dh = [[ 0.41274714,  1.3009933,   0.53866702,  2.99039088],
#         [ 0.31959666, -1.76261534,  0.7683015,  -2.16603904],
#         [ 0.6494384,   2.75054196,  0.6782023,  -1.50183476],
#         [ 0.46281651, -2.8207199,   0.76842228, -0.46092593],
#         [ 0.04146243,  1.85614506,  0.04792514,  0.94549541],
#         [ 0.13002658,  0.06880282,  0.04218721,  0.        ]]

print(dh)

arm = rp.SerialArm(dh)

viz = rp.VizScene()
viz.add_arm(arm)

q = np.zeros((arm.n,)) + (np.random.random((arm.n,)) * 2 - 1) * 0.1

for t in target_points:
    viz.add_marker(t, color=[1, 0, 0, 1], pxMode=False, size=0.02)

while viz.window.isVisible():

    for t in target_points:
        sol = arm.ik(t, q, method='pinv', tol=1e-2, maxdel=0.05, viz=viz, opt='SLSQP')
        q = sol.qf
        viz.hold(1)
