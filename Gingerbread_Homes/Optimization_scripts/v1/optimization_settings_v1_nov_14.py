import numpy as np


# Arm settings
n_links = 6
max_length = 3.0
min_length = 0.001

# # Motor Dictionary
# m_607937 = {'tau': 1.610, 'mass': 0.988, 'cost': 264.64}  # Original values
# m_607938 = {'tau': 1.560, 'mass': 0.988, 'cost': 264.64}
# m_607939 = {'tau': 1.490, 'mass': 0.988, 'cost': 264.64}

m_607937 = {'tau': 2.0, 'mass': 1.00, 'cost': 300.00}  # fake values to make things more interesting
m_607938 = {'tau': 1.5, 'mass': 0.95, 'cost': 200.00}
m_607939 = {'tau': 1.0, 'mass': 0.90, 'cost': 100.00}

motor_dict = {1:m_607937, 2:m_607938, 3:m_607939}

# Reachable workspace approximation dimensions: n x m x l
n = 7
m = 7
l = 7

# Workspace monte-carlo search sample number
p = 500
qs = np.random.random((p, n_links)) * 2 * np.pi - np.pi

# Linear Density of Links
linear_density = 2.05
linear_cost = 157 / 3.65

# Target locations for constraint calculation
target_points = np.array([[-1, 0, 0],  # four panels stacked on top of each other
                          [-1, 0, 0.25],
                          [-1, 0, 0.5],
                          [-1, 0, 0.75],
                          [1, 0, 1],  # four panel locations to reach
                          [1.5, 0.5, 1],
                          [1.5, -0.5, 1],
                          [2, 0, 1]])

# Nonlinear constraint bounds
lb = np.ones((target_points.shape[0],)) * 1 / (m * n * l)
ub = np.ones((target_points.shape[0],)) * np.inf

# Population settings
population_bounds = [(min_length, max_length), (-1 * np.pi, 1 * np.pi)] * n_links * 2
population_bounds = population_bounds + [(1, len(motor_dict.keys()))] * n_links

# Differential Evolution Parameters
maxiter = 10
n_vars = n_links * 5
popsize = 15
tol = 0.1
atol = 0.1
polish = False
seed = 1
workers = -1
integrality = np.zeros((n_vars,))
integrality[4 * n_links - 1:None] += 1


