import RoboPy as rp
import numpy as np
import optimization_settings_v1_nov_14 as params


def get_dollar_cost(dh, x_motors, motor_dict, linear_cost=0.1):
    cost = 0

    # Cost for dh parameters is just a linear link cost times the total length
    total_length = 0
    for d in dh:
        total_length += np.sqrt(d[0]**2 + d[2]**2)
    cost += total_length * linear_cost

    motor_cost = 0
    for i in x_motors:
        i = int(i)
        motor_cost += motor_dict[i]['cost']

    cost += motor_cost
    return cost


def get_aim_score(J: np.ndarray):
    # Calculate Y
    n = J.shape[1]
    vN = np.linalg.norm(J[0:3, :], axis=0)
    J[0:3, :] = J[0:3, :] / np.max(vN)
    N = np.linalg.norm(J, axis=0)
    Jn = J / N
    Y = np.minimum(np.abs(Jn.T @ Jn), 1) - np.eye(n)
    y = np.arccos(np.max(Y)) / np.pi * 2
    return y


def get_manipulability_score(dh, qs: np.ndarray):
    arm = rp.SerialArm(dh)
    p = qs.shape[0]
    aim_scores = np.zeros((p,))
    for i, q in enumerate(qs):
        aim_scores[i] = get_aim_score(arm.jacob(q))
    aim_scores.sort()
    inner_percentile_scores = aim_scores[int(p * 0.1):int(p * 0.9)]
    manipulability_score = np.mean(inner_percentile_scores)
    return manipulability_score


def get_dynamic_score(dh, x_motors, motor_dict: dict, qs: np.ndarray):
    motor_mass = [motor_dict[i]['mass'] for i in x_motors]
    arm = rp.SimpleDynArm(dh, motor_mass=motor_mass, linear_density=params.linear_density)
    p = qs.shape[0]
    dynamic_manipulability_score = np.zeros((p,))
    T = np.diag([1 / motor_dict[i]['tau'] for i in x_motors])
    for i, q in enumerate(qs):
        M = T @ arm.get_M(q)
        J = arm.jacob(q)
        dynamic_manipulability_score[i] = np.sqrt(np.linalg.det(J @ np.linalg.inv(M.T @ M) @ J.T))
    dynamic_manipulability_score.sort()
    inner_percentile_score = dynamic_manipulability_score[int(p * 0.1):int(p * 0.9)]
    dynamic_score = np.mean(inner_percentile_score)
    return dynamic_score


def get_cost(x):
    n = int(len(x) / 5)
    x_dh = x[0:4 * n]
    x_motor = x[4 * n:None]
    x_motor = x_motor.round().astype(int)

    dh = np.reshape(np.asarray(x_dh), (n, 4))

    # dollar_score = get_dollar_cost(dh, x_motor, params.motor_dict, params.linear_cost)
    # manipulability_score = -get_manipulability_score(dh, params.qs)
    dynamic_score = -get_dynamic_score(dh, x_motor, params.motor_dict, params.qs)

    # print("dollar cost", dollar_score)
    # return dollar_score

    # print("manipulability score", manipulability_score)
    # return manipulability_score

    print("dynamic score", dynamic_score)
    return dynamic_score


def pos_2_index(pos: np.ndarray, bounds: list, dims: tuple):
    x = pos[0]
    y = pos[1]
    z = pos[2]
    index = (0, 0, 0)
    i = min(int((x - bounds[0][0]) / (bounds[0][1] - bounds[0][0]) * dims[0]), dims[0] - 1)
    j = min(int((y - bounds[1][0]) / (bounds[1][1] - bounds[1][0]) * dims[1]), dims[1] - 1)
    k = min(int((z - bounds[2][0]) / (bounds[2][1] - bounds[2][0]) * dims[2]), dims[2] - 1)
    index = (i, j, k)
    return index


def get_reachability_pdf(arm: rp.SerialArmDyn):
    m = params.m
    n = params.n
    l = params.l
    dims = (m, n, l)

    p = params.p
    discrete_bins = np.zeros((m, n, l))

    qs = np.random.random((p, arm.n)) * 2 * np.pi - np.pi
    xs = arm.fk(qs, rep='cart')

    bounds = [(np.min(xs[:, 0]), np.max(xs[:, 0])),
              (np.min(xs[:, 1]), np.max(xs[:, 1])),
              (np.min(xs[:, 2]), np.max(xs[:, 2]))]

    for i, pos in enumerate(xs):
        index = pos_2_index(pos, bounds, dims)
        discrete_bins[index] += 1

    def pdf(pos):
        x, y, z = pos
        x = (x - bounds[0][0]) / (bounds[0][1] - bounds[0][0])
        y = (y - bounds[1][0]) / (bounds[1][1] - bounds[1][0])
        z = (z - bounds[2][0]) / (bounds[2][1] - bounds[2][0])

        if 0 < x < 1 and 0 < y < 1 and 0 < z < 1:
            index = pos_2_index(pos, bounds, dims)
            return discrete_bins[index] / p
        else:
            return 0

    return pdf, qs


def get_constraints(x):
    n = int(len(x) / 5)
    x_dh = x[0:4 * n]
    x_motor = x[4 * n:None]

    dh = np.reshape(np.asarray(x_dh), (n, 4))
    arm = rp.SerialArm(dh)
    pdf, qs = get_reachability_pdf(arm)

    m = params.target_points.shape[0]
    pd_at_target_points = np.zeros((m,))

    for i, pos in enumerate(params.target_points):
        pd_at_target_points[i] = pdf(pos)

    print("constraint_func", pd_at_target_points * params.m * params.n * params.l)
    return pd_at_target_points