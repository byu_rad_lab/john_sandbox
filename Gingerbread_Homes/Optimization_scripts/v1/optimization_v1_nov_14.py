import RoboPy as rp
from scipy import optimize
import numpy as np
from source_v1_nov_14 import get_cost, get_constraints
import optimization_settings_v1_nov_14 as params


cost_function = get_cost
constraint = optimize.NonlinearConstraint(get_constraints, params.lb, params.ub, keep_feasible=False)
bounds = params.population_bounds

viz = rp.VizScene()
for pos in params.target_points:
    viz.add_marker(pos, color=[1, 0, 0, 1], pxMode=False, size=0.1)

viz.add_arm(rp.PlanarDyn(n=params.n_links))

def callback(xk, convergence=None):
    viz.remove_arm()
    n = int(len(xk) / 5)
    x_dh = xk[0:4 * n]
    dh = np.reshape(np.asarray(x_dh), (n, 4))
    arm = rp.SerialArm(dh)
    viz.add_arm(arm)

    print(xk, convergence)

sol = optimize.differential_evolution(cost_function, bounds,
                                      maxiter=params.maxiter,
                                      popsize=params.popsize,
                                      tol=params.tol,
                                      atol=params.atol,
                                      seed=params.seed,
                                      # workers=params.workers,
                                      constraints=constraint,
                                      integrality=params.integrality,
                                      callback=callback,
                                      polish=params.polish)

print(sol)

dh = np.reshape(sol.x[0:params.n_links * 4], (params.n_links, 4))
x_motor = sol.x[params.n_links * 4:None]

print("DH Params: ", dh)
print("Motor Choice: ", x_motor)

callback(sol.x)
viz.wander()


"""
Results Feb 8 $ optimization
[ 0.19392996  1.294865    0.48565982 -0.68545655  0.07476455  1.57789563
  0.11441654  2.07901038  0.10895666 -1.16169907  0.09062814 -1.22513244
  0.31785672  2.74114318  0.54940374  0.22915354  0.42419155  1.31418257
  0.61809311  0.59959596  0.52525828  1.93180456  0.48829713 -3.
  2.          2.          2.          3.          2.          3.        ]
  
  Results Feb 9 Manipulability optimization
[ 0.12079527  0.8267963   0.97184539 -1.74311953  0.31023712 -1.60866373
  0.31618834  2.38210589  0.17762674 -0.96269747  0.82595724 -1.517136
  0.89850621  2.70593359  0.37206352  1.94774582  0.39235097  0.67242595
  0.75855145  1.40536782  0.32333157 -0.63218557  0.26281963 -1.
  2.          1.          2.          2.          3.          2.        ]
  
  Results Feb 10 Dynamic optimization
[ 0.41274714  1.3009933   0.53866702  2.99039088  0.31959666 -1.76261534
  0.7683015  -2.16603904  0.6494384   2.75054196  0.6782023  -1.50183476
  0.46281651 -2.8207199   0.76842228 -0.46092593  0.04146243  1.85614506
  0.04792514  0.94549541  0.13002658  0.06880282  0.04218721  0.
  2.          2.          2.          3.          2.          3.        ]
"""