import pyqtgraph as pg
from pyqtgraph import opengl as gl
import numpy as np
from RoboPy import VizSceneSource
import RoboPy as rp


class SIP(VizSceneSource.VizSceneRigidBody):

    def __init__(self, width, height, depth, mass=None, inertia=None):

        self.w = width
        self.h = height
        self.d = depth

        self.points = VizSceneSource.cube_vertex(width, depth, height)
        mesh = self.get_mesh(np.eye(4))
        colors = self.get_colors()

        self.glItem = gl.GLMeshItem(vertexes=mesh,
                                    vertexColors=colors,
                                    computeNormals=False,
                                    drawEdges=True)

        self.A_SIP_to_attachment_point = rp.se3(rp.rotz(np.pi), [width / 2, 0, 0])  # transform for a point on the SIP with origin at the attachment point, end effector must be aligned with this to attach
        self.A_attachment_to_SIP = rp.inv(self.A_SIP_to_attachment_point)
        self.attached = False

        if mass is None:
            rho = 0.5 * 1/1000 * 1/100**3
            mass = width * height * depth * rho

        if inertia is None:
            inertia = np.diag([height**2 + depth**2, width**2 + height**2, width**2 + depth**2]) * mass / 12

        self.m = mass
        self.I = inertia

    @staticmethod
    def points_to_mesh(points):
        return VizSceneSource.cube_vertex_to_mesh(points)

    def get_points(self):
        return self.points

    def get_colors(self, *args, **kwargs):
        colors = np.full((12, 3, 4), [0.5, 0, 0.5, 1])
        return colors

    def get_A_from_arm(self, arm, q):
        # assuming an arm is attached to the attachment point, find the transform for the SIP
        return arm.fk(q, base=True, tip=True) @ self.A_attachment_to_SIP

    def get_wrench(self, A_SIP, a=np.zeros((3,)), omega=np.zeros((3,)), alpha=np.zeros((3,)), g=np.array([0, 0, -9.81])):
        R = A_SIP[0:3, 0:3]
        I = R @ self.I @ R.T
        Wf = -(g - a) * self.m
        dIw = self.I @ alpha + np.cross(omega, np.cross(omega, self.I))
        r_sip2A = self.A_SIP_to_attachment_point[0:3, 3]
        Wm = -dIw - np.cross(r_sip2A, Wf)
        W = np.hstack([R.T @ Wf, R.T @ Wm])
        return W


def main1():
    import time

    dh = [[0, 0, 0.4, np.pi / 2]] * 7
    jt = ['r'] * 7
    q0 = [0] * 7
    q0[0] = np.pi

    # dh = [[0.1, 0, 0, 0], [0, 0, 0.75, 0], [0, 0, 0.75, 0], [0.15, np.pi / 2, 0.75, np.pi / 2], [0.15, np.pi / 2, 0, np.pi / 2], [0, 0, 0, -np.pi / 2], [0.25, 0, 0, -np.pi / 2]]
    # jt = ['p', 'r', 'r', 'r', 'r', 'r', 'r']
    # q0 = [0] * 7
    # q0[1] = np.pi

    arm = rp.SerialArm(dh, jt, base=rp.se3(p=[0, -1, 0]))

    sip = SIP(1, 1, 0.1)

    A_SIP = rp.se3(R=rp.rotx(np.pi / 2) @ rp.roty(np.pi), p=[2, 0, 0])
    A_attach = A_SIP @ sip.A_SIP_to_attachment_point
    A_target_1 = rp.se3(p=[0, 0.5, 0.5])
    A_target_2 = rp.se3(R=rp.rotz(np.pi / 2), p=[0, 0.0, 1.0])

    viz = rp.VizScene()
    viz.add_frame(A_attach)
    viz.add_frame(A_target_1)
    viz.add_frame(A_target_2)
    viz.add_arm(arm)
    sip.add(viz.window)
    sip.update(A_SIP)
    viz.app.processEvents()

    sol_1 = arm.ik(A_attach, q0=q0, rep='q', method='pinv', max_delta=0.1, max_iter=np.inf)
    print(sol_1)
    sol_2 = arm.ik(A_target_1, q0=sol_1.qf, rep='q', method='pinv', max_delta=0.1, max_iter=np.inf)
    print(sol_2)
    sol_3 = arm.ik(A_target_2, q0=sol_2.qf, rep='q', method='pinv', max_delta=0.1, max_iter=np.inf)
    print(sol_3)

    while True:
        for q in sol_1.qs:
            viz.update(qs=q)
            sip.update(A_SIP)
            viz.app.processEvents()
            time.sleep(0.01)

        for q in sol_2.qs:
            viz.update(qs=q)
            sip.update(sip.get_A_from_arm(arm, q))
            viz.app.processEvents()
            time.sleep(0.01)

        for q in sol_3.qs:
            viz.update(qs=q)
            sip.update(sip.get_A_from_arm(arm, q))
            viz.app.processEvents()
            time.sleep(0.01)

        time.sleep(1)

        for q in sol_3.qs[None:0:-1]:
            viz.update(qs=q)
            sip.update(sip.get_A_from_arm(arm, q))
            viz.app.processEvents()
            time.sleep(0.02)

        for q in sol_2.qs[None:0:-1]:
            viz.update(qs=q)
            sip.update(sip.get_A_from_arm(arm, q))
            viz.app.processEvents()
            time.sleep(0.02)

        for q in sol_1.qs[None:0:-1]:
            viz.update(qs=q)
            sip.update(A_SIP)
            viz.app.processEvents()
            time.sleep(0.02)

        time.sleep(1)

    viz.hold()


if __name__ == '__main__':

    main1()


