import numpy as np
import control as ct
from numpy.linalg import matrix_rank, eig, svd, solve, lstsq, inv, norm


def assign(A, B, des_poles, Vd, D):

    n, m = B.shape[0], B.shape[1]
    Cab = ct.ctrb(A, B)
    if matrix_rank(Cab) < n:
        print("System Uncontrollable!")
        return None

    I = np.eye(n)

    Va = np.zeros((n, n), dtype=Vd.dtype)
    U = np.zeros((m, n), dtype=Vd.dtype)

    for i in range(n):

        e = des_poles[i]
        vd = Vd[:, i]
        d = D[i]
        k = d.shape[0]

        M = np.block([[e*I - A, B],
                      [d, np.zeros((k, m))]])

        N = np.hstack([np.zeros((n,)), d @ vd])

        if m == k:
            c = solve(M, N)
        else:
            c, res, rank, sing = lstsq(M, N, rcond=None)

        va = c[0:n]
        u = c[n:None]

        Va[:, i] = va
        U[:, i] = u

    # Va = np.round(Va.real, 12) + np.round(Va.imag, 12) * 1j
    # U = np.round(U.real, 12) + np.round(U.imag, 12) * 1j
    K = U @ inv(Va)

    if norm(K.imag) < 1e-12:
        K = K.real
    else:
        print("non-conjugate pairs cannot be entered!")
        return None

    E, V = eig(A - B @ K)

    return K, E, V
