from Eigenstructure_assignment.source import assign
import numpy as np
from numpy.linalg import inv
import control as ct
import matplotlib.pyplot as plt
from scipy.integrate import odeint


m1 = 1.0
m2 = 1.0
k1 = 1.0
k2 = 1.0
b1 = 0.1
b2 = 0.1

"""
-----------------------------------------
Equation of Motion for Double Mass System
-----------------------------------------
states: [z1, z2, z1d, z2d]
Here z1 is the distance of the center of the first mass from the origin, and z2 is the distance of the center of the second mass from the origin.
z1d and z2d are the derivatives of above.

We model this with a spring and damper attaching z1 to the origin, and a spring and damper attaching z2 to z1, with the natural length of all springs at 0
We have two inputs, u = [f1, f2], with f1 the force on mass 1 and f2 the force on mass 2, positive in the positive z1, z2 direction

z1dd * m1 = f1 - k1*z1 + k2*(z2 - z1) - b1*z1d + b2*(z2d - z1d) = f1 + z1 * (-k1 - k2) + z2 * (k2) + z1d * (-b1 - b2) + z2d * (b2)
z2dd * m2 = f2 - k2*(z2 - z1) - b2*(z2d - z1d) = f2 + z1 * (k2) + z2 * (-k2) + z1d * (b2) + z2d * (-b2)
"""

A = np.array([[0, 0, 1, 0],
              [0, 0, 0, 1],
              [(-k1 -k2) / m1, k2 / m1, (-b1 -b2) / m1, b2 / m1],
              [k2 / m2, -k2 / m2, b2 / m2, -b2 / m2]])

Enat, Vnat = np.linalg.eig(A)

B = np.array([[0, 0],
              [0, 0],
              [1 / m1, 0],
              [0, 1 / m2]])

Cr = np.array([[1, 0, 0, 0],
               [0, 1, 0, 0]])

umax = np.array([100, 100])

""" 
-----------------
Controller Design 
-----------------
State space controller and eigenstructure controller
"""

# Desired system poles
des_poles = [-1+2j, -1-2j, -1.5+3j, -1.5-3j]
# des_poles = [-10+0.2j, -10-0.2j, -1+5j, -1-5j]
# des_poles = [-1+0.1j, -1-0.1j, -1.5+0.1j, -1.5-0.1j]

# Standard Pole Placement
K_pp = np.asarray(ct.place(A, B, des_poles))
kr_pp = -1 * inv(Cr @ inv(A - B @ K_pp) @ B)

Ep, Vp = np.linalg.eig(A - B @ K_pp)

# Desired system eigenvectors
# Vd = np.array([[1+1j, 0, 1+1j, 0],
#                [1-1j, 0, 1-1j, 0],
#                [0, 1+1j, 0, 1+1j],
#                [0, 1-1j, 0, 1-1j]]).T

Ad = np.array([[0, 0, 1, 0],
               [0, 0, 0, 1],
               [-1, 0, -1, 0],
               [0, -1, 0, -1.]])

Ed, Vd = np.linalg.eig(Ad)

# Vd = np.array([[1+1j, 1+1j, -2, -2],
#                [1-1j, 1-1j, -2, -2],
#                [1+1j, -1+1j, -2, -2],
#                [1-1j, -1-1j, -2, -2]]).T

# Weighting matrix for elements of eigenvectors
D = [np.array([[1, 0, 0, 0],
               [0, 1, 0, 0]]),
     np.array([[1, 0, 0, 0],
               [0, 1, 0, 0]]),
     np.array([[1, 0, 0, 0],
               [0, 1, 0, 0]]),
     np.array([[1, 0, 0, 0],
               [0, 1, 0, 0]])
     ]

# Eigenstructure assignment
K_eig, Ea, Va = assign(A, B, des_poles, Vd, D)
kr_eig = -1 * inv(Cr @ inv(A - B @ K_eig) @ B)

Q = np.diag([50, 7, 0.001, 0.001])
# Q = np.array([[1, 10, 0, 0],
#               [10, 1, 0, 0],
#               [0, 0, 0.1, 1],
#               [0, 0, 1, 0.1]])
R = np.diag([1, 1])
K_lqr = np.asarray(ct.lqr(A, B, Q, R)[0])
kr_lqr = -1 * inv(Cr @ inv(A - B @ K_lqr) @ B)
Alqr = (A - B @ K_lqr)
Elqr, Vlqr = np.linalg.eig(Alqr)

with np.printoptions(precision=4, suppress=True, floatmode='fixed'):
    print(f"Unforced Dynamics:")
    print(f"Eigenvalues: {Enat}\nEigenvectors:")
    for v in Vnat.T:
        print(v)

    print(f"Standard Pole Placement:\nK_pp:\n{K_pp}\nkr_pp:\n{kr_pp}\n")
    print(f"Eigenvalues: {Ep}\nEigenvectors:")
    for v in Vp.T:
        print(v)

    print(f"\nEigenstructure Assignemnt:\nK_eig:\n{K_eig}\nkr_eig:\n{kr_eig}\n")
    print(f"Eigenvalues: {Ea}\nEigenvectors:")
    for v in Va.T:
        print(v)

    print(f"\nLQR Assignemnt:\nK_lqr:\n{K_lqr}\nkr_lqr:\n{kr_lqr}\n")
    print(f"Eigenvalues: {Elqr}\nEigenvectors:")
    for v in Vlqr.T:
        print(v)


def sat(u):
    usat = np.zeros_like(u)
    for i in range(len(u)):
        usat[i] = min(abs(u[i]), umax[i]) * np.sign(u[i])
    return usat


def eom(x, u):
    xd = A @ x + B @ u
    return xd


def ref(t):
    return np.array([0, 0])


def f_pp(x, t):
    r = ref(t)
    u = -K_pp @ x + kr_pp @ r
    usat = sat(u)
    xd = eom(x, usat)
    return xd


def f_eig(x, t):
    r = ref(t)
    u = -K_eig @ x + kr_eig @ r
    usat = sat(u)
    xd = eom(x, usat)
    return xd


def f_lqr(x, t):
    r = ref(t)
    u = -K_lqr @ x + kr_lqr @ r
    usat = sat(u)
    xd = eom(x, usat)
    return xd


ts = np.linspace(0, 5, 1000)
x0 = np.array([2, 0, 0, 0])

y_pp = odeint(f_pp, x0, ts)
y_eig = odeint(f_eig, x0, ts)
y_lqr = odeint(f_lqr, x0, ts)

u_pp = np.zeros((2, y_pp.shape[0]))
u_eig = np.zeros((2, y_eig.shape[0]))
u_lqr = np.zeros((2, y_eig.shape[0]))

for i in range(y_pp.shape[0]):
    t = ts[i]
    r = ref(t)
    x_pp = y_pp[i, :]
    x_eig = y_eig[i, :]
    x_lqr = y_lqr[i, :]
    u_pp[:, i] = sat(-K_pp @ x_pp + kr_pp @ r)
    u_eig[:, i] = sat(-K_eig @ x_eig + kr_eig @ r)
    u_lqr[:, i] = sat(-K_lqr @ x_lqr + kr_lqr @ r)

fig, ax = plt.subplots(1, 2)
ax[0].plot(ts, y_pp[:, 0], color=np.array([0, 0, 0.7, 1]), ls='-')
ax[0].plot(ts, y_pp[:, 1], color=np.array([0, 0, 0.7, 1]), ls='--')
ax[0].plot(ts, y_eig[:, 0], color=np.array([0.7, 0, 0, 1]), ls='-')
ax[0].plot(ts, y_eig[:, 1], color=np.array([0.7, 0, 0, 1]), ls='--')
ax[0].plot(ts, y_lqr[:, 0], color=np.array([0, 0.7, 0, 1]), ls='-')
ax[0].plot(ts, y_lqr[:, 1], color=np.array([0, 0.7, 0, 1]), ls='--')
ax[0].legend(['x1 pp', 'x2 pp', 'x1 eig', 'x2 eig', 'x1 lqr', 'x2 lqr'])

ax[1].plot(ts, u_pp[0, :], color=np.array([0, 0, 0.7, 1]), ls='-')
ax[1].plot(ts, u_pp[1, :], color=np.array([0, 0, 0.7, 1]), ls='--')
ax[1].plot(ts, u_eig[0, :], color=np.array([0.7, 0, 0, 1]), ls='-')
ax[1].plot(ts, u_eig[1, :], color=np.array([0.7, 0, 0, 1]), ls='--')
ax[1].plot(ts, u_lqr[0, :], color=np.array([0, 0.7, 0, 1]), ls='-')
ax[1].plot(ts, u_lqr[1, :], color=np.array([0, 0.7, 0, 1]), ls='--')
ax[1].legend(['u1 pp', 'u2 pp', 'u1 eig', 'u2 eig', 'u1 lqr', 'u2 lqr'])

# fig, ax = plt.subplots()
# ax.plot(ts, y_pp[:, 0], color=np.array([0, 0, 0.7, 1]), ls='-')
# ax.plot(ts, y_pp[:, 1], color=np.array([0, 0, 0.7, 1]), ls='--')
# ax.plot(ts, y_eig[:, 0], color=np.array([0.7, 0, 0, 1]), ls='-')
# ax.plot(ts, y_eig[:, 1], color=np.array([0.7, 0, 0, 1]), ls='--')

plt.pause(0.1)
plt.show()

# t = 0.0
# ts = [0.0]
#
# x_0 = np.array([0, 0, 0, 0])
# x_pp = np.zeros_like(x_0) + x_0
# x_eig = np.zeros_like(x_0) + x_0
# xs_pp = [x_pp]
# xs_eig = [x_eig]


