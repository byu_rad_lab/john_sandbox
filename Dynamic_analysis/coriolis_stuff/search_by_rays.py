import numpy as np
import RoboPy as rp
import matplotlib.pyplot as plt
from scipy.optimize import minimize_scalar


dh = [[0, 0, 1, 0], [0, 0, 1, 0]]
arm = rp.SimpleDynArm(dh, linear_density=1.0, motor_mass=[1.0, 0.5])
T = np.diag([2, 4])
Tinv = np.linalg.inv(T)
q = np.radians([0, 45])
g = np.array([0, 0, 0])
G = arm.get_G(q, g)

p = 500
thetas = np.linspace(0, 2 * np.pi, p, endpoint=False)
qdhats = np.asarray([[np.cos(th), np.sin(th)] for th in thetas])


def f(r, *args):
    qd = r * args[0]
    C = arm.get_C(q, qd)
    tau = Tinv @ C @ qd + Tinv @ G
    return (1 - np.linalg.norm(tau, ord=np.inf))**2


rs = np.asarray([minimize_scalar(f, bounds=[0, 6], args=(qdhat,), tol=1e-4).x for qdhat in qdhats])
qds = np.asarray([qdhat * r for qdhat, r in zip(qdhats, rs)])
cs = np.asarray([[1, 0, 0, 1] if np.argmax(np.abs(Tinv @ arm.get_C(q, qd) @ qd + Tinv @ G)) == 1 else [0, 0, 1, 1] for qd in qds])

fig, ax = plt.subplots()
ax.plot(qds[:, 0], qds[:, 1], c='k')
ax.scatter(qds[:, 0], qds[:, 1], c=cs)
ax.axis('equal')
ax.grid(True)
plt.show()
