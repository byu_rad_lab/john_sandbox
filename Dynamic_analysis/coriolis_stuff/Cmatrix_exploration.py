import RoboPy as rp
import numpy as np
import matplotlib.pyplot as plt


arm = rp.PlanarDyn(n=2, L=1)
T = np.diag([10, 10])

q = np.array([0, np.pi / 2])
J = arm.jacob(q)[0:2]
# Jd = arm.jacobdot(q, qd)[0:2]

g = np.array([0, 0, 0])

# M, C, G = arm.get_MCG(q, qd, g)
M = arm.get_M(q)
G = arm.get_G(q, g)
Minv = np.linalg.inv(M)

from scipy import optimize

tau_ext = J @ Minv @ G
A = J @ Minv @ T
Ainv = np.linalg.pinv(A)

def get_D(qd):
    Jdot = arm.jacobdot(q, qd)[0:2]
    C = arm.get_C(q, qd)
    return Jdot - J @ Minv @ C

def cost(qd):
    return np.linalg.norm(qd)**2

def jac(qd):
    return np.array([2 * qd[0], 2 * qd[1]])

def hes(qd):
    return np.array([[2, 0], [0, 2]])

def con_func(qd):
    D = get_D(qd)
    residual = Ainv @ (D @ qd - tau_ext)
    return residual[0] ** 2 - 1

def callback(xk, *args):
    print(np.linalg.norm(xk))

con_dict = {'type':'ineq', 'fun':con_func}
con_dict2 = {'type':'eq', 'fun':con_func2}

sol = optimize.minimize(cost, x0=np.array([10, 10]), jac=jac, hess=hes, constraints=con_dict, method='SLSQP', options={'maxiter':1000}, callback=callback)
qd_0 = sol.x

# sol2 = optimize.minimize(cost2, x0=np.array([0, 0, 1, 0]), constraints=con_dict2, method='SLSQP', options={'maxiter':1000}, callback=callback)
# qd_0 = sol2.x[0:2]

D = get_D(qd_0)
print(qd_0)
# print(sol)

def con_func(qd):
    D = get_D(qd)
    residual = Ainv @ (D @ qd - tau_ext)
    return residual[1] ** 2 - 1

con_dict = {'type':'ineq', 'fun':con_func}

sol = optimize.minimize(cost, x0=np.array([10, 10]), jac=jac, hess=hes, constraints=con_dict, method='SLSQP', options={'maxiter':1000})
qd_1 = sol.x


D = get_D(qd_1)
print(qd_1)
# print(sol)

qd = np.array([0, 0])

# if np.linalg.norm(qd_0) < np.linalg.norm(qd_1):
#     qd = qd_0
# else:
#     qd = qd_1

# qd = np.array([0.33139788, -1.09250708])  # np.array([0.74361678, 2.20099233])

Jd = arm.jacobdot(q, qd)[0:2]
C = arm.get_C(q, qd)

print(f"J:\n{J}\nJdot:\n{Jd}\nM:\n{M}\nC:\n{C}\nG:\n{G}")

p = 10000
th = np.linspace(0, 2 * np.pi, p)

R = np.array([[np.cos(th), np.sin(th)],
                 [-np.sin(th), np.cos(th)]]).T

taus_2norm = R @ np.array([1, 0])
pquarter = int(p / 4)
taus_infnorm = np.vstack([np.linspace([1, 1], [1, -1], pquarter, endpoint=False),
                          np.linspace([1, -1], [-1, -1], pquarter, endpoint=False),
                          np.linspace([-1, -1], [-1, 1], pquarter, endpoint=False),
                          np.linspace([-1, 1], [1, 1], pquarter, endpoint=False)])

a_2norm = np.zeros((p, 2))
a_infnorm = np.zeros((p, 2))

for i, tau in enumerate(taus_2norm):
    a_2norm[i] = J @ Minv @ T @ tau - J @ Minv @ G - (J @ Minv @ C - Jd) @ qd

for i, tau in enumerate(taus_infnorm):
    a_infnorm[i] = J @ Minv @ T @ tau - J @ Minv @ G - (J @ Minv @ C - Jd) @ qd

G_accel = J @ Minv @ G
C_accel = (J @ Minv @ C - Jd) @ qd

a_reachable_2norm = R @ np.array([np.min(np.linalg.norm(a_2norm, axis=1)), 0])
a_reachable_infnorm = R @ np.array([np.min(np.linalg.norm(a_infnorm, axis=1)), 0])

fig, ax = plt.subplots()
ax.scatter(0, 0, c='k', marker='.', label='Origin = 0 Acceleration')
ax.plot(a_2norm[:, 0], a_2norm[:, 1], c=[0, 0, 1], label='Maximum Acceleration: 2 norm')
ax.plot(a_reachable_2norm[:, 0], a_reachable_2norm[:, 1], c=[1, 0, 0], label='Maximum Guarenteed Acceleration: 2 norm')
ax.plot(a_infnorm[:, 0], a_infnorm[:, 1], c=[0, 1, 1], label='Maximum Acceleration: inf norm')
ax.plot(a_reachable_infnorm[:, 0], a_reachable_infnorm[:, 1], c=[1, 1, 0], label='Maximum Guarenteed Acceleration: inf norm')
ax.plot([0, -G_accel[0]], [0, -G_accel[1]], c=[0.7, 0.5, 0], label='Acceleration due to G')
ax.plot([-G_accel[0], -G_accel[0] -C_accel[0]], [-G_accel[1], -G_accel[1] -C_accel[1]], c=[0, 0.7, 0.5], label='Acceleration due to C')
ax.plot(-G_accel[0] -C_accel[0], -G_accel[1] -C_accel[1], c=[0, 0, 1], marker='.', label='Center of Acceleration Ellipsoid')
plt.axis('equal')
plt.legend()





viz = rp.VizScene()
viz.add_arm(arm)
viz.update(q)

plt.show()
