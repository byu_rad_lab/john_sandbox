import RoboPy as rp
import numpy as np

from PyQt5 import QtCore, QtWidgets
import pyqtgraph as pg

import time


class MainThing(QtWidgets.QMainWindow):
    def __init__(self, p, n, g):
        QtWidgets.QMainWindow.__init__(self)
        self.p = p
        self.n = n
        self.arm = rp.PlanarDyn(n=n, L=1)
        self.g = g

        # self.qds = np.asarray([[np.cos(th), np.sin(th)] for th in np.linspace(-np.pi, np.pi, p, endpoint=False)])
        self.qds = np.random.random((p, n)) * 2 - 1
        self.qds = (self.qds.T / np.linalg.norm(self.qds, axis=1)).T
        self.accs = np.zeros((p, 2))

        self.armPos = np.zeros((n + 1, 2))

        w = QtWidgets.QWidget()
        self.setCentralWidget(w)
        mainLayout = QtWidgets.QHBoxLayout()
        plottingLayout = QtWidgets.QVBoxLayout()
        pw1 = pg.PlotWidget(title='(J M^-1 C - Jdot) * qdot')
        pw2 = pg.PlotWidget(title='Robot Arm')
        scale = 1.1 * self.n
        pw1.setXRange(-scale, scale, padding=0.1)
        pw1.setYRange(-scale, scale, padding=0.1)
        pw2.setXRange(-n, n, padding=0.1)
        pw2.setYRange(-n, n, padding=0.1)
        pw1.setAspectLocked()
        pw2.setAspectLocked()
        self.plot_Cqdot = pw1.plot(pen=None, symbol='o', symbolBrush=None)
        self.plot_MeanCqdot = pw1.plot(pen='r')
        # self.plot_MeanCqdot.setColor([1, 0, 0, 1])
        self.plot_ArmPos = pw2.plot()
        plottingLayout.addWidget(pw1)
        plottingLayout.addWidget(pw2)
        mainLayout.addLayout(plottingLayout)
        buttonLayout = QtWidgets.QVBoxLayout()
        mainLayout.setStretch(0, 2)
        mainLayout.setStretch(1, 1)
        self.sliderList = []
        self.textList = []

        for i in range(n):
            text = QtWidgets.QLabel(f"Joint {i + 1}: 0")
            self.textList.append(text)
            buttonLayout.addWidget(text)
            # text.setMaximumSize(10000, 20)

            slider = QtWidgets.QSlider(QtCore.Qt.Horizontal, self)
            slider.setMinimum(-180)
            slider.setMaximum(180)
            slider.setTickInterval(30)
            slider.tickPosition = QtWidgets.QSlider.TicksBelow
            buttonLayout.addWidget(slider)
            slider.sliderReleased.connect(self.update)
            slider.sliderMoved.connect(self.update_slider_text)
            slider.setTracking(True)
            self.sliderList.append(slider)

        testButton = QtWidgets.QPushButton("Reset")
        testButton.pressed.connect(self.reset_slider_to_zero)
        buttonLayout.addWidget(testButton)

        self.qdotLabel = QtWidgets.QLabel(f"Qdot Magnitude: {1}")
        buttonLayout.addWidget(self.qdotLabel)

        self.qdotSlider = QtWidgets.QSlider(QtCore.Qt.Horizontal, self)
        self.qdotSlider.setMinimum(1)
        self.qdotSlider.setMaximum(100)
        self.qdotSlider.setValue(10)
        self.qdotSlider.sliderMoved.connect(self.update_qdot_value)
        self.qdotSlider.sliderReleased.connect(self.update)
        buttonLayout.addWidget(self.qdotSlider)

        mainLayout.addLayout(buttonLayout)
        w.setLayout(mainLayout)
        self.update()
        self.show()

    def update(self):
        q = []
        for s in self.sliderList:
            theta = s.value() * np.pi / 180
            q.append(theta)

        self.update_arm_plot(q)
        self.update_acc_plot(q)

    def update_arm_plot(self, q):
        for i in range(1, n + 1):
            self.armPos[i] = self.arm.fk(q, i)[0:2, 3]

        self.plot_ArmPos.setData(self.armPos, symbol='s')

    def update_acc_plot(self, q):
        J = self.arm.jacob(q)[0:2]

        v = np.asarray([J @ qd for qd in self.qds])
        qds = (self.qds.T / np.linalg.norm(v, axis=1)).T

        M = self.arm.get_M(q)
        Minv = np.linalg.inv(M)
        G = self.arm.get_G(q, g)

        for i, qd in enumerate(qds):
            C = self.arm.get_C(q, qd)
            Jd = self.arm.jacobdot(q, qd)[0:2]
            self.accs[i] = (J @ Minv @ C - Jd) @ qd

        meanCqdot = np.zeros((2, 2))
        meanCqdot[0] = np.mean(self.accs, axis=0)
        self.plot_MeanCqdot.setData(meanCqdot)
        self.plot_Cqdot.setData(self.accs, symbolSize=1)

    def update_slider_text(self):
        for i in range(self.n):
            self.textList[i].setText(f"Joint {i + 1}: {self.sliderList[i].value()}")

    def reset_slider_to_zero(self):
        for i in range(self.n):
            self.sliderList[i].setValue(0)
        self.update()

    def update_qdot_value(self):
        norm = self.qdotSlider.value() / 10
        self.qds = (self.qds.T / np.linalg.norm(self.qds, axis=1) * norm).T
        self.qdotLabel.setText(f"Qdot magnitude: {norm}")


if __name__ == '__main__':

    app = QtWidgets.QApplication([])
    p = 200
    n = 2
    g = np.array([0, 0, 0])
    thing = MainThing(p, n, g)

    app.exec()
