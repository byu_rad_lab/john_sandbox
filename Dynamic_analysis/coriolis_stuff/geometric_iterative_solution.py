import numpy as np
import RoboPy as rp
from itertools import combinations
import matplotlib.pyplot as plt


def get_acceleration_radius(L, w, g, planar=False):
    m, n = L.shape
    if planar:
        iterate = range(n)
        k = n
        comb = None
    else:
        comb = [x for x in combinations(range(n), 2)]
        iterate = range(len(comb))
        k = len(comb)

    rs = np.zeros((k,))
    accs_possible = np.zeros((2 * k, m))

    if np.linalg.norm(g, ord=np.inf) < 1:
        for i in iterate:
            if planar:
                if m == 3:
                    acc_orth = np.cross(L[:, i], np.array([0, 0, 1]))
                else:
                    acc_orth = np.array([-L[1, i], L[0, i]])
            else:
                acc_orth = np.cross(L[:, comb[i][0]], L[:, comb[i][1]])

            h = np.linalg.norm(acc_orth)
            acc_orth = acc_orth / h
            c = np.sign(L.T @ acc_orth)
            z1 = acc_orth @ (L @ c + w)
            z2 = acc_orth @ (L @ -c + w)

            z = z1 if np.abs(z1) < np.abs(z2) else z2

            rs[i] = z
            accs_possible[i] = acc_orth * z
            accs_possible[i + k] = acc_orth * z1 if z == z2 else acc_orth * z2

        j = np.argmin(np.abs(rs))
        acc_radius = np.abs(rs[j])
        acc_solved = accs_possible[j]
        return acc_radius, acc_solved, accs_possible
    else:
        acc_radius = 0
        acc_solved = np.zeros((m,))
        return acc_radius, acc_solved, accs_possible


dh = [[0, 0, 0.5, 0], [0, 0, 0.75, 0]]
n = len(dh)
arm = rp.SimpleDynArm(dh, linear_density=1.0, motor_mass=[0, 0])
T = np.diag([1, 0.25])
Tinv = np.linalg.inv(T)
q = np.radians([0, -90])

g = np.array([0.5, 0, 0])

M = arm.get_M(q)
G = arm.get_G(q, g)
Minv = np.linalg.inv(M)
J = arm.jacob(q, rep='cart')
w = (J @ Minv @ G)[0:2]

p = 40
qd_iter = np.linspace(-1.5, 1.5, p)
qd_delta = qd_iter[1] - qd_iter[0]
qd_grid = np.asarray([[[qd1, qd2] for qd1 in qd_iter] for qd2 in qd_iter])
tau_grid = np.asarray([[Tinv @ arm.get_C(q, qd) @ qd + Tinv @ G for qd in qdrow] for qdrow in qd_grid])
tau_norm_2 = np.linalg.norm(tau_grid, ord=2, axis=2)
tau_norm_inf = np.linalg.norm(tau_grid, ord=np.inf, axis=2)

x = np.inf
ind = (0, 0)

for i in range(p):
    for j in range(p):
        if tau_norm_inf[i, j] > 1:
            if np.linalg.norm(qd_grid[i, j]) < x:
                x = np.linalg.norm(qd_grid[i, j])
                ind = (j, i)

print(x, ind, qd_grid[ind], tau_grid[ind], tau_norm_inf[ind])

qd_guess = qd_grid[ind]
c = 0.5

while True:
    print(np.linalg.norm(qd_guess), qd_guess)
    L = np.linalg.inv(Tinv @ arm.get_C(q, qd_guess))
    _, qd_new, _ = get_acceleration_radius(L, w, Tinv @ G, planar=True)

    # qd_new2 = np.zeros((n,))
    # x = np.inf
    # for i in range(n):
    #     y = (1 - np.abs(w[i])) * np.sign(w[i]) / np.linalg.norm(L[i])
    #     if y < x:
    #         qd_new2 = L[i] * y
    #         x = y
    #
    # qd_new = qd_new2

    qd_guess = qd_guess * c + qd_new * (1 - c)

    tau = Tinv @ (arm.get_C(q, qd_guess) @ qd_guess + G)

    if np.linalg.norm(qd_new - qd_guess) < 1e-4:
        break

ind2 = (qd_new + 1.5) / qd_delta

fig, ax = plt.subplots()

ax.matshow(np.linalg.norm(tau_grid, ord=2, axis=2), cmap='gist_heat')
ax.contour(tau_norm_2, levels=np.array([1.0]), colors='w')
ax.contour(tau_norm_inf, levels=np.array([1.0]), colors='b')
ax.scatter(ind[0], ind[1], color='w')
ax.scatter(ind2[0], ind2[1], color='k')

plt.show()

# acc_iter = np.linspace(-5, 5, p)
# acceleration_grid = np.asarray([[[ax, ay] for ax in acc_iter] for ay in acc_iter])
# tau_grid = np.asarray([[Tinv @ (M @ Jinv @ a + G) for a in arow] for arow in acceleration_grid])
# tau_norm_2 = np.linalg.norm(tau_grid, ord=2, axis=2)
# tau_norm_inf = np.linalg.norm(tau_grid, ord=np.inf, axis=2)