import RoboPy as rp
import numpy as np
import matplotlib.pyplot as plt
from scipy import optimize
import tqdm
from source import get_acceleration_radius


def get_max_guarenteed_acceleration(J, Minv, T, G, C_acc):

    # n = J.shape[1]
    # p = 10 * n
    # taus = np.asarray([np.array([np.cos(th), np.sin(th)]) / max(np.abs(np.cos(th)), np.abs(np.sin(th))) for th in np.linspace(0, 2 * np.pi, p, endpoint=False)])
    # #
    # # taus = np.random.random((p, n)) * 2 - 1
    # # taus = (taus.T / np.linalg.norm(taus, axis=1)).T
    # accs = np.zeros((p, 2))
    #
    # for i, tau in enumerate(taus):
    #     accs[i] = J @ Minv @ T @ tau - J @ Minv @ G - C_acc
    #
    # i = np.argmin(np.linalg.norm(accs, axis=1))
    # # print(taus[i])
    # max_guarenteed_acceleration = max(0, np.min(np.linalg.norm(accs, axis=1)))
    # return max_guarenteed_acceleration

    r = get_acceleration_radius(J @ Minv @ T, J @ Minv @ G, np.linalg.inv(T) @ G, planar=True)[0]


def get_max_2norm_acceleration(J, Minv, T, G, C_acc, a0):

    A = J @ Minv
    bstar = C_acc + J @ Minv @ G

    try:
        Adag = np.linalg.inv(A.T @ A) @ A.T
    except np.linalg.LinAlgError:
        return 0, a0
    C = Adag.T @ Adag

    def fun(a):
        return 0.5 * a @ a

    def fjac(a):
        return a

    def fhess(a):
        return np.eye(len(a))

    def con(a):
        return (a + bstar).T @ C @ (a + bstar) - 1

    def cjac(a):
        return a @ C + bstar @ C

    constraint = {'type': 'eq',
                  'fun': con,
                  'jac': cjac}

    sol = optimize.minimize(fun, x0=a0,
                            method='SLSQP',
                            jac=fjac,
                            # hess=fhess,
                            constraints=constraint)
    amax = sol.x
    return np.linalg.norm(amax), amax


def get_dynamic_manipulability(J, Minv, T):
    A = J @ Minv @ T @ T @ Minv.T @ J.T
    return np.sqrt(max(np.linalg.det(A), 0))


def get_max_qdot_for_qdd_0(J, T, G, get_C, q1, q2):

    Tinv = np.linalg.inv(T)

    def cost(qd):
        return 0.5 * qd @ qd

    def jac(qd):
        return qd

    def hes(qd):
        return np.eye(2)

    def con_func_1(qd):
        C = get_C(qd)
        tau = Tinv @ (C @ qd + G)
        return tau[0]**2 - 1

    con_dict_1 = {'type':'eq', 'fun':con_func_1}

    def con_func_2(qd):
        C = get_C(qd)
        tau = Tinv @ (C @ qd + G)
        return tau[1] ** 2 - 1

    con_dict_2 = {'type': 'eq', 'fun': con_func_2}

    method = 'SLSQP'

    sol1 = optimize.minimize(cost, x0=q1, jac=jac, constraints=con_dict_1, method=method,
                            options={'maxiter': 100})

    sol2 = optimize.minimize(cost, x0=q2, jac=jac, constraints=con_dict_2, method=method,
                             options={'maxiter': 100})

    return min(sol1.fun, sol2.fun), sol1.x, sol2.x


def get_max_v_for_a_0(J, Minv, T, G, get_Jd, get_C, q1, q2):

    Tinv = np.linalg.inv(T)

    def cost(qd):
        return 0.5 * (J @ qd) @ (J @ qd)

    def jac(qd):
        return (J.T @ J) @ qd

    def hes(qd):
        return J.T @ J

    def con_func_1(qd):
        C = get_C(qd)
        Jd = get_Jd(qd)
        tau = Tinv @ (G + (C - J @ Minv @ Jd) @ qd)
        return tau[0]**2 - 1

    con_dict_1 = {'type':'eq', 'fun':con_func_1}

    def con_func_2(qd):
        C = get_C(qd)
        Jd = get_Jd(qd)
        tau = Tinv @ (G + (C - J @ Minv @ Jd) @ qd)
        return tau[1] ** 2 - 1

    con_dict_2 = {'type': 'eq', 'fun': con_func_2}

    method = 'SLSQP'

    sol1 = optimize.minimize(cost, x0=q1, jac=jac, constraints=con_dict_1, method=method,
                            options={'maxiter': 100})

    sol2 = optimize.minimize(cost, x0=q2, jac=jac, constraints=con_dict_2, method=method,
                             options={'maxiter': 100})

    return min(sol1.fun, sol2.fun), sol1.x, sol2.x


arm = rp.PlanarDyn(n=2, L=1)
g = np.array([0, 1, 0])
maxr = 2.05
minr = -2.05
T = np.diag([1, 1])
k = 35
qs = np.asarray([[[a, b] for a in np.linspace(0, 2 * np.pi, k)] for b in np.linspace(0, 2 * np.pi, k)])
qs = np.reshape(qs, (k * k, 2))

# qs = np.random.random((k, arm.n)) * 2 * np.pi - np.pi

qd0 = np.array([1, 1])

m = 30
n = 30

def pos_2_index(pos):
    x = pos[0]
    y = pos[1]

    i = min(m - 1, int((x - minr) / (maxr - minr) * m))
    j = min(n - 1, int((y - minr) / (maxr - minr) * n))

    return (i, j)

maxReachableAccScore = np.zeros((m, n))
dynamicManipulabilityScore = np.zeros((m, n))
maxJointVelocityScore = np.zeros((m, n))
maxCartesianVelcityScore = np.zeros((m, n))

q1 = np.ones((2,))
q2 = np.ones((2,))

q1a = np.ones((2,))
q2a = np.ones((2,))

amax = np.zeros((2,))

for q in tqdm.tqdm(qs):
    pos1 = arm.fk(q)[0:2, 3]
    pos2 = np.copy(pos1)
    pos2[0] = -pos1[0]
    index1 = pos_2_index(pos1)
    index2 = pos_2_index(pos2)

    M = arm.get_M(q)
    Minv = np.linalg.inv(M)
    G = arm.get_G(q, g)
    J = arm.jacob(q)[0:2]
    # get_C = lambda qd: arm.get_C(q, qd)
    # get_Jd = lambda qd: arm.jacobdot(q, qd)[0:2]

    C = arm.get_C(q, qd0)
    Jd = arm.jacobdot(q, qd0)[0:2]

    C_acc = (J @ Minv @ C - Jd) @ qd0

    macc = get_max_guarenteed_acceleration(J, Minv, T, G, C_acc)
    # macc, amax = get_max_2norm_acceleration(J, Minv, T, G, C_acc, amax)
    if macc > maxReachableAccScore[index1]:
        maxReachableAccScore[index1] = macc

    # if macc > maxReachableAccScore[index2]:
    #     maxReachableAccScore[index2] = macc

    # dynManip = get_dynamic_manipulability(J, Minv, T)
    # if dynManip > dynamicManipulabilityScore[index]:
    #     dynamicManipulabilityScore[index] = dynManip

    # qdMax, q1, q2 = get_max_qdot_for_qdd_0(J, T, G, get_C, q1, q2)
    # if qdMax > maxJointVelocityScore[index]:
    #     maxJointVelocityScore[index] = qdMax
    #
    # vMax, q1a, q2a = get_max_v_for_a_0(J, Minv, T, G, get_Jd, get_C, q1a, q2a)
    # if vMax > maxCartesianVelcityScore[index]:
    #     maxCartesianVelcityScore[index] = vMax

# mval = np.max(maxJointVelocityScore)
# for i in range(m):
#     for j in range(n):
#         if maxJointVelocityScore[i, j] > 0.9 * mval:
#             maxJointVelocityScore[i, j] = 0

# maxReachableAccScore = maxReachableAccScore / np.max(maxReachableAccScore)
# dynamicManipulabilityScore = dynamicManipulabilityScore / np.max(dynamicManipulabilityScore)
# maxJointVelocityScore = maxJointVelocityScore / np.max(maxJointVelocityScore)
#
# maxReachableAccScore = np.log(maxReachableAccScore + 0.1)
# dynamicManipulabilityScore = np.log(dynamicManipulabilityScore + 0.1)

fig, axs = plt.subplots(1, 1)
axs.matshow(maxReachableAccScore, cmap='gist_heat')
# axs[0].matshow(maxReachableAccScore, cmap='gist_heat')
# axs[1].matshow(dynamicManipulabilityScore, cmap='gist_heat')
# axs[0, 1].matshow(maxJointVelocityScore, cmap='gist_heat')
# axs[1, 1].matshow(maxCartesianVelcityScore, cmap='gist_heat')

# plt.colorbar()
plt.show()
