import numpy as np
import RoboPy as rp
from PyQt5 import QtWidgets, QtCore, QtGui
import pyqtgraph as pg
from pyqtgraph import opengl as gl
from itertools import *
from source import get_acceleration_radius
import time
from scipy.linalg import null_space


class FigureGenerator(QtWidgets.QMainWindow):
    def __init__(self, dh, T, q, g):
        super().__init__()
        self.setGeometry(50, 50, 1000, 800)
        self.app = QtWidgets.QApplication.instance()
        self.n = len(dh)
        self.arm = rp.SimpleDynArm(dh, linear_density=1, motor_mass=[0]*self.n)
        self.q = q
        self.g = g
        self.J = self.arm.jacob(q, rep='cart')
        self.M = self.arm.get_M(q)
        self.G = self.arm.get_G(q, g)
        self.Minv = np.linalg.inv(self.M)
        self.b = self.J @ self.Minv @ self.G
        self.T = np.diag(T)
        self.Tinv = np.linalg.inv(self.T)
        self.L = self.J @ self.Minv @ self.T
        self.arm.base = rp.transl(-self.arm.fk(q, rep='cart'))

        self.w = gl.GLViewWidget(rotationMethod='euler')
        self.w.setBackgroundColor(255, 255, 255, 0)
        self.w.addItem(gl.GLGridItem(color=(0, 0, 0, 255)))
        self.w.setCameraPosition(elevation=90, azimuth=90)

        self.armViz = rp.ArmMeshObject(self.arm, link_colors=(0.75, 0.75, 0.75, 0.25), joint_colors=[(0.5, 0.2, 0.2, 0.5),
                                                                                                     (0.2, 0.5, 0.2, 0.5),
                                                                                                     (0.2, 0.2, 0.5, 0.5)],
                                       ee_color=(0.2, 0.2, 0.2, 0.5),
                                       q0=q)
        self.w.addItem(self.armViz.mesh_object)

        # self.w.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)

        layout = QtWidgets.QVBoxLayout()
        layout.addWidget((self.w))

        sliderLayout = QtWidgets.QVBoxLayout()
        self.sliders = []
        self.labels = []
        for i in range(self.n):
            t = QtWidgets.QLabel(f"Joint {i + 1} torque: {0.0}")
            s = QtWidgets.QSlider(QtCore.Qt.Horizontal)
            s.setMinimum(-100)
            s.setMaximum(100)
            self.labels.append(t)
            self.sliders.append(s)
            holder = QtWidgets.QHBoxLayout()
            holder.addWidget(t)
            holder.addWidget(s)
            sliderLayout.addLayout(holder)

        layout.addLayout(sliderLayout)
        holder = QtWidgets.QWidget()
        holder.setLayout(layout)

        # self.setCentralWidget(holder)
        self.setCentralWidget(self.w)
        self.scatter_initial_torques()
        self.show()

    def scatter_initial_torques(self):

        self.L = self.L * 0.25

        for i, La in enumerate(self.L.T):

            iterate = [a for a in product((-1, 1), repeat=self.n - 1)]
            neg_vertex = np.asarray([np.asarray(it[0:i] + (-1,) + it[i:None]) for it in iterate])
            pos_vertex = np.asarray([np.asarray(it[0:i] + (1,) + it[i:None]) for it in iterate])

            tau_vertexes = np.vstack([neg_vertex, pos_vertex])
            acc_vertexes = tau_vertexes @ self.L.T + self.b

            scatter = gl.GLScatterPlotItem(pos=acc_vertexes, color=[0.2, 0.2, 0.2, 1], size=10)
            scatter.setGLOptions('translucent')
            self.w.addItem(scatter)

            orthogonal_direction = np.array([-La[1], La[0], 0.0])
            signs = np.sign(orthogonal_direction @ self.L)

            for n, p in zip(neg_vertex, pos_vertex):

                if np.abs(np.sum(n @ signs)) == self.n - 1:
                    color = [0.0, 0.0, 0.0, 1.0]
                else:
                    color = [0.5, 0.5, 0.5, 0.5]
                points = np.vstack([self.L @ n + self.b, self.L @ p + self.b])
                self.w.addItem(gl.GLLinePlotItem(pos=points, color=color, antialias=True, width=5))

        # Plot Lines of L matrix
        line_colors = [(1, 0, 0, 1),
                       (0, 1, 0, 1),
                       (0, 0, 1, 1)]

        K = [1, 1, 1]

        # for i, l in enumerate(self.L.T):
        #     points = np.vstack([np.zeros((3,)),
        #                         l])
        #     self.w.addItem(gl.GLLinePlotItem(pos=points, color=line_colors[i], antialias=True, width=5))

        points = np.asarray([[0, 0, 0], self.L.T[0]])
        self.w.addItem(gl.GLLinePlotItem(pos=points, color=[1, 0, 0, 1], antialias=True, width=5))

        points = np.asarray([self.L.T[0], self.L.T[0] - self.L.T[1]])
        self.w.addItem(gl.GLLinePlotItem(pos=points, color=[0, 1, 0, 1], antialias=True, width=5))

        points = np.asarray([self.L.T[0] - self.L.T[1] + self.L.T[2], self.L.T[0] - self.L.T[1] - self.L.T[2]])
        self.w.addItem(gl.GLLinePlotItem(pos=points, color=[0, 0, 1, 1], antialias=True, width=5))

        origin = gl.GLScatterPlotItem(pos=np.array([[0, 0, 0]]), color=[0, 0, 0, 1], size=15)
        origin.setGLOptions('translucent')
        self.w.addItem(origin)

        r = 1.0
        angs = np.linspace(0, 2 * np.pi, 20)
        circle_points = np.asarray([[np.cos(th), np.sin(th), 0.0] for th in angs]) * r
        circle = gl.GLLinePlotItem(pos=circle_points, mode='line_strip', color=[1, 0, 0, 1], antialias=True, width=5)
        circle.setGLOptions('translucent')
        # self.w.addItem(circle)

        zero_torque_point = gl.GLScatterPlotItem(pos=self.b[None], color=[0, 0, 1, 1], size=10)
        zero_torque_point.setGLOptions('translucent')
        self.w.addItem(zero_torque_point)
        # self.w.addItem(gl.GLLinePlotItem(pos=np.vstack([np.zeros((3,)), self.b]), color=[0, 0, 1, 1], width=2, antialias=True))

        self.probe = gl.GLScatterPlotItem(pos=np.array([[0, 0, 0]]), color=[1.0, 0.5, 0.0, 1.0], size=15)
        self.probe.setGLOptions('translucent')
        # self.w.addItem(self.probe)

    def animate(self):

        # t = time.perf_counter()
        # while time.perf_counter() - t < 1.0:
        #     self.app.processEvents()
        self.hold = True

        def onclick(x):
            self.hold = False

        self.w.mousePressEvent = onclick

        while self.hold:
            self.app.processEvents()

        tauk = np.linspace(-1.0, 1.0, 100)

        def update_probe(tau):
            for i in range(self.n):
                self.labels[i].setText(f"Joint {i + 1} torque: {tau[i].round(2)}")
                self.sliders[i].setValue(int(tau[i] * 100))
            a = self.L @ tau + self.b
            self.probe.setData(pos=a[None])
            self.app.processEvents()
            time.sleep(0.001)

        while self.isVisible():

            tau = np.array([1.0, 1.0, -1.0])
            for t in tauk:
                tau[2] = t
                update_probe(tau)

            tau = np.array([1.0, 1.0, 1.0])
            for t in np.flip(tauk):
                tau[1] = t
                update_probe(tau)

            tau = np.array([1.0, -1.0, 1.0])
            for t in np.flip(tauk):
                tau[0] = t
                update_probe(tau)

            tau = np.array([-1.0, -1.0, 1.0])
            for t in tauk:
                tau[1] = t
                update_probe(tau)

            tau = np.array([-1.0, 1.0, 1.0])
            for t in np.flip(tauk):
                tau[2] = t
                update_probe(tau)

            tau = np.array([-1.0, 1.0, -1.0])
            for t in tauk:
                tau[0] = t
                update_probe(tau)

            tau = np.array([1.0, 1.0, -1.0])
            for t in np.flip(tauk):
                tau[1] = t
                update_probe(tau)

            tau = np.array([1.0, -1.0, -1.0])
            for t in tauk:
                tau[2] = t
                update_probe(tau)

            tau = np.array([1.0, -1.0, 1.0])
            for t in np.flip(tauk):
                tau[0] = t
                update_probe(tau)

            tau = np.array([-1.0, -1.0, 1.0])
            for t in np.flip(tauk):
                tau[2] = t
                update_probe(tau)

            tau = np.array([-1.0, -1.0, -1.0])
            for t in tauk:
                tau[1] = t
                update_probe(tau)

            tau = np.array([-1.0, 1.0, -1.0])
            for t in tauk:
                tau[0] = t
                update_probe(tau)


def main():
    app = QtWidgets.QApplication([])
    n = 3
    dh = [[0, 0, 0.5, 0], [0, 0, 0.75, 0], [0, 0, 0.25, 0]]
    q = np.radians([0, -60, 60])
    T = [1, 0.25, 0.1]
    g = np.array([0.0, 0, 0])
    gui = FigureGenerator(dh, T, q, g)
    # gui.animate()
    app.exec()

    """
    dh = [[0, 0, 0.5, 0], [0, 0, 0.75, 0], [0, 0, 0.25, 0]]
arm = rp.SimpleDynArm(dh, linear_density=1.0, motor_mass=[0, 0, 0])
T = np.diag([1, 0.25, 0.1])
    """


if __name__ == '__main__':
    main()
