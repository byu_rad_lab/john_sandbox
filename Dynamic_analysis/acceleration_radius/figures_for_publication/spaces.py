import numpy as np
import matplotlib.pyplot as plt


fig, axs = plt.subplots(1, 2)

theta = np.linspace(0, 2 * np.pi, 500)

tau_2norm = np.asarray([[np.cos(th), np.sin(th)] for th in theta])
tau_infnorm = np.asarray([tau / np.linalg.norm(tau, ord=np.inf) for tau in tau_2norm])

axs[0].plot(tau_2norm[:, 0], tau_2norm[:, 1], c='b', label='||tau||_2 = 1')
axs[0].plot(tau_infnorm[:, 0], tau_infnorm[:, 1], c='r', label='||tau||_inf = 1')

L = np.array([[1, 2],
              [3, -4]])

w = np.array([0.5, -0.25])

accs_2norm = (L @ tau_2norm.T).T + w
accs_infnorm = (L @ tau_infnorm.T).T + w

axs[1].plot(accs_2norm[:, 0], accs_2norm[:, 1], c='b')
axs[1].plot(accs_infnorm[:, 0], accs_infnorm[:, 1], c='r')

axs[0].axis('equal')
axs[0].legend()
axs[0].set_xlabel('Joint 1 normalized torque')
axs[0].set_ylabel('Joint 2 normalized torque')
axs[1].set_xlabel('x acceleration')
axs[1].set_ylabel('y acceleration')
axs[1].axis('equal')
axs[0].grid(True)
axs[1].grid(True)
plt.show()
