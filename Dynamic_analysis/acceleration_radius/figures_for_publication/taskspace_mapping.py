import numpy as np
import RoboPy as rp
import matplotlib.pyplot as plt
from itertools import combinations
from scipy.linalg import null_space


def get_acceleration_radius(L, w, g):
    m, n = L.shape
    comb = [x for x in combinations(range(n), m - 1)]
    iterate = range(len(comb))
    k = len(comb)

    rs = np.zeros((k,)) + np.inf
    accs_possible = np.zeros((2 * k, m))

    if np.linalg.norm(g, ord=np.inf) < 1:
        for i in iterate:

            if np.linalg.matrix_rank(L[:, comb[i]]) == m - 1:
                acc_orth = np.ravel(null_space(L[:, comb[i]].T))
            else:
                continue

            h = np.linalg.norm(acc_orth)
            acc_orth = acc_orth / h
            c = np.sign(L.T @ acc_orth)
            z1 = acc_orth @ (L @ c + w)
            z2 = acc_orth @ (L @ -c + w)

            z = z1 if np.abs(z1) < np.abs(z2) else z2

            rs[i] = z
            accs_possible[i] = acc_orth * z
            accs_possible[i + k] = acc_orth * z1 if z == z2 else acc_orth * z2

        j = np.argmin(np.abs(rs))
        acc_radius = np.abs(rs[j])
    else:
        acc_radius = 1 - np.linalg.norm(g, ord=np.inf)

    return acc_radius


n = 3
m = 2
dh = [[0, 0, x, 0] for x in [1.0, 0.75, 0.5]]
# dh = [[0, 0, 1, 0], [0, 0, 0.75, 0], [0, 0, 0.5, 0], [0, 0, 0.25, 0], [0, 0, 0.1, 0]]
length = np.sum([np.abs(d[2]) for d in dh])
arm = rp.SimpleDynArm(dh, linear_density=1.0, motor_mass=[1, 1, 1])
T = np.diag([10, 10, 10])
Tinv = np.linalg.inv(T)
g = np.array([1.0, 0, 0.0])

k = 100
bins = np.ones((k, k)) * 0


def xy2ij(xy):
    x, y = xy
    i = int((x / length + 1) * k / 2)
    j = int((y / length + 1) * k / 2)
    return i, j


fig, ax = plt.subplots()
ax.matshow(bins, cmap='gist_heat')
plt.pause(0.1)

p = 0
count = 0
while p < 100:
    p += 1
    count += 1
    q = np.random.random((n,)) * 2 * np.pi - np.pi
    x = arm.fk(q, rep='xy')

    M = arm.get_M(q)
    Minv = np.linalg.inv(M)
    G = arm.get_G(q, g)
    J = arm.jacob(q, rep='xy')

    L = J @ Minv @ T
    w = J @ Minv @ G
    b = Tinv @ G

    r = get_acceleration_radius(L, w, b)

    ind = xy2ij(x)
    if r > bins[ind]:
        bins[ind] = r
        p = 0

    x[1] = x[1] * -1
    ind = xy2ij(x)
    if r > bins[ind]:
        bins[ind] = r
        p = 0

    x[0] = x[0] * -1
    ind = xy2ij(x)
    if r > bins[ind]:
        bins[ind] = r
        p = 0

    x[1] = x[1] * -1
    ind = xy2ij(x)
    if r > bins[ind]:
        bins[ind] = r
        p = 0

    if count % 500 == 0:
        count = 1
        ax.clear()
        ax.matshow(bins, cmap='gist_heat')
        print(p)
        plt.pause(0.1)

ax.clear()
ax.matshow(bins, cmap='gist_heat')
plt.pause(0.1)
print("finished")
plt.show()
