import numpy as np
import RoboPy as rp
from pyqtgraph.opengl import GLLinePlotItem


dh = [[0, 0, 0.5, 0], [0, 0, 0.75, 0]]
arm = rp.SimpleDynArm(dh, linear_density=1.0, motor_mass=[0, 0])
T = np.diag([1, 0.25])
Tinv = np.linalg.inv(T)
q = np.radians([180, 90])
qd = [0, 0]

g = np.array([2, 0, 0])

M, C, G = arm.get_MCG(q, qd, g)
Minv = np.linalg.inv(M)
J = arm.jacob(q, rep='cart')
# Jinv = np.linalg.inv(J)

p = 500
# acc_iter = np.linspace(-5, 5, p)
# acceleration_grid = np.asarray([[[ax, ay] for ax in acc_iter] for ay in acc_iter])
# tau_grid = np.asarray([[Tinv @ (M @ Jinv @ a + G) for a in arow] for arow in acceleration_grid])
# tau_norm_2 = np.linalg.norm(tau_grid, ord=2, axis=2)
# tau_norm_inf = np.linalg.norm(tau_grid, ord=np.inf, axis=2)

taus = np.asarray([[np.cos(th), np.sin(th)] for th in np.linspace(-np.pi, np.pi, p)])
taus_inf = (taus.T / np.linalg.norm(taus, axis=1, ord=np.inf)).T
pe = arm.fk(q, rep='cart')
k = 0.1
accs_2 = np.asarray([J @ Minv @ (tau - G) * k + pe for tau in taus])
accs_inf = np.asarray([J @ Minv @ (tau - G) * k + pe for tau in taus_inf])

acc_axis = []
for i in range(1, 10):
    acc_axis.append([i, 0, 0.5])
    acc_axis.append([i - 1, 0, 0.5])

    acc_axis.append([0, -i, 0.5])
    acc_axis.append([0, -i + 1, 0.5])

    acc_axis.append([i, 0, 0.5])
    acc_axis.append([i, 0.5, 0.5])

    acc_axis.append([0, -i, 0.5])
    acc_axis.append([-0.5, -i, 0.5])

acc_axis = np.asarray(acc_axis) * 0.1 + pe

circle_2norm = np.hstack([taus, np.zeros((p, 1))]) * 0.047 + pe
circle_infnorm = np.hstack([taus, np.zeros((p, 1))]) * 0.088 + pe

# print(acceleration_grid[:, :, 1])
# print(tau_grid.ndim)

# fig, ax = plt.subplots()
# ax.matshow(tau_norm_2, cmap='gist_heat')
# ax.contour(tau_norm_2, levels=np.array([1.0]), colors='k')
# ax.contour(tau_norm_inf, levels=np.array([1.0]), colors='k')

viz = rp.VizScene()
viz.add_arm(arm, link_colors=np.array([0.7, 0.7, 0.7, 1.0]), joint_colors=np.array([0.5, 0.5, 0.5, 1.0]), ee_color=np.array([0.5, 0.5, 0.5, 1.0]))
viz.update(q)
viz.window.setBackgroundColor('w')
# viz.window.addItem(GLLinePlotItem(pos=[[0, 0, 0], g], width=10))
viz.window.addItem(GLLinePlotItem(pos=accs_2, width=5, color=(0.9, 0.1, 0.1, 1.0), antialias=True))
viz.window.addItem(GLLinePlotItem(pos=circle_2norm, width=5, color=(0.9, 0.1, 0.1, 1.0), antialias=True))
viz.window.addItem(GLLinePlotItem(pos=accs_inf, width=5, color=(0.1, 0.1, 0.9, 1.0), antialias=True))
viz.window.addItem(GLLinePlotItem(pos=circle_infnorm, width=5, color=(0.1, 0.1, 0.9, 1.0), antialias=True))
viz.window.addItem(GLLinePlotItem(pos=acc_axis, width=2, color=(0.0, 0.0, 0.0, 1.0), antialias=True, mode='lines'))
viz.app.processEvents()

viz.hold()
