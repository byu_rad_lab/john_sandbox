import numpy as np
import RoboPy as rp
import matplotlib.pyplot as plt


dh = [[0, 0, 0.5, 0], [0, 0, 0.75, 0], [0, 0, 0.25, 0]]
arm = rp.SimpleDynArm(dh, linear_density=1.0, motor_mass=[0, 0, 0])
T = np.diag([1, 0.25, 0.1])
Tinv = np.linalg.inv(T)
q = np.radians([0, -60, 60])
qd = [0, 0, 0]

g = np.array([0.5, 0, 0])

M, C, G = arm.get_MCG(q, qd, g)
Minv = np.linalg.inv(M)
J = arm.jacob(q, rep='xy')

L = J @ Minv @ T
w = J @ Minv @ G

print(L)

fig, (ax1, ax2, ax3) = plt.subplots(3, 1)
colors = [(0.7, 0.1, 0.1, 1.0), (0.1, 0.7, 0.1, 1.0), (0.1, 0.1, 0.7, 1.0)]

for v, c, l in zip(L.T, colors, ['L1', 'L2', 'L3']):
    ax1.plot([0, v[0]], [0, v[1]], c=c, linewidth=5, label=l)



x = w
ax2.plot([0, w[0]], [0, w[1]], c='k', label='gravity offset')
signs1 = [1, 1, 1]
signs2 = [1, -1, 1]

for i in range(3):
    l = L[:, i] * signs1[i]
    if i == 2:
        ax2.plot([x[0] + l[0], x[0] - l[0]], [x[1] + l[1], x[1] - l[1]], c=colors[i], linestyle='--', label=f'torque {i}')
    else:
        ax2.plot([x[0], x[0] + l[0]], [x[1], x[1] + l[1]], label=f'torque {i}', c=colors[i])
    x = x + l

x = w
ax3.plot([0, w[0]], [0, w[1]], c='k', label='gravity offset')
for i in range(3):
    l = L[:, i] * signs2[i]
    if i == 2:
        ax3.plot([x[0] + l[0], x[0] - l[0]], [x[1] + l[1], x[1] - l[1]], c=colors[i], linestyle='--', label=f'torque {i}')
    else:
        ax3.plot([x[0], x[0] + l[0]], [x[1], x[1] + l[1]], label=f'torque {i}', c=colors[i])
    x = x + l

ax1.grid(True)
ax2.grid(True)
ax3.grid(True)
ax1.legend()
ax2.legend()
ax3.legend()
ax1.axis('equal')
ax2.axis('equal')
ax3.axis('equal')
plt.show()
