import numpy as np
import RoboPy as rp
import matplotlib.pyplot as plt


n = 3
m = 2
dh = [[0, 0, 1, 0], [0, 0, 1, 0], [0, 0, 1, 0]]
arm = rp.SimpleDynArm(dh, linear_density=1.0, motor_mass=0)
T = np.diag([10, 3, 1])

q = np.radians([0, 45, -90])
qd = np.array([0, 0, 0])
g = np.array([0, 0, 0])

M, C, G = arm.get_MCG(q, qd, g)
Minv = np.linalg.inv(M)
Tinv = np.linalg.inv(T)
J = arm.jacob(q, rep='cart')[0:m]

L = J @ Minv @ T
w = J @ Minv @ G

Ldag = np.linalg.pinv(L)
wdag = Ldag @ w

p = 10000

acc_fst = []
acc_fsa = []
acc_f2a = []

for i in range(p):
    tau = np.random.random((n,)) * 2 - 1
    acc_fst.append(L @ (tau / np.linalg.norm(tau, ord=np.inf)) + w)

    # tau = tau / np.sqrt(3)
    acc_f2a.append(L @ tau / np.linalg.norm(tau, ord=2) + w)

acc_fst = np.asarray(acc_fst)
acc_f2a = np.asarray(acc_f2a)

ran = np.array([np.max(acc_fst[:, 0]) - np.min(acc_fst[:, 0]), np.max(acc_fst[:, 1]) - np.min(acc_fst[:, 1])])
low = np.array([np.min(acc_fst[:, 0]), np.min(acc_fst[:, 1])])

k = 0
while k < int(p):
    acc = np.random.random((m,)) * ran + low
    tau = Ldag @ acc - wdag
    if np.linalg.norm(tau, ord=np.inf) <= 1:
        acc_fsa.append(acc)
        k += 1

acc_fsa = np.asarray(acc_fsa)

fig, ax = plt.subplots()

s = 2
ax.scatter(acc_fst[:, 0], acc_fst[:, 1], c='k', s=s)
ax.scatter(acc_fsa[:, 0], acc_fsa[:, 1], c='r', s=s)
ax.scatter(acc_f2a[:, 0], acc_f2a[:, 1], c='b', s=s)
ax.grid()
ax.axis('equal')

plt.show()

