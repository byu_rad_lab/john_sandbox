import numpy as np
import RoboPy as rp
from itertools import combinations


n = 4
dh = [[0, 0, 1 / (1 + i), np.pi / 2] for i in range(n)]
planar = False
# dh = [[0, 0, 1, 0]] * n
# planar = True
arm = rp.SimpleDynArm(dh, linear_density=5.0)
q = [0, np.pi / 4, np.pi / -2, np.pi / 3, np.pi / 4][0:n]
qd = [0] * arm.n
g = np.array([-0.1, 0.15, 0])
T = np.diag([5 / (1 + i)**3 for i in range(n)])
# T = np.diag([5, 3, 2, 0.5, 0.25])

m = 3  # task space dimension
J = arm.jacob(q, rep='cart')
M, C, G = arm.get_MCG(q, qd, g=g)
# G = np.array([4.0, 2.0, 0.0])
Tinv = np.linalg.inv(T)
Minv = np.linalg.inv(M)
L = J @ Minv @ T


def tau2a(tau):
    a = J @ Minv @ (T @ tau - G)
    return a


# First, use a brute force approach to validate other methods
taus = np.random.random((500 * arm.n ** 2, arm.n)) * 2 - 1
for i, t in enumerate(taus):
    c = np.argsort(np.abs(taus[i]))
    for j in range(n - 1):
        taus[i, c[j]] /= np.abs(taus[i, c[j]])
accs = taus @ L.T + J @ Minv @ G

# Now use your clever brain to solve
w = Tinv @ G
comb = [x for x in combinations(range(n), 2)]
num = len(comb)
rs = np.zeros((num,))
accs_possible = np.zeros((num, m))

probes = np.zeros((num, 2, m))


for i in range(num):

    if planar:
        acc_orth = np.cross(L[:, i], np.array([0, 0, 1]))
    else:
        acc_orth = np.cross(L[:, comb[i][0]], L[:, comb[i][1]])

    acc_orth = acc_orth / np.linalg.norm(acc_orth)

    c = np.sign(L.T @ acc_orth)
    z1 = acc_orth @ (L @ c + J @ Minv @ G)
    z2 = acc_orth @ (L @ -c + J @ Minv @ G)
    z = z1 if np.abs(z1) < np.abs(z2) else z2

    # Find theta using dot product. Magnitude of tau for planar case is just sin(theta) * norm(a0), keep track of feasible solutions.

    rs[i] = z
    accs_possible[i] = acc_orth * z

if np.linalg.norm(Tinv @ G, ord=np.inf) < 1:  # this is only approximate and does not actually give correct answer!
    k = np.argmin(np.abs(rs))
    feasible = 1
else:
    k = np.argmax(np.abs(rs))
    feasible = -1

print(rs)
acc_solved = accs_possible[k]
print("Solved solution", acc_solved, feasible * np.linalg.norm(acc_solved, ord=np.inf))

viz = rp.VizScene()
viz.add_arm(arm)
viz.update(q)
x = arm.fk(q, rep='cart')

accs = accs + x
viz.add_marker(pos=accs, color=[0.5, 0.5, 0.5, 0.5])
viz.add_marker(x, color=[1, 0, 0, 1])
viz.add_marker(acc_solved + x, color=[1, 0, 1, 1])
accs_possible = accs_possible + x
viz.add_marker(accs_possible, color=[1, 0.5, 0, 0.8], size=20)

# # Display lines parallel to acceleration axes
# for i in range(n):
#     taus = np.zeros((50, 3))
#     taus[:, i] = (np.random.random((50,)) * 2 - 1) * T[i, i]
#     accs = (L @ taus.T).T
#     # accs = np.hstack([accs, np.zeros((500, 1))]) + x
#     accs = accs + x
#     color = [0, 0, 0, 1]
#     color[i] = 1
#     viz.add_marker(pos=accs, color=color)

# # Display the lines orthogonal to the acceleration axes
# for i in range(n):
#     color = [0, 0, 0, 1]
#     color[i] = 1
#     viz.add_marker(probes[i] + x, color=color, size=15)

viz.hold()
