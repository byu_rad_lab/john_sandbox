import numpy as np
import RoboPy as rp
from source import get_acceleration_radius
from itertools import count
import matplotlib.pyplot as plt


dh = [[0, 0, 1, np.pi / 2], [0, 0, 0.5, np.pi / 2], [0, 0, 0.25, np.pi / 2]]
# dh = [[0, 0, 1, 0], [0, 0, 0.5, 0]]
T = np.diag([3, 2, 1])
# T = np.diag([3, 2])
arm = rp.SimpleDynArm(dh, motor_mass=[0]*3, linear_density=1.0)

q = np.radians([0, 60, -90])[0:arm.n]

n = arm.n

J = arm.jacob(q)[0:n]
M = arm.get_M(q)
G = arm.get_G(q, g=np.zeros((3,)))
L = J @ np.linalg.inv(M) @ T
N = np.linalg.inv(L)
w = np.zeros((n,))
g = np.zeros((n,))
r, a, ap = get_acceleration_radius(L, w, g, planar=(n == 2))
print(r, a, N @ a)

samples = [np.inf] * 10
error = []
for i in range(10):
    error.append([])

for i in count(0):
    for j in range(len(samples)):
        tauhat = np.random.random((n,)) * 2 - 1
        tauhat = tauhat / np.max(np.abs(tauhat))
        ahat = L @ tauhat
        rhat = np.linalg.norm(ahat)
        if rhat < r:
            print(rhat, ahat, tauhat)
        if rhat < samples[j]:
            samples[j] = rhat
        error[j].append(samples[j] - r)

    es = max([rhat - r for rhat in samples])
    if i % 500 == 0:
        print(es)
    # error.append(es)
    if es < 1e-2:
        break

print(samples)
for e in error:
    plt.loglog(e)
plt.xlabel('Number of samples')
plt.ylabel('Error in estimate of r')
plt.show()
