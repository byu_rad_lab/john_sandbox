import numpy as np
import RoboPy as rp
from itertools import combinations, product
from scipy.linalg import null_space


def get_acceleration_radius(L, w, g, planar):
    if planar:
        L = L[0:2]
        w = w[0:2]

    m, n = L.shape

    comb = [x for x in combinations(range(n), m - 1)]
    iterate = range(len(comb))
    k = len(comb)

    rs = np.zeros((k,)) + np.inf
    accs_possible = np.zeros((2 * k, m))

    if np.linalg.norm(g, ord=np.inf) < 1:
        for i in iterate:

            if np.linalg.matrix_rank(L[:, comb[i]]) == m - 1:
                acc_orth = np.ravel(null_space(L[:, comb[i]].T))
            else:
                continue

            # if planar:
            #     acc_orth = np.cross(L[:, i], np.array([0, 0, 1]))[0:m]
            # else:
            #     acc_orth = np.cross(L[:, comb[i][0]], L[:, comb[i][1]])

            h = np.linalg.norm(acc_orth)
            acc_orth = acc_orth / h
            c = np.sign(L.T @ acc_orth)
            z1 = acc_orth @ (L @ c + w)
            z2 = acc_orth @ (L @ -c + w)

            z = z1 if np.abs(z1) < np.abs(z2) else z2

            rs[i] = z
            accs_possible[i] = acc_orth * z
            accs_possible[i + k] = acc_orth * z1 if z == z2 else acc_orth * z2

        j = np.argmin(np.abs(rs))
        acc_radius = np.abs(rs[j])
        acc_solved = accs_possible[j]
    else:
        acc_radius = 1 - np.linalg.norm(g, ord=np.inf)
        acc_solved = np.zeros((m,))

    if planar:
        acc_solved = np.hstack([acc_solved, [0]])
        accs_possible = np.hstack([accs_possible, np.zeros((2 * k, 1))])

    return acc_radius, acc_solved, accs_possible
