import RoboPy as rp
import numpy as np
import pyqtgraph as pg
from PyQt5 import QtWidgets, QtCore
from pyqtgraph import opengl as gl
from source import get_acceleration_radius


class TorqueVisualizer(QtWidgets.QMainWindow):
    def __init__(self, dh, T, planar=False):
        super().__init__()
        self.setGeometry(50, 50, 1000, 800)
        self.planar = planar
        self.n = len(dh)
        self.arm = rp.SimpleDynArm(dh, linear_density=1)
        self.q = [0] * self.n
        self.q = [0, np.pi, 0, 0]
        self.J = self.arm.jacob(self.q, rep='cart')
        self.M = self.arm.get_M(self.q)
        self.Minv = np.linalg.inv(self.M)
        self.T = np.diag(T)
        self.Tinv = np.linalg.inv(self.T)
        self.L = self.J @ self.Minv @ self.T

        holder = QtWidgets.QWidget()
        self.setCentralWidget(holder)
        mainLayout = QtWidgets.QHBoxLayout()
        self.w = gl.GLViewWidget()
        self.w.addItem(gl.GLGridItem())
        self.armViz = rp.ArmMeshObject(self.arm)
        # self.w.addItem(self.armViz.mesh_object)
        self.w.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        mainLayout.addWidget(self.w, stretch=3)
        # mainLayout.addWidget(QtWidgets.QLabel('testing'))

        controlLayout = QtWidgets.QVBoxLayout()
        self.sliderTorqueList = []
        self.sliderTextList = []
        self.sliderJointList = []

        for i in range(self.n):
            s = QtWidgets.QSlider(QtCore.Qt.Horizontal)
            s.setMinimum(-100)
            s.setMaximum(100)
            s.setTickInterval(1)
            s.tickPosition = QtWidgets.QSlider.TicksBelow
            s.setValue(0)
            s.sliderMoved.connect(self.update)
            s.setTracking(True)
            self.sliderTorqueList.append(s)

            w = QtWidgets.QSlider(QtCore.Qt.Horizontal)
            w.setMinimum(-3600)
            w.setMaximum(3600)
            w.setTickInterval(1)
            w.tickPosition = QtWidgets.QSlider.TicksBelow
            w.setValue(0)
            w.sliderMoved.connect(self.joint_slider_callback)
            w.setTracking(True)
            self.sliderJointList.append(w)

            t = QtWidgets.QLabel(f"Joint {i + 1}: Torque: 0\nAngle: 0")
            self.sliderTextList.append(t)

            sliderLayout = QtWidgets.QHBoxLayout()
            sliderLayout.addWidget(s)
            sliderLayout.addWidget(w)

            controlLayout.addWidget(t)
            controlLayout.addLayout(sliderLayout)

        self.resetButton = QtWidgets.QPushButton('RESET')
        self.resetButton.pressed.connect(self.reset_sliders)
        controlLayout.addWidget(self.resetButton)

        self.probe = gl.GLScatterPlotItem(pos=np.zeros((3,)), color=[0.5, 0.0, 1.0, 1], size=15)
        self.w.addItem(self.probe)

        mainLayout.addLayout(controlLayout, stretch=1)
        holder.setLayout(mainLayout)

        self.scatter = None
        self.joints_stale = False
        self.scatter_initial_torques()
        self.show()

    def joint_slider_callback(self):
        self.joints_stale = True
        self.update()

    def update(self):
        q, tau = self.read_sliders()
        self.q = q
        if self.joints_stale:
            self.update_joint(q, tau)
            self.joints_stale = False
        self.update_torque(q, tau)

    def read_sliders(self):
        q = np.asarray([w.value() / 1800 * np.pi for w in self.sliderJointList])
        tau = np.asarray([s.value() / 100.0 for s in self.sliderTorqueList])
        return q, tau

    def update_torque(self, q, tau):
        self.update_text(q, tau)
        a = self.L @ tau
        self.probe.setData(pos=a[None])

    def update_joint(self, q, tau):
        self.update_text(q, tau)
        self.Minv = np.linalg.inv(self.arm.get_M(q))
        self.J = self.arm.jacob(q, rep='cart')
        self.L = self.J @ self.Minv @ self.T
        self.update_scatter()
        self.armViz.update(q)

    def update_text(self, q, tau):
        for i in range(self.n):
            self.sliderTextList[i].setText(f"Joint {i + 1} Torque: {tau[i].round(3)}\tAngle: {(q[i] * 180 / np.pi).round(1)}")

    def scatter_initial_torques(self):
        p = 500 * self.n
        self.taus = np.random.random((p, self.n)) * 2 - 1
        for i, t in enumerate(self.taus):
            c = np.argsort(np.random.random((self.n,)))
            for j in range(self.n - 1):
                self.taus[i, c[j]] /= np.abs(self.taus[i, c[j]])

        accs = self.taus @ self.L.T
        self.scatter = gl.GLScatterPlotItem(pos=accs, color=[0.5, 0.5, 0.5, 0.5], size=5)
        self.w.addItem(self.scatter)
        r, acc_min, accs_possible = get_acceleration_radius(self.L, np.zeros((3,)), np.zeros((self.n,)), planar=self.planar)
        self.acc_min = gl.GLScatterPlotItem(pos=acc_min[None], color=[1, 0.4, 0.0, 1.0], size=10)
        self.accs_possible = gl.GLScatterPlotItem(pos=accs_possible, color=[0.5, 0.25, 0.0, 1.0], size=7)
        self.w.addItem(self.acc_min)
        self.w.addItem(self.accs_possible)

    def update_scatter(self):
        accs = self.taus @ self.L.T
        self.scatter.setData(pos=accs)
        r, acc_min, accs_possible = get_acceleration_radius(self.L, np.zeros((3,)), np.zeros((self.arm.n,)), planar=self.planar)
        self.acc_min.setData(pos=acc_min[None])
        self.accs_possible.setData(pos=accs_possible)

    def reset_sliders(self):
        for i in range(self.n):
            self.sliderTorqueList[i].setValue(0)
            self.sliderJointList[i].setValue(0)
        self.joints_stale = True
        self.update()


def main():
    app = QtWidgets.QApplication([])
    n = 4
    dh = [[0, 0, 1, np.radians(30)]] * n
    T = [7, 3, 1.5, 1, 0.5][0:n]
    gui = TorqueVisualizer(dh, T, False)
    app.exec()


if __name__ == '__main__':
    main()
