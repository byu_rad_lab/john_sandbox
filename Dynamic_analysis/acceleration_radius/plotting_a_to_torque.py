import RoboPy as rp
import matplotlib.pyplot as plt
import numpy as np


n = 3
arm = rp.PlanarDyn(n=n, L=2 / n)
g = np.array([0, 0, 0])
# T = np.diag([1, 1, 1, 1])
T = np.diag([1, 1, 1])

# q = np.array([0, -np.pi / 2, -np.pi / 6, -2 * np.pi / 3])
q = np.array([np.pi / 4, -np.pi / 2, np.pi / 2])
qd = np.zeros((n,))

# viz = rp.VizScene()
# viz.add_arm(arm)
# viz.update(q)

J = arm.jacob(q)[0:2]
Jd = arm.jacobdot(q, qd)[0:2]
M, C, G = arm.get_MCG(q, qd, g)
Minv = np.linalg.inv(M)

A = J @ Minv @ T
Adag = np.linalg.pinv(A)
# Adag = np.linalg.inv(A.T @ A) @ A.T
bstar = J @ Minv @ (C @ qd + G) #- Jd @ qd

print(np.linalg.inv(A.T @ A) @ A.T)

AWdag = np.zeros((n, n, 2))
for i in range(n):
    x = np.ones((n,)) * 0.001
    x[i] = 1
    Winv = np.diag(1 / x)
    print(Winv @ A.T @ np.linalg.pinv(A @ Winv @ A.T))
    AWdag[i] = Winv @ A.T @ np.linalg.pinv(A @ Winv @ A.T)

m = 500
lin = np.linspace(-20, 20, m)

accs = np.array([[[ax, ay] for ax in lin] for ay in lin])
accs = np.reshape(accs, (m * m, 2))
taus2 = np.zeros((m * m, n))
tausInf = np.zeros((m * m, n))

for i, a in enumerate(accs):
    taus2[i] = Adag @ (a + bstar)
    h = np.array([A @ (a + bstar) for A in AWdag])
    y = np.linalg.norm(h, axis=1, ord=np.inf)
    index = np.argmin(y)
    tausInf[i] = h[index]

tauNormsInf = np.linalg.norm(tausInf, axis=1, ord=np.inf)
tauNormsInfMatrix = np.reshape(tauNormsInf, (m, m))

tauNorms2 = np.linalg.norm(taus2, axis=1, ord=2)
tauNorms2Matrix = np.reshape(tauNorms2, (m, m))

# levels = np.linspace(0, 2, 5)
levels = [0, 1]

fig, axs = plt.subplots(1, 2)
axs = np.ravel(axs)

map = axs[0].matshow(tauNorms2Matrix, cmap='gist_heat')
con2 = axs[0].contour(tauNorms2Matrix, cmap='pink', levels=levels)
axs[0].plot([m / 2], [m / 2], c=[1, 1, 1, 1], marker='x', markersize=5)
# fig.colorbar(conInf, ax=axs[i], location='left')
fig.colorbar(map, ax=axs[0], location='right')

map = axs[1].matshow(tauNormsInfMatrix, cmap='gist_heat')
conInf = axs[1].contour(tauNormsInfMatrix, cmap='pink', levels=levels)
axs[1].plot([m / 2], [m / 2], c=[1, 1, 1, 1], marker='x', markersize=5)
# fig.colorbar(conInf, ax=axs[i], location='left')
fig.colorbar(map, ax=axs[1], location='right')

plt.show()
