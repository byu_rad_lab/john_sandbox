from RoboPy import kinematics as K
from calculate_map import get_map

dh = [[0, 0, 1, 0],
      [0, 0, 1, 0]]

arm = K.SerialArm(dh)
Tmax = [1, 1]
W = [1, 0, 0]

map = get_map(arm, Tmax=Tmax, W=W, q0=[0, 0], m=100)

