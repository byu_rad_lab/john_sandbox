from RoboPy import kinematics as K
import sympy as sp
import numpy as np
import matplotlib.pyplot as plt

def get_map(arm, Tmax, W, q0=None, m=100):

    if q0 is None:
        q0 = np.zeros((arm.n, 1))

    if isinstance(Tmax, list):
        Tmax = np.array([Tmax]).T

    if isinstance(W, list):
        W = np.array([W]).T

    x0 = arm.fk(q0)[0:3, 3]

    def get_potential(q):
        x = np.abs(arm.fk(q)[0:3, 3] - x0)
        return x.T @ W

    def get_work(q):
        return np.abs(q.T) @ Tmax

    qs = np.random.random_sample((arm.n, m)) * (2*np.pi) - np.pi
    works = np.zeros((m, 1))
    pots = np.zeros((m, 1))
    map = np.zeros((m, 1))
    biggest = 0

    for i in range(m):
        works[i] = get_work(qs[:,i])
        pots[i] = get_potential(qs[:,i])
        map[i] = works[i] - pots[i]
        if np.abs(map[i]) > biggest:
            biggest = np.abs(map[i])

    # map = works - pots

    return map, qs, biggest

def draw_map(arm, map, qs, biggest):

    # get max arm length
    dh = arm.dh
    length = 0
    for i in range(len(dh)):
        length += np.sqrt(dh[i][1]**2 + dh[i][2]**2)

    # get bounds
    bounds = [[-np.pi, np.pi], [-np.pi, np.pi]]

    # create figure
    fig, ax = plt.subplots()

    # get colors
    m = map.shape[0]
    colors = np.ones((m, 4))
    for i in range(m):
        if map[i, 0] > 0:
            colors[i, :] = np.array([0, map[i, 0] / biggest, 0, 1])
        else:
            # colors[i] = np.array([-map[i, 0] / biggest, 0, 0, 1])
            colors[i, :] = np.array([1, 0, 0, 1])

    # print(colors)

    # plot net
    ax.scatter(qs[0, :], qs[1, :], s=15000/m, c=colors)

    return fig, ax



if __name__ == '__main__':

    dh = [[0, 0, 1, 0],
          [0, 0, 1, 0]]

    arm = K.SerialArm(dh)
    Tmax = [1, 1]
    W = [1, 1, 1]

    q0 = [0, 0]
    map, qs, biggest = get_map(arm, Tmax, W, m=10000, q0=q0)

    fig, ax = draw_map(arm, map, qs, biggest)

    plt.title('')

    plt.show()
