import matplotlib.pyplot as plt
import numpy as np
from RoboPy import kinematics as kn
from source import AUM


def judge(x):
    dh = [[0, 0, x[0], 0], [0, 0, x[1], 0]]
    arm = kn.SerialArm(dh)
    p = 200
    qs = np.random.random_sample((p, 2)) * 2 * np.pi - np.pi
    scores = np.zeros((p,))

    for i in range(p):
        J = arm.jacob(qs[i])
        scores[i] = AUM(J)

    return np.mean(scores)

# p = 10
# dh = [[0, 0, 0.1, 0], [0, 0, 1, 0]]
# arm = kn.SerialArm(dh)
# qs = np.random.random_sample((p, 2)) * 2 * np.pi - np.pi
# colors = np.zeros((p, 4))
# scores = np.zeros((p,))
# for i in range(p):
#     J = arm.jacob(qs[i])
#     scores[i] = AUM(J)
#     colors[i] = np.array([0, scores[i], 0, 1])
#
# S = np.mean(scores)
# # colors[:, 1] = colors[:, 1] / np.max(colors[:, 1])
#
# fig, ax = plt.subplots()
# ax.scatter(qs[:, 0], qs[:, 1], s=30000/p, c=colors)
# print(f"Max Value: {np.max(scores)}  Mean Value: {S}  Min Value: {np.min(scores)}")
# plt.show()

p = 1000

xs = np.random.random_sample((p, 2)) * 2 + 0.01
scores = np.zeros((p,))
colors = np.zeros((p, 4))

for i in range(p):
    scores[i] = judge(xs[i])
    colors[i] = np.array([0, scores[i] ** 2, 0, 1])
    if i % 100 == 0:
        print(f"[{i}] / [{p}]")

max = np.max(scores)
colors[:, 1] = colors[:, 1] / max
fig, ax = plt.subplots()

ax.scatter(xs[:, 0], xs[:, 1], s=30000/p, c=colors)

print(f"max {max}, min {np.min(scores)}")
plt.show()
