from Jacobian_Orthogonality.source import AUM, AIM
from scipy.optimize import differential_evolution
import RoboPy as rp
import numpy as np
import scipy as sp
from scipy.stats import linregress
import matplotlib.pyplot as plt
pi = np.pi

dh0 = [[0, 0, 1, 0], [0, 0, 1, 0], [0, 0, 1, 0]]

length = 3

def x2dh(x):
    dh = [[0, 0, 1, 0], [0, 0, 1, 0], [0, 0, 1, 0]]
    for i in range(len(x)):
        dh[i][2] = x[i]
    return dh

p_arms = 200
p_ik = 20
p_ang = 6

xs = np.random.random_sample((p_arms, 3))
xs = (xs.T * (length / np.linalg.norm(xs, axis=1))).T

p_qs = 100
qs_sample = np.random.random_sample((p_qs, 3)) * (2*pi) - pi

AUM_scores = np.zeros((p_arms,))
Yosh_scores = np.zeros_like(AUM_scores)
Cond_scores = np.zeros_like(AUM_scores)
arms = []

# xs[0] = np.array([1.5, 0.75, 0.25])

for i in range(p_arms):
    arm = rp.SerialArm(x2dh(xs[i,:]))
    arms.append(arm)

    for j in range(p_qs):
        J = arm.jacob(qs_sample[j])
        J = J[np.ix_((0, 1, 5), (0, 1, 2))]
        AUM_scores[i] += AIM(J, 'w')
        Yosh_scores[i] += np.sqrt(max(np.linalg.det(J @ J.T), 0))
        Cond_scores[i] += 1 / np.linalg.cond(J @ J.T)
    AUM_scores[i] /= p_qs
    Yosh_scores[i] /= p_qs
    Cond_scores[i] /= p_qs

max_iter = 30

q0 = [0, 0, 0]
ik_outputs = []
ik_iter = np.zeros((p_arms, p_ik, p_ang))
ik_status = np.zeros((p_arms, p_ik, p_ang))
iter_score = np.zeros((p_arms,))
status_score = np.zeros((p_arms,))


# vizes = [rp.PlanarMPL(arms[0]), rp.PlanarMPL(arms[1])]

for i in range(p_arms):
    ik_outputs.append([])
    for j in range(p_ik):
        p_target = np.random.random_sample((3,)) * 2 - 1
        p_target[2] = 0
        p_target = p_target / np.linalg.norm(p_target) * length

        for k in range(p_ang):
            A_target = rp.se3(rp.rotz(2 * pi / p_ik * j), p_target)
            sol = arms[i].ik(A_target, q0=q0, method='pinv', rep='planar', max_iter=max_iter, tol=1e-3, min_delta=1e-5, max_delta=10, viz=None)
            # print(sol)
            ik_outputs[i].append(sol)
            if sol.status == True:
                ik_iter[i,j,k] = sol.nit
                ik_status[i,j,k] = 1
            else:
                ik_iter[i,j,k] = max_iter
                ik_status[i,j,k] = 0
    iter_score[i] = (max_iter * p_ik * p_ang - np.sum(ik_iter[i,:,:])) / (max_iter * p_ik * p_ang)
    status_score[i] = np.sum(ik_status[i,:,:]) / (p_ik * p_ang)
    print(f"[{i+1} / {p_arms}]")


# print(f"RESULTS:\nArm 1: {arms[0].dh}\nAUM Score: {scores[0]} Iter Score: {iter_score[0]} Status Score: {status_score[0]}\n")
# print(f"Arm 2: {arms[1].dh}\nAUM Score: {scores[1]} Iter Score: {iter_score[1]} Status Score: {status_score[1]}")

fig, axs = plt.subplots(1, 3)

s = 20

axs[0].scatter(AUM_scores, iter_score, color=[0, 0, 0.5, 1], s=s)
axs[0].scatter(AUM_scores, status_score, marker='x', color=[0, 0, 0.5, 1], s=s)
axs[0].set_xlabel('AUM Score')
axs[1].scatter(Yosh_scores, iter_score, color=[0, 0.5, 0, 1], s=s)
axs[1].scatter(Yosh_scores, status_score, marker='x', color=[0, 0.5, 0, 1], s=s)
axs[1].set_xlabel('Yoshikawa Score')
axs[2].scatter(Cond_scores, iter_score, color=[0.5, 0, 0, 1], s=s)
axs[2].scatter(Cond_scores, status_score, marker='x', color=[0.5, 0, 0., 1], s=s)
axs[2].set_xlabel('Condition Score')
# axs[0].set_xlim([0, 1])
# axs[0].set_ylim([0, 1])
# axs[1].set_xlim([0, 1])
# axs[1].set_ylim([0, 1])
# axs[2].set_xlim([0, 1])
# axs[2].set_ylim([0, 1])

# print(Yosh_scores - Cond_scores)

""" Find lstsqr fit, and R value for each number """
m, b, r_aum, p_aum, std_aum = linregress(AUM_scores, iter_score)
m, b, r_aum2, p_aum2, std_aum2 = linregress(AUM_scores, status_score)
m, b, r_yosh, p_yosh, std_yosh = linregress(Yosh_scores, iter_score)
m, b, r_yosh2, p_yosh2, std_yosh2 = linregress(Yosh_scores, status_score)
m, b, r_cond, p_cond, std_cond = linregress(Cond_scores, iter_score)
m, b, r_cond2, p_cond2, std_cond2 = linregress(Cond_scores, status_score)

print(f"AUM:\nr^2: {r_aum} p: {p_aum} std-dev: {std_aum}")
print(f"AUM status:\nr^2: {r_aum2} p: {p_aum2} std-dev: {std_aum2}")
print(f"Yosh:\nr^2: {r_yosh} p: {p_yosh} std-dev: {std_yosh}")
print(f"Yosh status:\nr^2: {r_yosh2} p: {p_yosh2} std-dev: {std_yosh2}")
print(f"Cond:\nr^2: {r_cond} p: {p_cond} std-dev: {std_cond}")
print(f"Cond status:\nr^2: {r_cond2} p: {p_cond2} std-dev: {std_cond2}")

plt.show()

# viz1 = rp.PlanarMPL(arms[0])
# viz2 = rp.PlanarMPL(arms[1])
#
# viz1.play()
# viz2.play()
#
# viz1.show()

