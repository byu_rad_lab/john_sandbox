import matplotlib.pyplot as plt

fig, ax = plt.subplots()
x0 = [0, 0]
ax.quiver(0, 0, 1, 0, scale=1.5)
ax.quiver(0, 0, 0, 1, scale=1.5)
ax.quiver(0.01, 0.01, 0, 1, scale=2)
ax.set_xlim([0, 1])
ax.set_ylim([0, 1])
plt.show()