from Jacobian_Orthogonality.source import YMetric
from robot_kin_dyn_contrl_ta.src import Kinematics as K
import sympy as sp
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.colors import Normalize

n = 100
q1_range = np.linspace(-np.pi, np.pi, n)
q2_range = np.linspace(-np.pi, np.pi, n)

Q1s, Q2s = np.meshgrid(q1_range, q2_range, indexing='ij')
Z = np.zeros_like(Q1s)
X = np.zeros_like(Q1s)
Y = np.zeros_like(Q1s)
manip = np.zeros_like(Y)

dh = [[0, 0, 20, 0], [0, 0, 10, 0]]

arm = K.SerialArm(dh)

color = np.zeros((n, n, 4))

for i in range(n):
    for j in range(n):
        J = arm.jacob_fast(Q1s[i, j], Q2s[i, j])
        T = arm.fk_fast(Q1s[i, j], Q2s[i, j])
        # J = arm.jacob([Q1s[i, j], Q2s[i, j]])
        # Z[i, j], hodler = YMetric(J[0:2, 0:2], True)
        # manip[i, j] = sp.sqrt((sp.Matrix(J[0:2,0:2]@J[0:2,0:2].T)).det()).evalf()
        # Z[i, j] = n*i + j
        X[i, j] = T[0, 3]
        Y[i, j] = T[1, 3]

        color[i, j, :] = np.array([i / n, j / n, 1 - (i + j) / (2*n), 0.3])

        # print(f"index: {i}, {j}\tq = [{Q1s[i, j]}, {Q2s[i, j]}]\nY: ")
        # sp.pprint(hodler)
        # print(J[0:2, 0:2])
        # print(manip[i, j])
        # print()

fig, axs = plt.subplots(1, 2)

q1 = Q1s.reshape((n**2,))
q2 = Q2s.reshape((n**2,))
z = Z.reshape((n ** 2,))

x = X.reshape((n**2,))
y = Y.reshape((n**2,))
color = color.reshape((n**2, 4))

size = 100
axs[0].scatter(q1, q2, c=color, s=size)
axs[1].scatter(x, y, c=color, s=size)
# axs[0].scatter(Q1s, Q2s, c=Z, alpha=0.3)
# axs[1].scatter(X, Y, c=Z, alpha=0.3)
normy = Normalize(0, n**2 + n)
fig.colorbar(cm.ScalarMappable(norm=Normalize(0, 1)), ax=axs[1])
axs[0].axis('equal')
axs[1].axis('equal')

# print(f"Average over workspace: {np.sum(z) / n**2}")

plt.show()
