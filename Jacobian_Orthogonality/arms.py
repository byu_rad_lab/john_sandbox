import RoboPy as rp
import numpy as np
from numpy import pi
from Jacobian_Orthogonality.source import YMetric as OM
import mpmath as mp

r90 = pi / 2
d1 = 0.1
d2 = 0.5
d3 = 0.5
d6 = 0.01
a4 = 0.01
a5 = 0.01

ortho_dh = [[d1, r90, 0, r90],
      [d2, r90, 0, r90],
      [d3, 0, 0, r90],
      [0, r90, a4, -r90],
      [0, r90, a5, -r90],
      [d6, 0, 0, 0]]

ortho_joint_type = ['p', 'p', 'p', 'r', 'r', 'r']

ortho_arm = rp.SerialArm(ortho_dh, ortho_joint_type)
ortho_q0 = [0]*ortho_arm.n

a = 1/6
r_link = (45 * pi / 180)

twist_dh = [[0, 0, a, r_link]] * 6
twist_joint_type = ['r'] * 6

twist_arm = rp.SerialArm(twist_dh, twist_joint_type, base=rp.transl([5, 0, 0]))
twist_q0 = [0]*twist_arm.n
q = np.array([pi / 4, 0, 0, 0, 0, 0])

J = twist_arm.jacob(twist_q0)

print("Twisting Arm")
rp.mprint(J)

J = ortho_arm.jacob(q)
print("\nOrtho Arm")
rp.mprint(J)

viz = rp.VizScene()
viz.add_arm(ortho_arm)
viz.add_arm(twist_arm)
dq = np.ones_like(q) * 0.001
while True:
      q += dq
      viz.update([q, q])