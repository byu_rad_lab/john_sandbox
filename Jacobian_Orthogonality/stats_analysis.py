from scipy.io import loadmat
from scipy.stats import pearsonr, linregress
import matplotlib.pyplot as plt
import numpy as np

d = loadmat('Test_data_jointspace_arms_50_n_8_time_0.0776378282.mat', squeeze_me=True)

s_a_min = d['score_aim_min']
s_a_fro = d['score_aim_fro']
s_a_sum = d['score_aim_sum']
s_y = d['score_yosh']
s_c = d['score_cond']
ik_nit = d['ik_nit']
ik_success = d['ik_success']
dhs = d['dhs']

SAMPLE_TYPE = 'jointspace'
if SAMPLE_TYPE == 'jointspace':
    p_arms, p_ik = ik_success.shape
    s_ik = np.sum(ik_success, axis=(1)) / (p_ik)
elif SAMPLE_TYPE == 'reach':
    p_arms, p_ik, p_ang = ik_success.shape
    s_ik = np.sum(ik_success, axis=(1, 2)) / (p_ik * p_ang)

np.nan_to_num(s_y, copy=False)

print(np.mean(s_ik))
# print(s_a_min)

m_a_m, b_a_m, r_a_m, p_a_m, std_a_m = linregress(s_a_min, s_ik)
m_a_f, b_a_f, r_a_f, p_a_f, std_a_f = linregress(s_a_fro, s_ik)
m_a_s, b_a_s, r_a_s, p_a_s, std_a_s = linregress(s_a_sum, s_ik)
m_a, b_a, r_a, p_a, std_a = linregress(s_a_min, s_ik)
m_y, b_y, r_y, p_y, std_y = linregress(s_y, s_ik)
m_c, b_c, r_c, p_c, std_c = linregress(s_c, s_ik)

print(f"AIM-Min:\nr^2: {r_a_m} p: {p_a_m} std-dev: {std_a}")
print(f"AIM-Fro:\nr^2: {r_a_f} p: {p_a_f} std-dev: {std_a}")
print(f"AIM-Sum:\nr^2: {r_a_s} p: {p_a_s} std-dev: {std_a}")
print(f"Yosh:\nr^2: {r_y} p: {p_y} std-dev: {std_y}")
print(f"Cond:\nr^2: {r_c} p: {p_c} std-dev: {std_c}")

rp_a_m, pp_a_m = pearsonr(s_a_min, s_ik)
rp_a_f, pp_a_f = pearsonr(s_a_fro, s_ik)
rp_a_s, pp_a_s = pearsonr(s_a_sum, s_ik)
rp_y, pp_y = pearsonr(s_y, s_ik)
rp_c, pp_c = pearsonr(s_c, s_ik)

print(f"AIM-min:\nr^2: {rp_a_m} p: {pp_a_m}")
print(f"AIM-fro:\nr^2: {rp_a_f} p: {pp_a_f}")
print(f"AIM-sum:\nr^2: {rp_a_s} p: {pp_a_s}")
print(f"Yosh:\nr^2: {rp_y} p: {pp_y}")
print(f"Cond:\nr^2: {rp_c} p: {pp_c}")

fig, axs = plt.subplots(1, 2)
ax1 = axs[0]
ax2 = axs[1]

P, residuals, rank, singular_values, rcond = np.polyfit(s_a_sum, s_ik, deg=2, full=True)
print(P)
print(residuals / p_arms)
print(rank)
print(singular_values)
print(rcond)

P2, residuals, rank, singular_values, rcond = np.polyfit(s_y, s_ik, deg=2, full=True)
print(P2)
print(residuals / p_arms)
print(rank)
print(singular_values)
print(rcond)
# print(V)

# print(s_a_min, '\n\n', ik_nit)
ax1.scatter(s_a_min, s_ik, color=[0.2, 0, 0, 1], marker='o')
ax1.scatter(s_a_fro, s_ik, color=[0.5, 0, 0, 1], marker='^')
ax1.scatter(s_a_sum, s_ik, color=[0.7, 0, 0, 1], marker='+')
ax2.scatter(s_y, s_ik, color=[0, 0.5, 0, 1], marker='o')
ax2.scatter(s_c, s_ik, color=[0, 0, 0.5, 1], marker='o')
ax1.plot([0, 1], [b_a_m, b_a_m + m_a_m], linestyle='-', color=[0.2, 0, 0, 1])
ax1.plot([0, 1], [b_a_f, b_a_f + m_a_f], linestyle='-.', color=[0.5, 0, 0, 1])
ax1.plot([0, 1], [b_a_s, b_a_s + m_a_s], linestyle=':', color=[0.7, 0, 0, 1])
ax2.plot([0, 1], [b_y, b_y + m_y], linestyle='--', color=[0, 0.7, 0, 1])
ax2.plot([0, 1], [b_c, b_c + m_c], linestyle='--', color=[0, 0, 0.7, 1])

ax1.set_xlim([0, 1])
ax1.set_ylim([0, 1])
ax2.set_xlim([0, 1])
ax2.set_ylim([0, 1])
ax1.set_xlabel("Metric Score")
ax1.set_ylabel("% Successful IK Test")
ax2.set_xlabel("Metric Score")
ax2.set_ylabel("% Successful IK Test")
xs = np.linspace(0, 1, 10)
ax1.plot(xs, np.polyval(P, xs), color=[0, 0, 1, 1])
ax2.plot(xs, np.polyval(P2, xs), color=[0, 0, 1, 1])

ax1.legend(['AIM-Min', 'AIM-for', 'AIM-sum', 'AIM-Min-fit', 'AIM-fro-fit', 'AIM-sum-fit', 'poly-fit'])
ax2.legend(['Yoshikawa', 'Condition', 'Yoshikawa fit', 'Condition fit', 'poly-fit'])

print(s_ik.shape)

plt.rcParams.update({'font.size': 16})
fig2, ax = plt.subplots()
ax.scatter(s_a_sum, s_ik, color=[0.6, 0, 0, 1], marker='o')
ax.scatter(s_y, s_ik, color=[0, 0.6, 0, 1], marker='^')
ax.scatter(s_c, s_ik, color=[0, 0, 0.6, 1], marker='+')
ax.plot([0, 1], [b_a_s, b_a_s + m_a_s], linestyle='-', color=[0.6, 0, 0, 1])
ax.plot([0, 1], [b_y, b_y + m_y], linestyle='-.', color=[0, 0.6, 0, 1])
ax.plot([0, 1], [b_c, b_c + m_c], linestyle=':', color=[0, 0, 0.6, 1])
ax.set_xlabel("Metric Score")
ax.set_ylabel("IK Iterations")
ax.set_xlim([0, 0.6])
ax.set_ylim([0.5, 1])
ax.legend(['AIM', 'Yoshikawa', '1/k'])

plt.show()