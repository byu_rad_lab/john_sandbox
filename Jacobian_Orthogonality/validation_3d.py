from Jacobian_Orthogonality.source import AUM, yoshikawa, rcond, AIM
from scipy.optimize import differential_evolution
import RoboPy as rp
import numpy as np
from numpy import pi
from numpy.random import random_sample as rand
from numpy.linalg import norm
import scipy.io
from scipy.stats import linregress
import matplotlib.pyplot as plt
from TimingAnalysis import TimingAnalysis as TA
from time import perf_counter

timer = TA()
timer.time_in('MAIN')
reach = 5

n = 7

q0 = np.zeros((n,))
def generate_random_arm():
    dh = rand((n, 4)) * 2 - 1
    length = np.sum(norm(dh[:, [0, 2]], axis=1))
    dh[:, [0, 2]] *= reach / length
    dh[:, [1, 3]] *= pi
    return rp.SerialArm(dh)

p_arms = 3
p_qs = 100
p_ik = 40
p_ang = 5

arms = []
dhs = []
score_aim_min = np.zeros((p_arms,))
score_aim_fro = np.zeros((p_arms,))
score_aim_sum = np.zeros((p_arms,))
score_yosh = np.zeros((p_arms,))
score_cond = np.zeros((p_arms,))


for i in range(p_arms):
    timer.time_in("gen arm")
    arms.append(generate_random_arm())
    timer.time_out("gen arm")
    dhs.append(arms[-1].dh)
    timer.time_in("eval arm")
    for j in range(p_qs):
        q = rand((n,)) * 2 * pi - pi
        J = arms[-1].jacob(q)
        timer.time_in("AIM-min")
        score_aim_min[i] += AIM(J) / p_qs
        timer.time_out("AIM-min")
        timer.time_in("AIM-fro")
        score_aim_fro[i] += AIM(J, 'fro') / p_qs
        timer.time_out("AIM-fro")
        timer.time_in("AIM-sum")
        score_aim_sum[i] += AIM(J, 'w') / p_qs
        timer.time_out("AIM-sum")
        timer.time_in("yosh")
        score_yosh[i] += yoshikawa(J) / p_qs
        timer.time_out("yosh")
        timer.time_in("cond")
        score_cond[i] += rcond(J) / p_qs
        timer.time_out("cond")
    timer.time_out("eval arm")

arms[0] = rp.HumanArm()
print(arms[0])

# SAMPLE_METHOD = 'Reachable'
SAMPLE_METHOD = 'jointspace'

# Run IK Algorithm Using Randomly Sampled points and orientations

if SAMPLE_METHOD == 'Reachable':
    ik_outputs = np.empty((p_arms, p_ik, p_ang), dtype=rp.IKOutput)
    ik_nit = np.zeros_like(ik_outputs, dtype=np.integer)
    ik_success = np.zeros_like(ik_nit)
    score_ik = np.zeros((p_arms,))

    max_iter = 30

    for i in range(p_arms):
        # viz = rp.VizScene()
        # viz.add_arm(arms[i])
        for j in range(p_ik):
            p = rand((3,)) * 2 - 1
            p = p / norm(p) * reach / 2
            for k in range(p_ang):
                random_orient = rand((3,)) * 2 * pi - pi
                R = rp.rpy2R(random_orient)
                A_t = rp.se3(R, p)
                # viz.add_frame(A_t)
                timer.time_in("ik")
                sol = arms[i].ik(A_t, q0=q0, method='pinv', rep='rpy', max_iter=max_iter, tol=1e-3, viz=None, min_delta=1e-5, max_delta=np.inf)
                timer.time_out("ik")
                ik_outputs[i,j,k] = sol
                ik_nit[i,j,k] = sol.nit
                ik_success[i,j,k] = sol.status
                # rp.mprint(sol.ef)
                # rp.mprint(sol.nef)
        # score_ik[i] = (max_iter * p_ik * p_ang - np.sum(ik_nit[i,:,:])) / (max_iter * p_ik * p_ang)
        score_ik[i] = np.sum(ik_success[i,:,:]) / (p_ik * p_ang)
        print(f"[{i+1} / {p_arms}]")

        scipy.io.savemat(f'Test_data_{SAMPLE_METHOD}_arms_{p_arms}_n_{n}_time_{perf_counter()/1000}.mat',
                         mdict={'score_aim_min': score_aim_min, 'score_yosh': score_yosh, 'score_cond': score_cond,
                                'score_ik': score_ik, 'ik_nit': ik_nit, 'ik_success': ik_success, 'dhs': dhs,
                                'score_aim_fro': score_aim_fro, 'score_aim_sum':score_aim_sum})

# Run IK Algorithm using joint space sampling (so that every point is reachable)
if SAMPLE_METHOD == 'jointspace':
    ik_outputs = np.empty((p_arms, p_ik), dtype=rp.IKOutput)
    ik_nit = np.zeros_like(ik_outputs, dtype=np.integer)
    ik_success = np.zeros_like(ik_nit)
    score_ik = np.zeros((p_arms,))

    max_iter = 30

    qs = rand((p_ik, n)) * 2 * pi - pi
    for i in range(p_arms):
        # viz = rp.VizScene()
        # viz.add_arm(arms[i])
        for j in range(p_ik):
            A_t = arms[i].fk(qs[j, :])
            # viz.add_frame(A_t)
            timer.time_in("ik")
            sol = arms[i].ik(A_t, q0=q0, method='pinv', rep='rpy', max_iter=max_iter, tol=1e-3, viz=None, min_delta=1e-5, max_delta=np.inf)
            timer.time_out("ik")
            ik_outputs[i,j] = sol
            ik_nit[i,j] = sol.nit
            ik_success[i,j] = sol.status
            # rp.mprint(sol.ef)
            # rp.mprint(sol.nef)
        score_ik[i] = (max_iter * p_ik - np.sum(ik_nit[i,:])) / (max_iter * p_ik)
        # score_ik[i] = np.sum(ik_success[i,:]) / (p_ik * p_ang)
        print(f"[{i+1} / {p_arms}]")

    scipy.io.savemat(f'Test_data_{SAMPLE_METHOD}_arms_{p_arms}_n_{n}_time_{perf_counter() / 1000}.mat',
                     mdict={'score_aim_min': score_aim_min, 'score_yosh': score_yosh, 'score_cond': score_cond,
                            'score_ik': score_ik, 'ik_nit': ik_nit, 'ik_success': ik_success, 'dhs': dhs,
                            'score_aim_fro': score_aim_fro, 'score_aim_sum': score_aim_sum})


timer.report_all()

fig1, ax1 = plt.subplots()
ax1.scatter(np.linspace(1, p_arms, p_arms), score_aim_min / np.max(score_aim_min), color=[1, 0, 0, 1])
ax1.scatter(np.linspace(1, p_arms, p_arms), score_yosh / np.max(score_yosh), color=[0, 1, 0, 1])
ax1.scatter(np.linspace(1, p_arms, p_arms), score_cond / np.max(score_cond), color=[0, 0, 1, 1])

fig2, ax2 = plt.subplots()
ax2.scatter(score_aim_min, score_ik, color=[1, 0, 0, 1])
ax2.scatter(score_yosh, score_ik, color=[0, 1, 0, 1])
ax2.scatter(score_cond, score_ik, color=[0, 0, 1, 1])

m, b, r_aum, p_aum, std_aum = linregress(score_aim_min, score_ik)
m, b, r_yosh, p_yosh, std_yosh = linregress(score_yosh, score_ik)
m, b, r_cond, p_cond, std_cond = linregress(score_cond, score_ik)

print(f"AUM:\nr^2: {r_aum} p: {p_aum} std-dev: {std_aum}")
print(f"Yosh:\nr^2: {r_yosh} p: {p_yosh} std-dev: {std_yosh}")
print(f"Cond:\nr^2: {r_cond} p: {p_cond} std-dev: {std_cond}")

# print(ik_outputs)

print(score_ik[0])
print(score_aim_min[0])

plt.show()

