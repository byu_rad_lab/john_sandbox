import numpy as np
from numpy.linalg import norm
from numpy import sin, cos
import sympy as sp
from RoboPy import kinematics as K

def yoshikawa(J):
    # print(np.linalg.det(J @ J.T))
    y = np.sqrt(np.max(np.linalg.det(J @ J.T), 0))
    if np.isnan(y):
        y = 0.0
    return y

def rcond(J):
    return 1 / np.linalg.cond(J @ J.T)

def findY(J):
    vN = norm(J[0:3,:], axis=0)
    J[0:3,:] = J[0:3,:] / np.max(vN)
    N = norm(J, axis=0)
    Jn = J / N
    Y = np.minimum(np.abs(Jn.T @ Jn), 1)
    return Y - np.eye(Y.shape[0])

def AIM(J, s='min'):
    Y = findY(J)
    y = None
    n = Y.shape[0]
    if s == 'min':
        y = np.arccos(np.max(Y)) / np.pi * 2
    elif s == 'fro':
        y = np.linalg.norm(np.arccos(Y)  / np.pi * 2, 'fro') / (n**2 - n)
    elif s == 'sum':
        y = np.sum(np.arccos(Y) / np.pi * 2) / (n**2 - n)
    elif s == 'w' or s == 'weight':
        N = int((n**2 - n) / 2)
        angles = np.arccos(Y[np.tril_indices_from(Y, k=-1)]) / np.pi * 2
        angles.sort()
        W = np.fromfunction(lambda i: 1 / (2**(i+1)), (N,), dtype=np.float64)
        return W @ angles

    else:
        print("Invalid argument to AIM")
    return y


def AUM(J):
    m = J.shape[0]
    n = J.shape[1]
    eps = 1e-10
    max = np.max(norm(J[0:3, :], axis=0))
    J[0:3, :] = J[0:3, :] / max
    N = norm(J, axis=0)

    Y = np.zeros((n, n))
    for i in range(n):
        for j in range(n):
            if i == j:
                Y[i, j] = 1
            elif N[i] < eps or N[j] < eps:
                Y[j, i] = 0
                Y[i, j] = 0
            else:
                theta = np.arccos(np.abs(J[:, i] @ J[:, j]) / (N[i] * N[j]))
                Y[i, j] = np.abs(theta * 2 / np.pi)
                Y[j, i] = Y[i, j]

    score = Y.min()
    return score

def YMetric(A, verb=False, eps=1e-10):
    m = A.shape[0]
    n = A.shape[1]

    if not isinstance(A, sp.Matrix):
        A = sp.Matrix(A)

    Y = sp.zeros(n, n)
    for i in range(0, n):
        for j in range(i+1, n):
            if A[:, i].norm() < eps or A[:, j].norm() < eps:
                Y[j, i] = 0
            else:
                theta = sp.acos(sp.Abs(A[:, i].dot(A[:, j]) / (A[:, i].norm() * A[:, j].norm())))
                Y[i, j] = sp.Abs(theta * 2 / sp.pi).evalf()
                # Y[i, j] = sp.Abs(sp.sin(theta)).evalf()
            # Y[i, j] = theta.evalf()
            Y[j, i] = Y[i, j]

    score = Y.norm('fro')

    if n > m:
        score = score / sp.sqrt((n**2 - 3*n + 2*m))
    else:
        score = score / sp.sqrt((n**2 - n))

    score = score.evalf()

    if verb:
        return score, Y
    else:
        return score

def OM2(A, verb=False, eps=1e-10):
    m = A.shape[0]
    n = A.shape[1]

    if not isinstance(A, np.ndarray):
        A = np.array(A, dtype=np.float64)

    Y = np.ones((n, n))
    for i in range(0, n):
        for j in range(i + 1, n):
            if np.linalg.norm(A[:, i]) < eps or np.linalg.norm(A[:, j]) < eps:
                Y[j, i] = 0
            else:
                theta = np.arccos(np.abs(A[:, i] @ (A[:, j])) / (np.linalg.norm(A[:, i]) * np.linalg.norm(A[:, j])))
                Y[i, j] = np.abs(theta * 2 / np.pi)

            Y[j, i] = Y[i, j]

    score = Y.min()

    if verb:
        return score, Y
    else:
        return score




if __name__ == '__main__':

    q = [0, sp.pi / 4, 0]
    dh1 = [[0, 0, 1, 0], [0, 0, 0.1, 0], [0, 0, 1, 0]]
    arm1 = K.SerialArm(dh1)
    dh2 = [[0, 0, 1, 0], [0, 0, 1, 0], [0, 0, 1, 0]]
    arm2 = K.SerialArm(dh2)
    score1, Y1 = OM2(arm1.jacob(q), True)
    score2, Y2 = OM2(arm2.jacob(q), True)
    print("DHs:")
    print(dh1, '\n', dh2)
    print("Scores:")
    sp.pprint(score1)
    sp.pprint(score2)
    print("Jacobains:")
    sp.pprint(arm1.jacob(q))
    print()
    sp.pprint(arm2.jacob(q))
    print("Matrices:")
    sp.pprint(Y1)
    print()
    sp.pprint(Y2)

