import RoboPy as rp
import numpy as np
import matplotlib.pyplot as plt
from numpy import pi
from Jacobian_Orthogonality.source import *

p_arms = 10
p_qs = 100
n = 6

score_AIM_min = np.zeros((p_arms * p_qs,))
score_AIM_fro = np.zeros((p_arms * p_qs,))
score_AIM_sum = np.zeros((p_arms * p_qs,))
score_yosh = np.zeros((p_arms * p_qs,))
score_cond = np.zeros((p_arms * p_qs,))

reach = 6

def generate_random_arm():
    dh = np.random.random_sample((n,4)) * 2 - 1;
    length = np.sum(norm(dh[:, [0, 2]], axis=1))
    dh[:, [0, 2]] *= reach / length
    dh[:, [1, 3]] *= pi
    return rp.SerialArm(dh)

for i in range(p_arms):
    arm = generate_random_arm()
    qs = np.random.random_sample((p_qs, n))
    for j in range(p_qs):
        J = arm.jacob(qs[j,:])
        score_AIM_min[i*p_qs + j] = AIM(J, 'min')
        score_AIM_fro[i * p_qs + j] = AIM(J, 'fro')
        score_AIM_sum[i * p_qs + j] = AIM(J, 'sum')
        score_yosh[i * p_qs + j] = yoshikawa(J)
        score_cond[i * p_qs + j] = rcond(J)

plt.rcParams.update({'font.size': 16})
fig, ax = plt.subplots(2, 1)
ax[0].scatter(score_AIM_min, score_yosh, color=[0.2,0,0,1])
ax[1].scatter(score_AIM_min, score_cond, color=[0,0.2,0,1])
ax[1].set_xlabel('AIM Score')
ax[0].set_ylabel('Yoshikawa Score')
ax[1].set_ylabel('Inverse Condition #')
plt.yscale('linear')
plt.show()
