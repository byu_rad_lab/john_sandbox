import RoboPy as rp
import numpy as np

def x2dh(x):
    return [[0, 0, x[0], 0], [0, 0, x[1], 0], [0, 0, x[2], 0]]

arm_yosh = rp.SerialArm(x2dh([0.986, 0.939, 0.049]), base=rp.transl([0,2,0]))
arm_cond = rp.SerialArm(x2dh([0.98916981, 0.94005641, 0.03244325]), base=rp.transl([0,4,0]))
arm_AIM = rp.SerialArm(x2dh([1.25621351, 0.62125734, 0.0105336]), base=rp.transl([0,0,0]))

viz = rp.VizScene()
viz.add_arm(arm_yosh)
viz.add_arm(arm_cond)
viz.add_arm(arm_AIM)

viz.hold()