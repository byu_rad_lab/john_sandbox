from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg
import pyqtgraph.opengl as gl
import numpy as np

thing = QtGui.QApplication([])

# All programs require app to run, if app already exists don't create new one
if QtGui.QApplication.instance():
    app = QtGui.QApplication.instance()
else:
    app = QtGui.QApplication([])

# # Basic main window
# mw = QtGui.QMainWindow()
# mw.resize(800, 800)
# mw.show()
# app.exec()

# Grid Layout Window
gridwin = pg.GraphicsLayoutWidget(show=True, title='Grid Layout Window')
gridwin.resize(1000, 800)

# Adding plots to grid window
p1 = gridwin.addPlot(title='basic xy plot', x=np.linspace(0, 10), y=np.linspace(5, -5))
p2 = gridwin.addPlot(title='two in one row', y=np.random.random((10,)), pen=(255, 0, 0))

# Add a new row
gridwin.nextRow()
p3 = gridwin.addPlot(title='next row', y=np.random.random((100,)), pen='w')
p3.showGrid(x=True, y=True)
p3.setLogMode(x=False, y=True)
p3.setLabel('left', 'Y Axis', units='A')
p3.setLabel('top', 'X Axis', units='s')
# pen=None for no line, symbolBrush for symbol color, symbolPen for symbol outline
p4 = gridwin.addPlot(title='scatter', y=np.random.random((20,)), x=np.random.random((20,)), pen=None, symbolBrush=(200, 200, 0), symbolPen=(200, 0, 200), symbol='t', symbolSize=20)

gridwin.nextRow()
# ScatterPlotItem is shortcut for making more complicated scatter plots with different symbols etc.
# https://pyqtgraph.readthedocs.io/en/latest/graphicsItems/scatterplotitem.html#pyqtgraph.ScatterPlotItem.setSymbol
p5 = gridwin.addPlot(title='custom scatter plot')
spots = [{'pos': (i, i), 'size': i + 2, 'pen': (i * 255 / 20, 10, 10),
          'brush': (i * 255 / 20, 10, 10), 'symbol': 't'} for i in range(20)]
p5.addItem(pg.ScatterPlotItem(spots=spots))

p6 = gridwin.addPlot(title='changing')
p6.enableAutoRange('xy', False)
# set range
# https://pyqtgraph.readthedocs.io/en/latest/graphicsItems/viewbox.html#pyqtgraph.ViewBox.setXRange
p6.setXRange(-0.2, 1.2)
p6.setYRange(-0.2, 1.2)
data = np.random.random((100, 2))
line = p6.plot(data)
def update():
    global line, data, p6
    line.setData(data + 0.01 * np.random.random((100, 2)))
timer = QtCore.QTimer()
timer.timeout.connect(update)
timer.start(1)

app.exec()
