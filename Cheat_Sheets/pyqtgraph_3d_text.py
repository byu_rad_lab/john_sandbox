from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph.opengl as gl
from pyqtgraph.opengl.GLGraphicsItem import GLGraphicsItem
import numpy as np

class GLTextItem(GLGraphicsItem):
    """
    Class for plotting text on a GLWidget
    """

    def __init__(self, pos=None, text=None):
        GLGraphicsItem.__init__(self)
        self.setGLOptions('translucent')
        self.text = text
        self.pos = pos

    def setGLViewWidget(self, GLViewWidget):
        self.GLViewWidget = GLViewWidget

    def setText(self, text):
        self.text = text
        self.update()

    def setPos(self, pos):
        self.pos = pos
        self.update()

    def setData(self, pos=None, text=None):
        if pos is not None:
            self.pos = pos

        if text is not None:
            self.text = text

        self.update()

    def paint(self):
        self.GLViewWidget.qglColor(QtCore.Qt.white)
        self.GLViewWidget.renderText(self.pos[0], self.pos[1], self.pos[2], self.text)


if __name__ == '__main__':
    # Create app
    app = QtGui.QApplication([])
    import warnings
    w1 = gl.GLViewWidget()
    w1.resize(800, 800)
    w1.show()
    w1.setWindowTitle('Earth 3D')
    w1.addItem(gl.GLGridItem())
    with warnings.catch_warnings():
        warnings.simplefilter('ignore')
        pos = np.array([1., 2., 3.])
        gl_txt = GLTextItem(pos, 'Sample test')
        gl_txt.setGLViewWidget(w1)
        w1.addItem(gl_txt)
        t = 0
        while w1.isVisible():
            t += 0.001
            pos = np.array([np.sin(t)*5, np.cos(t)*10, np.sin(t) * np.cos(t)*10])
            gl_txt.setData(pos=pos, text=str(t))
            app.processEvents()