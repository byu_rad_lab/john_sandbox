import sympy as sp
from sympy import *
from sympy.abc import a, b, c
import mpmath as mp
from helper_files.SympyHelpers import *

s, t = sp.symbols('s, t', finite=True, real=True)

def L(exp, s=s, t=t):
    return laplace_transform(exp, s, t, noconds=True)

def invL(exp, s=s, t=t):
    return inverse_laplace_transform(exp, s, t) / Heaviside(t)

x1 = 3*t**2-1
x2 = 5*t**3-3*t
x3 = 2*t**2-t

def ip(f, g):
    return sp.integrate(f*g, (t,-1,1))

def norm(f):
    return sp.sqrt(ip(f, f))

def ang(f, g):
    return sp.acos(ip(f, g) / (norm(f) * norm(g)))

print("x1 and x2")
print(ang(x1, x2))

print("x1 and x3")
print(ang(x1, x3).evalf(3))

print("x2 and x3")
print(ang(x2, x3))

