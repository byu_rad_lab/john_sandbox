import RoboPy as rp
import numpy as np
import matplotlib.pyplot as plt


arm = rp.PlanarDyn(n=2, L=1)
w = 5
a = 1
qs = np.asarray([r * a * np.array([np.cos(r * w), np.sin(r * w)]) for r in np.linspace(0, 2 * np.pi / a, 500)])

viz = rp.VizScene()
viz.add_arm(arm)

xs = arm.fk(qs, rep='cart')[:, 0:2]
fig, ax = plt.subplots(1, 2)
ax[0].plot(xs[:, 0], xs[:, 1])
ax[1].plot(qs[:, 0], qs[:, 1])

plt.pause(0.01)

while True:

    for q in qs:
        viz.update(q)
        viz.hold(0.01)
