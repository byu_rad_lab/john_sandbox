import numpy as np
import matplotlib.pyplot as plt

V = np.array([[1, 0],
              [10, 10],
              [0, 1]]).T

fig, ax1 = plt.subplots()

p = 10000

w1 = np.zeros((p, 2))
w2 = np.zeros((p, 2))

for i in range(p):
    x = np.random.random((3,)) * 2 - 1

    w1[i] = V @ x
    k = np.random.randint(0, 3)
    x[k] = x[k] / np.abs(x[k])
    w2[i] = V @ x

ax1.scatter(w1[:, 0], w1[:, 1], c='k', s=1)
ax1.scatter(w2[:, 0], w2[:, 1], c='r', s=1)

ax1.axis('equal')
plt.show()
