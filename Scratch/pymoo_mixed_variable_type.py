import numpy as np
from pymoo.core.problem import ElementwiseProblem
from pymoo.core.variable import Real, Integer
from pymoo.core.mixed import MixedVariableMating, MixedVariableSampling, MixedVariableDuplicateElimination
from pymoo.algorithms.moo.nsga2 import NSGA2, RankAndCrowdingSurvival
from pymoo.optimize import minimize


class MixedVariableProblem(ElementwiseProblem):

    def __init__(self, **kwargs):
        vars = {}
        for i in range(5):
            vars[f'y{i}'] = Integer(bounds=(0, 5))
            vars[f'z{i}'] = Real(bounds=(0, 1))
        super().__init__(vars=vars, n_obj=3, n_ieq_constr=1, **kwargs)

    def _evaluate(self, x, out, *args, **kwargs):
        y = []
        z = []
        for i in range(5):
            y.append(x[f'y{i}'])
            z.append(x[f'z{i}'])
        y = np.asarray(y, dtype=int)
        z = np.asarray(z, dtype=float)

        out["F"] = (np.sum(z), -y @ z, np.sum(y))
        out["G"] = 5 - np.sum(y)


problem = MixedVariableProblem()
algorithm = NSGA2(pop_size=10,
                  sampling=MixedVariableSampling(),
                  mating=MixedVariableMating(eliminate_duplicates=MixedVariableDuplicateElimination()),
                  eliminate_duplicates=MixedVariableDuplicateElimination())
sol = minimize(problem, algorithm, termination=('n_gen', 100), verbose=True)

print(sol)
print(sol.X)
print(sol.F)