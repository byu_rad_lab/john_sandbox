class Parent:
    def __init__(self):
        self.a = 1
        self.b = 2


class Parent(Parent):
    def __init__(self):
        super().__init__()
        self.b = 4

    def sum(self):
        return self.a + self.b


p = Parent()
c = Parent()
print(p.b)
print(c.b)
print(c.sum())
