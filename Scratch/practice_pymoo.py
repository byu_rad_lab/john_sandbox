from pymoo.core.problem import ElementwiseProblem, StarmapParallelization
from pymoo.algorithms.moo.nsga2 import NSGA2
from pymoo.operators.crossover.sbx import SBX
from pymoo.operators.mutation.pm import PM
from pymoo.operators.sampling.rnd import FloatRandomSampling
from pymoo.termination import get_termination
from pymoo.optimize import minimize
import numpy as np

import matplotlib.pyplot as plt
import multiprocessing


class MyProblem(ElementwiseProblem):

    def __init__(self, **kwargs):
        super().__init__(n_var=2,
                         n_obj=2,
                         n_ieq_constr=2,
                         xl=np.array([-2, -2], dtype=float),
                         xu=np.array([2, 2], dtype=float),
                         **kwargs)

    def _evaluate(self, x : np.ndarray, out : dict, *args, **kwargs):
        f1 = 100 * (x[0]**2 + x[1]**2)
        f2 = (x[0] - 1)**2 + x[1]**2

        g1 = 2 * (x[0] - 0.1) * (x[0] - 0.9) / 0.18
        g2 = -20 * (x[0] - 0.4) * (x[0] - 0.6) / 4.8

        out['F'] = [f1, f2]
        out['G'] = [g1, g2]


pool = multiprocessing.Pool(4)
runner = StarmapParallelization(pool.starmap)

# define the problem by passing the starmap interface of the thread pool
problem = MyProblem(runner=runner)
termination = get_termination("n_gen", 40)
algorithm = NSGA2(
    pop_size=40,
    n_offsprings=10,
    sampling=FloatRandomSampling(),
    crossover=SBX(prob=0.9, eta=15),
    mutation=PM(eta=20),
    eliminate_duplicates=True
)

sol = minimize(problem,
               algorithm,
               termination,
               seed=1,
               save_history=True,
               verbose=True)

X = sol.X
F = sol.F

xl, xu = problem.bounds()
fig, axs = plt.subplots(1, 2)
axs[0].scatter(X[:, 0], X[:, 1], s=30, facecolors='none', edgecolors='r')
axs[0].set_xlim(xl[0], xu[0])
axs[0].set_ylim(xl[1], xu[1])
axs[0].set_title("Design Space")

axs[1].scatter(F[:, 0], F[:, 1], s=30, facecolors='none', edgecolors='blue')
axs[1].set_title("Objective Space")
plt.show()
