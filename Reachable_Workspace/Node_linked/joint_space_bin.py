import numpy as np
from pyqtgraph import opengl as gl
from task_space_bin import Workspace2D


class Jointspace:
    def __init__(self, qmin, qmax, grid_spacing=None):
        self.n = qmin.shape[0]
        self.qmin = qmin
        self.qmax = qmax
        if grid_spacing is None:
            grid_spacing = np.ones((self.n,)) * 0.1

        self.shape = []
        self.grid_spacing = np.zeros((self.n,))
        for i in range(self.n):
            dq = qmax[i] - qmin[i]
            num = int(np.round(dq / grid_spacing[i]))
            self.shape.append(num)
            self.grid_spacing[i] = dq / num

        self.space = np.zeros(self.shape)
        self.iter_index = np.zeros((self.n,), dtype=int)

    def get_bin_index(self, q):
        index = []
        check = True
        for a, lim, dx, n in zip(q, self.qmin, self.grid_spacing, self.shape):
            b = a - lim
            i = np.trunc(b / dx) if b >= 0 else np.trunc(b / dx - 1)
            if i >= n:
                i = n - 1
            index.append(int(i))
            if not 0 <= i < n:
                check = False
        return tuple(index), check

    def get_angle_from_index(self, index):
        q = np.zeros((self.n,))
        for i in range(self.n):
            q[i] = index[i] * self.grid_spacing[i]
        return q

    def get_rand_index(self):
        index = []
        for i in range(self.n):
            index.append(np.random.randint(self.shape[i]))
        return tuple(index)

    def __iter__(self):
        self.iter_index = np.zeros((self.n,), dtype=int)
        return self

    def __next__(self):
        q = self.get_angle_from_index(self.iter_index)
        num = self.space[self.iter_index]
        for i in range(self.n):
            self.iter_index[i] += 1
            if self.iter_index[i] > self.shape[i] - 1:
                if i == self.n:
                    raise StopIteration
                self.iter_index[i] = 0
            else:
                break
        return q, num


if __name__ == '__main__':

    import RoboPy as rp
    dh = [[0, 0, 3 / 6, np.pi / 3]] * 6
    # dh = [[0, 0, 3/2, 0],
    #       [0, 0, 3/2, 0]]
    n = len(dh)
    arm = rp.SerialArm(dh)
    qmin = np.ones((n,)) * -np.pi
    qmax = np.ones((n,)) * np.pi
    # grid_spacing = np.ones((n,)) * 0.2
    grid_spacing = np.ones((n,)) * 0.2
    viz = rp.VizScene()
    viz.add_arm(arm)
    # input("Press enter to begin workspace search...")
    points = np.array([[-3.0, -3.0],
                       [3.0, -3.0],
                       [3.0, 3.0],
                       [-3.0, 3.0]])
    task_space = Workspace2D(points, [0.3, 0.3, 0.2])

    joint_space = Jointspace(qmin, qmax, grid_spacing)

    total_samples = 0
    in_space_samples = 0
    novel_samples = 0

    bins_max_reachable = 0
    for pos, num in task_space:
        if np.linalg.norm(pos) < arm.max_reach + np.linalg.norm(np.array([task_space.dx / 2, task_space.dy / 2])) and not np.isnan(
                num):
            bins_max_reachable += 1

    # method = 'joint'
    # method = 'brute'
    method = 'task'

    DRAW_FULL = False
    DRAW_IN = False
    MAX_SAMPLE = np.inf

    if DRAW_IN:
        task_space.add_to_scene(viz)

    if method == 'brute':
        count = 0
        while count < 500 and total_samples < MAX_SAMPLE:
            count += 1
            total_samples += 1
            # print(total_samples)
            index = joint_space.get_rand_index()
            if joint_space.space[index] == 0:
                q = joint_space.get_angle_from_index(index)
                pos = arm.fk(q)[0:3, 3]
                check, i, j = task_space.put_in_bin(pos)
                if check:
                    in_space_samples += 1
                    if task_space.bin[i, j] == 1:
                        count = 0
                        novel_samples += 1
                        if DRAW_IN:
                            viz.add_marker(task_space.get_pos_from_index((i,j)), color=[1, 1, 0,1])
                            viz.add_marker(pos, color=[0, 1, 0, 0.5])
                            viz.update(qs=q)
                    elif DRAW_FULL:
                        viz.add_marker(pos, color=[1, 0, 1, 0.2])
                        viz.update(qs=q)
                elif DRAW_FULL:
                    viz.add_marker(pos, color=[1, 0, 0, 0.2])
                    viz.update(qs=q)

    if method == 'joint':
        outer_count = 0
        inner_count = 0
        while outer_count < 500 or in_space_samples < 20:
            outer_count += 1
            sample_list = [joint_space.get_rand_index()]

            while len(sample_list) > 0 and inner_count < 20000 and total_samples < MAX_SAMPLE:
                # print(inner_count)
                inner_count += 1
                total_samples += 1
                index = sample_list.pop()
                joint_space.space[index] += 1
                q = joint_space.get_angle_from_index(index)
                pos = arm.fk(q)[0:3, 3]
                check, i, j = task_space.put_in_bin(pos)
                if check:
                    in_space_samples += 1
                    if task_space.bin[i, j] == 1:
                        outer_count = 0
                        inner_count = 0
                        novel_samples += 1
                        if DRAW_IN:
                            viz.add_marker(task_space.get_pos_from_index((i, j)), color=[1, 1, 0, 1])
                            viz.add_marker(pos, color=[0, 1, 0, 0.5])
                            viz.update(qs=q)
                    elif DRAW_FULL:
                        viz.add_marker(pos, color=[1, 0, 1, 0.2])
                        viz.update(qs=q)

                    for i in range(arm.n):
                        if index[i] > 0:
                            holder = list(index)
                            holder[i] += -1
                            num = joint_space.space[tuple(holder)]
                            if num == 0:
                                sample_list.append(tuple(holder))
                                joint_space.space[tuple(holder)] += 1
                        if index[i] < joint_space.shape[i] - 1:
                            holder = list(index)
                            holder[i] += 1
                            num = joint_space.space[tuple(holder)]
                            if num == 0:
                                sample_list.append(tuple(holder))
                                joint_space.space[tuple(holder)] += 1
                elif DRAW_FULL:
                    viz.add_marker(pos, color=[1, 0, 0, 0.2])
                    viz.update(qs=q)

    if method == 'task':
        count = 0
        while (count < 100 and total_samples < MAX_SAMPLE) or in_space_samples < 20:
            flag = False
            index = ()
            while not flag:
                index = joint_space.get_rand_index()
                if joint_space.space[index] == 0:
                    flag = True
            q = joint_space.get_angle_from_index(index)
            joint_space.space[index] += 1
            q_list = [q]
            count += 1

            while len(q_list) > 0:
                count += 1
                total_samples += 1
                qc = q_list.pop()
                pos = arm.fk(qc)[0:3, 3]
                check, i, j = task_space.put_in_bin(pos)
                if check:
                    in_space_samples += 1
                    if task_space.bin[i, j] == 1:
                        novel_samples += 1
                        count = 0
                        if DRAW_IN:
                            viz.add_marker(task_space.get_pos_from_index((i, j)), color=[1, 1, 0, 0.5])
                            viz.add_marker(pos, color=[0, 1, 0, 0.5])
                            viz.update(qs=qc)

                        # Jacobian search
                        J = arm.jacob(qc)[0:3, :]
                        Jinv = np.linalg.pinv(J)
                        dq_x_pos = Jinv @ np.array([task_space.dx, 0, -pos[2] * 2 / 3]) / 3
                        dq_y_pos = Jinv @ np.array([0, task_space.dy, -pos[2] * 2 / 3]) / 3
                        dq_x_neg = Jinv @ np.array([-task_space.dx, 0, -pos[2] * 2 / 3]) / 3
                        dq_y_neg = Jinv @ np.array([0, -task_space.dy, -pos[2] * 2 / 3]) / 3
                        qs = [dq_x_pos, dq_y_pos, dq_x_neg, dq_y_neg]
                        for g in range(9):
                            for dq in qs:
                                qnew = rp.wrap_angle((g + 1) * dq)
                                # q_list.append(qc + qnew)
                                index, check = joint_space.get_bin_index(qnew)
                                if check:
                                    if joint_space.space[index] < 3:
                                        joint_space.space[index] += 1
                                        q_list.append(qc + qnew)

                    elif DRAW_FULL:
                        viz.add_marker(pos, color=[1, 0, 1, 0.2])
                        viz.update(qs=qc)
                elif DRAW_FULL:
                    viz.add_marker(pos, color=[1, 0, 0, 0.2])
                    viz.update(qs=qc)
            # if count > 100:
            #     q_list = []
            #     count = 0

    for i in range(1, task_space.nx - 1):
        for j in range(1, task_space.ny - 1):
            if task_space.bin[i, j] == 0:
                if task_space.bin[i + 1, j] > 0 and task_space.bin[i - 1, j] > 0 and task_space.bin[i, j + 1] > 0 and \
                        task_space.bin[i, j - 1] > 0:
                    task_space.bin[i, j] += 1
                    viz.add_marker(task_space.get_pos_from_index((i, j)), color=[0.8, 0.8, 0.0, 1.0])

    bins_reached = 0
    for pos, num in task_space:
        check, i, j = task_space.put_in_bin(pos)
        if num > 0 and not np.isnan(num):
            bins_reached += 1
        elif num == 0 and check and np.linalg.norm(pos) < arm.max_reach + np.linalg.norm(np.array([task_space.dx / 2, task_space.dy / 2])):
            viz.add_marker(np.array([pos[0], pos[1], 0.05]), color=[1, 0, 0, 1])

    print(f"Method: {method}")
    print(
        f"Total Samples: {total_samples}\tSamples in Space: {in_space_samples}\tIn-Space Ratio: {in_space_samples / total_samples}")
    print(f"Novel Samples: {novel_samples}\tNovel Ratio: {novel_samples / total_samples}")
    print(
        f"Bins Reached: {bins_reached}\tTotal Bins Reachable: {bins_max_reachable}\tReached Ratio: {bins_reached / bins_max_reachable}")
    viz.hold()

"""
Method: task
Total Samples: 1996	Samples in Space: 795	In-Space Ratio: 0.3982965931863727
Novel Samples: 83	Novel Ratio: 0.04158316633266533
Bins Reached: 83	Total Bins Reachable: 88	Reached Ratio: 0.9431818181818182

Total Samples: 1757	Samples in Space: 741	In-Space Ratio: 0.4217416050085373
Novel Samples: 83	Novel Ratio: 0.04723961297666477
Bins Reached: 83	Total Bins Reachable: 88	Reached Ratio: 0.9431818181818182

Total Samples: 1775	Samples in Space: 832	In-Space Ratio: 0.4687323943661972
Novel Samples: 84	Novel Ratio: 0.04732394366197183
Bins Reached: 84	Total Bins Reachable: 88	Reached Ratio: 0.9545454545454546

Method: task
Total Samples: 3213	Samples in Space: 1710	In-Space Ratio: 0.5322128851540616
Novel Samples: 85	Novel Ratio: 0.026455026455026454
Bins Reached: 85	Total Bins Reachable: 88	Reached Ratio: 0.9659090909090909

Total Samples: 3134	Samples in Space: 1870	In-Space Ratio: 0.5966815571155073
Novel Samples: 86	Novel Ratio: 0.02744097000638162
Bins Reached: 86	Total Bins Reachable: 88	Reached Ratio: 0.9772727272727273

Total Samples: 3105	Samples in Space: 1825	In-Space Ratio: 0.5877616747181964
Novel Samples: 86	Novel Ratio: 0.027697262479871174
Bins Reached: 86	Total Bins Reachable: 88	Reached Ratio: 0.9772727272727273
"""