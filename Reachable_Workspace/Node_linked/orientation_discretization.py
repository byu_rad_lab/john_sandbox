import pyqtgraph as pg
from pyqtgraph import opengl as gl
from pyqtgraph import QtGui, QtCore

import numpy as np
from numpy.linalg import norm


class CubeSurfaceBin:
    """
    Surface of 2 x 2 x 2 cube split into m x n x p discrete bins.
    Length m list of 2d numpy arrays, top and bottom are n x p and all middle ones are n x 2
    Have methods for:
    - Taking point on sphere and assigning to bin
    - incrementing bin values
    - visualization, lines connecting points on sphere and bin centers
    - location on sphere given bin index
    """
    def __init__(self, m, n, p):
        self.m = m
        self.n = n
        self.p = p

        self.data = []
        self.cube_data = []
        self.sphere_data = []
        for i in range(m):
            flat_slice = []
            for j in range(n):
                line = []
                if 0 in (i, j) or i == m - 1 or j == n - 1:
                    r = range(p)
                else:
                    r = (0, p - 1)
                for k in r:
                    pos = np.array([i / (m - 1) * 2 - 1, j / (n - 1) * 2 - 1, k / (p - 1) * 2 - 1])
                    line.append(pos)
                    self.cube_data.append(pos)
                    self.sphere_data.append(pos / norm(pos))
                flat_slice.append(line)
            self.data.append(flat_slice)
        self.cube_data = np.asarray(self.cube_data)
        self.sphere_data = np.asarray(self.sphere_data)

    def get_3d_index(self, index1):
        i, j, k = 0, 0, 0
        if index1 >= self.n * self.p:
            i += 1
            index1 -= self.n * self.p
            while index1 >= 2 * (self.n + self.p - 2) and i < self.m - 1:
                i += 1
                index1 -= 2 * (self.n + self.p - 2)
        if index1 >= self.p:
            j += 1
            index1 -= self.p
            if i == self.m - 1 or i == 0:
                while index1 >= self.p:
                    j += 1
                    index1 -= self.p
            else:
                while index1 >= 2 and j < self.n - 1:
                    j += 1
                    index1 -= 2
        if not (j == 0 or j == self.n - 1 or i == 0 or i == self.m - 1) and index1 > 0:
            k = self.p - 1
        else:
            k = index1
        return (i, j, k)

    def get_1d_index(self, index3):
        i, j, k = index3
        m, n, p = index3
        index1 = 0

        if i > 0:
            index1 += self.n * self.p
            i -= 1
            while i > 0:
                index1 += 2 * (self.n + self.p - 2)
                i -= 1

        if j > 0:
            index1 += self.p
            j -= 1
            while j > 0:
                if m == 0 or m == self.m - 1:
                    index1 += self.p
                else:
                    index1 += 2
                j -= 1

        if m == 0 or m == self.m - 1 or n == 0 or n == self.n - 1:
            index1 += k
        elif k == self.p - 1:
            index1 += 1
        elif k > 0:
            print("INVALID COORDINATE FOR SPHERE SURFACE")
            return

        return index1

    def bin_pos(self, pos):
        pos = (pos / np.max(np.abs(pos)) + 1) / 2 * (np.array([self.m, self.n, self.p]) - 1)
        index = tuple(np.array(np.round(pos), dtype=int))
        print(index)
        return self.get_1d_index(index)


if __name__ == '__main__':

    app = QtGui.QApplication([])
    w = gl.GLViewWidget()
    w.resize(800, 800)
    w.show()
    m, n, p = 8, 8, 8
    cube = CubeSurfaceBin(m, n, p)

    cube_colors = np.full((cube.cube_data.shape[0], 4), [1, 0, 0, 0.5])
    sphere_colors = np.full((cube.sphere_data.shape[0], 4), [0, 0, 1, 0.5])

    cube_points = gl.GLScatterPlotItem(pos=cube.cube_data, color=cube_colors)
    sphere_points = gl.GLScatterPlotItem(pos=cube.sphere_data, color=sphere_colors)

    line_data = np.empty((cube.cube_data.shape[0] * 2, cube.cube_data.shape[1]), dtype=cube.cube_data.dtype)
    line_data[0::2] = cube.cube_data
    line_data[1::2] = cube.sphere_data
    line = gl.GLLinePlotItem(pos=line_data, mode='lines')

    w.addItem(cube_points)
    w.addItem(sphere_points)
    w.addItem(line)

    for i in range(50):
        x = np.random.random((3,)) * 2 - 1
        x = x / np.linalg.norm(x)

        point = gl.GLScatterPlotItem(pos=x, color=[1, 1, 1, 1])
        w.addItem(point)
        index = cube.bin_pos(x)
        q = cube.sphere_data[index]
        w.addItem(gl.GLLinePlotItem(pos=np.vstack([x, q])))  # cube.sphere_data[index]

    app.exec()
