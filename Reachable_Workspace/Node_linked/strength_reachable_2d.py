"""

Repeat the work with a two dimensional task space, but change search method to find the strength-accessible region.
Start with an IK search that gives a couple viable starting positions, then branch from these. Need some sort of way
to organize the search; hierarchy of lists? Top level are feasible starting points, each step down includes some
successful step in a joint direction, so that we can terminate searches in a smart way. Probably still need one joint
space grid to prevent repeat searches?

RRT search? Closest member search, need a way to make this faster.
https://www.geeksforgeeks.org/find-closest-element-binary-search-tree/

Classes:
TwoDTaskSpace
SearchTree?

"""

import RoboPy as rp
import numpy as np
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg
from pyqtgraph import opengl as gl
import time

app = QtGui.QApplication([])

w = pg.GraphicsLayoutWidget()
w.resize(1200, 800)
w.show()
js = w.addPlot(title='Joint Space')
js.setXRange(-np.pi, np.pi)
js.setYRange(-np.pi, np.pi)
ts = w.addPlot(title='Task Space')
ts.setXRange(-2, 2)
ts.setYRange(-2, 2)

arm = rp.PlanarDyn(n=2, L=1.0)

app.exec()
