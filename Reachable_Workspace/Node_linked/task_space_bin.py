import numpy as np
from pyqtgraph import opengl as gl


class Workspace2D:
    def __init__(self, points=np.array([]), grid_spacing=np.ones((3,))*0.1):
        self.points = np.asarray(points)
        self.n = len(points)
        self.dx = grid_spacing[0]
        self.dy = grid_spacing[1]
        self.lines = np.asarray([points[i] - points[i - 1] for i in range(self.n)])
        self.xmax = np.max(points[:, 0])
        self.xmin = np.min(points[:, 0])
        self.ymax = np.max(points[:, 1])
        self.ymin = np.min(points[:, 1])

        self.nx = int((self.xmax - self.xmin) / self.dx)
        self.ny = int((self.ymax - self.ymin) / self.dy)

        self.dx = (self.xmax - self.xmin) / self.nx
        self.dy = (self.ymax - self.ymin) / self.ny
        self.dz = grid_spacing[2]

        self.bin = np.empty((self.nx, self.ny))
        for i in range(self.nx):
            for j in range(self.ny):
                pos = self.get_pos_from_index((i, j))
                if self.check_in_bounds(pos):
                    self.bin[i, j] = 0
                else:
                    self.bin[i, j] = np.NaN

        self.LinePlotItem = None
        self.i = 0
        self.j = 0

    def get_pos_from_index(self, ij):
        i, j = ij
        x = self.xmin + (i + 0.5) * self.dx
        y = self.ymin + (j + 0.5) * self.dy
        return np.array([x, y])

    def check_in_bounds(self, pos):
        if not(self.xmin <= pos[0] <= self.xmax and self.ymin <= pos[1] <= self.ymax):
            return False
        for v, p in zip(self.lines, self.points):
            n = np.array([v[1], -v[0]])
            w = pos - p
            if w @ n > 0:
                return False
        return True

    def get_bin_index(self, pos):
        x = pos[0] - self.xmin
        y = pos[1] - self.ymin
        i = np.trunc(x / self.dx) if x >= 0 else np.trunc(x / self.dx - 1)
        j = np.trunc(y / self.dy) if y >= 0 else np.trunc(y / self.dy - 1)
        return int(i), int(j)

    def check_in_bin(self, pos):
        i, j = self.get_bin_index(pos)
        if 0 <= i < self.nx and 0 <= j < self.ny:
            return True
        return False

    def put_in_bin(self, pos):
        if len(pos) == 3:
            if np.abs(pos[2]) > self.dz / 2:
                return False, -1, -1
        i, j = self.get_bin_index(pos)
        if 0 <= i < self.nx and 0 <= j < self.ny:
            if not np.isnan(self.bin[i, j]):
                self.bin[i, j] += 1.0
                return True, i, j
        return False, -1, -1

    def add_to_scene(self, viz):
        self.LinePlotItem = gl.GLLinePlotItem(pos=np.vstack([self.points, self.points[0]]))
        viz.window.addItem(self.LinePlotItem)
        pos = np.array([[self.get_pos_from_index((i, j)) for j in range(self.ny)] for i in range(self.nx)])
        color = [
            [[0.8, 0.8, 0.8, 0.5] if not np.isnan(self.bin[i, j]) else [0.2, 0.2, 0.2, 0.5] for i in range(self.nx)] for
            j in range(self.ny)]
        viz.add_marker(pos, color)
        # for i in range(self.nx):
        #     for j in range(self.ny):
        #         pos = self.get_pos_from_index((i, j))
        #         if not np.isnan(self.bin[i, j]):
        #             viz.add_marker(pos, [0.8, 0.8, 0.8, 0.5])
        #         else:
        #             viz.add_marker(pos, [0.2, 0.2, 0.2, 0.5])
        viz.app.processEvents()

    def __iter__(self):
        self.i = 0
        self.j = 0
        return self

    def __next__(self):
        pos = self.get_pos_from_index((self.i, self.j))
        num = self.bin[self.i, self.j]
        if self.i + 1 < self.nx:
            self.i += 1
        elif self.j + 1 < self.ny:
            self.i = 0
            self.j += 1
        else:
            raise StopIteration
        return pos, num


if __name__ == '__main__':

    import RoboPy as rp
    dh = [[0, 0, 1, np.pi / 2],
          [0, 0, 0.5, np.pi / 2],
          [0, 0, 0.5, np.pi / 2],
          [0, 0, 0.5, 0]]
    n = len(dh)
    arm = rp.SerialArm(dh)
    viz = rp.VizScene()
    viz.add_arm(arm)
    points = np.array([[0.25, 0.25],
                       [1.5, 0.25],
                       [2, 2],
                       [0.25, 2]])
    work = Workspace2D(points, grid_spacing=[0.15, 0.15])
    work.add_to_scene(viz)

    bins_max_reachable = 0
    for pos, num in work:
        if np.linalg.norm(pos) < arm.max_reach + np.linalg.norm(np.array([work.dx / 2, work.dy / 2])) and not np.isnan(num):
            bins_max_reachable += 1

    sample_variance = 0.2
    count = 0
    samples_in_space = 0
    samples_total = 0
    samples_novel = 0

    method = 'brute'
    q_list = []

    if method == 'smart':
        while count < 100:
            q = np.random.random_sample((n,)) * 2 * np.pi - np.pi
            q_list.append(q)
            count += 1

            while len(q_list) > 0:
                samples_total += 1
                qc = q_list[-1]
                q_list.pop()
                x = arm.fk(qc)[0:2, 3]
                i, j = work.get_bin_index(x)
                if 0. <= i < work.nx and 0. <= j < work.ny:
                    if not np.isnan(work.bin[i, j]):
                        samples_in_space += 1
                        color = [0, 0.7, 0.7, 0.3]
                        work.bin[i, j] += 1
                        if work.bin[i, j] < 2:
                            samples_novel += 1
                            count = 0
                            color = [0, 1, 0, 0.3]
                            viz.update(qs=qc)
                            viz.add_marker(x, color=color)
                            viz.add_marker(work.get_pos_from_index((i, j)), color=[0.8, 0.8, 0.0, 1.0])
                            for g in range(3):
                                dq = np.random.random((n,)) * 2 - 1
                                dq = dq / np.linalg.norm(dq) * sample_variance
                                for k in range(4):
                                    q_list.append(qc + dq * (k + 1))
                        # else:
                        #     viz.update(qs=qc)
                        #     viz.add_marker(x, color=color)
                #     else:
                #         color = [1, 0, 0, 0.3]
                #         viz.update(qs=qc)
                #         viz.add_marker(x, color=color)
                # else:
                #     color = [1, 0, 0, 0.3]
                #     viz.update(qs=qc)
                #     viz.add_marker(x, color=color)

    if method == 'brute':
        while count < 100:
            q = np.random.random((n,)) * 2 * np.pi - np.pi
            count += 1
            samples_total += 1
            x = arm.fk(q)[0:2, 3]
            i, j = work.get_bin_index(x)
            if 0. <= i < work.nx and 0. <= j < work.ny:
                if not np.isnan(work.bin[i, j]):
                    samples_in_space += 1
                    color = [0, 0.7, 0.7, 0.3]
                    work.bin[i, j] += 1
                    if work.bin[i, j] < 2:
                        samples_novel += 1
                        count = 0
                        color = [0, 1, 0, 0.3]
                        viz.add_marker(work.get_pos_from_index((i, j)), color=[0.8, 0.8, 0.0, 1.0])
                        viz.update(qs=q)
                        viz.add_marker(x, color=color)

    for i in range(1, work.nx - 1):
        for j in range(1, work.ny - 1):
            if work.bin[i, j] == 0:
                if work.bin[i + 1, j] > 0 and work.bin[i - 1, j] > 0 and work.bin[i, j + 1] > 0 and work.bin[i, j - 1] > 0:
                    work.bin[i, j] += 1
                    viz.add_marker(work.get_pos_from_index((i, j)), color=[0.8, 0.8, 0.0, 1.0])

    bins_reached = 0
    for pos, num in work:
        if num > 0 and not np.isnan(num):
            bins_reached += 1
        elif num == 0 and np.linalg.norm(pos) < arm.max_reach + np.linalg.norm(np.array([work.dx / 2, work.dy / 2])):
            viz.add_marker(np.array([pos[0], pos[1], 0.05]), color=[1, 0, 0, 1])

    print(f"Method: {method}")
    print(f"Total Samples: {samples_total}\tSamples in Space: {samples_in_space}\tIn-Space Ratio: {samples_in_space / samples_total}")
    print(f"Novel Samples: {samples_novel}\tNovel Ratio: {samples_novel / samples_total}")
    print(f"Bins Reached: {bins_reached}\tTotal Bins Reachable: {bins_max_reachable}\tReached Ratio: {bins_reached / bins_max_reachable}")
    viz.hold()

'''
***** Heuristic *****
Total Samples: 904	Samples in Space: 689	In-Space Ratio: 0.7621681415929203
Novel Samples: 176	Novel Ratio: 0.19469026548672566
Bins Reached: 183	Total Bins Reachable: 189	Reached Ratio: 0.9682539682539683

Total Samples: 982	Samples in Space: 725	In-Space Ratio: 0.7382892057026477
Novel Samples: 185	Novel Ratio: 0.18839103869653767
Bins Reached: 189	Total Bins Reachable: 189	Reached Ratio: 1.0

Total Samples: 798	Samples in Space: 662	In-Space Ratio: 0.8295739348370927
Novel Samples: 170	Novel Ratio: 0.21303258145363407
Bins Reached: 177	Total Bins Reachable: 189	Reached Ratio: 0.9365079365079365

***** Brute Force *****
Total Samples: 2286	Samples in Space: 360	In-Space Ratio: 0.15748031496062992
Novel Samples: 152	Novel Ratio: 0.06649168853893263
Bins Reached: 164	Total Bins Reachable: 209	Reached Ratio: 0.784688995215311

Total Samples: 1070	Samples in Space: 156	In-Space Ratio: 0.14579439252336449
Novel Samples: 101	Novel Ratio: 0.09439252336448598
Bins Reached: 107	Total Bins Reachable: 189	Reached Ratio: 0.5661375661375662

Total Samples: 1946	Samples in Space: 299	In-Space Ratio: 0.15364850976361769
Novel Samples: 145	Novel Ratio: 0.07451181911613566
Bins Reached: 152	Total Bins Reachable: 189	Reached Ratio: 0.8042328042328042


H
Total Samples: 773	Samples in Space: 341	In-Space Ratio: 0.4411384217335058
Novel Samples: 56	Novel Ratio: 0.0724450194049159
Bins Reached: 57	Total Bins Reachable: 79	Reached Ratio: 0.7215189873417721

B
Total Samples: 1086	Samples in Space: 231	In-Space Ratio: 0.212707182320442
Novel Samples: 56	Novel Ratio: 0.05156537753222836
Bins Reached: 57	Total Bins Reachable: 79	Reached Ratio: 0.7215189873417721
'''