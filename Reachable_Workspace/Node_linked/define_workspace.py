import RoboPy as rp
import pyqtgraph as pg
import pyqtgraph.opengl as gl
import numpy as np


dh = [[0, 0, 1, 0],
      [0, 0, 0.5, 0],
      [0, 0, 0.5, 0]]
n = len(dh)
arm = rp.SerialArm(dh)
viz = rp.VizScene()
viz.add_arm(arm)
line_coord = np.array([[0.5, 0.5, 0],
                       [1.5, 0.5, 0],
                       [1.5, 1.5, 0],
                       [0.5, 1.5, 0],
                       [0.5, 0.5, 0]])
line = gl.GLLinePlotItem(pos=line_coord)
viz.window.addItem(line)
viz.app.processEvents()

b = [[line_coord[i], line_coord[i + 1]] for i in range(len(line_coord) - 1)]


def check_in_bound(x, bounds):
    inside = True
    zhat = np.array([0, 0, 1])
    for p0, p1 in bounds:
        v = p1 - p0
        n = np.cross(zhat, v)
        w = x - p0
        if n @ w < 0:
            inside = False
            break
    return inside


dq = 0.5

for i in range(100):
    q = np.random.random_sample((n,)) * 2 * np.pi - np.pi
    x = arm.fk(q, rep='cart')
    try_list = [q]
    while len(try_list) > 0:
        print(len(try_list))
        qc = try_list[-1]
        try_list.pop()
        x = arm.fk(qc, rep='cart')
        if check_in_bound(x, b):
            viz.update(qs=q)
            viz.add_marker(x, color=[0, 1, 0, 0.5])
            try_list.append(qc + np.random.random_sample((n,)) * 2 * dq - dq)
            if len(try_list) < 5:
                try_list.append(qc + np.random.random_sample((n,)) * dq - dq / 2)
        else:
            viz.update(qs=q)
            viz.add_marker(x, color=[1, 0, 0, 0.5])


print('done')
viz.hold()
