"""

Repeat what was done in task_space_bin, but now in R3 x SO3. Need to define a task space in 3D with orientation
constraints (maybe angle between x unit vector and reference angle?) and implement brute force search, joint space node
search, and Jacobian smart search.

Classes:
- TaskSpace class (define task space in bins, allow for lookup of indexed location, etc)
- WorkspaceSearch class (store information and encapsulate the functions needed for searching)

"""

import pyqtgraph as pg
from pyqtgraph import opengl as gl
from pyqtgraph import QtGui, QtCore

from RoboPy import rotx, roty, rotz

import numpy as np

app = QtGui.QApplication([])
w = gl.GLViewWidget()
w.resize(800, 800)
w.show()

sample = []


def normalized_cube_surface(v, m):
    q = np.asarray(v)
    q = (q / (m - 1)) * 2 - 1
    for i in range(len(q)):
        q[i] = (np.abs(q[i]))**1.1 * np.sign(q[i])
    q = q / np.linalg.norm(q)
    return q


def cube_to_sphere(v):
    q = v / np.linalg.norm(v)
    return q


def sphere_to_cube(v):
    q = v / np.max(np.abs(v))
    return q


m = 20

for i in range(m):
    if i == 0 or i == m - 1:
        for j in range(m):
            for k in range(m):
                sample.append([i, j, k])
    else:
        for j in range(m):
            if j == 0 or j == m - 1:
                for k in range(m):
                    sample.append([i, j, k])
            else:
                sample.append([i, j, 0])
                sample.append([i, j, m - 1])

array = np.asarray(sample)

points = np.asarray([normalized_cube_surface(v, m) for v in array])
c1 = ([1, 0, 0, 0.5], [0, 1, 0, 0.5], [0, 0, 1, 0.5])
c2 = ([1, 0, 0, 0.5], [0, 1, 1, 0.5], [0, 1, 0, 0.5], [1, 0, 1, 0.5], [0, 0, 1, 0.5], [1, 1, 0, 0.5])
# colors = (1, 1, 1, 0.25)
# colors = np.asarray([[(y[0] + 1) / 2 + 0.1, (y[1] + 1) / 2, (y[2] + 1) / 2, 0.5] for y in points])
colors = np.asarray([c1[np.argmax(np.abs(y))] for y in points])
# colors = np.asarray([c2[int(y[0])] for y in array])

scatter = gl.GLScatterPlotItem(pos=points, color=colors)
w.addItem(scatter)
app.processEvents()
app.exec()
