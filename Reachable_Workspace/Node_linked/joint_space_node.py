import RoboPy as rp
import numpy as np
from numpy.linalg import norm
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui
import time


class Node:
    def __init__(self, pos, parent=None):
        self.pos = pos
        self.parent = parent
        self.children = []

    def closest_children(self, pos):
        l = self.children.copy()
        l.append(self)
        l.sort(key=lambda node: norm(pos - node.pos))
        if l[0] is self:
            return self, True
        while l.pop() is not self:
            pass
        return l, False


class Tree:
    def __init__(self, pos):
        self.root = Node(pos)
        self.list = [self.root]
        self.iter_index = 0

    def __iter__(self):
        self.iter_index = 0
        return self

    def __next__(self):
        node = self.list[self.iter_index]
        self.iter_index += 1
        return node

    def nearest(self, pos):
        search_list = [self.root]
        candidate_list = []
        while len(search_list) > 0:
            node = search_list.pop(0)
            out, test = node.closest_children(pos)
            if test:
                candidate_list.append(out)
            else:
                search_list += out
        candidate_list.sort(key=lambda n: norm(pos - n.pos))
        return candidate_list[0]

    def branch(self, pos):
        stem = self.nearest(pos)
        bud = Node(pos, stem)
        self.list.append(bud)
        stem.children.append(bud)
        return bud


if __name__ == '__main__':

    arm = rp.PlanarDyn(n=2, L=1.0)
    from task_space_bin import Workspace2D

