import numpy as np
from numpy import pi
from numpy.linalg import norm
import RoboPy as rp
import pyqtgraph as pg
from PyQt5.QtWidgets import QApplication
import matplotlib.pyplot as plt

blue = lambda a: np.array([0.2, 0.2, 0.8, a])
green = lambda a : np.array([0.2, 0.8, 0.2, a])
red = lambda a: np.array([0.8, 0.2, 0.2, a])
black = lambda a: np.array([0, 0, 0, a])
purple = lambda a: np.array([0.6, 0.1, 0.8, a])
yellow = lambda a: np.array([0.8, 0.6, 0.1, a])

fail = [purple, red, purple, red]

n = 2
arm = rp.PlanarDyn(n=n, L=1)
joint_ub = np.asarray([pi] * n)
joint_lb = np.asarray([-pi] * n)
tau_lim = np.array([5, 5])
Wext = np.array([2, 5, 0])
gravity = np.array([0, 0, 0])

dq = 0.1
p = ((joint_ub - joint_lb) / dq).astype(int)


def index_2_q(*args):
    q = np.zeros((n,))
    for i in range(n):
        q[i] = args[i] / p[i] * (joint_ub[i] - joint_lb[i]) + joint_lb[i]
    return q


def get_work(q0, q1):
    return tau_lim @ np.abs(q0 - q1)


def get_potential(q0, q1):
    x0 = arm.fk(q0)[0:3, 3]
    x1 = arm.fk(q1)[0:3, 3]
    Pext = Wext @ (x0 - x1)
    Pgrav = arm.get_PE(q0, g=gravity) - arm.get_PE(q1, g=gravity)
    return np.abs(Pext + Pgrav)


def get_energy(q0, q1):
    return get_work(q0, q1) - get_potential(q0, q1)


def get_applied_torque(q0, q1):
    qm = (q0 + q1) / 2
    g = arm.get_G(qm, gravity)
    J = arm.jacob(qm)[0:3, :]
    w = -J.T @ Wext
    tau = g + w
    return tau


def check_torque(q0, q1):
    tau = np.abs(get_applied_torque(q0, q1))
    T = tau_lim - tau
    if np.min(T) < 0:
        return -1
    else:
        return 1


energy_array = np.full(np.append(4, p), 0.0)

next_cell_list = []
xpos = np.zeros(p)
ypos = np.zeros(p)

index = (p / 2).astype(int).tolist()
next_cell_list.append(index)

for i in range(0, p[0]):
    for j in range(0, p[1]):
        qc = index_2_q(i, j)
        xpos[i, j] = qc[0]
        ypos[i, j] = qc[1]

        q0 = index_2_q(i, j + 1)
        q1 = index_2_q(i + 1, j)
        q2 = index_2_q(i, j - 1)
        q3 = index_2_q(i - 1, j)

        # energy_array[0, i, j] = get_energy(qc, q0)
        # energy_array[1, i, j] = get_energy(qc, q1)
        # energy_array[2, i, j] = get_energy(qc, q2)
        # energy_array[3, i, j] = get_energy(qc, q3)

        energy_array[0, i, j] = check_torque(qc, q0)
        energy_array[1, i, j] = check_torque(qc, q1)
        energy_array[2, i, j] = check_torque(qc, q2)
        energy_array[3, i, j] = check_torque(qc, q3)

energy_map = np.ravel(np.mean(energy_array, axis=0))
m = energy_map.shape[0]
energy_map_dir = np.vstack([np.ravel(ar) for ar in energy_array])
xpos = np.ravel(xpos)
ypos = np.ravel(ypos)

colors = np.zeros((m, 4))
colors_dir = np.zeros((4, m, 4))
for i in range(m):
    E = energy_map[i]
    if E > 0:
        colors[i] = yellow(0.7)
    else:
        colors[i] = purple(0.7)

    for j in range(4):
        Edir = energy_map_dir[j, i]
        if Edir > 0:
            colors_dir[j, i] = yellow(0.3)
        else:
            colors_dir[j, i] = purple(0.3)

fig, ax = plt.subplots()
size = 80000
ax.scatter(xpos, ypos, c=colors, s=size / m)
ax.scatter(xpos, ypos + dq / 4, c=colors_dir[0], s=size / m)
ax.scatter(xpos + dq / 4, ypos, c=colors_dir[1], s=size / m)
ax.scatter(xpos, ypos - dq / 4, c=colors_dir[2], s=size / m)
ax.scatter(xpos - dq / 4, ypos, c=colors_dir[3], s=size / m)
ax.axis('equal')

fig2, ax2 = plt.subplots()
pos = np.asarray([arm.fk([q0, q1])[0:2, 3] for q0, q1 in zip(xpos, ypos)])
pos0 = np.asarray([arm.fk([q0, q1 + dq / 4])[0:2, 3] for q0, q1 in zip(xpos, ypos)])
pos1 = np.asarray([arm.fk([q0 + dq / 4, q1])[0:2, 3] for q0, q1 in zip(xpos, ypos)])
pos2 = np.asarray([arm.fk([q0, q1 - dq / 4])[0:2, 3] for q0, q1 in zip(xpos, ypos)])
pos3 = np.asarray([arm.fk([q0 - dq / 4, q1])[0:2, 3] for q0, q1 in zip(xpos, ypos)])
ax2.scatter(pos[:, 0], pos[:, 1], c=colors, s=size / m)
ax2.scatter(pos0[:, 0], pos0[:, 1], c=colors_dir[0], s=size / m)
ax2.scatter(pos1[:, 0], pos1[:, 1], c=colors_dir[1], s=size / m)
ax2.scatter(pos2[:, 0], pos2[:, 1], c=colors_dir[2], s=size / m)
ax2.scatter(pos3[:, 0], pos3[:, 1], c=colors_dir[3], s=size / m)
ax2.axis('equal')
plt.show()

'''
green node connection is a necessary but not sufficient test for feasible path? Because there are situations where despite
being energetically possible, the specific torque on a small joint may be too much for movement?

1) Connect nodes by actually calculating torque, not just energy (for now)
2) If there's a torque limit, you can find the reachable nodes
3) With no torque limit, you could find the maximum external force for each node connection
4) Could find maximum non-accelerating joint speed at each node by finding excess torque and energy
5) Maybe start looking at cartesion workspace stuff.
'''