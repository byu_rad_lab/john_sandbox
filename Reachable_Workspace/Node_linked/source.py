import numpy as np
from numpy.linalg import norm
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui
from pyqtgraph import opengl as gl
import time


class Workspace2D:
    def __init__(self, points=np.array([]), grid_spacing=np.ones((3,))*0.1):
        self.points = np.asarray(points)
        self.n = len(points)
        self.dx = grid_spacing[0]
        self.dy = grid_spacing[1]
        self.lines = np.asarray([points[i] - points[i - 1] for i in range(self.n)])
        self.xmax = np.max(points[:, 0])
        self.xmin = np.min(points[:, 0])
        self.ymax = np.max(points[:, 1])
        self.ymin = np.min(points[:, 1])

        self.nx = int((self.xmax - self.xmin) / self.dx)
        self.ny = int((self.ymax - self.ymin) / self.dy)

        self.dx = (self.xmax - self.xmin) / self.nx
        self.dy = (self.ymax - self.ymin) / self.ny
        self.dz = grid_spacing[2]

        self.bin = np.empty((self.nx, self.ny))
        for i in range(self.nx):
            for j in range(self.ny):
                pos = self.get_pos_from_index((i, j))
                if self.check_in_bounds(pos):
                    self.bin[i, j] = 0
                else:
                    self.bin[i, j] = np.NaN

        self.LinePlotItem = None
        self.i = 0
        self.j = 0

    def get_pos_from_index(self, ij):
        i, j = ij
        x = self.xmin + (i + 0.5) * self.dx
        y = self.ymin + (j + 0.5) * self.dy
        return np.array([x, y])

    def check_in_bounds(self, pos):
        if not(self.xmin <= pos[0] <= self.xmax and self.ymin <= pos[1] <= self.ymax):
            return False
        for v, p in zip(self.lines, self.points):
            n = np.array([v[1], -v[0]])
            w = pos[0:2] - p
            if w @ n > 0:
                return False
        return True

    def get_bin_index(self, pos):
        x = pos[0] - self.xmin
        y = pos[1] - self.ymin
        i = np.trunc(x / self.dx) if x >= 0 else np.trunc(x / self.dx - 1)
        j = np.trunc(y / self.dy) if y >= 0 else np.trunc(y / self.dy - 1)
        return int(i), int(j)

    def check_in_bin(self, pos):
        i, j = self.get_bin_index(pos)
        if 0 <= i < self.nx and 0 <= j < self.ny:
            return True
        return False

    def put_in_bin(self, pos):
        if len(pos) == 3:
            if np.abs(pos[2]) > self.dz / 2:
                return False, -1, -1
        i, j = self.get_bin_index(pos)
        if 0 <= i < self.nx and 0 <= j < self.ny:
            if not np.isnan(self.bin[i, j]):
                self.bin[i, j] += 1.0
                return True, i, j
        return False, -1, -1

    def add_to_scene(self, viz):
        self.LinePlotItem = gl.GLLinePlotItem(pos=np.vstack([self.points, self.points[0]]))
        viz.window.addItem(self.LinePlotItem)
        pos = np.array([[self.get_pos_from_index((i, j)) for j in range(self.ny)] for i in range(self.nx)])
        color = [[[0.8, 0.8, 0.8, 0.5] if not np.isnan(self.bin[i, j]) else [0.2, 0.2, 0.2, 0.5] for i in range(self.nx)] for j in range(self.ny)]
        viz.add_marker(pos, color)
        # for i in range(self.nx):
        #     for j in range(self.ny):
        #         pos = self.get_pos_from_index((i, j))
        #         if not np.isnan(self.bin[i, j]):
        #             viz.add_marker(pos, [0.8, 0.8, 0.8, 0.5])
        #         else:
        #             viz.add_marker(pos, [0.2, 0.2, 0.2, 0.5])
        viz.app.processEvents()

    def __iter__(self):
        self.i = 0
        self.j = 0
        return self

    def __next__(self):
        pos = self.get_pos_from_index((self.i, self.j))
        num = self.bin[self.i, self.j]
        if self.i + 1 < self.nx:
            self.i += 1
        elif self.j + 1 < self.ny:
            self.i = 0
            self.j += 1
        else:
            raise StopIteration
        return pos, num


class Jointspace:
    def __init__(self, qmin, qmax, grid_spacing=None):
        self.n = qmin.shape[0]
        self.qmin = qmin
        self.qmax = qmax
        if grid_spacing is None:
            grid_spacing = np.ones((self.n,)) * 0.1

        self.shape = []
        self.grid_spacing = np.zeros((self.n,))
        for i in range(self.n):
            dq = qmax[i] - qmin[i]
            num = int(np.round(dq / grid_spacing[i]))
            self.shape.append(num)
            self.grid_spacing[i] = dq / num

        self.space = np.zeros(self.shape)
        self.iter_index = np.zeros((self.n,), dtype=int)

    def get_bin_index(self, q):
        index = []
        check = True
        for a, lim, dx, n in zip(q, self.qmin, self.grid_spacing, self.shape):
            b = a - lim
            i = np.trunc(b / dx) if b >= 0 else np.trunc(b / dx - 1)
            if i >= n:
                i = n - 1
            index.append(int(i))
            if not 0 <= i < n:
                check = False
        return tuple(index), check

    def get_angle_from_index(self, index):
        q = np.zeros((self.n,))
        for i in range(self.n):
            q[i] = index[i] * self.grid_spacing[i]
        return q

    def get_rand_index(self):
        index = []
        for i in range(self.n):
            index.append(np.random.randint(self.shape[i]))
        return tuple(index)

    def __iter__(self):
        self.iter_index = np.zeros((self.n,), dtype=int)
        return self

    def __next__(self):
        q = self.get_angle_from_index(self.iter_index)
        num = self.space[self.iter_index]
        for i in range(self.n):
            self.iter_index[i] += 1
            if self.iter_index[i] > self.shape[i] - 1:
                if i == self.n:
                    raise StopIteration
                self.iter_index[i] = 0
            else:
                break
        return q, num


class Node:
    def __init__(self, pos, parent=None):
        self.pos = pos
        self.parent = parent
        self.children = []

    def closest_children(self, pos):
        l = self.children.copy()
        l.append(self)
        l.sort(key=lambda node: norm(pos - node.pos))
        if l[0] is self:
            return self, True
        while l.pop() is not self:
            pass
        return l, False


class Tree:
    def __init__(self, pos):
        self.root = Node(pos)
        self.list = [self.root]
        self.iter_index = 0

    def __iter__(self):
        self.iter_index = 0
        return self

    def __next__(self):
        node = self.list[self.iter_index]
        self.iter_index += 1
        return node

    def nearest(self, pos):
        search_list = [self.root]
        candidate_list = []
        while len(search_list) > 0:
            node = search_list.pop(0)
            out, test = node.closest_children(pos)
            if test:
                candidate_list.append(out)
            else:
                search_list += out
        candidate_list.sort(key=lambda n: norm(pos - n.pos))
        return candidate_list[0]

    def branch(self, pos):
        stem = self.nearest(pos)
        bud = Node(pos, stem)
        self.list.append(bud)
        stem.children.append(bud)
        return bud


def task_search(arm, taskspace, max_torque, gravity=np.zeros((3,)), external_load=np.zeros((6,)), viz=None):

    def get_applied_torque(q, J):
        g = arm.get_G(q, gravity)
        w = -J.T @ external_load
        tau = g + w
        return tau

    def check_torque(q, J):
        tau = np.abs(get_applied_torque(q, J))
        T = max_torque - tau
        if np.min(T) < 0:
            return False
        else:
            return True

    MAX_COUNT = 500
    MAX_SAMPLE = np.inf
    INI_SAMPLE = 100
    DRAW_FULL = False
    DRAW_NEW = True

    n = arm.n
    tree = Tree(np.zeros((n,)))
    dq = 0.2
    H = np.vstack([np.eye(n), -np.eye(n)])

    count = 0
    total_count = 0

    qlist = []

    while count < INI_SAMPLE:
        qrand = np.random.random((n,)) * 2 * np.pi - np.pi
        xrand = arm.fk(qrand, rep='cart')
        if taskspace.check_in_bounds(xrand):
            count += 1
            torque_feasible = False
            for v in H:
                qtest = dq * v / 2 + qrand
                Jtest = arm.jacob(qtest)
                if check_torque(qtest, Jtest):
                    torque_feasible = True
                    break
            if torque_feasible:
                qlist.append((qrand, np.zeros((n,))))

            if DRAW_FULL:
                if torque_feasible:
                    viz.add_marker(xrand, color=[1, 1, 0, 1])
                else:
                    viz.add_marker(xrand, color=[1, 0, 1, 1])
        else:
            if DRAW_FULL:
                viz.add_marker(xrand, color=[1, 0, 0, 1])

    count = 0

    while count < MAX_COUNT and total_count < MAX_SAMPLE and len(qlist) > 0:
        count += 1
        total_count += 1
        print(count, len(qlist))

        qc, z = qlist.pop(np.random.randint(0, len(qlist)))
        # qc, z = qlist.pop()
        tree.branch(qc)
        xc = arm.fk(qc, rep='cart')
        check, i, j = taskspace.put_in_bin(xc)
        if check:
            if taskspace.bin[i, j] == 1:
                count = 0
                if DRAW_NEW:
                    viz.add_marker(taskspace.get_pos_from_index((i, j)), color=[0, 1, 0, 1])
            if taskspace.bin[i, j] < 15:
                for v in H:
                    if norm(z - v) == 0:
                        pass
                    else:
                        qtest = qc + v * dq + (np.random.random((n,)) * 2 - 1) * dq / 2
                        xtest = arm.fk(qtest, rep='cart')
                        Jtest = arm.jacob(qtest)
                        torque_feasible = check_torque(qtest, Jtest)
                        distance_feasible = norm(tree.nearest(qtest).pos - qtest) < dq * 1.5
                        in_space = taskspace.check_in_bounds(xtest)
                        if torque_feasible and distance_feasible and in_space:
                            qlist.append((qtest, v))
                        if DRAW_FULL:
                            viz.update(qs=qtest)
                            if torque_feasible and distance_feasible and in_space:
                                viz.add_marker(xtest, color=[0.5, 0.5, 0, 0.5])
                            elif not in_space:
                                viz.add_marker(xtest, color=[1, 0, 0, 1])  # bounds check
                            elif not torque_feasible:
                                viz.add_marker(xtest, color=[0.5, 0, 0.5, 0.5])  # torque check
                            else:
                                viz.add_marker(xtest, color=[0, 0.5, 0.5, 0.5])  # distance check
            else:
                print('check')

    print(taskspace.bin)
    print(total_count)
    return taskspace


if __name__ == '__main__':

    import RoboPy as rp

    n = 3
    arm = rp.PlanarDyn(n=n, L=2/n)
    max_torque = np.array([10, 5, 2])
    gravity = np.array([10, 0, 0])
    external_load = np.array([0, 0, 0, 0, 0, 5])

    grid_spacing = np.ones((3,)) * 0.2
    viz = rp.VizScene()
    viz.add_arm(arm)

    points = np.array([[-2.0, -1.0],
                       [0.0, -1.0],
                       [0.0, 1.0],
                       [-2.0, 1.0]])

    taskspace = Workspace2D(points, grid_spacing)
    # taskspace.add_to_scene(viz)
    task_search(arm, taskspace, max_torque, gravity, external_load, viz)

    def get_applied_torque(q, J):
        g = arm.get_G(q, gravity)
        w = -J.T @ external_load
        tau = g + w
        return tau

    def check_torque(q, J):
        tau = np.abs(get_applied_torque(q, J))
        T = max_torque - tau
        if np.min(T) < 0:
            return False
        else:
            return True

    p = 10000
    qs = np.random.random((p, n)) * 2 * np.pi - np.pi
    offset = np.array([0, -2, 0])
    for q in qs:
        x = arm.fk(q, rep='cart')
        J = arm.jacob(q)
        if taskspace.check_in_bounds(x) and check_torque(q, J):
            viz.add_marker(x + offset, color=[0, 1, 0, 1])

    viz.hold()

