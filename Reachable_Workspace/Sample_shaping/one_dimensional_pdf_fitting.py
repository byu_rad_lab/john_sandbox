import numpy as np
import numpy.random as ran
import matplotlib.pyplot as plt
import scipy.stats as stats


def smoothification_algorithm(x0, y0, n):
    xn = np.linspace(x0[0], x0[-1], n)
    yn = np.zeros(n)
    yn[0] = y0[0]

    counter = 1
    counter_last = 0

    for i in range(1, n):
        while x0[counter] < xn[i]:
            counter += 1
            counter_last += 1
        # yn[i] = 1 / 3 * (y0[counter] + y0[counter - 1] + y0[counter - 2])
        yn[i] = y0[counter]
        # yn[i] = np.sum(y0[counter - counter_last:counter]) / (counter_last + 1)

    return xn, yn


n = 10000
p = 0.5
samples = np.asarray([ran.normal(-5.0, 1.0) if ran.random() > p else ran.normal(5.0, 1.0) for i in range(n)])
samples.sort()
sample_bin_frequency, sample_histogram_bins = np.histogram(samples, bins=1000, density=True)
sample_histogram_bins = sample_histogram_bins[0:-1]

xs = np.linspace(samples[0], samples[-1], n)
cpf_from_distribution = stats.norm.cdf(xs, loc=-5.0, scale=1.0) * (1 - p) + stats.norm.cdf(xs, loc=5.0, scale=1.0) * p
pdf_from_distribution = stats.norm.pdf(xs, loc=-5.0, scale=1.0) * (1 - p) + stats.norm.pdf(xs, loc=5.0, scale=1.0) * p
cpf_from_data = np.asarray([i / n for i in range(n)])
k = 2**6
regularized_samples, interpolated_cpf = smoothification_algorithm(samples, cpf_from_data, k)
interpolated_pdf = np.asarray([(interpolated_cpf[i+1] - interpolated_cpf[i]) / (regularized_samples[i+1] - regularized_samples[i]) for i in range(k - 1)])
interpolated_pdf = np.append(interpolated_pdf, 0.0)

polynomial_coefficients = np.polyfit(regularized_samples, interpolated_cpf, deg=15)
# polynomial_coefficients = np.polyfit(samples, cpf_from_data, deg=20)
polynomial_approximated_cpf = np.polyval(polynomial_coefficients, regularized_samples)
differentiate_coefficients = [c * (len(polynomial_coefficients) - i - 1) for i, c in enumerate(polynomial_coefficients[0:-1])]
polynomial_approximated_pdf = np.polyval(differentiate_coefficients, regularized_samples)


fig, ax = plt.subplots()
legend = []
ax.plot(xs, cpf_from_distribution)
legend.append('Actual CDF')
ax.plot(xs, pdf_from_distribution)
legend.append('Actual PDF')
# ax.plot(samples, cpf_from_data)
# legend.append('Sampled CDF')
# ax.plot(regularized_samples, interpolated_cpf)
# legend.append('Interp CDF')
# ax.plot(regularized_samples, interpolated_pdf)
# legend.append('Interp Num. PDF')
ax.plot(regularized_samples, polynomial_approximated_cpf)
legend.append('Poly CDF')
ax.plot(regularized_samples, polynomial_approximated_pdf)
legend.append('Poly PDF')
# ax.bar(sample_histogram_bins, sample_bin_frequency, align='edge')
# legend.append('Binned Data')
ax.legend(legend)
plt.show()
