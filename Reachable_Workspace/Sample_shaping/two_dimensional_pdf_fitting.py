import numpy as np
import scipy.stats as stats
import matplotlib.pyplot as plt
import pyqtgraph.opengl as gl
import pyqtgraph as pg
from scipy.optimize import curve_fit


# get initial samples
n = 2
p = 10000
mu1 = np.array([-2, 2])
mu2 = np.array([2, -2])
cov = np.eye(n)
d1 = stats.multivariate_normal(mean=mu1, cov=cov)
d2 = stats.multivariate_normal(mean=mu2, cov=cov)
samples = np.vstack([d1.rvs((int(p / 2),)), d2.rvs(int(p / 2))])

# discretize and bin samples
k = 50
xmin, xmax = np.min(samples[:, 0]) - 2, np.max(samples[:, 0]) + 2
ymin, ymax = np.min(samples[:, 1]) - 2, np.max(samples[:, 1]) + 2
xgrid = np.linspace(xmin, xmax, k)
ygrid = np.linspace(ymin, ymax, k)
delta_x = (xmax - xmin) / k
delta_y = (ymax - ymin) / k

def xy_2_index(xy):
    ix = min(int((xy[0] - xmin) / (xmax - xmin) * k), k - 1)
    iy = min(int((xy[1] - ymin) / (ymax - ymin) * k), k - 1)
    return (ix, iy)

def index_2_xy(ij):
    x = (ij[0] + 0.5) * (xmax - xmin) / k + xmin
    y = (ij[1] + 0.5) * (ymax - ymin) / k + ymin
    return (x, y)

gridcells = np.zeros((k, k), dtype=int)
for s in samples:
    gridcells[xy_2_index(s)] += 1

cdf_x = np.zeros((k,))
cdf_y = np.zeros((k,))

for i in range(k):
    cdf_x[i] = np.sum(gridcells[0:i + 1])
    cdf_y[i] = np.sum(gridcells[:, 0:i + 1])

cdf_x = cdf_x / p
cdf_y = cdf_y / p

Fxy = np.zeros((k, k))
for i in range(k):
    for j in range(k):
        Fxy[i, j] = np.sum(gridcells[0:i, 0:j]) / p

true_data = np.zeros((k, k, 4))
for i in range(k):
    for j in range(k):
        xy = index_2_xy((i, j))
        true_data[i, j, :] = [xy[0], xy[1], d1.cdf(xy) / 2 + d2.cdf(xy) / 2, d1.pdf(xy) / 2 + d2.pdf(xy) / 2]

# finite difference fit
approx_pdf = np.zeros((k, k, 5))
for i in range(k):
    for j in range(k):
        xy = index_2_xy((i, j))
        if i == 0:
            dx = (Fxy[i + 1, j] - Fxy[i, j]) / delta_x
        elif i == k - 1:
            dx = (Fxy[i, j] - Fxy[i - 1, j]) / delta_x
        else:
            dx = (Fxy[i + 1, j] - Fxy[i - 1, j]) / (2 * delta_x)

        if j == 0:
            dy = (Fxy[i, j + 1] - Fxy[i, j]) / delta_y
        elif j == k - 1:
            dy = (Fxy[i, j] - Fxy[i, j - 1]) / delta_y
        else:
            dy = (Fxy[i, j + 1] - Fxy[i, j - 1]) / (2 * delta_y)

        approx_pdf[i, j] = [xy[0], xy[1], dx, dy, 0]

for i in range(k):
    for j in range(k):
        if j == 0:
            dxdy = (approx_pdf[i, j + 1, 2] - approx_pdf[i, j, 2]) / delta_y
        elif j == k - 1:
            dxdy = (approx_pdf[i, j, 2] - approx_pdf[i, j - 1, 2]) / delta_y
        else:
            dxdy = (approx_pdf[i, j + 1, 2] - approx_pdf[i, j - 1, 2]) / (2 * delta_y)

        approx_pdf[i, j, 4] = dxdy

approx_pdf[:, :, 4] = approx_pdf[:, :, 4] / (np.sum(approx_pdf[:, :, 4]) * delta_x * delta_y)

cdf_data = np.reshape([[np.append(index_2_xy((i, j)), Fxy[i, j]) for j in range(k)] for i in range(k)], (k * k, 3))

def f_poly(xy, *popt):
    x = xy[:, 0]
    y = xy[:, 1]
    output = popt[0]
    m = len(popt)
    n = int(m / 3)
    x_coef = popt[1:n + 1]
    y_coef = popt[n + 1:2*n + 1]
    xy_coef = popt[2*n + 1: 3*n]
    for i in range(0, n):
        output += x_coef[i] * x**(i + 1)
        output += y_coef[i] * y**(i + 1)
    for i in range(0, n - 1):
        output += xy_coef[i] * (x**(i + 1) * y**(n - 1 - i))
    return output

def f_sin(xy, *popt):
    x = xy[:, 0]
    y = xy[:, 1]
    output = popt[0]
    m = len(popt)
    n = int(m / 3)
    x_coef = popt[1:n + 1]
    y_coef = popt[n + 1:2*n + 1]
    xy_coef = popt[2*n + 1: 3*n]
    for i in range(0, n):
        output += x_coef[i] * np.sin(x * (i + 1))
        output += y_coef[i] * np.sin(y * (i + 1))
    for i in range(0, n - 1):
        output += xy_coef[i] * np.sin(x * y * (i + 1))
    return output

order = 5
m = order * 3

popt, pcov = curve_fit(f_sin, cdf_data[:, 0:2], cdf_data[:, 2], p0=[1] * m)

def ffit_poly(xy):
    x = xy[0]
    y = xy[1]
    output = popt[0]
    m = len(popt)
    n = int(m / 3)
    x_coef = popt[1:n + 1]
    y_coef = popt[n + 1:2 * n + 1]
    xy_coef = popt[2 * n + 1: 3 * n]
    for i in range(0, n):
        output += x_coef[i] * x ** (i + 1)
        output += y_coef[i] * y ** (i + 1)
    for i in range(0, n - 1):
        output += xy_coef[i] * (x ** (i + 1) * y ** (n - 1 - i))
    return output

def dfdx_poly(xy):
    x = xy[0]
    y = xy[1]
    output = 0
    m = len(popt)
    n = int(m / 3)
    x_coef = popt[1:n + 1]
    y_coef = popt[n + 1:2 * n + 1]
    xy_coef = popt[2 * n + 1: 3 * n]
    for i in range(0, n):
        output += (i + 1) * x_coef[i] * x ** (i)
    for i in range(0, n - 1):
        output += xy_coef[i] * ((i + 1) * x ** (i) * y ** (n - 1 - i))
    return output

def dfdy_poly(xy):
    x = xy[0]
    y = xy[1]
    output = 0
    m = len(popt)
    n = int(m / 3)
    x_coef = popt[1:n + 1]
    y_coef = popt[n + 1:2 * n + 1]
    xy_coef = popt[2 * n + 1: 3 * n]
    for i in range(0, n):
        output += (i + 1) * y_coef[i] * y ** (i)
    for i in range(0, n - 1):
        output += xy_coef[i] * (x ** (i + 1) * (n - 2 - i) * y ** (n - 2 - i))
    return output

def dfdxdy_poly(xy):
    x = xy[0]
    y = xy[1]
    output = popt[0]
    m = len(popt)
    n = int(m / 3)
    x_coef = popt[1:n + 1]
    y_coef = popt[n + 1:2 * n + 1]
    xy_coef = popt[2 * n + 1: 3 * n]
    for i in range(0, n - 1):
        output += xy_coef[i] * ((i + 1) * x ** (i) * (n - 2 - i) * y ** (n - 2 - i))
    return output

def ffit_sin(xy):
    x = xy[0]
    y = xy[1]
    w = 0.1
    output = popt[0]
    m = len(popt)
    n = int(m / 3)
    x_coef = popt[1:n + 1]
    y_coef = popt[n + 1:2 * n + 1]
    xy_coef = popt[2 * n + 1: 3 * n]
    for i in range(0, n):
        output += x_coef[i] * np.sin(x * (i + 1) * w)
        output += y_coef[i] * np.sin(y * (i + 1) * w)
    for i in range(0, n - 1):
        output += xy_coef[i] * np.sin(x * y * (i + 1) * w)
    return output

def dfdx_sin(xy):
    x = xy[0]
    y = xy[1]
    w = 0.1
    output = 0
    m = len(popt)
    n = int(m / 3)
    x_coef = popt[1:n + 1]
    y_coef = popt[n + 1:2 * n + 1]
    xy_coef = popt[2 * n + 1: 3 * n]
    for i in range(0, n):
        output += w * (i + 1) * x_coef[i] * np.cos(x * (i + 1) * w)
    for i in range(0, n - 1):
        output += w * y * (i + 1) * xy_coef[i] * np.cos(x * y * (i + 1) * w)
    return output

def dfdxdy_sin(xy):
    x = xy[0]
    y = xy[1]
    w = 0.1
    output = 0
    m = len(popt)
    n = int(m / 3)
    x_coef = popt[1:n + 1]
    y_coef = popt[n + 1:2 * n + 1]
    xy_coef = popt[2 * n + 1: 3 * n]
    for i in range(0, n - 1):
        output += w * (i + 1) * (y * -x * w * (i + 1) * np.sin(x * y * (i + 1) * w) + np.cos(x * y * (i + 1) * w))
    return output

recon_data = np.zeros((k, k, 4))
for i in range(k):
    for j in range(k):
        xy = index_2_xy((i, j))
        recon_data[i, j, :] = [xy[0], xy[1], ffit_sin(xy), dfdxdy_sin(xy)]

recon_data[:, :, 3] = recon_data[:, :, 3] / (np.sum(recon_data[:, :, 3]) * delta_y * delta_x)

count = 0
threshhold = np.max(approx_pdf[:, :, 4])
approx_samples = []
while count < p:
    xy = np.random.random((2,)) @ np.diag([xmax - xmin, ymax - ymin]) + np.array([xmin, ymin])
    z = np.random.random() * threshhold
    index = xy_2_index(xy)
    if approx_pdf[index[0], index[1], 4] >= z:
        approx_samples.append([xy[0], xy[1], 0])
        count += 1

count = 0
threshhold = np.max(recon_data[:, :, 2])
recon_samples = []
while count < p:
    xy = np.random.random((2,)) @ np.diag([xmax - xmin, ymax - ymin]) + np.array([xmin, ymin])
    z = np.random.random() * threshhold
    index = xy_2_index(xy)
    if ffit_poly(xy) >= z:
        recon_samples.append([xy[0], xy[1], 0])
        count += 1

app = pg.QtWidgets.QApplication([])
w = gl.GLViewWidget()

recon_data[:, :, 2] /= np.max(np.abs(recon_data[:, :, 2]))
recon_data[:, :, 3] /= np.max(np.abs(recon_data[:, :, 3]))
true_data[:, :, 3] /= np.max(true_data[:, :, 3])
approx_pdf[:, :, 4] /= np.max(approx_pdf[:, :, 4])

point_size = 3

# Plot samples
sample_object = gl.GLScatterPlotItem(pos=samples + np.array([0, 0]), color=[1, 1, 1, 1], size=2)
w.addItem(sample_object)

# Plot samples from reconstructed distribution
approx_samples_object = gl.GLScatterPlotItem(pos=approx_samples + np.array([0, 2 * ymax, 0]), color=[1, 1, 0, 1], size=2)
w.addItem(approx_samples_object)

# Plot samples from polyfit distribution
approx_samples_object = gl.GLScatterPlotItem(pos=recon_samples + np.array([0, 4 * ymax, 0]), color=[0, 1, 1, 1], size=2)
w.addItem(approx_samples_object)

# plot true cdf
true_cdf_scatter_object = gl.GLScatterPlotItem(pos=np.reshape(true_data[:, :, [0, 1, 2]], (k * k, 3)) + np.array([2 * xmax, 0, 0]), color=[1, 1, 1, 1], size=point_size)
w.addItem(true_cdf_scatter_object)

# plot approximated CDF
cdf_scatter_object = gl.GLScatterPlotItem(pos=cdf_data + np.array([2 * xmax, 2 * ymax, 0]), color=[1, 1, 0, 1], size=point_size)
w.addItem(cdf_scatter_object)

# plot polyfit CDF
recon_scatter_object = gl.GLScatterPlotItem(pos=np.reshape(recon_data[:, :, 0:3], (k * k, 3)) + np.array([2 * xmax, 4 * ymax, 0]), color=[0, 1, 1, 1], size=point_size)
w.addItem(recon_scatter_object)

# plot true pdf
true_pdf_scatter_object = gl.GLScatterPlotItem(pos=np.reshape(true_data[:, :, [0, 1, 3]], (k * k, 3)) + np.array([4 * xmax, 0 * ymax, 0]), color=[1, 1, 1, 1], size=point_size)
w.addItem(true_pdf_scatter_object)

# plot finite differenced PDF
approx_pdf_object = gl.GLScatterPlotItem(pos=np.reshape(approx_pdf[:, :, [0, 1, 4]], (k * k, 3)) + np.array([4 * xmax, 2 * ymax, 0]), color=[1, 1, 0, 1], size=point_size)
w.addItem(approx_pdf_object)

# plot polyfit PDF
pdf_scatter_object = gl.GLScatterPlotItem(pos=np.reshape(recon_data[:, :, [0, 1, 3]], (k * k, 3)) + np.array([4 * xmax, 4 * ymax, 0]), color=[0, 1, 1, 1], size=point_size)
w.addItem(pdf_scatter_object)

w.show()
app.exec()
