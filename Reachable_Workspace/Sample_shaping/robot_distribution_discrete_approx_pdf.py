import RoboPy as rp
import numpy as np
import pyqtgraph as pg
from pyqtgraph import opengl as gl


# Generate initial samples
dh = [[0, 0, 1, np.pi / 2], [0, 0, 0.5, np.pi / 2], [0, 0, 0.1, 0]]
n = len(dh)
arm = rp.SerialArm(dh)
p = 5000
joint_samples = np.random.random((p, n)) @ np.diag([2 * np.pi] * n) - np.ones((n,)) * np.pi
task_samples = arm.fk(joint_samples, rep='cart')

# Create sample CDF and PDF

def format_sample(t):
    return [np.linalg.norm(t), np.arctan2(t[2], np.linalg.norm(t[0:2]))]
    # return [t[0], t[1], t[2]]

def unformat_sample(s):
    z = s[0] * np.sin(s[1])
    theta = np.random.random() * 2 * np.pi - np.pi
    x = s[0] * np.cos(s[1]) * np.cos(theta)
    y = s[0] * np.cos(s[1]) * np.sin(theta)
    return [x, y, z]
    # return [s[0], s[1], s[2]]

m = len(format_sample(task_samples[0]))
k = 50
formatted_samples = np.zeros((p, m))

for i, t in enumerate(task_samples):
    # formatted_samples[i] = [np.linalg.norm(t), np.arctan2(t[1], t[0])]
    s = format_sample(t)
    formatted_samples[i] = s

bounds = []
for i in range(m):
    spacer = 0.1 * (np.max(formatted_samples[:, i]) - np.min(formatted_samples[:, i]))
    minval = np.min(formatted_samples[:, i]) - spacer
    maxval = np.max(formatted_samples[:, i]) + spacer
    bounds.append((minval, maxval, maxval - minval, (maxval - minval) / k))

binned_samples = np.zeros([k] * m)

def pos_2_index(x):
    index = ()
    for i, z in enumerate(x):
        ind = min(int((z - bounds[i][0]) / (bounds[i][1] - bounds[i][0]) * k), k - 1)
        index = index + (ind,)
    return index

for x in formatted_samples:
    index = pos_2_index(x)
    binned_samples[index] += 1

Fn = np.zeros([k] * m)

def fill_cdf(index=()):
    global Fn
    if len(Fn[index].shape) >= 1:
        for i in range(k):
            fill_cdf(index + (i,))
    else:
        slicing_index = tuple(slice(0, i + 1, 1) for i in index)
        sub_block = binned_samples[slicing_index]
        Fn[index] = np.sum(sub_block)

    if index == ():
        Fn = Fn * (1 / p)

fill_cdf()

approximate_pdf = np.zeros_like(Fn)
base_slice = (slice(0, k, 1),) * m
holder = np.copy(Fn)

def get_fd_index(i):
    if i == 0:
        f = 1
        b = 0
        c = 0
    elif i == k - 1:
        f = k - 1
        b = k - 2
        c = k - 1
    else:
        f = i + 1
        b = i - 1
        c = i
    return f, b, c

def get_slices(ind, i):
    if ind == 0:
        s = (i,) + base_slice[ind + 1:None]
    elif ind == m - 1:
        s = base_slice[0:m - 1] + (i,)
    else:
        s = base_slice[0:ind - 1] + (i,) + base_slice[ind + 1:None]
    return s

for ind in range(m):
    if ind > 0:
        holder = np.copy(approximate_pdf)
    for i in range(k):
        f, b, c = get_fd_index(i)
        delta = bounds[ind][3] * (f - b)
        sf = get_slices(ind, f)
        sb = get_slices(ind, b)
        sc = get_slices(ind, c)

        approximate_pdf[sc] = (holder[sf] - holder[sb]) / delta

def sample_pdf(pdf, num):
    count = 0
    threshhold = np.max(pdf)
    samples = np.zeros((num, m))
    while count < num:
        x = np.random.random((m,)) @ np.diag([b[2] for b in bounds]) + np.array([b[0] for b in bounds])
        index = pos_2_index(x)
        z = np.random.random() * threshhold
        if pdf[index] >= z:
            samples[count] = x
            count += 1
    return samples

recon_formatted_sample = sample_pdf(binned_samples / p, p)
recon_task_sample = np.zeros((p, 3))
for i, s in enumerate(recon_formatted_sample):
    recon_task_sample[i] = unformat_sample(s)

app = pg.QtWidgets.QApplication([])
w = gl.GLViewWidget()

sample_point_size = 2

sample_object = gl.GLScatterPlotItem(pos=task_samples, color=[1, 1, 1, 1], size=sample_point_size)
w.addItem(sample_object)

recon_sample_object = gl.GLScatterPlotItem(pos=recon_task_sample + np.array([2 * bounds[0][1], 0, 0]), color=[1, 0, 0, 1], size=sample_point_size)
w.addItem(recon_sample_object)

formatted_sample_object = gl.GLScatterPlotItem(pos=formatted_samples + np.array([0, 2 * bounds[0][1]]), color=[1, 1, 1, 1], size=sample_point_size)
w.addItem(formatted_sample_object)

recon_formatted_sample_object = gl.GLScatterPlotItem(pos=recon_formatted_sample + np.array([2 * bounds[0][1], 2 * bounds[0][1]]), color=[1, 0, 0, 1], size=sample_point_size)
w.addItem(recon_formatted_sample_object)

w.show()
app.exec()

