import RoboPy as rp
import numpy as np
from sklearn.mixture import GaussianMixture


# Robot Arm
dh = [[0, 0, 1, 0], [0, 0, 1, 0]]
arm = rp.SerialArm(dh)
n = len(dh)

# Random Joint Samples
p = 10000
joint_samples = np.random.random((p, n)) * 2 * np.pi - np.pi
task_samples = arm.fk(joint_samples, rep='cart')

# Guassian Mixture Model
k = 1
pdf_model = GaussianMixture(n_components=k,
                            covariance_type='full',
                            tol=1e-3,
                            reg_covar=1e-6,
                            max_iter=100,
                            n_init=1,
                            init_params='kmeans',
                            weights_init=None,
                            means_init=None,
                            precisions_init=None,
                            random_state=None,
                            warm_start=False,
                            verbose=0,
                            verbose_interval=10)
pdf_model.fit(task_samples)
fitted_samples, labels = pdf_model.sample(p)
fitted_samples = fitted_samples + np.array([0, 6, 0])

# Visualization
color_options = [np.random.random((3,)) for i in range(k)]
colors = np.asarray([color_options[l] for l in labels])
viz = rp.VizScene()
viz.add_arm(arm)
viz.add_marker(pos=task_samples, color=[0, 1, 0, 1], size=0.02, pxMode=False)
viz.add_marker(pos=fitted_samples, color=colors, size=0.02, pxMode=False)
viz.wander()
viz.hold()
