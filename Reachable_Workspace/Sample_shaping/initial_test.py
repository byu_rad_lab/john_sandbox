import RoboPy as rp
import numpy as np
from scipy import optimize
from scipy.stats import qmc
import pyqtgraph.opengl as gl


n = 3
# x0 = np.array([2, 0.5])
x0 = np.ones((n,)) * 6 / n
# x0 = np.array([3.5, 0.4, 0.3, 0.2, 0.1, 0.1])
arm_length_bounds = [[0.01, 4]] * n
# joint_angle_bounds = np.array([(-np.pi / (n + 1 - i), np.pi / (n + 1 - i)) for i in range(n)])
joint_angle_bounds = np.array([(-np.pi * 2, np.pi * 2) for i in range(n)])
p = 600 * n
p_prime = int(p * 1 / 5)
k_nearest = 10 * n
# joint_angle_samples = np.asarray([[np.random.random() * (b[1] - b[0]) + b[0] for b in joint_angle_bounds] for i in range(p)])
hypercube = qmc.LatinHypercube(d=n)
joint_angle_samples = hypercube.random(n=p) * 2 * np.pi - np.pi
targets = np.array([[0.0, 3.0, 0],
                    [0.0, 2.0, 0],
                    [0.0, 2.5, 0]])
target_radii = np.ones((len(targets),)) * 0.2


def get_serial_arm_from_x(x):
    alpha = 0.0
    dh = [[0, 0, a, alpha] for a in x]
    # dh = [[0, 0, x[0], np.pi / 2], [0, 0, x[1], 0]]
    joint_type = ['r'] * n
    arm = rp.SerialArm(dh, joint_type)
    return arm


def calculate_x_from_qs(arm, qs):
    xs = np.asarray([arm.fk(q, base=True)[0:3, 3] for q in qs])
    return xs


def calculate_list_of_distance_from_targets(targets, xs):
    dists = np.zeros((p,))
    for i, x in enumerate(xs):
        d = min([np.linalg.norm(x - t) for t in targets])
        dists[i] = d
    return dists


def calculate_sum_of_distance_from_xs(targets, xs):
    dists = calculate_list_of_distance_from_targets(targets, xs)
    sum_of_distance = np.sum(dists) / p
    return sum_of_distance


def calculate_inner_quartile_distance_from_xs(targets, xs):
    dists = calculate_list_of_distance_from_targets(targets, xs)
    dists = np.sort(dists)
    sum_of_distance = np.sum(dists[0:p_prime]) / p_prime
    return sum_of_distance


def calculate_distance_lists_for_each_target(targets, xs):
    distance_lists = []
    for t, r in zip(targets, target_radii):
        distance_lists.append(np.linalg.norm(xs - t, axis=1) / r)
        distance_lists[-1].sort()
    return distance_lists


def target_scoring_metric(d, r):
    # val = -np.sum(d[0:k_nearest] < r) + 0.1 * np.sum(d[0:k_nearest])
    val = np.sum(d[0:k_nearest] ** 2)
    # val = d[0] + d[k_nearest]
    return val / k_nearest


def calculate_sum_of_distances_for_each_target(targets, xs):
    distance_lists = calculate_distance_lists_for_each_target(targets, xs)
    sum_of_distances = max([target_scoring_metric(d, r) for d, r in zip(distance_lists, target_radii)])
    return sum_of_distances


def calculate_score_from_average_position(targets, xs):
    mean_neighbor_list = []
    variance_list = []
    for t, r in zip(targets, target_radii):
        xs_relative_to_t = list(-(xs - t))
        xs_relative_to_t.sort(key=lambda e: np.linalg.norm(e))
        xs_relative_to_t = np.asarray(xs_relative_to_t)
        mean_neighbor = np.sum(xs_relative_to_t[0:k_nearest]) / k_nearest
        mean_neighbor_list.append(mean_neighbor)
        variance_list.append(np.linalg.norm(np.var(xs_relative_to_t[0:k_nearest], axis=0)))

    score = max([np.linalg.norm(x) * (1 + v) for x, v in zip(mean_neighbor_list, variance_list)])
    return score


def approximate_probability_distribution(xs):
    rs = np.linalg.norm(xs, axis=1)
    rs.sort()
    cdf = np.asarray([i for i in range(p)]) / p

    xn = np.linspace(0, np.max(np.linalg.norm(targets, axis=1)) + 1, 200)
    yn = np.interp(xn, rs, cdf)

    m = 10 * n
    l_padding = 0.5 * n

    xn_padded = np.hstack([np.linspace(xn[0] - l_padding, xn[0], m), xn, np.linspace(xn[-1], xn[-1] + l_padding, m)])
    yn_padded = np.hstack([np.zeros((m,)), yn, np.ones((m,))])

    cdf_poly_coeff = np.polyfit(xn_padded, yn_padded, deg=20)
    pdf_poly_coeff = np.asarray([c * (len(cdf_poly_coeff) - i - 1) for i, c in enumerate(cdf_poly_coeff[0:-1])])

    def pdf(r):
        prob = 0
        if rs[-1] >= r >= rs[0]:
            for i, c in enumerate(pdf_poly_coeff):
                prob += c * r ** (len(pdf_poly_coeff) - i - 1)
        return prob

    def cdf(r):
        cum_prob = 0
        if rs[-1] >= r >= rs[0]:
            for i, c in enumerate(cdf_poly_coeff):
                cum_prob += c * r ** (len(cdf_poly_coeff) - i - 1)
        elif r < rs[0]:
            cum_prob = 0.0
        else:
            cum_prob = 1.0
        return cum_prob

    return pdf, cdf


def pdf_based_cost_function(targets, xs):
    pdf, cdf = approximate_probability_distribution(xs)
    cost = -min([pdf(np.linalg.norm(t)) for t in targets])
    return cost


def optimization_cost_function(x):
    arm = get_serial_arm_from_x(x)
    xs = calculate_x_from_qs(arm, joint_angle_samples)
    # cost = calculate_sum_of_distance_from_xs(targets, xs)
    # cost = calculate_inner_quartile_distance_from_xs(targets, xs)
    # cost = calculate_sum_of_distances_for_each_target(targets, xs)
    # cost = calculate_score_from_average_position(targets, xs)
    cost = pdf_based_cost_function(targets, xs)
    return cost


initial_arm = get_serial_arm_from_x(x0)
initial_taskspace_sample = calculate_x_from_qs(initial_arm, joint_angle_samples)
viz = rp.VizScene()
viz.add_marker(pos=targets, color=[1, 0, 0, 1], size=target_radii, pxMode=False)
viz.add_marker(pos=initial_taskspace_sample, color=[0, 1, 0, 1], size=0.03, pxMode=False)
viz.add_marker(pos=targets[0], color=[1, 1, 1, 1], size=target_radii[0], pxMode=False)
viz.add_arm(initial_arm)
viz.update(qs=joint_angle_samples[0])
pdf_line_positions = np.zeros((200, 3))
pdf_line_positions[:, 1] = np.linspace(0, 10, 200)
pdf_line = gl.GLLinePlotItem(pos=pdf_line_positions, color=[0, 1, 1, 1])

cdf_line_positions = np.zeros((200, 3))
cdf_line_positions[:, 1] = np.linspace(0, 10, 200)
cdf_line = gl.GLLinePlotItem(pos=cdf_line_positions, color=[1, 1, 0, 1])

viz.window.addItem(pdf_line)
viz.window.addItem(cdf_line)


def optimization_callback_function(xk, *args, **kwargs):
    current_arm = get_serial_arm_from_x(xk)
    current_taskspace_sample = calculate_x_from_qs(current_arm, joint_angle_samples)
    viz.remove_arm()
    viz.add_arm(current_arm)
    pdf, cdf = approximate_probability_distribution(current_taskspace_sample)
    pdf_line_positions[:, 2] = np.asarray([pdf(x) for x in pdf_line_positions[:, 1]])
    pdf_line.setData(pos=pdf_line_positions)
    cdf_line_positions[:, 2] = np.asarray([cdf(x) for x in cdf_line_positions[:, 1]])
    cdf_line.setData(pos=cdf_line_positions)
    target_pdf_scores = np.asarray([pdf(np.linalg.norm(t)) for t in targets])
    index = np.argmin(target_pdf_scores)
    print(target_pdf_scores[index])
    viz.update(poss=[targets, current_taskspace_sample, targets[index]])


optimization_solution = optimize.differential_evolution(optimization_cost_function,
                                                        bounds=arm_length_bounds,
                                                        maxiter=5,
                                                        popsize=10,
                                                        callback=optimization_callback_function,
                                                        polish=False)

print(optimization_solution)

optimization_solution = optimize.minimize(optimization_cost_function, optimization_solution.x,
                                          bounds=arm_length_bounds,
                                          method='COBYLA',
                                          callback=optimization_callback_function,
                                          tol=1e-3)

# optimization_solution = optimize.minimize(optimization_cost_function, x0,
#                                           bounds=arm_length_bounds,
#                                           method='powell',
#                                           callback=optimization_callback_function,
#                                           tol=1e-3)

optimization_callback_function(optimization_solution.x)

print(optimization_solution)
viz.wander()
