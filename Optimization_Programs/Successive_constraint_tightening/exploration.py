import numpy as np
import pyqtgraph as pg
import tqdm
from PyQt5 import QtWidgets


# Visualization
app = QtWidgets.QApplication([])
w = pg.PlotWidget()
w.setXRange(0, 1)
w.setYRange(0, 1)
w.show()
p = pg.ScatterPlotItem()
xs = np.linspace(0, 1, 100)
w.addItem(p)
app.processEvents()


def f(x):
    return 1.001 * x[0]**2 + x[1]**2


def g(x, t):
    e1 = np.linalg.norm(x - np.array([0.5, 0.0]))
    ep1 = (e1 / (0.001 + t)) ** 2
    g1 = (ep1 ** 2 - 1) * np.exp(-0.5 * ep1) + 0.1

    e2 = np.linalg.norm(x - np.array([1, 1]) / np.linalg.norm(np.array([1, 1])) * 0.5)
    ep2 = (e2 / (0.001 + t)) ** 2
    g2 = (ep2 ** 2 - 1) * np.exp(-0.5 * ep2) + 0.1

    e3 = np.linalg.norm(x - np.array([0.0, 0.5]))
    ep3 = (e3 / (0.001 + t)) ** 2
    g3 = (ep3 ** 2 - 1) * np.exp(-0.5 * ep3) + 0.1
    return np.min([g1, g2, g3])


class MyProblem:
    def __init__(self, pop_size=50, n_gen=100, n_offspring=25, c_init=1.0, t_tight=0.9):
        """
        Initialize optimization problem
        :param pop_size: number of population members: number of function evaluations will be pop_size * (n_gen + 1)
        :param n_gen: number of generations to simulate: number of function evaluations will be pop_size * (n_gen + 1)
        :param n_offspring: number of designs to perturbate each generation: (pop_size - n_offspring) best designs will remain unchanged each generation
        :param c_init: constraint looseness at generation 0
        :param t_tight: constraints will be fully tightened at: g_tight = int(n_gen * t_tight)
        """
        self.n = pop_size
        self.xs = np.random.random((pop_size, 2))
        self.n_offspring = n_offspring
        self.n_gen = n_gen
        self.gen = 1

        self.tau = -5 / n_gen
        self.c = -c_init / (np.exp(self.tau * t_tight * n_gen) - 1)
        self.b = c_init - self.c

    def t(self, gen):
        return max(0, self.c * np.exp(self.tau * gen) + self.b)

    def minimize(self):
        for i in tqdm.tqdm(range(self.n_gen)):
            self.update()
        return self.xs

    def update(self):
        # Find fitness and constraint values for each member of population
        F = np.zeros((self.n,))
        G = np.zeros((self.n,))

        # t is the constraint loosening value, 0 means constraints are fully tightened
        t = self.t(self.gen)
        # t = 0

        for i, x in enumerate(self.xs):
            F[i] = f(x)
            G[i] = g(x, t)

        # Order w.r.t. constraint values
        index = np.argsort(G)
        self.xs = self.xs[index]
        F = F[index]
        G = G[index]

        # Split population into constraint-satisfying and constraint-breaking portions
        j = -1
        for i, c in enumerate(G):
            if c > 0:
                break
            j = i

        if j == -1:  # the entire population is constraint-violating
            pass
        else:  # a portion of the population is feasible, sort according to fitness
            index = np.argsort(F[0:j])
            F[0:j] = F[0:j][index]
            self.xs[0:j] = self.xs[0:j][index]

        # keep the top (pop - offspring) members and perturbate remainder with gaussian normal noise
        self.xs = np.vstack([self.xs[0:self.n - self.n_offspring], self.xs[0:self.n_offspring] + np.random.multivariate_normal(np.zeros((2,)), np.eye(2) * np.exp(-self.gen * 20 / self.n_gen), self.n_offspring)])
        for i, x in enumerate(self.xs):
            F[i] = f(x)
            G[i] = g(x, t)

        # update figures
        brushes = [pg.mkBrush(255, 0, 0, 255) if gv > 0 else pg.mkBrush(0, 255, 0, 255) for gv in G]
        p.setData(pos=self.xs)
        p.setBrush(brushes)
        app.processEvents()

        self.gen += 1

        # Cludgy way to make sure things are sorted correctly for final evaluation
        if self.gen == self.n_gen:
            # Split population into constraint-satisfying and constraint-breaking portions
            j = -1
            for i, c in enumerate(G):
                if c > 0:
                    break
                j = i

            if j == -1:  # the entire population is constraint-violating
                pass
            else:  # a portion of the population is feasible, sort according to fitness
                index = np.argsort(F[0:j])
                F[0:j] = F[0:j][index]
                G[0:j] = G[0:j][index]
                self.xs[0:j] = self.xs[0:j][index]


from scipy.optimize import minimize

# sol = minimize(f, x0=np.array([0.5, 0.5]), constraints={'type': 'ineq', 'fun': lambda x: np.linalg.norm(x - np.array([0.5, 0.5]))**2 - 0.1}, method='COBYLA')
# print(sol)

problem = MyProblem(pop_size=200, n_offspring=75, n_gen=1000, c_init=2.0, t_tight=0.5)
xf = problem.minimize()

print(f(xf[0]))
print(g(xf[0], 0))

app.exec()
