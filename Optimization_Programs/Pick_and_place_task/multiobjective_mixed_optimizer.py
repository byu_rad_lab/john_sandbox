"""
I didn't want to do this either, but at this point I think it's going to save time
 ^ clearly a lie, I obviously wanted to do this
To Do:
    1. Research sampling methods (latin-hypercube, etc.) for mixed real / integer variables [X]
    2. Multi-objective tournament [X]
    3. Mixed variable crossover [X]
    4. Mixed variable mutation [X]
    5. Delayed / progressive constraint tightening [X]
"""
import time
import numpy as np
import scipy.stats as stats
import multiprocessing as mp


class MMO:
    def __init__(self, design_class,
                 n_continuous,
                 n_discrete,
                 n_obj=3,
                 n_con=1,
                 n_pop=100,
                 n_gen=50,
                 continuous_bounds=None,
                 discrete_bounds=None,
                 n_children=50,
                 eta_mutation=0.5,
                 acceptable_final_constraint=0.9,
                 initial_constraint=0.5,
                 full_constraint_time=0.5,
                 constraint_adaptive_rate=0.01,
                 callback=None,
                 callback_args=(),
                 parallel=False,
                 seed=None):

        self.design_class = design_class
        self.n_continuous = n_continuous
        self.n_discrete = n_discrete
        self.n_obj = n_obj
        self.n_con = n_con
        self.n_pop = n_pop
        self.n_gen = n_gen
        self.gen = 0
        self.continuous_bounds = continuous_bounds
        self.discrete_bounds = discrete_bounds
        self.n_children = n_children
        self.eta_mutation = eta_mutation
        self.continuous_std = (continuous_bounds[1] - continuous_bounds[0]) / 5
        self.discrete_std = (discrete_bounds[1] - discrete_bounds[0]) / 5

        self.acceptable_final_constraint = acceptable_final_constraint
        self.initial_constraint = initial_constraint
        self.full_constraint_time = full_constraint_time
        self.acceptable_constraint_value = initial_constraint
        self.constraint_adaptive_rate = constraint_adaptive_rate

        self.callback = callback
        self.callback_args = callback_args
        self.pop = []

        self.parallel = parallel

        self.seed = seed if seed is not None else np.random.randint(0, 10000)

    def _update_constraint(self):
        feasible_proportion = np.sum([p.feasible for p in self.pop]) / self.n_pop
        constraint_delta = self.constraint_adaptive_rate * feasible_proportion
        adapted_acceptable_constraint = self.acceptable_constraint_value + constraint_delta
        t_constraint = self.gen / (self.full_constraint_time * self.n_gen)
        linear_acceptable_constraint = t_constraint * (self.acceptable_final_constraint - self.initial_constraint) + self.initial_constraint
        self.acceptable_constraint_value = max(adapted_acceptable_constraint, linear_acceptable_constraint)
        self.acceptable_constraint_value = min(self.acceptable_final_constraint, self.acceptable_constraint_value)

    def _sample(self):
        self.pop = []
        print("Latin hyper-cube sampling to fill population:")
        lhc_sampler = stats.qmc.LatinHypercube(d=(self.n_continuous + self.n_discrete), optimization=None, strength=1,
                                               seed=self.seed)
        lhc = lhc_sampler.random(self.n_pop)
        xs_c = lhc[:, 0:self.n_continuous] * (self.continuous_bounds[1] - self.continuous_bounds[0]) + \
               self.continuous_bounds[0]
        xs_d = lhc[:, self.n_continuous:(self.n_continuous + self.n_discrete)] * (self.discrete_bounds[1] + 1)
        xs_d = np.floor(xs_d).astype(int)
        for x_c, x_d in zip(xs_c, xs_d):
            self.pop.append(self.design_class(x_c, x_d))

        self.pop = self._rank(self.pop)
        # median_constraint_value = np.median([p.g for p in self.pop])
        # self.acceptable_constraint_value = median_constraint_value
        print("Finished sampling space")

    @staticmethod
    def evaluate_population_member(input):
        p, acceptable_constraint_value = input
        p.eval(acceptable_constraint_value)
        p.clean()
        return p

    def _rank(self, pop):
        """
        Given the population, return a sorted list of the population with ranks attached to each, according to domination
        and non-crowding scores.
        """
        feasible_pop = []
        infeasible_pop = []

        """ ****************** HIGH EFFORT FOR LOOP ********************** """
        if not self.parallel:
            for p in pop:
                p.eval_con()
                if min(p.g) > self.acceptable_constraint_value:
                    p.feasible = 1
                    p.eval_obj()
                    feasible_pop.append(p)
                else:
                    p.feasible = 0
                    infeasible_pop.append(p)
        """ ****************** END HIGH EFFORT LOOP *********************** """

        """ ****************** Parallel version ****************** """
        if self.parallel:
            with mp.Pool(processes=mp.cpu_count()) as pool:
                evaled_pop = []
                iterable = [(p, self.acceptable_constraint_value) for p in pop]
                for p in pool.imap_unordered(self.evaluate_population_member, iterable):
                    evaled_pop.append(p)
                pop = evaled_pop
                # pop = pool.map(self.evaluate_population_member, [(p, self.acceptable_constraint_value) for p in pop])
            for p in pop:
                if p.feasible == 1:
                    feasible_pop.append(p)
                else:
                    infeasible_pop.append(p)
        """ ****************** Parallel end ****************** """

        # perform non-domination sorting, to give a domination score [1, 2, ... k]
        domination_fronts = [[]]
        for p1 in feasible_pop:
            p1.dominated_number = 0
            p1.dominating_set = []
            for p2 in feasible_pop:
                if p1 == p2:
                    pass
                else:
                    f1_dominates_f2 = True
                    f1_dominated_by_f2 = True
                    equal_scores = 0
                    for f1, f2 in zip(p1.f, p2.f):
                        if f1 > f2:
                            f1_dominated_by_f2 = False
                        elif f1 == f2:
                            equal_scores += 1
                        else:
                            f1_dominates_f2 = False
                    if equal_scores == self.n_obj:  # designs have identical scores
                        f1_dominates_f2 = False
                        f1_dominated_by_f2 = False
                    if f1_dominated_by_f2:
                        p1.dominated_number += 1
                    if f1_dominates_f2:
                        p1.dominating_set.append(p2)
            if p1.dominated_number == 0:
                p1.domination_rank = 1
                domination_fronts[0].append(p1)

        remaining_ranks = True
        while remaining_ranks:
            remaining_ranks = False
            domination_fronts.append([])
            for p1 in domination_fronts[-2]:
                for p2 in p1.dominating_set:
                    p2.dominated_number -= 1
                    if p2.dominated_number == 0:
                        p2.domination_rank = len(domination_fronts)
                        domination_fronts[-1].append(p2)
                        remaining_ranks = True
        domination_fronts.pop(-1)

        for i, front in enumerate(domination_fronts):
            if len(front) <= 1:
                for p in front:
                    p.crowding_score = 1
            else:
                sorted_orders = [np.argsort([p.f[obj] for p in front]) for obj in range(self.n_obj)]
                for j, p in enumerate(front):
                    crowding_volume = 0
                    for k in range(self.n_obj):
                        rank = np.where(sorted_orders[k] == j)[0][0]
                        if rank == 0:
                            a = 2 * np.abs(p.f[k] - front[sorted_orders[k][1]].f[k])
                        elif rank == len(front) - 1:
                            a = 2 * np.abs(p.f[k] - front[sorted_orders[k][-2]].f[k])
                        else:
                            a = np.abs(front[sorted_orders[k][rank - 1]].f[k] - front[sorted_orders[k][rank + 1]].f[k])
                        crowding_volume += a
                    p.crowding_score = crowding_volume

        ranked_population = []
        for front in domination_fronts:
            scores = [-x.crowding_score for x in front]
            ind = np.argsort(scores)
            ranked_population = ranked_population + [front[i] for i in ind]
        scores = [-min(x.g) for x in infeasible_pop]
        ind = np.argsort(scores)
        ranked_population = ranked_population + [infeasible_pop[i] for i in ind]
        return ranked_population

    @staticmethod
    def _compare(p1, p2):
        if p1.feasible != p2.feasible:
            return p1.feasible > p2.feasible
        else:
            if p1.domination_rank == p2.domination_rank:
                return p1.crowding_score > p2.crowding_score
            else:
                return p1.domination_rank < p2.domination_rank

    @staticmethod
    def _cross(parent1, parent2):
        c1 = 0.5 * parent1.x_c + 0.5 * parent2.x_c
        c2 = 2 * parent1.x_c - 1.0 * parent2.x_c
        d1 = np.round(0.5 * parent1.x_d + 0.5 * parent2.x_d).astype(int)
        d2 = np.round(2 * parent1.x_d - 1.0 * parent2.x_d).astype(int)
        return c1, d1, c2, d2

    def _mutate(self, c, d, sigma_c, sigma_d):
        for i in range(len(c)):
            if np.random.random() < self.eta_mutation:
                c[i] = c[i] + np.random.normal(0, sigma_c[i])
            if np.random.random() < self.eta_mutation:
                d[i] = d[i] + np.random.normal(0, sigma_d[i])
        c = np.clip(c, self.continuous_bounds[0], self.continuous_bounds[1])
        d = np.round(np.clip(d, self.discrete_bounds[0], self.discrete_bounds[1])).astype(int)
        return c, d

    def _mate(self, parent1: 'PopulationMember', parent2: 'PopulationMember'):
        if self._compare(parent1, parent2):
            c1, d1, c2, d2 = self._cross(parent1, parent2)
        else:
            c1, d1, c2, d2 = self._cross(parent2, parent1)
        sigma_c = np.abs(c1 - c2) / 3 * 2 * (parent1.domination_rank + parent2.domination_rank)  # more spread for suboptimal parents
        sigma_d = [2 for i in range(self.n_discrete)]
        c1, d1 = self._mutate(c1, d1, sigma_c, sigma_d)
        c2, d2 = self._mutate(c2, d2, sigma_c, sigma_d)
        child1 = self.design_class(c1, d1)
        child2 = self.design_class(c2, d2)
        return child1, child2

    def _advance(self):
        """
        We start with a population of size N from the _sample method which have been ranked (all must be either infeasible, or have a domination rank and crowding score)

        First, tournament selection and mating to form a second population of size N

        Second, rank all members of the combined 2N population

        Third, pick the top N of this population according to domination rank / crowding and feasibility second
        """
        mating_population = []
        np.random.shuffle(self.pop)
        for i in range(int(self.n_children / 2)):
            p1, p2 = self.pop[np.random.randint(0, self.n_pop)], self.pop[np.random.randint(0, self.n_pop)]
            p3, p4 = self.pop[np.random.randint(0, self.n_pop)], self.pop[np.random.randint(0, self.n_pop)]
            p1 = p1 if self._compare(p1, p2) else p2
            p2 = p3 if self._compare(p3, p4) else p4
            p = p1 if self._compare(p1, p2) else p2
            mating_population.append(p)

        offspring = []
        for i in range(int(self.n_children / 2)):
            p1, p2 = mating_population[i], mating_population[(i + 1) % int(self.n_children / 2)]
            c1, c2 = self._mate(p1, p2)
            offspring.append(c1)
            offspring.append(c2)

        pop = self._rank(self.pop + offspring)
        self.pop = pop[0:self.n_pop]

    def solve(self):
        tic = time.perf_counter()
        self._sample()
        for i in range(self.n_gen):
            self.gen = i
            # self.acceptable_constraint_value = self._get_constraint(self.gen)
            self._update_constraint()
            self._advance()
            if hasattr(self.callback, '__call__'):
                self.callback(self, *self.callback_args)
        print("Solve finished:")
        toc = time.perf_counter()
        print(f"Optimization termination after {np.round(toc - tic, 2)} seconds wall time.\nAverage time per generation: {np.round((toc - tic) / self.n_gen, 2)}")
        for p in self.pop:
            p.clean()
        return OptimizationOutput(self.pop, self.n_obj)


class PopulationMember:
    def __init__(self, x_c: np.ndarray, x_d: np.ndarray):
        self.x_c = np.asarray(x_c)
        self.x_d = np.asarray(x_d)
        self.f = None
        self.g = None

        self.feasible = 0

        self.dominated_number = 0
        self.dominating_set = []
        self.crowding_score = 0
        self.domination_rank = 0

    def eval_con(self):
        raise NotImplementedError

    def eval_obj(self):
        raise NotImplementedError

    def eval(self, acceptable_constraint_value: float):
        if self.g is None:
            self.eval_con()

        if min(self.g) > acceptable_constraint_value:
            self.feasible = 1
            if self.f is None:
                self.eval_obj()
        else:
            self.feasible = 0

    def clean(self):
        pass


class OptimizationOutput:
    def __init__(self, pop, n_obj):
        self.pop = pop
        self.feasible_pop = []
        self.infeasible_pop = []

        self.n_obj = n_obj

        for p in pop:
            if p.feasible:
                self.feasible_pop.append(p)
            else:
                self.infeasible_pop.append(p)

        self.pareto_set = []
        for p in self.feasible_pop:
            if p.domination_rank == 1:
                self.pareto_set.append(p)

        self.max_objective_values = []
        self.min_objective_values = []

        if len(self.pareto_set) > 0:
            for i in range(self.n_obj):
                f = [p.f[i] for p in self.pareto_set]
                self.max_objective_values.append(max(f))
                self.min_objective_values.append(min(f))
        else:
            print("Empty Pareto-set!")

    def search_pareto_front(self, search_vector: np.ndarray) -> PopulationMember:
        v = search_vector / np.linalg.norm(search_vector)
        dist = []
        maxes = np.array(self.max_objective_values)
        mines = np.array(self.min_objective_values)
        T = np.diag(1 / (maxes - mines))
        for p in self.pareto_set:
            f = T @ (np.array(p.f) - mines)
            # w = f - v * (v @ f)
            # dist.append(w @ w)
            dist.append(f @ v)
        # ind = np.argmin(dist)
        ind = np.argmax(dist)
        return self.pareto_set[ind]
