import numpy as np
import RoboPy as rp
import pickle
from multiobjective_mixed_optimizer import PopulationMember, OptimizationOutput, MMO
import params
from sc_optimization import PickAndPlaceTask


with open('results/opt1.pkl', 'rb') as f:
    sol1 = pickle.load(f)

with open('results/opt2.pkl', 'rb') as f:
    sol = pickle.load(f)

# pop = sol1.pop + sol2.pop
# sol = OptimizationOutput(pop, 3)


def get_arm(p: PopulationMember, base=np.array([0, 0, 0])):
    dh = [[0, 0, x, 0] for x in p.x_c]
    mm = [params.motor_dict[m]['mass'] for m in p.x_d]
    return rp.SimpleDynArm(dh, linear_density=1.0, motor_mass=mm, base=rp.transl(base))


viz = rp.VizScene()

# p_con = sol.pop[np.argmax([p.g[0] for p in sol.pop])]
# p_mga = sol.pop[np.argmax([p.f[0] for p in sol.pop])]
# p_sta = sol.pop[np.argmax([p.f[1] for p in sol.pop])]
# p_cos = sol.pop[np.argmax([p.f[2] for p in sol.pop])]
# p_mid = sol.search_pareto_front(np.array([1, 1, 0]))
#
# best_constraint = get_arm(p_con, np.array([0, 0, 0]))
# best_mga = get_arm(p_mga, np.array([0, 1, 0]))
# best_static = get_arm(p_sta, np.array([0, 2, 0]))
# best_cost = get_arm(p_cos, np.array([0, 3, 0]))
# mid_arm = get_arm(p_mid, np.array([0, 1.5, 1]))
#
# print("Constraint: ", p_con.x_c, p_con.x_d, p_con.f, p_con.g)
# print("Accel-radius: ", p_mga.x_c, p_mga.x_d, p_mga.f, p_mga.g)
# print("Static-load: ", p_sta.x_c, p_sta.x_d, p_sta.f, p_sta.g)
# print("Min-cost: ", p_cos.x_c, p_cos.x_d, p_cos.f, p_cos.g)
# print("Mga - Cost: ", p_mid.x_c, p_mid.x_d, p_mid.f, p_mid.g)
#
# viz.add_arm(best_constraint)
# viz.add_arm(best_mga)
# viz.add_arm(best_static)
# viz.add_arm(best_cost)
# viz.add_arm(mid_arm)

spectrum = []
iteratorias = np.linspace(0, 1, 10, endpoint=False)

for i in iteratorias:
    p = sol.search_pareto_front(np.array([0, i, 1 - i]))
    if len(spectrum) == 0:
        spectrum.append(p)
        print(f"{p.x_c}, {p.x_d}, {p.f}")
        arm = get_arm(p, np.array([0, len(spectrum) - 1, 0]))
        viz.add_arm(arm)
    else:
        if spectrum[-1] is not p:
            spectrum.append(p)
            print(f"{p.x_c}, {p.x_d}, {p.f}")
            arm = get_arm(p, np.array([0, len(spectrum) - 1, 0]))
            viz.add_arm(arm)

for i in iteratorias:
    p = sol.search_pareto_front(np.array([i, 1 - i, 0]))
    if spectrum[-1] is not p:
        spectrum.append(p)
        print(f"{p.x_c}, {p.x_d}, {p.f}")
        arm = get_arm(p, np.array([0, len(spectrum) - 1, 0]))
        viz.add_arm(arm)

for i in iteratorias:
    p = sol.search_pareto_front(np.array([1 - i, 0, i]))
    if spectrum[-1] is not p:
        spectrum.append(p)
        print(f"{p.x_c}, {p.x_d}, {p.f}")
        arm = get_arm(p, np.array([0, len(spectrum) - 1, 0]))
        viz.add_arm(arm)

viz.wander()

viz.hold()
