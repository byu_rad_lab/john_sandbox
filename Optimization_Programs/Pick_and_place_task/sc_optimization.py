from multiobjective_mixed_optimizer import MMO, PopulationMember, OptimizationOutput
import numpy as np
import params
from source import TaskSpace, get_acceleration_radius, get_max_static_load
import pickle
import sys
import os
import argparse


class PickAndPlaceTask(PopulationMember):
    def __init__(self, x_c, x_d):
        super().__init__(x_c, x_d)
        self.n = x_c.shape[0]
        self.taskspace = TaskSpace(params.xbounds, params.ybounds, params.zbounds, params.dx, params.dy, params.dz,
                                   ('mga'))
        self.dh = [[0, 0, a, 0] for a in x_c]
        self.motors = [params.motor_dict[m] for m in x_d]
        self.T = np.diag([motor['tau'] for motor in self.motors])

        self.Ins = []
        self.ms = []
        self.coms = []
        for i in range(self.n):
            a = self.x_c[i]
            lm = params.linear_density * a
            mm = self.motors[i]['mass']
            self.ms.append(mm + lm)

            com = (lm * 0.5 + mm) / (lm + mm) * a
            self.coms.append(com)
            In = lm * (a**2 / 3 - a * com + com**2) + (a - com)**2 * mm
            self.Ins.append(In)

    def fk(self, q, index=None, rep=None):
        if len(q.shape) > 1:
            xs = np.array([self.fk(th, index, rep) for th in q])
            return xs
        A = np.array([[1, 0, 0],
                      [0, 1, -0.5],
                      [0, 0, 1]])
        for th, a in zip(q[0:index], self.x_c[0:index]):
            cth = np.cos(th)
            sth = np.sin(th)
            T = np.array([[cth, -sth, cth * a],
                              [sth, cth, sth * a],
                              [0, 0, 1]])
            A = A @ T
        if rep == 'cart':
            x = A[:, 2]
            x[2] = 0
            return x
        elif rep == 'xy':
            x = A[0:2, 2]
            return x
        elif rep == 'xyth':
            x = A[0:3, 2]
            th = np.arctan2(A[1, 0], A[0, 0])
            x[2] = th
            return x
        else:
            return A

    def jacob(self, q, index=None):
        if index is None:
            index = self.n
        pe = self.fk(q, rep='cart', index=index)
        J = np.zeros((3, self.n))
        for i in range(index):
            p = self.fk(q, rep='cart', index=i)
            J[:, i] = np.array([p[1] - pe[1], pe[0] - p[0], 0.0])
        return J

    def get_M(self, q):
        M = np.zeros((self.n, self.n))
        for i in range(self.n):
            com = self.coms[i]
            rhat = self.fk(q, index=i, rep='cart') - self.fk(q, index=i + 1, rep='cart')
            r = rhat / np.linalg.norm(rhat) * com
            Jv = self.jacob(q, index=i + 1)
            Jv[:, 0:i+1] = (Jv[:, 0:i+1].T + np.array([-r[1], r[0], 0])).T
            Jw = np.array([1.0 if j <= i else 0.0 for j in range(self.n)])
            M = M + self.ms[i] * Jv.T @ Jv + self.Ins[i] * np.outer(Jw, Jw)
        return M

    def get_G(self, q, g):
        G = np.zeros((self.n,))
        for i in range(self.n):
            com = self.coms[i]
            rhat = self.fk(q, index=i, rep='cart') - self.fk(q, index=i + 1, rep='cart')
            r = rhat / np.linalg.norm(rhat) * com
            Jv = self.jacob(q, index=i + 1)
            Jv[:, 0:i + 1] = (Jv[:, 0:i + 1].T + np.array([-r[1], r[0], 0])).T
            G = G - Jv.T @ g * self.ms[i]
        return G

    def eval_con(self):
        if self.g is None:
            k = 0
            count = 0
            while k < 50 and count < params.p:
                k += 1
                count += 1
                q = np.random.random((params.n,)) * 2 * np.pi - np.pi
                x = self.fk(q, rep='xyth')
                index = self.taskspace.get_pos_index(x)
                if index is not None:
                    try:
                        if self.taskspace[index, 'N'] < 5:
                            k = 0
                        self.taskspace[index, 'N'] += 1
                        self.taskspace[index, 'qs'].append(q)
                        self.taskspace[index, 'xs'].append(x)
                    except IndexError:
                        print(index)

            num_reached = 0
            for x in self.taskspace:
                if x['N'] > 0:
                    num_reached += 1
            self.g = [num_reached / self.taskspace.num_bins]

    def get_monetary_cost(self):
        return params.linear_cost * np.sum(self.x_c) + np.sum([motor['cost'] for motor in self.motors])

    def eval_obj(self):
        if self.f is None:
            for x in self.taskspace:
                for q in x['qs']:
                    J = self.jacob(q)
                    M = self.get_M(q)
                    G = self.get_G(q, params.g)
                    Minv = np.linalg.inv(M)
                    x['mga'].append(get_acceleration_radius(J, Minv, self.T, G, planar=True)[0])
                    # x['static'].append(get_max_static_load(J, self.T, G, params.g))

            mga_score = np.inf
            static_score = np.inf

            for x in self.taskspace:
                if x['N'] > 0:
                    mga_score = min(np.max(x['mga']), mga_score)
                    static_score = min(np.max(x['static']), static_score)

            self.taskspace = None

            cost_score = -(params.linear_cost * np.sum(self.x_c) + np.sum([motor['cost'] for motor in self.motors]))

            self.f = [mga_score, cost_score]

    def clean(self):
        self.taskspace = None


def main(kwargs):

    def callback(alg):
        print(f"Generation: {alg.gen}")

    algorithm = MMO(design_class=PickAndPlaceTask,
                    n_continuous=params.n,
                    n_discrete=params.n,
                    n_obj=2,
                    n_con=1,
                    continuous_bounds=params.link_length_bounds,
                    discrete_bounds=params.motor_dict_bounds,
                    eta_mutation=0.35,
                    initial_constraint=0.65,
                    acceptable_final_constraint=0.95,
                    full_constraint_time=0.5,
                    constraint_adaptive_rate=0.1,
                    parallel=True,
                    callback=callback,
                    **kwargs)

    try:
        sol = algorithm.solve()
    except Exception as e:
        print("Error in optimization step: ", e)
        return 1

    try:
        current_directory = os.getcwd()
        with open(current_directory + "\\optresult.txt", 'w') as f:
            for p in sol.pareto_set:
                f.write(f"{list(p.x_c)}, {list(p.x_d)}, {p.f}, {p.g}\n")

        with open(current_directory + "\\optsol.pkl", 'w+b') as f:
            pickle.dump(sol, f)

    except Exception as e:
        print("Error in file saving process: ", e)
        return 2

    return 0


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Arm optimization script.")
    parser.add_argument('-np', '--n_pop', help="Population number", required=False, default=100, type=int)
    parser.add_argument('-nc', '--n_children', help="Number of children to generate each generation", required=False, default=80, type=int)
    parser.add_argument('-ng', '--n_gen', help="Number of generations to run", required=False, default=20, type=int)
    kwargs = vars(parser.parse_args())
    print(kwargs)
    sys.exit(main(kwargs))
