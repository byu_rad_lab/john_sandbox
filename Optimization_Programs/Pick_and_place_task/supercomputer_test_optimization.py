from multiobjective_mixed_optimizer import MMO, PopulationMember, OptimizationOutput
import numpy as np
import pickle
import sys


class SuperComputerTask(PopulationMember):
    def __init__(self, x_c, x_d):
        super().__init__(x_c, x_d)

    def eval_con(self):
        self.g = [1 - max(self.x_c[0] * self.x_d[0], self.x_c[1] * self.x_d[1])]

    def eval_obj(self):
        f1 = self.x_c[0] * self.x_d[1] + self.x_c[1] * self.x_d[0]
        f2 = self.x_c[0] * self.x_d[0] - self.x_c[1] * self.x_d[1]
        self.f = [f1, f2]


def a():
    n_cont = 2
    n_disc = 2
    cont_bounds = [-5, 5]
    disc_bounds = [-5, 5]

    algorithm = MMO(design_class=SuperComputerTask,
                    n_continuous=n_cont,
                    n_discrete=n_disc,
                    n_obj=2,
                    n_con=1,
                    n_pop=20,
                    n_children=10,
                    n_gen=50,
                    continuous_bounds=cont_bounds,
                    discrete_bounds=disc_bounds,
                    eta_mutation=0.35,
                    initial_constraint=0.25,
                    acceptable_final_constraint=1.0,
                    full_constraint_time=0.25,
                    constraint_adaptive_rate=0.1)

    sol = algorithm.solve()
    with open('optsol.pkl', mode='wb') as file:
        pickle.dump(sol, file)

    for p in sol.pareto_set:
        print(p.x_c, p.x_d, p.f, p.g[0])


if __name__ == '__main__':
    a()
    sys.exit(0)

