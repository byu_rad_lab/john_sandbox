import numpy as np

# Optimization parameters
n = 3
p = 100000
link_length_bounds = (0.01, 1.25)
motor_dict_bounds = (0, 5)
motor_dict = {0: {'mass': 1.0, 'cost': 10, 'tau': 10.0},
              1: {'mass': 2.0, 'cost': 20, 'tau': 20.0},
              2: {'mass': 3.0, 'cost': 20, 'tau': 25.0},
              3: {'mass': 5.0, 'cost': 50, 'tau': 50.0},
              4: {'mass': 7.0, 'cost': 50, 'tau': 60.0},
              5: {'mass': 10.0, 'cost': 80, 'tau': 100.0}}

# Task parameters
xbounds = (-1, 1)
ybounds = (0, 1)
zbounds = (-np.pi, np.pi)
dx = 0.2
dy = 0.1
dz = np.pi / 6

qs = (np.random.random((p, n)) * 2 - 1) * np.pi

# Arm details
linear_density = 1
linear_cost = 1
g = np.array([0., 0, 0])