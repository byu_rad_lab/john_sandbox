"""
Metrics for optimization
Task space model for sampling
Fitness evaluation model
"""
import numpy as np
from itertools import combinations


def get_acceleration_radius(J, Minv, T, G, planar=False):
    L = J @ Minv @ T
    w = J @ Minv @ G
    Tinv = np.linalg.inv(T)
    g = Tinv @ G
    m, n = L.shape

    if np.linalg.norm(g, ord=np.inf) > 1:  # if the arm can't maintain qdd = 0 because of gravity, return penalty.
        return 1 - np.linalg.norm(g, ord=np.inf), None, None

    if planar:
        iterate = range(n)
        k = n
        comb = None
    else:
        comb = [x for x in combinations(range(n), 2)]
        iterate = range(len(comb))
        k = len(comb)

    rs = np.zeros((k,))
    accs_possible = np.zeros((2 * k, m))

    for i in iterate:
        if planar:
            acc_orth = np.cross(L[:, i], np.array([0, 0, 1]))[0:m]
        else:
            acc_orth = np.cross(L[:, comb[i][0]], L[:, comb[i][1]])

        acc_orth = acc_orth / np.linalg.norm(acc_orth)
        c = np.sign(L.T @ acc_orth)
        z1 = acc_orth @ (L @ c + w)
        z2 = acc_orth @ (L @ -c + w)

        z = z1 if np.abs(z1) < np.abs(z2) else z2

        rs[i] = z
        accs_possible[i] = acc_orth * z
        accs_possible[i + k] = acc_orth * z1 if z == z2 else acc_orth * z2

    j = np.argmin(np.abs(rs))
    acc_radius = np.abs(rs[j])
    acc_solved = accs_possible[j]

    return acc_radius, acc_solved, accs_possible


def get_aim_score(J):
    n = J.shape[1]
    Jn = J / np.linalg.norm(J, axis=0)
    Y = np.minimum(np.abs(Jn.T @ Jn), 1) - np.eye(n)
    y = np.arccos(np.max(Y)) / np.pi * 2
    return y


def get_velocity_radius(J):
    m, n = J.shape
    U, S, Vt = np.linalg.svd(J)
    return S[m - 1]


def get_max_static_load(J, T, G, g):
    m, n = J.shape
    b = np.linalg.inv(T) @ G
    if np.max(np.abs(b)) > 1:  # if we can't support the arm itself, report by how much we are deficient
        return 1 - np.max(np.abs(b))
    a = np.linalg.inv(T) @ J[0:3].T @ g
    ms = np.zeros((n,))
    for i in range(n):
        ms[i] = (np.sign(a[i]) - b[i]) / a[i]
    max_load = np.min(ms)
    return max_load


def get_condition_score(J):
    c = np.linalg.cond(J @ J.T)


class TaskSpace:
    def __init__(self, xbound, ybound, zbound, dx, dy, dz, metric_labels=()):

        self.dims = (int(np.ceil((xbound[1] - xbound[0]) / dx)),
                     int(np.ceil((ybound[1] - ybound[0]) / dy)),
                     int(np.ceil((zbound[1] - zbound[0]) / dz)))
        pxmax = (xbound[0] + (self.dims[0] - 1) * dx + xbound[1]) / 2
        pymax = (ybound[0] + (self.dims[1] - 1) * dy + ybound[1]) / 2
        pzmax = (zbound[0] + (self.dims[2] - 1) * dz + zbound[1]) / 2

        self.bins = [
            [[{'N': 0, 'qs': [], 'xs': [], 'index': (i, j, k), 'pos': np.array([min(xbound[0] + (i + 0.5) * dx, pxmax),
                                                                                min(ybound[0] + (j + 0.5) * dy, pymax),
                                                                                min(zbound[0] + (k + 0.5) * dz,
                                                                                    pzmax)])}
              for k in range(self.dims[2])] for j in range(self.dims[1])] for i in range(self.dims[0])]

        if len(metric_labels) > 0:
            for i in range(self.dims[0]):
                for j in range(self.dims[1]):
                    for k in range(self.dims[2]):
                        for label in metric_labels:
                            self[i, j, k, label] = []

        self.bounds = (xbound, ybound, zbound)
        self.deltas = (dx, dy, dz)
        self.iter_index = [0, 0, 0]
        self.num_bins = self.dims[0] * self.dims[1] * self.dims[2]

    def get_pos_index(self, pos):
        for b, p in zip(self.bounds, pos):
            if not b[0] <= p <= b[1]:
                return None
        x = pos[0] - self.bounds[0][0]
        y = pos[1] - self.bounds[1][0]
        z = pos[2] - self.bounds[2][0]
        i = np.trunc(x / self.deltas[0]) if x >= 0 else np.trunc(x / self.deltas[0] - 1)
        j = np.trunc(y / self.deltas[1]) if y >= 0 else np.trunc(y / self.deltas[1] - 1)
        k = np.trunc(z / self.deltas[2]) if z >= 0 else np.trunc(y / self.deltas[2] - 1)
        return int(i), int(j), int(k)

    def __getitem__(self, key: (int, list, tuple, str)):
        key2 = []
        for k in key:
            if not isinstance(k, str) and hasattr(k, '__iter__'):
                key2 = key2 + [x for x in k]
            else:
                key2 = key2 + [k]
        key = key2
        item = None
        for i, k in enumerate(key):
            if i == 0:
                item = self.bins[k]
            else:
                item = item[k]
        return item

    def __setitem__(self, key, ins):
        flattened_key = []
        for k in key:
            if hasattr(k, '__iter__') and not isinstance(k, str):
                flattened_key = flattened_key + [x for x in k]
            else:
                flattened_key = flattened_key + [k]
        key = flattened_key
        item = self.bins[key[0]]
        for i in range(1, len(key) - 1):
            item = item[key[i]]
        item[key[-1]] = ins

    def __iter__(self):
        s = self
        s.iter_index = [0, 0, 0]
        return s

    def __next__(self):
        if self.iter_index[2] > self.dims[2] - 1:
            self.iter_index[1] += 1
            if self.iter_index[1] > self.dims[1] - 1:
                self.iter_index[0] += 1
                if self.iter_index[0] > self.dims[0] - 1:
                    raise StopIteration()
                self.iter_index[1] = 0
            self.iter_index[2] = 0
        out = self[self.iter_index]
        self.iter_index[2] += 1
        return out

    def __str__(self):
        s = 'Task-space bins:\n'
        for x in self:
            s += str(x) + '\n'
        return s

    def __repr__(self):
        s = f'TaskSpace({self.bounds[0]}, {self.bounds[1]}, {self.bounds[2]}, {self.deltas[0]}, {self.deltas[1]}, {self.deltas[2]})'
        return s

    def get_bin_mean(self, key):
        out = np.zeros((self.dims))
        for i in range(self.dims[0]):
            for j in range(self.dims[1]):
                for k in range(self.dims[2]):
                    arr = self.bins[i][j][k][key]
                    if len(arr) > 0:
                        out[i, j, k] = np.mean(arr)
        return out
