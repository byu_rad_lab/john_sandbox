from multiobjective_mixed_optimizer import MMO, PopulationMember
import numpy as np
import matplotlib.pyplot as plt


class TestClass(PopulationMember):
    def __init__(self, x_c, x_d):
        super().__init__(x_c, x_d)

    def eval_con(self):
        if self.g is None:
            self.g = [1 - np.sum(self.x_d * self.x_c)]

    def eval_obj(self):
        if self.f is None:
            self.f = np.asarray([self.x_c[0] * self.x_d[1] / (1 + self.x_c[1]),
                                 self.x_c[1] * self.x_d[0] / (1 + self.x_c[0])])


def callback(algorithm, *args):
    fig, ax = args
    ax.clear()

    pops = algorithm.pop

    feasible_pops = []
    infeasible_pops = []

    for p in pops:
        if p.feasible == 1:
            feasible_pops.append(p)
        else:
            infeasible_pops.append(p)

    k = max([p.domination_rank for p in feasible_pops])

    print(len(feasible_pops), len(infeasible_pops), min([p.g for p in feasible_pops]),
          algorithm.acceptable_constraint_value)

    cs = np.zeros((k, 4))
    cs[:, 3] += 1
    cs[0] = np.array([0., 0., 0., 1])
    for i in range(1, k):
        count = 0
        while (np.min(np.linalg.norm(cs[max(0, i - 5):i, 0:3] - cs[i, 0:3],
                                     axis=1)) < 0.5 and count < 500) or not 0.25 < np.linalg.norm(
            cs[i, 0:3]) < 1.25:
            cs[i, 0:3] = np.random.random((3,))
            count += 1

    colors = np.asarray([cs[p.domination_rank - 1] for p in feasible_pops])
    sizes = np.asarray([p.crowding_score * 500 for p in feasible_pops])
    fs = np.asarray([p.f for p in feasible_pops])

    ax.scatter(fs[:, 0], fs[:, 1], c=colors, s=sizes)
    # ax.scatter()
    plt.pause(0.001)


def main():
    fig, ax = plt.subplots()

    algorithm = MMO(design_class=TestClass,
                    n_continuous=2,
                    n_discrete=2,
                    n_obj=2,
                    n_con=1,
                    n_pop=500,
                    n_gen=50,
                    continuous_bounds=(0, 1),
                    discrete_bounds=(0, 1),
                    n_children=200,
                    eta_mutation=0,
                    acceptable_final_constraint=0.5,
                    initial_constraint=0.4,
                    full_constraint_time=0.25,
                    callback=callback,
                    callback_args=(fig, ax),
                    parallel=False)

    algorithm.solve()
    plt.show()


if __name__ == '__main__':
    main()
