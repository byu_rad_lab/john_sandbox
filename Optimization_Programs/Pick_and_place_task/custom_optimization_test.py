from multiobjective_mixed_optimizer import MMO, PopulationMember
import numpy as np
import params
from source import TaskSpace
import RoboPy as rp
from source import get_acceleration_radius, get_max_static_load, get_condition_score
import matplotlib.pyplot as plt
import pickle


class PickAndPlaceTask(PopulationMember):
    def __init__(self, x_c, x_d):
        super().__init__(x_c, x_d)
        self.n = x_c.shape[0]
        self.taskspace = TaskSpace(params.xbounds, params.ybounds, params.zbounds, params.dx, params.dy, params.dz,
                                   ('mga',))
        self.dh = [[0, 0, a, 0] for a in x_c]
        self.motors = [params.motor_dict[m] for m in x_d]
        self.T = np.diag([motor['tau'] for motor in self.motors])

        self.Ins = []
        self.ms = []
        self.coms = []
        for i in range(self.n):
            a = self.x_c[i]
            lm = params.linear_density * a
            mm = self.motors[i]['mass']
            self.ms.append(mm + lm)

            com = (lm * 0.5 + mm) / (lm + mm) * a
            self.coms.append(com)
            In = lm * (a**2 / 3 - a * com + com**2) + (a - com)**2 * mm
            self.Ins.append(In)

    def fk(self, q, index=None, rep=None):
        if len(q.shape) > 1:
            xs = np.array([self.fk(th, index, rep) for th in q])
            return xs
        A = np.array([[1, 0, 0],
                      [0, 1, -0.5],
                      [0, 0, 1]])
        for th, a in zip(q[0:index], self.x_c[0:index]):
            cth = np.cos(th)
            sth = np.sin(th)
            T = np.array([[cth, -sth, cth * a],
                              [sth, cth, sth * a],
                              [0, 0, 1]])
            A = A @ T
        if rep == 'cart':
            x = A[:, 2]
            x[2] = 0
            return x
        elif rep == 'xy':
            x = A[0:2, 2]
            return x
        elif rep == 'xyth':
            x = A[0:3, 2]
            th = np.arctan2(A[1, 0], A[0, 0])
            x[2] = th
            return x
        else:
            return A

    def jacob(self, q, index=None):
        if index is None:
            index = self.n
        pe = self.fk(q, rep='cart', index=index)
        J = np.zeros((3, self.n))
        for i in range(index):
            p = self.fk(q, rep='cart', index=i)
            J[:, i] = np.array([p[1] - pe[1], pe[0] - p[0], 0.0])
        return J

    def get_M(self, q):
        M = np.zeros((self.n, self.n))
        for i in range(self.n):
            com = self.coms[i]
            rhat = self.fk(q, index=i, rep='cart') - self.fk(q, index=i + 1, rep='cart')
            r = rhat / np.linalg.norm(rhat) * com
            Jv = self.jacob(q, index=i + 1)
            Jv[:, 0:i+1] = (Jv[:, 0:i+1].T + np.array([-r[1], r[0], 0])).T
            Jw = np.array([1.0 if j <= i else 0.0 for j in range(self.n)])
            M = M + self.ms[i] * Jv.T @ Jv + self.Ins[i] * np.outer(Jw, Jw)
        return M

    def get_G(self, q, g):
        G = np.zeros((self.n,))
        for i in range(self.n):
            com = self.coms[i]
            rhat = self.fk(q, index=i, rep='cart') - self.fk(q, index=i + 1, rep='cart')
            r = rhat / np.linalg.norm(rhat) * com
            Jv = self.jacob(q, index=i + 1)
            Jv[:, 0:i + 1] = (Jv[:, 0:i + 1].T + np.array([-r[1], r[0], 0])).T
            G = G - Jv.T @ g * self.ms[i]
        return G

    def eval_con(self):

        if self.g is None:
            k = 0
            count = 0
            while k < 50 and count < params.p:
                k += 1
                count += 1
                q = np.random.random((params.n,)) * 2 * np.pi - np.pi
                x = self.fk(q, rep='xyth')
                index = self.taskspace.get_pos_index(x)
                if index is not None:
                    try:
                        if self.taskspace[index, 'N'] < 5:
                            k = 0
                        self.taskspace[index, 'N'] += 1
                        self.taskspace[index, 'qs'].append(q)
                        self.taskspace[index, 'xs'].append(x)
                    except IndexError:
                        print(index)

            num_reached = 0
            for x in self.taskspace:
                if x['N'] > 0:
                    num_reached += 1
            self.g = num_reached / self.taskspace.num_bins

    def get_monetary_cost(self):
        return params.linear_cost * np.sum(self.x_c) + np.sum([motor['cost'] for motor in self.motors])

    def eval_obj(self):
        if self.f is None:
            for x in self.taskspace:
                for q in x['qs']:
                    J = self.jacob(q)
                    M = self.get_M(q)
                    G = self.get_G(q, params.g)
                    Minv = np.linalg.inv(M)
                    x['mga'].append(get_acceleration_radius(J, Minv, self.T, G, planar=True)[0])
                    # x['static'].append(get_max_static_load(J, self.T, G, params.g))
                    # x['acc'].append(get_condition_score(J))

            # mga_score = np.sum(self.taskspace.get_bin_mean('mga')) / self.taskspace.num_bins
            # static_score = np.sum(self.taskspace.get_bin_mean('static')) / self.taskspace.num_bins

            mga_score = np.inf
            static_score = np.inf

            for x in self.taskspace:
                if x['N'] > 0:
                    mga_score = min(np.max(x['mga']), mga_score)
                    static_score = min(np.max(x['static']), static_score)

            self.taskspace = None

            cost_score = -(params.linear_cost * np.sum(self.x_c) + np.sum([motor['cost'] for motor in self.motors]))

            self.f = [mga_score, cost_score]


def callback(alg: MMO, *args):
    fig, axs, gens, costs, mgas, reachs = args
    pops = []
    for p in alg.pop:
        if p.feasible:
            pops.append(p)
    np.set_printoptions(precision=3, linewidth=1000)
    if len(pops) > 0:
        min_cost = min([p.get_monetary_cost() for p in pops])
        max_mga = max([p.f[0] for p in pops])
        # max_static = max([p.f[1] for p in pops])
        max_reach = max([p.g for p in pops])
    else:
        min_cost = min([p.get_monetary_cost() for p in alg.pop])
        max_mga = 0.001
        # max_static = 0.001
        max_reach = max([p.g for p in alg.pop])

    gens.append(alg.gen)
    costs.append(min_cost)
    mgas.append(max_mga)
    # statics.append(max_static)
    reachs.append(max_reach)

    x = np.array(gens)

    for ax in axs:
        ax.clear()

    axs[0].scatter(x, np.array(costs), c='r', label='min cost')
    axs[1].scatter(x + 0.1, np.array(mgas), c='g', label='max acc')
    # axs[2].scatter(x + 0.2, np.array(statics), c='b', label='max static')
    axs[3].scatter(x + 0.3, np.array(reachs), c='k', label='max reach')

    print(f"Gen {alg.gen}, Acc cv: {alg.acceptable_constraint_value}, max cv: {max([p.g for p in alg.pop])}, median cv: {np.median([p.g for p in alg.pop])}, min cost: {min_cost}, max mga: {max_mga}")  #, max static: {max_static}")

    plt.pause(0.1)


class CallbackObject:
    def __init__(self):
        self.viz = rp.VizScene()
        self.previous_points = np.zeros((0, 3))

    def normalize_points(self, new_points: np.ndarray):
        if new_points.size == 0:
            new_points = np.zeros((0, 3))
            if new_points.size == 0 and self.previous_points.size == 0:
                return np.zeros((1, 3)), np.zeros((1, 3))
        combined_points = np.vstack([self.previous_points, new_points])

        normalized_previous_points = np.zeros_like(self.previous_points)
        normalized_current_points = np.zeros_like(new_points)

        for i in range(3):
            lower = np.min(combined_points[:, i])
            upper = np.max(combined_points[:, i])

            if np.isclose(upper - lower, 0):
                span = 1
            else:
                span = upper - lower

            normalized_previous_points[:, i] = 1 / span * (self.previous_points[:, i] - lower)
            normalized_current_points[:, i] = 1 / span * (new_points[:, i] - lower)

        return normalized_previous_points, normalized_current_points

    def callback(self, alg: MMO):
        pareto_pop = []
        for p in alg.pop:
            if p.feasible:
                if p.domination_rank == 1:
                    pareto_pop.append(p)

        pareto_points = np.asarray([p.f for p in pareto_pop])

        npp, ncp = self.normalize_points(pareto_points)

        self.viz.remove_marker()

        self.viz.add_marker(pos=np.array([[0, 0, 0],
                                          [1, 0, 0],
                                          [0, 1, 0],
                                          [0, 0, 1]]), color=[[1, 1, 1, 1],
                                                              [1, 0, 0, 1],
                                                              [0, 1, 0, 1],
                                                              [0, 0, 1, 1]], size=10)
        self.viz.add_marker(pos=npp, color=[0.25, 0.25, 0.25, 0.25], size=5)
        self.viz.add_marker(pos=ncp, color=[0.5, 1.0, 0.5, 1.0], size=10)

        self.viz.remove_arm()

        if len(pareto_pop) > 0:
            self.previous_points = np.vstack([self.previous_points, pareto_points])

            ind_mga = np.argmax(pareto_points[:, 0])
            ind_static = np.argmax(pareto_points[:, 1])
            ind_cost = np.argmax(pareto_points[:, 2])
            ind_con = np.argmax([p.g for p in pareto_pop])

            self.viz.add_arm(rp.SerialArm(pareto_pop[ind_mga].dh, base=rp.transl([0, 2, 0])))
            self.viz.add_arm(rp.SerialArm(pareto_pop[ind_static].dh, base=rp.transl([0, 2, 1])))
            self.viz.add_arm(rp.SerialArm(pareto_pop[ind_cost].dh, base=rp.transl([0, 2, 2])))
            self.viz.add_arm(rp.SerialArm(alg.pop[ind_con].dh, base=rp.transl([0, 2, 3])))

            print(
                f"Gen {alg.gen}, Acc cv: {alg.acceptable_constraint_value},"
                f" max cv: {max([p.g for p in alg.pop])},"
                f" median cv: {np.median([p.g for p in alg.pop])},"
                f" min cost: {pareto_points[ind_cost]},"
                f" max mga: {pareto_points[ind_mga]},"
                f" max static: {pareto_points[ind_static]}")
        else:
            print(
                f"Gen {alg.gen}, Acc cv: {alg.acceptable_constraint_value},"
                f" max cv: {max([p.g for p in alg.pop])},"
                f" median cv: {np.median([p.g for p in alg.pop])}")

        self.viz.app.processEvents()
        self.viz.hold(5)


def main():

    # fig, ax = plt.subplots()
    # fig.set_figwidth(15)
    # axs = [ax]
    # for i in range(3):
    #     ax2 = ax.twinx()
    #     axs.append(ax2)
    #
    # for i, ax in enumerate(axs):
    #     ax.yaxis.tick_left()
    #     ax.tick_params(axis='y', pad=30*i, colors=['red', 'green', 'blue', 'black'][i])
    #
    # callback_args = (fig, axs, [], [], [], [], [])
    callbackObj = CallbackObject()
    callback_args2 = ()

    algorithm = MMO(design_class=PickAndPlaceTask,
                    n_continuous=params.n,
                    n_discrete=params.n,
                    n_obj=2,
                    n_con=1,
                    n_pop=50,
                    n_gen=20,
                    continuous_bounds=params.link_length_bounds,
                    discrete_bounds=params.motor_dict_bounds,
                    n_children=20,
                    eta_mutation=0.35,
                    initial_constraint=0.25,
                    acceptable_final_constraint=0.95,
                    full_constraint_time=0.75,
                    constraint_adaptive_rate=0.1,
                    callback=None,
                    callback_args=None,
                    parallel=True)

    sol = algorithm.solve()
    print(sol.max_objective_values)
    print(sol.min_objective_values)
    for p in sol.pareto_set:
        print(p.x_c, p.x_d, p.f)

    with open("optimization_result.txt", 'w') as f:
        for p in sol.pareto_set:
            f.write(f"{p.x_c}, {p.x_d}, {p.f}\n")

    with open(f"optsol.pkl", 'w+b') as f:
        pickle.dump(sol, f)

    plt.show()
    callbackObj.viz.hold()


if __name__ == '__main__':
    main()

"""
objectives are: mga, static, -cost, reachable workspace

eta = 0.1  Converged around generation 20
[24.02237058644137, 151.42547163136356, -21.905050179107096, 0.984375]
[1.3701234140457268, 15.706777646865596, -162.01248148893796, 0.953125]

eta = 0.25 convergence around generation 30-35
[23.486471722461467, 180.06096261550792, -21.964278352497214, 0.984375]
[1.4892547515774597, 18.205150250197136, -161.9600142706478, 0.953125]

eta = 0.35 Looked to still be converging
[27.078292623378317, 162.09852268454156, -21.967277041809922, 1.0]
[1.1532368712051955, 16.65888402336625, -162.07764548745254, 0.953125]

eta = 0.5 Possibly still not converged (e.g. still improving at the end)
[25.198676869563, 169.44209568294787, -21.93849573268784, 1.0]
[1.4422748492152255, 15.578073878200788, -162.0139106577115, 0.953125]

eta = 0.75 Probably not converged but clearly quite slow and ineffective
[13.779485767023449, 72.60359990655704, -21.938140971147693, 0.96875]
[1.1286037392925792, 18.715216435562454, -71.99199510868148, 0.953125]


eta = 0.35, 100 gen  hmm, did worse than the 50 gen solution and reveals that this analysis might be flawed by high variance in results
[28.60930326482039, 112.93840298057788, -22.1326347090734, 0.96875]
[1.0864433113780765, 14.037885959948007, -132.12967875071013, 0.953125]

eta = 0.35, 100 gen, but constraints tightened over optimization. I told you it was a good idea! 0.5 - 0.95, tightened at 0.5
[29.447162229685315, 167.25022148729346, -21.869000429433775, 0.984375]
[1.137482416544853, 15.918006177568511, -162.00467092957751, 0.953125]

eta = 0.35, n_gen = 200, n_pop = 400, n_children = 200, tightening from 0.5 - 0.95 at 0.4t
Convergence around generation 140
[30.95964786840437, 182.47806076416896, -21.864511562381928, 1.0]
[1.2010172036558386, 15.444494517897455, -162.08173161305967, 0.96]
"""