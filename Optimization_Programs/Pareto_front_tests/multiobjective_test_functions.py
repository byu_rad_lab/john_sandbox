import numpy as np


# Tests found at https://en.wikipedia.org/wiki/Test_functions_for_optimization

# Binh and Korn function:
# Pareto front is a curved line bowing in towards origin

def binh_and_korn_f(x):
    c = np.array([0, 0])
    c[0] = 4 * x[0]**2 + 4 * x[1]**2
    c[1] = (x[0] - 5)**2 + (x[1] - 5)**2
    return c

# constraints of the form g(x) <= 0
def binh_and_korn_con(x):
    c = np.array([0, 0])
    c[0] = (x[0] - 5)**2 + x[1]**2 - 25
    c[1] = 7.7 - (x[0] - 8)**2 + (x[1] + 3)**2
    return c

binh_and_korn_bounds = [(0, 5), (0, 3)]


# Zitzler-Deb-Thiele's function N.3
# Pareto front is weird, nearly vertical lines with gaps between them

def f1(x):
    return x[0]

def f2(x):
    return g(x) * h(f1(x), g(x))

def g(x):
    y = 1
    y += 9 / 29 * np.sum(x[1:None])
    return y

def h(x, y):
    1 - np.sqrt(x / y) - (x / y) * np.sin(10 * np.pi * x)

def zdt_f(x):
    c = np.array([f1(x), f2(x)])
    return c

zdt_con = None

zdt_bounds = [(0, 1)] * 30


def pymoo_f(x):
    c = np.array([0, 0])
    c[0] = 100 * (x[0]**2 + x[1]**2)
    c[1] = (x[0] - 1)**2 + x[1]**2
    return c

def pymoo_con(x):
    c = np.array([0, 0])
    c[0] = 2 * (x[0] - 0.1) * (x[0] - 0.9)
    c[1] = -1 * (20 * (x[0] - 0.4) * (x[0] - 0.6))
    return c

pymoo_bounds = [(-2, 2), (-2, 2)]
