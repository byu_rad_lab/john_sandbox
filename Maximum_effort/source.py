import numpy as np
import parameters as p

class MsdDynamics:
    '''Class representing a mass-spring-damper system.
    Properties:
    m, mass
    k, spring constant
    b, damping constant
    dt, simulation sampling time
    state, ndarray of floats representing [z, zdot]'''
    def __init__(self, x0=np.array([0.0, 0.0]), m=1.0, k=3, b=0.5, alpha=0.0, dt=0.01, fmax=10., beta=1.0):
        '''__init__(self, x0, m, k, b, alpha, dt)
        x0=[0, 0], initial condition
        m=1.0, mass
        k=3.0, spring const
        b=0.5, damping const
        alpha=0.0, parameter variation (e.g., self.m = m +- rand(alpha)*m
        dt=0.01, time step for sim'''
        self.m = m * np.random.uniform(1 - alpha, 1 + alpha)
        self.k = k * np.random.uniform(1 - alpha, 1 + alpha)
        self.b = b * np.random.uniform(1 - alpha, 1 + alpha)
        self.dt = dt
        self.fmax = fmax

        print(f"System Parameters:\nm: {self.m}\nk: {self.k}\nb: {self.b}")

        self.state = x0
        self.u_ = 0.0
        self.beta = beta

    def _eom(self, state, u):
        z = state[0]
        zd = state[1]
        f = u * self.beta + (1 - self.beta) * self.u_
        xdot = np.zeros_like(self.state)
        xdot[0] = zd
        xdot[1] = -z * self.k / self.m - zd * self.b / self.m + f / self.m
        return xdot

    def update(self, u):
        '''update(self, u)
        u is a float representing the force on the system.'''
        if np.abs(u) > self.fmax:
            u = self.fmax * np.sign(u)

        f1 = self._eom(self.state, u)
        f2 = self._eom(self.state + self.dt/2 * f1, u)
        f3 = self._eom(self.state + self.dt/2 * f2, u)
        f4 = self._eom(self.state + self.dt * f3, u)
        self.state += self.dt / 6 * (f1 + 2 * f2 + 2 * f3 + f4)

        self.u_ = u * self.beta + (1 - self.beta) * self.u_

class PIDControl:
    '''Simple PID controller'''
    def __init__(self, kp, kd, ki, umax, dt=0.01):
        self.kp = kp
        self.kd = kd
        self.ki = ki

        self.umax = umax
        self.dt = dt

        self.int = 0.0
        self.e_ = 0.0

    def get_control(self, r, state):
        z = state[0]
        zd = state[1]

        e = r - z
        self.int += (e + self.e_) / 2 * self.dt
        self.e_ = e

        u = self.kp * e - self.kd * zd + self.ki * self.int

        if np.abs(u) > self.umax:
            u = self.umax * np.sign(u)

        return u

class SSControl:
    '''Simple state space controller'''
    def __init__(self, k, kr, k1, ki, umax, dt=0.01, integral=False):
        self.k = k
        self.kr = kr
        self.k1 = k1
        self.ki = ki
        self.dt = dt
        self.flag = integral
        self.int = 0.0
        self.e_ = 0.0
        self.umax = umax

    def get_control(self, r, state):
        e = r - state[0]
        self.int += (e + self.e_) * self.dt / 2
        self.e_ = e

        if self.flag:
            u = -self.ki * self.int - self.k1 @ state
        else:
            u = self.kr * r - self.k @ state

        u = u.item(0)

        if np.abs(u) > self.umax:
            u = self.umax * np.sign(u)

        return u

class MaxEffort:
    def __init__(self, energy_function, umax, C, beta, D):
        self.f = energy_function
        self.umax = umax
        self.C = C
        self.D = D
        self.beta = beta

        self.u_ = 0.0
        self.flag = True
        self.baseline = 0.0

    def get_control(self, r, state):

        z = state[0]
        zd = state[1]
        e = r - z
        state_r = np.array([r, 0])

        current_energy = self.f(state)
        target_energy = self.f(state_r)
        maximum_work = np.abs(e) * self.umax * self.C
        delta_energy = target_energy - current_energy
        S = abs(delta_energy) - maximum_work

        u = np.abs((e - zd * p.dt) / p.dt**2 * p.m)
        # u = self.umax

        if u > self.umax:
            u = self.umax

        if S < 0:
            u = u * np.sign(e)
        else:
            u = -u * np.sign(zd)

        relay = min(np.abs(e) / self.D, 1)
        u = u * relay

        if self.flag:
            self.u_ = u
            self.flag = False

        u = self.beta * u + (1 - self.beta) * self.u_

        self.u_ = u

        return u
