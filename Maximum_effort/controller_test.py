from source import MsdDynamics
from source import MaxEffort, PIDControl, SSControl
import numpy as np
import matplotlib.pyplot as plt
import parameters as p

x0 = np.array([0., 0.])

max_control = MaxEffort(p.get_msd_energy, p.fmax, p.C, p.beta, p.D)
pid_control = PIDControl(p.kp, p.kd, p.ki_pid, p.fmax)
ss_control = SSControl(p.k0, p.kr, p.k1, p.ki_ss, p.fmax, integral=True)
mass_pid = MsdDynamics(x0=np.copy(x0), m=p.m, b=p.b, k=p.k, alpha=p.alpha, fmax=p.fmax, dt=p.dt, beta=p.motor_beta)
mass_ss = MsdDynamics(x0=np.copy(x0), m=mass_pid.m, b=mass_pid.b, k=mass_pid.k, alpha=0.0, fmax=p.fmax, dt=p.dt, beta=p.motor_beta)
mass_me = MsdDynamics(x0=np.copy(x0), m=mass_pid.m, b=mass_pid.b, k=mass_pid.k, alpha=0.0, fmax=p.fmax, dt=p.dt, beta=p.motor_beta)

stagger_control = []
for i in np.linspace(0.1, 1, 20):
    stagger_control.append(MaxEffort(p.get_msd_energy, p.fmax * i, p.C, p.beta, p.D))

def get_stagger_control(r, state):
    u = 0.0
    for cont in stagger_control:
        u = u + cont.get_control(r, state)

    # u = u / len(stagger_control)
    u = min(abs(u), p.fmax) * np.sign(u)
    return u

def square_wave(t, frequency=0.1, amp=0.0, y_offset=1.0):
    return amp * np.sign(np.sin(t * frequency)) + y_offset

t = 0.

us_pid = [0]
zs_pid = [0]
zds_pid = [0]

us_ss = [0]
zs_ss = [0]
zds_ss = [0]

us_me = [0]
zs_me = [0]
zds_me = [0]

u_in_me = [0]

rs = [0]
ts = [0]

while t < 10.:
    r = square_wave(t)
    d = square_wave(t, y_offset=-2.) * 0

    u_pid = pid_control.get_control(r, mass_pid.state) + d
    mass_pid.update(u_pid)
    us_pid.append(u_pid)
    zs_pid.append(mass_pid.state[0])
    zds_pid.append(mass_pid.state[1])

    u_ss = ss_control.get_control(r, mass_ss.state) + d
    mass_ss.update(u_ss)
    us_ss.append(u_ss)
    zs_ss.append(mass_ss.state[0])
    zds_ss.append(mass_ss.state[1])

    u_me = max_control.get_control(r, mass_me.state) + d
    # u_me = get_stagger_control(r, mass_me.state)
    mass_me.update(u_me)
    holder = us_me[-1]
    yolk = 0.0
    us_me.append(yolk * holder + (1 - yolk) * u_me)
    zs_me.append(mass_me.state[0])
    zds_me.append(mass_me.state[1])

    u_in_me.append(mass_me.u_)

    rs.append(r)
    ts.append(t)
    t += p.dt

fig, ax = plt.subplots(1, 2, figsize=(10, 8))

ax[0].plot(ts, zs_pid, c=[1, 0, 0, 0.5])
ax[0].plot(ts, zs_ss, c=[0, 1, 0, 0.5])
ax[0].plot(ts, zs_me, c=[0, 0, 1, 0.5])
ax[0].plot(ts, rs, c=[0, 0, 0, 1.0])
ax[0].legend(['PID Controller', 'State Space Controller', 'Maximum Effort Controller', 'Reference Command'])

ax[1].plot(ts, us_pid, c=[1, 0, 0, 0.5])
ax[1].plot(ts, us_ss, c=[0, 1, 0, 0.5])
ax[1].plot(ts, us_me, c=[0, 0, 1, 0.5])
ax[1].legend(['PID Controller', 'State Space Controller', 'Maximum Effort Controller'])

fig2, ax2 = plt.subplots()
ax2.plot(ts, u_in_me, c=[0.7, 0.7, 0, 0.5])

plt.show()