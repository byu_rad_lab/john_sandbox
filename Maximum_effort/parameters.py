import numpy as np
import control as cnt

''' System Parameters '''

m = 3.0
k = 3.0
b = 1.0
alpha = 1.0
dt = 0.01

motor_beta = 1.0

fmax = 5.


'''PID Controller Gains'''
tr = 2.0
zeta = 0.9
wn = 0.5 * np.pi / (tr * np.sqrt(1 - zeta**2))
open_loop_char_poly = [1, b / m, k / m]
des_poly = [1, 2 * zeta * wn, wn**2]
kp = (des_poly[2] - open_loop_char_poly[2]) * m
kd = (des_poly[1] - open_loop_char_poly[1]) * m
ki_pid = 5

print("PID CONTROL:")
print("kp: ", kp, ", kd: ", kd, ", ki: ", ki_pid)
print("\n")

'''State Space Controller Gains'''
A = np.array([[0, 1], [-k / m, -b / m]])
B = np.array([[0], [1/m]])
Cr = np.array([1, 0])

A1 = np.block([[A, np.zeros((2, 1))], [-Cr, 0]])
B1 = np.block([[B], [0]])

tr = 2.0
zeta = 0.9
wn = 0.5 * np.pi / (tr * np.sqrt(1 - zeta**2))
des_poly = [1, 2 * zeta * wn, wn**2]
des_poles = np.roots(des_poly)

k0 = np.asarray(cnt.place(A, B, des_poles))
kr = np.asarray(-1 / (Cr @ np.linalg.inv(A - B @ k0) @ B)).item(0)

des_poles = np.block([des_poles, -10])

k1 = np.asarray(cnt.place(A1, B1, des_poles))
ki_ss = k1[0, -1]
k1 = k1[:,0:-1]

print("STATE SPACE CONTROL:")
print("k: ", k0, ", kr: ", kr, ", k1: ", k1, ", ki: ", ki_ss)
print("\n")

'''Maximum Effort Controller Function'''
def get_msd_energy(state):
    z = state[0]
    zd = state[1]
    PE = 0.5 * z ** 2 * k
    KE = 0.5 * zd**2 * m
    return PE + KE

C = 1.0
D = 0.00001
beta = 1.0

print("MAXIMUM EFFORT CONTROL:")
print("Conservative Factor: C = ", C, ", Output filter beta: ", beta)
