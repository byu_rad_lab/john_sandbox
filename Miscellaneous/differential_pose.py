import sympy as sp
import numpy as np
from sympy import sin, cos, Matrix, pprint
from mpmath import radians, degrees
import matplotlib.pyplot as plt
from helper_files.SympyHelpers import ppt

def rx(a):
    return Matrix([[1, 0, 0], [0, cos(a), -sin(a)], [0, sin(a), cos(a)]])

def ry(a):
    return Matrix([[cos(a), 0, sin(a)], [0, 1, 0], [-sin(a), 0, cos(a)]])

def rz(a):
    return Matrix([[cos(a), -sin(a), 0], [sin(a), cos(a), 0], [0, 0, 1]])

def odist(R, Rb=sp.eye(3)):
    dR = R - Rb
    dx = dR[0:3, 0].norm()
    dy = dR[0:3, 1].norm()
    dz = dR[0:3, 2].norm()

    dx = sp.asin(dx / 2) * 2
    dy = sp.asin(dy / 2) * 2
    dz = sp.asin(dz / 2) * 2
    return dx + dy + dz

def oiden(R, Rb=sp.eye(3)):
    dR = R - Rb
    dx = dR[0:3, 0].norm()
    dy = dR[0:3, 1].norm()
    dz = dR[0:3, 2].norm()

    dx = sp.asin(dx / 2) * 2
    dy = sp.asin(dy / 2) * 2
    dz = sp.asin(dz / 2) * 2
    return sp.Matrix([dx, dy, dz])

a, b, c = sp.symbols("a, b, c", real=True)

R = rx(a) @ ry(b) @ rz(c)

ppt(R, wrap_line=False)

phi = oiden(R)

phi.simplify()

ppt(phi)

phid = phi.jacobian([a, b, c])

phid.simplify()

ppt(phid)
