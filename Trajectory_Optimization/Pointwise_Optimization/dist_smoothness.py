import numpy as np
from source import *
from pyoptsparse import Optimization
import param as p


def objFunc(xdict):
    dts = abs(xdict['dts'])
    qs = np.reshape(xdict['qs'], (p.m, p.n))
    poss = qs2poss(qs)
    qds, qdds = deriv(dts, qs)
    funcs = {}
    funcs['obj'] = dist_objective(poss) * 1.0 * dts + smooth_objective(poss) * 3.0  # + path_objective(poss) * 10.0
    funcs['dyncon'] = dynamic_constraint(qs, qds, qdds)
    funcs['endcon'] = end_constraint(qs)
    funcs['pathcon'] = path_constraint(poss)
    funcs['velcon'] = velocity_constraint(qds)
    fail = False

    return funcs, fail


optProb = Optimization("Trajectory Optimization", objFunc)
optProb.addVarGroup("dts", 1,
                    varType='c',
                    lower=p.dt_lb,
                    upper=p.dt_ub,
                    value=p.dt_initial,
                    scale=p.dt_scale)

optProb.addVarGroup("qs", p.m * p.n,
                    varType='c',
                    lower=p.qs_lb,
                    upper=p.qs_ub,
                    value=p.qs_initial,
                    scale=1.0)

optProb.addConGroup('dyncon', p.m * p.n,
                    lower=p.dyn_con_lb,
                    upper=p.dyn_con_ub)

optProb.addConGroup('endcon', 2,
                    lower=p.end_con_lb,
                    upper=p.end_con_ub)

optProb.addConGroup('pathcon', p.m,
                    lower=p.path_con_lb,
                    upper=p.path_con_ub)

optProb.addConGroup('velcon', 1,
                    lower=p.vel_con_lb,
                    upper=p.vel_con_ub)

optProb.addObj('obj')
