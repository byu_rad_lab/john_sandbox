import RoboPy as rp
import numpy as np
import param as p
from dataclasses import dataclass


@dataclass
class TrajResult:
    dts: float = 0.0
    qs: np.ndarray = np.array([])
    pos_start: np.ndarray = np.array([])
    pos_end: np.ndarray = np.array([])
    qs_initial: np.ndarray = np.array([])
    tmin: np.ndarray = np.array([])
    tmax: np.ndarray = np.array([])
    opt_time: float = 0.0


def unpack(x):
    dts = x[0]
    qs = np.reshape(x[1:None], (p.m, p.n))
    return dts, qs


def q2pos(q):
    A = p.arm.fk(q)
    pos = A[0:2, 3]
    return pos


def qs2poss(qs):
    poss = np.zeros((p.m, 2))
    for i, q in enumerate(qs):
        poss[i] = q2pos(q)
    return poss


def deriv(dts, qs):
    # we make an assumption that qd0 = qdd0 = 0
    qds = np.zeros_like(qs)
    qdds = np.zeros_like(qs)
    if not hasattr(dts, '__iter__'):
        dts = np.ones((p.m - 1)) * dts

    for i, q in enumerate(qs):
        if i == 0:
            qds[i] = np.zeros((p.n,))
        else:
            # qds[i] = (q - qs[i - 1]) / dts[i - 1]
            delq = rp.wrap_diff(q, qs[i - 1])
            qds[i] = delq / dts[i - 1]

    for i, qd in enumerate(qds):
        if i == 0:
            qdds[i] = np.zeros((p.n,))
        else:
            # qdds[i] = (qd - qds[i - 1]) / dts[i - 1]
            qdds[i] = (qd - qds[i - 1]) / dts[i - 1]

    return qds, qdds


def dynamic_constraint(qs, qds, qdds):
    taus = np.zeros_like(qs)
    for i in range(p.m):
        tau, wrench = p.arm.rne(qs[i], qds[i], qdds[i], g=p.g)
        tau = (tau - p.tmin) / (p.tmax - p.tmin)
        taus[i] = tau

    con = np.reshape(taus, (p.m * p.n))
    return con


def scipy_dyn_upper(x):
    dts, qs = unpack(x)
    qds, qdds = deriv(dts, qs)
    con = dynamic_constraint(qs, qds, qdds)
    con = -(con - 1.0)
    return con


def scipy_dyn_lower(x):
    dts, qs = unpack(x)
    qds, qdds = deriv(dts, qs)
    con = dynamic_constraint(qs, qds, qdds)
    return con


def end_constraint(qs):
    pos_s = q2pos(qs[0])
    pos_f = q2pos(qs[-1])

    d_s = np.linalg.norm(pos_s - p.pos_start) / p.end_pos_tol
    d_f = np.linalg.norm(pos_f - p.pos_end) / p.end_pos_tol

    return np.array([d_s, d_f])


def scipy_end_upper(x):
    dts, qs = unpack(x)
    con = end_constraint(qs)
    return -(con - 1.0)


def path_constraint(poss):
    v = p.pos_end - p.pos_start
    vhat = v / np.linalg.norm(v)

    ds = np.zeros((p.m,))
    for i, pos in enumerate(poss):
        w = pos - p.pos_start
        wT = w - w @ vhat * vhat
        ds[i] = np.linalg.norm(wT) / p.path_tol

    return ds


def scipy_path_upper(x):
    dts, qs = unpack(x)
    poss = qs2poss(qs)
    con = path_constraint(poss)
    con = -(con - 1.0)
    return con


def velocity_constraint(qds):
    qd_final = np.linalg.norm(qds[-1]) / p.vel_con_tol
    return qd_final


def time_objective(dts):
    # return np.sum(dts)
    return dts * p.m


def dist_objective(poss):
    ds = np.linalg.norm(poss - p.pos_end, axis=1)
    cost = np.sum(ds)
    return cost


def smooth_objective(poss):
    delta = np.zeros((p.m - 1,))
    for i, pos in enumerate(poss):
        if i == 0:
            pass
        else:
            delta[i-1] = np.linalg.norm(pos - poss[i-1])
    cost = np.sum(delta)
    return cost


def path_objective(poss):
    v = p.pos_end - p.pos_start
    vhat = v / np.linalg.norm(v)

    ds = np.zeros((p.m,))
    for i, pos in enumerate(poss):
        w = pos - p.pos_start
        wT = w - w @ vhat * vhat
        ds[i] = np.linalg.norm(wT) / p.path_tol
    cost = np.sum(ds**2)
    return cost


def scipy_obj(x):
    dts, qs = unpack(x)
    poss = qs2poss(qs)
    dist = dist_objective(poss)
    # path = path_objective(poss)
    smooth = smooth_objective(poss)
    cost = 1.0 * dist + 3.0 * smooth
    return cost


def step_constraint(poss):
    ds = np.zeros((p.m - 1))
    pos_prev = 0
    for i, pos in enumerate(poss):
        if i == 0:
            pass
        else:
            ds[i - 1] = np.linalg.norm(pos - pos_prev)
        pos_prev = pos
    return ds / p.step_tol