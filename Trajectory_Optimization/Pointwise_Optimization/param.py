import numpy as np
import RoboPy as rp
import pickle

# Arm subject to trajectory
arm = rp.PlanarDyn(n=2, L=1)
g = np.array([0, 0, 0])

# Optimization Inputs
n = arm.n  # num links
m = 15  # num waypoints
R = 1.75
# pos_start = np.random.random_sample((2,)) * 2 * R - R  # starting point
# pos_end = np.random.random_sample((2,)) * 2 * R - R  # ending point
pos_start = np.array([1.5, 0])
pos_end = np.array([-1.0, -1])

# constraint info
tmax = np.array([1, 10])  # joint torques
tmin = np.array([-1, -10])

dyn_con_lb = np.zeros((m * n,))  # dyn constraint limits
dyn_con_ub = np.ones((m * n,))

end_pos_tol = 1e-4  # distance of final point from target
end_con_lb = None
end_con_ub = np.ones((2,))

vel_con_tol = 1e-2  # tolerance on final velocity
vel_con_lb = None
vel_con_ub = 1.0

path_tol = 1e-2  # deviation of points from straight line path
path_con_lb = None
path_con_ub = 1.0  # np.ones((m,))  # distance is scaled by path_tol

step_tol = np.linalg.norm(pos_start - pos_end) / (m - 1)
step_con_lb = None
step_con_ub = 1.0

# Variable Bounds
qs_ub = np.ones((m * n)) * np.pi * 3
qs_lb = np.ones((m * n)) * -np.pi * 3
dt_ub = 0.5
dt_lb = 1e-4

scipy_bounds = [(-np.pi * 2, np.pi * 2)] * (m * n + 1)
scipy_bounds[0] = (dt_lb, dt_ub)

# Initial Conditions
A_start = rp.transl(np.append(pos_start, 0))
A_end = rp.transl(np.append(pos_end, 0))
q_start = arm.ik(A_start, np.random.random_sample((n,)), rep='cart', method='pinv').qf
q_end = arm.ik(A_end, q_start, rep='cart', method='pinv').qf
q_end = rp.wrap_relative(q_end, q_start)
qs_initial_path = np.linspace(q_start, q_end, m)
# with open('trajectory_results_10_10.obj', 'rb') as file:
#     result = pickle.load(file)
#     qs_initial_path = result.qs
qs_initial = np.reshape(qs_initial_path, (m * n,))
dts_initial = np.ones((m - 1,)) * dt_ub
dt_initial = 5.0
dt_scale = 10

scipy_x0 = np.hstack([np.array([dt_initial]), qs_initial])

