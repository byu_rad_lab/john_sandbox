import RoboPy as rp
import numpy as np
import param as p
from source import *
from pyoptsparse import Optimization, SLSQP, IPOPT, PSQP, NSGA2, CONMIN, ALPSO
import matplotlib.pyplot as plt
from scipy.optimize import minimize
import pickle

# from time_constrained import optProb
# from dist_smoothness import optProb
from multi_stage import optProb1, objfunc2

# print(optProb_time_constrained)
print(optProb1)

optOptions = {"IPRINT": 0}
opt1 = CONMIN(options=optOptions)

sol1 = opt1(optProb1, sens='FD', storeHistory='trajectory1.hst')
print(sol1)
# dts, qs = sol.xStar['dts'], np.reshape(sol.xStar['qs'], (p.m, p.n))
qs = np.reshape(sol1.xStar['qs'], (p.m, p.n))
qds, qdds = deriv(p.dts_initial, qs)
dyn_con = dynamic_constraint(qs, qds, qdds)
print("Dynamics Constraint Initial: ", dyn_con)


def objfunc(x): return objfunc2(x, qs)


optProb2 = Optimization('Stage 2 Optimization', objfunc)
optProb2.addVarGroup('dts', p.m - 1,
                     varType='c',
                     lower=p.dt_lb,
                     upper=p.dt_ub,
                     value=p.dts_initial,
                     scale=1.0)

optProb2.addConGroup('dyncon', p.m * p.n,
                     lower=p.dyn_con_lb,
                     upper=p.dyn_con_ub)

optProb2.addObj('obj')

opt2 = SLSQP(options=optOptions)
print(optProb2)

sol2 = opt2(optProb2, sens='FD', storeHistory='trajectory2.hst')
print(sol2)
dts = sol2.xStar['dts']
print("Time Steps: ", dts)

result = TrajResult(dts=dts, qs=qs, pos_start=p.pos_start, pos_end=p.pos_end, qs_initial=p.qs_initial_path,
                    tmin=p.tmin, tmax=p.tmax, opt_time=sol1.interfaceTime + sol2.interfaceTime)

with open('trajectory_results.obj', 'wb') as filename:
    pickle.dump(result, filename)
