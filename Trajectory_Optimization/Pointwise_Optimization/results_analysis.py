import matplotlib.pyplot as plt
import numpy as np
from numpy import array
from source import *
import pickle
import param as p
import time

import sys
print(sys.path)


with open('trajectory_results.obj', 'rb') as filename:
    result = pickle.load(filename)

dts = result.dts
qs = result.qs
qs_initial_path = result.qs_initial
pos_start = result.pos_start
pos_end = result.pos_end
arm = p.arm
m = qs.shape[0]
n = qs.shape[1]
opt_time = result.opt_time

if hasattr(dts, '__iter__'):
    ts = np.zeros((p.m,))
    for i in range(1, p.m):
        ts[i] = np.sum(dts[0:i])
else:
    ts = np.linspace(0, dts * (m - 1), m)
poss = np.zeros((m, 2))
poss2 = np.zeros((0, 2))
poss_initial = np.zeros((m, 2))
for i in range(m):
    poss[i] = q2pos(qs[i])
    poss_initial[i] = q2pos(qs_initial_path[i])

for i in range(1, m):
    qsec = np.linspace(qs[i - 1], qs[i], 10)
    possec = qs2poss(qsec)
    poss2 = np.vstack([poss2, possec])

print(f"dts: {dts}")
print(f"Path Time: {np.sum(dts)}")
print(f"Optimization Time: {result.opt_time}")

fig, ax = plt.subplots(1, 3)
ax[0].plot(ts, qs[:, 0])
ax[0].plot(ts, qs[:, 1])
ax[0].scatter(ts, qs[:, 0], c='b', marker='.')
ax[0].scatter(ts, qs[:, 1], c='b', marker='.')
ax[0].legend(['joint 1', 'joint 2'])
ax[1].plot(qs[:, 0], qs[:, 1])
ax[1].plot(qs_initial_path[:, 0], qs_initial_path[:, 1])
ax[1].scatter(qs[0, 0], qs[0, 1])
ax[1].scatter(qs[:, 0], qs[:, 1], marker='.', c='b')
ax[1].scatter(qs[-1, 0], qs[-1, 1], marker='x', c='b')
ax[2].plot(poss2[:, 0], poss2[:, 1])
ax[2].plot(poss_initial[:, 0], poss_initial[:, 1])
ax[2].scatter(poss2[0, 0], poss2[0, 1])
ax[2].scatter(poss[-1, 0], poss[-1, 1], marker='x')
ax[2].scatter(pos_end[0], pos_end[1], c='r', marker='x')
ax[2].scatter(pos_start[0], pos_start[1], c='r', marker='*')
ax[0].set_title('Joint position vs time')
ax[1].set_title('Joint space path')
ax[2].set_title('Task space path')
ax[1].legend(['Opt Path', 'Init Path'])
ax[2].legend(['Opt Path', 'Init Path'])
ax[2].axis('equal')

plt.pause(0.1)

viz = rp.VizScene()
viz.add_arm(arm)
viz.add_marker(pos=pos_start, color=np.array([0, 1, 0, 1]), size=100)
viz.add_marker(pos=pos_end, color=np.array([1, 0, 0, 1]), size=100)
while True:
    for i in range(m - 1):
        q_sec = np.linspace(qs[i], qs[i+1], 10)
        for j, q in enumerate(q_sec):
            viz.update(qs=q)
            dt = min(abs(dts[i] / 10), 0.05)
            time.sleep(dt)
