import numpy as np
from source import *
from pyoptsparse import Optimization
import param as p


def objfunc1(xdict):
    qs = np.reshape(xdict['qs'], (p.m, p.n))
    poss = qs2poss(qs)
    funcs = {}
    funcs['obj'] = dist_objective(poss)
    funcs['endcon'] = end_constraint(qs)
    funcs['stepcon'] = step_constraint(poss)
    fail = False

    return funcs, fail


optProb1 = Optimization('Stage 1 Optimization', objfunc1)
optProb1.addVarGroup('qs', p.m * p.n,
                    varType='c',
                    lower=p.qs_lb,
                    upper=p.qs_ub,
                    value=p.qs_initial,
                    scale=1.0)

optProb1.addConGroup('endcon', 2,
                    lower=p.end_con_lb,
                    upper=p.end_con_ub)

optProb1.addConGroup('stepcon', p.m - 1,
                    lower=p.step_con_lb,
                    upper=p.step_con_ub)

optProb1.addObj('obj')


def objfunc2(xdict, qs):
    dts = xdict['dts']
    qds, qdds = deriv(dts, qs)
    funcs = {}
    funcs['obj'] = np.sum(dts)
    funcs['dyncon'] = dynamic_constraint(qs, qds, qdds)
    if np.min(dts) < 0:
        fail = True
    else:
        fail = False

    return funcs, fail


optProb2 = Optimization('Stage 1 Optimization', objfunc1)
optProb2.addVarGroup('dts', p.m - 1,
                    varType='c',
                    lower=p.dt_lb,
                    upper=p.dt_ub,
                    value=p.dts_initial,
                    scale=1.0)

optProb2.addConGroup('dyncon', p.m * p.n,
                    lower=p.dyn_con_lb,
                    upper=p.dyn_con_ub)

optProb2.addObj('obj')






