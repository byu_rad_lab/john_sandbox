import numpy as np
from numpy.linalg import norm
import RoboPy as rp
from pyoptsparse import Optimization, SLSQP, CONMIN, NSGA2, IPOPT
import pyqtgraph as pg
from PyQt5.QtWidgets import QApplication
import time


n = 4
m = 5
arm = rp.PlanarDyn(n=n, L=4/n)
tau_lim = np.linspace(10, 1, n)
tau_ub = np.ravel(np.asarray([tau_lim for i in range(m - 1)]))
tau_lb = -tau_ub

x0 = np.array([3.0, 0.5, 0.0])
xf = np.array([2.0, 2.0, 0.0])
v = (xf - x0)
vhat = v / norm(v)

viz = rp.VizScene()
viz.add_arm(arm)
viz.add_marker(x0, color=[0.8, 0.8, 0.2, 1])
viz.add_marker(xf, color=[0.2, 0.8, 0.8, 1])

xs = np.linspace(x0, xf, m)
q0 = arm.ik(rp.transl(x0), rep='cart', method='pinv').qf
q_init = np.zeros((m, n))
q_init[0] = q0
for i in range(1, m):
    q = arm.ik(rp.transl(x0 + v * i / (m - 1)), q0=q_init[i - 1], rep='cart', method='pinv').qf
    q_init[i] = rp.wrap_relative(q, q_init[i - 1])

q_init_flat = np.ravel(q_init[1:None])
dt_init = np.ones((m - 1,)) * 5 / (m - 1)

q_plots = []
qd_plots = []
tau_plots = []

t_knot = np.asarray([np.sum(dt_init[0:i]) for i in range(m)])

f, fd, fdd = rp.cubic_spline(t_knot, q_init, deriv=True)

p = 100
ts = np.linspace(t_knot[0], t_knot[-1], p)
qs = np.asarray([f(t) for t in ts])
qds = np.asarray([fd(t) for t in ts])
qdds = np.asarray([fdd(t) for t in ts])
tau = np.asarray([arm.rne(q, qd, qdd) for q, qd, qdd in zip(qs, qds, qdds)])
xs = np.asarray([arm.fk(q)[0:2, 3] for q in qs])

app = QApplication([])
w = pg.GraphicsLayoutWidget()
p1 = w.addPlot(row=0, col=1, title='Joint Trajectory')
p2 = w.addPlot(row=0, col=0, title='Joint Speed Trajectory')
p3 = w.addPlot(row=1, col=0, title='Torque Trajectory')
p4 = w.addPlot(row=1, col=1, title='Cartesian Position')
p4.plot([x0[0], xf[0]], [x0[1], xf[1]], pen=None, symbol='x')
p4.setAspectLocked()
p1.addLegend()
p2.addLegend()
p3.addLegend()

for i in range(n):
    q_plots.append(p1.plot(ts, qs[:, i], pen=(i, n), name=f'q {i}'))
    qd_plots.append(p2.plot(ts, qds[:, i], pen=(i, n), name=f'qd {i}'))
    tau_plots.append(p3.plot(ts, tau[:, i], pen=(i, n), name=f'tau {i}'))

cart_plot = p4.plot(xs[:, 0], xs[:, 1], name='Arm Path')

w.show()
app.processEvents()
time.sleep(0.001)

def optfunc(xdict):
    if 'q_flat' in xdict.keys():
        q_flat = xdict['q_flat']
        q_knot = np.reshape(q_flat, (m - 1, n))
        q_knot = np.vstack([q0, q_knot])
    else:
        q_knot = q_init
    dt = np.abs(xdict['dt'])
    t_knot = np.asarray([np.sum(dt[0:i]) for i in range(m)])
    print(t_knot)

    f, fd, fdd = rp.cubic_spline(t_knot, q_knot, deriv=True)
    # f, fd, fdd = rp.quintic_spline(t_knot, q_knot, deriv=True, cost='minjerk')

    tau_con = np.zeros((m - 1, n))
    path_con = np.zeros((m - 1,))
    for i in range(1, m):
        ts = np.linspace(t_knot[i - 1], t_knot[i], 20, endpoint=False)
        tau_max = np.zeros((n,))
        d_max = 0.0
        for t in ts:
            q, qd, qdd = f(t), fd(t), fdd(t)
            tau = arm.rne(q, qd, qdd)
            for j in range(n):
                if np.abs(tau[j]) > np.abs(tau_max[j]):
                    tau_max[j] = tau[j]
            x = arm.fk(q)[0:3, 3]
            w = x - x0
            d = norm(w - w @ vhat * vhat)
            if d > d_max:
                d_max = d
        tau_con[i - 1] = tau_max
        path_con[i - 1] = d_max

    start_con = arm.fk(q_knot[0])[0:2, 3]
    end_con = norm(arm.fk(q_knot[-1])[0:2, 3] - xf[0:2])
    output = {}
    output['time'] = t_knot[-1]
    output['torque'] = np.ravel(tau_con)
    output['path'] = path_con
    output['start'] = start_con
    output['end'] = end_con

    p = 100
    ts = np.linspace(t_knot[0], t_knot[-1], p)
    qs = np.asarray([f(t) for t in ts])
    qds = np.asarray([fd(t) for t in ts])
    qdds = np.asarray([fdd(t) for t in ts])
    tau = np.asarray([arm.rne(q, qd, qdd) for q, qd, qdd in zip(qs, qds, qdds)])
    xs = np.asarray([arm.fk(q)[0:2, 3] for q in qs])

    for i in range(n):
        q_plots[i].setData(ts, qs[:, i])
        qd_plots[i].setData(ts, qds[:, i])
        tau_plots[i].setData(ts, tau[:, i])

    cart_plot.setData(xs[:, 0], xs[:, 1])

    app.processEvents()
    time.sleep(0.001)
    return output, False

optProb = Optimization("Global Starting Optimization", optfunc)
optProb.addVarGroup('dt', m - 1,
                    varType='c',
                    lower=np.ones((m - 1,)) * 0.05,
                    upper=np.ones((m - 1,)) * 5,
                    value=dt_init)
# optProb.addVarGroup('q_flat', m * n - n,
#                     varType='c',
#                     lower=q_init_flat - np.ones((m * n - n,)) * 0.5,
#                     upper=q_init_flat + np.ones((m * n - n,)) * 0.5,
#                     value=q_init_flat)
optProb.addConGroup('torque', n * m - n,
                    lower=tau_lb,
                    upper=tau_ub)
# optProb.addConGroup('path', m - 1,
#                     upper=np.ones((m - 1,)) * 0.1)
# optProb.addConGroup('start', 2,
#                     upper=x0[0:2],
#                     lower=xf[0:2])
# optProb.addConGroup('end', 1,
#                     upper=0.01)
optProb.addObj('time')

opt_slsqp = SLSQP(options={'IPRINT': 0, 'ACC': 1e-2, 'MAXIT': 50})
opt_conmin = CONMIN(options={'IPRINT': 1, 'ITMAX': 50, 'DABFUN': 1e-3, 'DELFUN': 1e-4})
opt_nsga2 = NSGA2(options={'PopSize': 4, 'maxGen': 60, 'PrintOut': 1})
opt_ipopt = IPOPT(options={'max_wall_time': 300.0, 'tol': 1e-1, 'constr_viol_tol': 1e-2, 'print_level': 0})
# sol = opt_nsga2(optProb, sens='FD')
sol = opt_ipopt(optProb, sens='FD')
# sol = opt_conmin(optProb, sens='FD')
print(sol)

if 'q_flat' in sol.xStar.keys():
    q_flat = sol.xStar['q_flat']
    q_knot = np.reshape(q_flat, (m - 1, n))
    q_knot = np.vstack([q0, q_knot])
else:
    q_knot = q_init
dt = np.abs(sol.xStar['dt'])
t_knot = np.asarray([np.sum(dt[0:i]) for i in range(m)])
f, fd, fdd = rp.cubic_spline(t_knot, q_knot, deriv=True)
# f, fd, fdd = rp.quintic_spline(t_knot, q_knot, deriv=True, cost='minjerk')

print(f"Total Time: {t_knot[-1]}")

p = 500
ts = np.linspace(t_knot[0], t_knot[-1], p)
qs = np.asarray([f(t) for t in ts])
qds = np.asarray([fd(t) for t in ts])
qdds = np.asarray([fdd(t) for t in ts])
tau = np.asarray([arm.rne(q, qd, qdd) for q, qd, qdd in zip(qs, qds, qdds)])
xs = np.asarray([arm.fk(q)[0:2, 3] for q in qs])

for i in range(n):
    q_plots[i].setData(ts, qs[:, i])
    qd_plots[i].setData(ts, qds[:, i])
    tau_plots[i].setData(ts, tau[:, i])

cart_plot.setData(xs[:, 0], xs[:, 1])

app.processEvents()
time.sleep(0.001)

while True:
    for q in qs:
        viz.update(qs=q)
        time.sleep(ts[-1] / p)

# 2.46, 24 pop and 10 gen, time ~= 60
# 2.06, 80 pop and 100 gen, time = 1700  Note time is approximately 0.21 seconds per (pop x gen)
# 2.07, 12 pop and 120 gen, time = 305  Time is consistent, I picked this to be about 300 seconds
# 2.28, 4 pop and 100 gen, time = 78.4
# 5.28, 100 pop and 4 gen, time = 85.85
# 2.84, 12 pop and 25 gen, time = 66.5
# 4.22, 8 pop and 40 gen, time = 73
# 2.6, 4 pop and 60 gen, time = 56.5


