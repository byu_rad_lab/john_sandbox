from cmath import pi
import scipy.optimize
import numpy as np
import matplotlib.pyplot as plt
import RoboPy as rp
from copy import deepcopy
import matplotlib.pyplot as plt
from RoboPy import VizScene
import time


class TaskSpaceOpt:
    def __init__(self, robot, Q, R, H, dt, totalTime, pe, tau_ub):

        # robot
        self.robot = robot
        self.numJoints = self.robot.n        

        # time
        self.dt = dt   
        self.t = np.arange(0, totalTime + self.dt, self.dt)

        # desired Cartesian position
        self.pe = pe

        # Cost Function Params
        self.Q = Q 
        self.R = R
        self.H = H

        # Constraints
        self.tau_lb = -1.*np.array([tau_ub, tau_ub])
        self.tau_ub = np.array([tau_ub, tau_ub])
        self.ub = np.array([2*pi, 2*pi])
        self.lb = -1.*np.array([2*pi, 2*pi])

        # Visualization
        self.viz = VizScene()
        self.viz.add_arm(self.robot, draw_frames=False)

    def simulate(self, q0, qd0):

        # initial and current position and velocity of robot
        qPrev = q0 # this is a "bad" estimate on the i = 1
        qCurr = q0
        qdCurr = qd0
        
        # trajectory history lists
        qHist = np.zeros((np.size(self.t), self.numJoints))
        qHist[0,:] = qCurr
        qdHist = np.zeros((np.size(self.t), self.numJoints))
        qdHist[0,:] = qdCurr

        xHist = np.zeros(np.size(self.t))
        xHist[0] = self.robot.fk(qCurr)[0,3]
        yHist = np.zeros(np.size(self.t))
        yHist[0] = self.robot.fk(qCurr)[1,3]
        zHist = np.zeros(np.size(self.t))
        zHist[0] = self.robot.fk(qCurr)[2,3]

        # calculate optimal trajectory
        for i in range(0, np.size(self.t) - 1):  

            print('i: ', i)

            # constraints
            tauConst = scipy.optimize.NonlinearConstraint(lambda q: self.tau_const(q, qCurr, qdCurr), self.tau_lb, self.tau_ub)
            boundsConst = scipy.optimize.Bounds(self.lb, self.ub)

            # optimal q at next timestep
            qNext = scipy.optimize.minimize(self.cost_func, qCurr, args=(qCurr), method='SLSQP', constraints=[tauConst], bounds=boundsConst, options={'eps': 1e-06})
            qNext = qNext.x

            # calculate qdot at next timestep
            qdNext = (qNext - qCurr)/self.dt
            # qdNext = (3*qNext - 4*qCurr + qPrev)/(2*self.dt)

            # saving states to list
            qHist[i + 1,:] = qNext
            qdHist[i + 1,:] = qdNext
            xHist[i + 1] = self.robot.fk(qNext)[0,3]
            yHist[i + 1] = self.robot.fk(qNext)[1,3]
            zHist[i + 1] = self.robot.fk(qNext)[2,3]

            # updating states
            qCurr = qNext
            qdCurr = qdNext

            # print(qdCurr)
        
        # end conditions 
        print(qHist, '\n')
        print('Final Joint Angles: ', qHist[np.size(self.t) - 1,:], '\n')
        print('Final Pose: ', self.robot.fk(qHist[np.size(self.t) - 1,:])[0:3,3], '\n')

        # visualize robot
        self.visualize(qHist)

        # plot graphs
        self.plotting(xHist, yHist, zHist, qHist) 

    def visualize(self, qHist):

        for i in range(0, np.size(self.t)): 

            self.viz.update(qHist[i,:]) 
            time.sleep(.1)

    def plotting(self, xHist, yHist, zHist, qHist):        

        fig, ax = plt.subplots(3, 1, figsize=(18,12))
        ax[0].plot(self.t, xHist, 'r-', self.t, yHist, 'b-', self.t, zHist, 'g-')
        ax[0].plot(self.t, self.pe[0]*np.ones(np.size(self.t)), 'c--', self.t, self.pe[1]*np.ones(np.size(self.t)), 'y--', self.t, self.pe[2]*np.ones(np.size(self.t)), 'm--')
        ax[0].legend(['x', 'y', 'z', 'xref', 'yref', 'zref'], loc='center right')
        ax[0].set_xlabel('time')
        ax[0].set_ylabel('Cartesian position')

        ax[1].plot(xHist, yHist)
        ax[1].plot(xHist[0], yHist[0], 'r*', xHist[np.size(self.t)-1], yHist[np.size(self.t)-1], 'g*', self.pe[0], self.pe[1], 'k*')
        ax[1].set_xlabel('x')
        ax[1].set_ylabel('y')
        ax[1].axis('equal')

        ax[2].plot(self.t, qHist[:,0], self.t, qHist[:,1])
        ax[2].set_xlabel('time')
        ax[2].set_ylabel('Joint Angles (radians)')
        ax[2].legend(['q1', 'q2'])

        plt.show()               

        return None

    def cost_func(self, qNext, qCurr):

        # Location of end-effector
        p = self.robot.fk(qNext)[0:3, 3].reshape(3,1)
        pLast = self.robot.fk(qCurr)[0:3,3].reshape(3,1)

        # cost
        qNext = qNext.reshape(self.numJoints, 1)
        qCurr = qCurr.reshape(self.numJoints, 1)

        J = (p - self.pe).T @ self.Q @ (p - self.pe) + (qNext - qCurr).T @ self.R @ (qNext - qCurr) + (p - pLast).T @ self.H @ (p - pLast)

        return J.squeeze()

    def tau_const(self, qNext, qCurr, qdCurr):

        # derivatives for RNE
        qdNext = (qNext - qCurr)/self.dt
        qddNext = (qdNext - qdCurr)/self.dt

        # torques
        tau_rne, wrench = self.robot.rne(qNext, qdNext, qddNext)
        tau = tau_rne

        # print('qNext: ', qNext, '\n')
        # print('qdNext', qdNext, '\n')
        # print('qddNext', qddNext, '\n')
        # print('tau: ', tau, '\n', '\n')

        return tau

if __name__ == "__main__":

    # robot params
    dh = [[0, 0, 1, 0], [0, 0, 1, 0]]
    mass = [1, 1]
    r_coms = [np.array([-0.5, 0, 0]), np.array([-0.5, 0, 0])]
    link_inertias = [0.*np.identity(3), 0.*np.identity(3)]
    robot = rp.SerialArmDyn(dh, mass=mass, r_com=r_coms, link_inertia=link_inertias)

    # time params
    dt = .1
    totalTime = 3

    # optimization params
    Q = np.identity(3)
    R = 0.*np.identity(2)
    H = 3.*np.identity(3)
    tau_ub = 10.

    # desired end-effector position in Cartesian space
    x = .707
    y = 1.707
    z = 0.
    pe = np.array([[x, y, z]]).T

    # inital conditions
    q0 = np.array([0., 0.])
    qd0 = np.array([0, 0])

    # class object
    controller = TaskSpaceOpt(robot, Q, R, H, dt, totalTime, pe, tau_ub)

    # simulate
    controller.simulate(q0, qd0)
