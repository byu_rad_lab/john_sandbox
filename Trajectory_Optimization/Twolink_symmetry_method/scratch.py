import numpy as np
import RoboPy as rp
import matplotlib.pyplot as plt


dh = [[0, 0, 1, 0], [0, 0, 1, 0]]
arm = rp.SimpleDynArm(dh, linear_density=1.0, motor_mass=[1.0, 0.5])
T = np.diag([1.0, 0.5])
Tinv = np.linalg.inv(T)
g = np.array([0, 0, 0])

dt = 1e-3

x0 = np.array([1.0, 0.0])
xf = np.array([0.0, 1.0])

sol = arm.ik(x0, retry=5)
print(sol)
q0 = sol.qf
qs = arm.ik(xf, q0, maxdel=0.01).qs
qf = qs[-1]


#forward pass
def f(q, qd):
    M, C, G = arm.get_MCG(q, qd, g)
    Minv = np.linalg.inv(M)
    J = arm.jacob(q, rep='xy')
    L = J @ Minv @ T
    Linv = np.linalg.inv(L)
    w = J @ Minv @ (G + C @ qd)
    x = arm.fk(q, rep='xy')
    vhat = (xf - x) / np.linalg.norm(xf - x)
    xd = J @ qd
    ahat = vhat
    ahat = ahat / max(np.linalg.norm(ahat), 0.01)
    tau = Linv @ ahat - Linv @ w
    qdd = Minv @ (tau - C @ qd - G)
    return qd, qdd


xi = x0
qi = q0
qdi = np.zeros((2,))

qpath = []
qdpath = []
xpath = []

t = 0

viz = rp.VizScene()
viz.add_arm(arm)
viz.add_marker([1, 0, 0])
viz.add_marker([0, 1, 0])

while np.linalg.norm(xi - xf) > 1e-2:
    qdnext, qddnext = f(qi, qdi)
    qi = qi + qdnext * dt + 0.5 * qddnext * dt**2
    qdi = qdi + qddnext * dt
    xi = arm.fk(qi, rep='xy')

    qpath.append(qi)
    qdpath.append(qdi)
    xpath.append(xi)

    viz.update(qi)
    viz.hold(dt)

print('finished')

while True:
    for q in qpath:
        viz.update(q)
        viz.hold(0.01)
    viz.hold(1.0)
