import RoboPy as rp
from scipy.optimize import minimize
import numpy as np
from pyoptsparse import Optimization, SLSQP, CONMIN, NSGA2, IPOPT
import pyqtgraph as pg
from PyQt5.QtWidgets import QApplication

n = 1
m = 5

arm = rp.PlanarDyn(n=n, L=1.0)
tau_lim = np.linspace(10, 2, n)

q_knot = np.random.random_sample((m, n)) * 3 - 1.5
dts = np.random.random_sample((m - 1,)) * 2 + 0.2
t_knot = np.asarray([np.sum(dts[0:i]) for i in range(m)])


def unpack(x):
    qds = np.reshape(x, (m - 2, n))
    qds = np.vstack([np.zeros((n,)), qds, np.zeros((n,))])
    return qds


def optfunc(x):
    qds = unpack(x)
    f, fd, fdd = rp.cubic_spline(t_knot, q_knot, qds, deriv=True)

    cost = 0.0
    p = 100
    ts = np.linspace(t_knot[0], t_knot[-1], p)
    for t in ts:
        cost += np.linalg.norm(fdd(t))**4 / p
    print(cost)
    return cost

sol = minimize(optfunc, np.zeros((m - 2, n)), method='BFGS', options={'disp': True})
qd_knot = unpack(sol.x)
# qd_knot = np.zeros_like(q_knot)

q_knot = np.squeeze(q_knot)
qd_knot = np.squeeze(qd_knot)

f, fd, fdd = rp.cubic_spline(t_knot, q_knot, qd_knot, deriv=True)
g, gd, gdd = rp.cubic_spline(t_knot, q_knot, deriv=True)

ps = np.linspace(t_knot[0] - 1, t_knot[-1] + 1, 500)
zs = np.squeeze(np.asarray([f(p) for p in ps]))
zds = np.squeeze(np.asarray([fd(p) for p in ps]))
zdds = np.squeeze(np.asarray([fdd(p) for p in ps]))

qs = np.squeeze(np.asarray([g(p) for p in ps]))
qds = np.squeeze(np.asarray([gd(p) for p in ps]))
qdds = np.squeeze(np.asarray([gdd(p) for p in ps]))


app = QApplication([])
window = pg.GraphicsLayoutWidget()
p1 = window.addPlot(col=0, row=0, title='qd_knot')
p1.plot(t_knot, q_knot, pen=None, symbol='x')
p1.plot(ps, zs, pen=(0,3))
p1.plot(ps, zds, pen=(1,3))
p1.plot(ps, zdds, pen=(2,3))

p2 = window.addPlot(col=1, row=0, title='no input')
p2.plot(t_knot, q_knot, pen=None, symbol='x')
p2.plot(ps, qs, pen=(0,3))
p2.plot(ps, qds, pen=(1,3))
p2.plot(ps, qdds, pen=(2,3))

window.show()
app.exec()