import numpy as np
import RoboPy as rp
from PyQt5.QtWidgets import QApplication
import pyqtgraph as pg


ys = np.array([0, 1, 0, 1, 2, 0, 2, 3, 3.5])
xs = np.array([0, 0.75, 2, 3, 4, 5.3, 6, 7, 8])

pf, pfd, pfdd = rp.cubic_spline(xs, ys, deriv=True)

ps = np.linspace(-2, 10, 500)
zs = []
zds = []
zdds = []
for p in ps:
    zs.append(pf(p))
    zds.append(pfd(p))
    zdds.append(pfdd(p))

zs = np.squeeze(np.asarray(zs))
zds = np.squeeze(np.asarray(zds))
zdds = np.squeeze(np.asarray(zdds))

app = QApplication([])
window = pg.GraphicsLayoutWidget()
p1 = window.addPlot(col=0, row=0, title='Splined')
p1.plot(xs, ys, pen=None, symbol='x')
p1.plot(ps, zs, pen=(0,3))
p1.plot(ps, zds, pen=(1,3))
p1.plot(ps, zdds, pen=(2,3))
window.show()
app.exec()
