import numpy as np
from pyoptsparse import Optimization, SLSQP
import matplotlib.pyplot as plt
import RoboPy as rp


t0, tf = 0, 1
x0, xf = 0, 1
v0, vf = 0, 0
a0, af = 0, 0,

dt = 0.01
ts = np.arange(t0, tf, dt)

def f(t, c, d):
    y = c[0] + d[0] * t
    for i in range(1, 6):
        y += c[i] * np.sin(t * d[i])
    return y

def fd(t, c, d):
    y = d[0]
    for i in range(1, 6):
        y += c[i] * np.cos(t * d[i]) * d[i]
    return y

def fdd(t, c, d):
    y = 0.0
    for i in range(1, 6):
        y += -c[i] * np.sin(t * d[i]) * d[i]**2
    return y

def optfunc(xdict):
    c = xdict['c']
    d = xdict['d']
    output = {}
    H = 0.0
    for t in ts:
        H += fdd(t, c, d)**2 * dt
    output['obj'] = H
    output['x0'] = f(t0, c, d)
    output['xf'] = f(tf, c, d)
    output['v0'] = fd(t0, c, d)
    output['vf'] = fd(tf, c, d)
    output['a0'] = fdd(t0, c, d)
    output['af'] = fdd(tf, c, d)
    return output, False

optProb = Optimization('Minimum Acceleration', optfunc)
optProb.addVarGroup('c', 6,
                    varType='c',
                    value=np.ones((6,)))
optProb.addVarGroup('d', 6, varType='c',
                    value=np.ones((6,)))

optProb.addCon('x0', lower=x0, upper=x0)
optProb.addCon('xf', lower=xf, upper=xf)
optProb.addCon('v0', lower=v0, upper=v0)
optProb.addCon('vf', lower=vf, upper=vf)
optProb.addCon('a0', lower=a0, upper=a0)
optProb.addCon('af', lower=af, upper=af)
optProb.addObj('obj')

optimizer = SLSQP({'IPRINT': 0})

sol = optimizer(optProb, sens='FD')
c = sol.xStar['c']
d = sol.xStar['d']
print(sol)
print(sol.xStar['c'], sol.xStar['d'])

def f(t):
    y = c[0] + d[0] * t
    for i in range(1, 6):
        y += c[i] * np.sin(t * d[i])
    return y

def fd(t):
    y = d[0]
    for i in range(1, 6):
        y += c[i] * np.cos(t * d[i]) * d[i]
    return y

def fdd(t):
    y = 0.0
    for i in range(1, 6):
        y += -c[i] * np.sin(t * d[i]) * d[i]**2
    return y


# f, fd, fdd = rp.minimum_acceleration_interpolation(t0, tf, x0, xf, v0, vf, a0, af, deriv=True)

fig, ax = plt.subplots()
ys = np.zeros_like(ts)
yds = np.zeros_like(ts)
ydds = np.zeros_like(ts)

for i, t in enumerate(ts):
    ys[i] = f(t)
    yds[i] = fd(t)
    ydds[i] = fdd(t)

ax.plot(ts, ys)
ax.plot(ts, yds)
ax.plot(ts, ydds)
plt.show()
