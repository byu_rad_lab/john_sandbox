import RoboPy as rp
import numpy as np
from numpy.linalg import norm
from pyoptsparse import Optimization, SLSQP, CONMIN, NSGA2, IPOPT
from scipy.optimize import minimize


def get_joint_trajectory(arm, x0, xf, q0=None, dev_tol=0.05, p=None, viz=None):

    if p is not None:
        n = arm.n
        xs = np.linspace(x0, xf, p)
        qs = np.zeros((p, n))
        if q0 is None:
            qs[0] = arm.ik(rp.transl(xs[0]), q0=np.zeros((n,)), rep='cart', method='pinv')
        else:
            qs[0] = q0
        for i in range(1, p):
            sol = arm.ik(rp.transl(xs[i]), q0=qs[i - 1], rep='cart', method='pinv')
            q = sol.qf
            qs[i] = rp.wrap_relative(q, qs[i - 1])
            # qs[i] = rp.wrap_angle(q)
            if viz is not None:
                viz.add_marker(arm.fk(q)[0:3, 3], color=[0, 0.7, 0, 1])
                viz.update(q)
        return qs

    if q0 is None:
        q0 = arm.ik(rp.transl(x0), q0, rep='cart', method='pinv').qf
    else:
        pass
    n = arm.n
    v = xf - x0
    vhat = v / norm(v)
    qf = arm.ik(rp.transl(xf), q0, rep='cart', method='pinv').qf
    q_knot = np.vstack([q0, qf])
    alpha = [0., 1.]

    dev = 0.0
    m = 2

    while dev > dev_tol or m == 2:
        qs = np.zeros((0, n))
        for i in range(1, m):
            qnew = rp.angle_linspace(q_knot[i - 1], q_knot[i], 50, endpoint=True)
            qs = np.vstack([qs, qnew])

        ds = np.zeros((50 * m,))
        for i, q in enumerate(qs):
            x = arm.fk(q)[0:3, 3]
            w = x - x0
            ds[i] = norm(w - w @ vhat * vhat)

        pos = np.argmax(ds)
        dev = ds[pos]

        if dev > dev_tol or m < 3:
            t = pos / (50 * m)
            index = 1
            for i, a in enumerate(alpha):
                index = i
                if t < a:
                    break
            alpha = np.insert(alpha, index, t)
            m += 1
            q_knot = q_knot[0:index]
            for i in range(index, m):
                xt = x0 + alpha[i] * v
                sol = arm.ik(rp.transl(xt), q0=q_knot[i - 1], rep='cart', method='pinv')
                q_knot = np.vstack([q_knot, rp.wrap_relative(sol.qf, q_knot[-1])])
                if viz is not None and i == index:
                    viz.add_marker(arm.fk(sol.qf)[0:3, 3], color=[0, 0.7, 0, 1])
                    viz.update(sol.qf)

    return q_knot


def scale_traj_dynamic(arm, q_knot, tau_lim, t0=0.0):
    m, n = q_knot.shape[0], q_knot.shape[1]
    qds = None
    def optfunc(xdict):
        dt = xdict['dt']
        dt = np.abs(dt) + 1e-3
        t_knot = np.asarray([np.sum(dt[0:i]) for i in range(m)]) + t0

        f, fd, fdd = rp.cubic_spline(t_knot, q_knot, deriv=True)
        # f, fd, fdd = rp.quintic_spline(t_knot, q_knot, deriv=True, cost='minacc')

        tau_con = np.zeros((m - 1, n))
        for i in range(1, m):
            ts = np.linspace(t_knot[i-1], t_knot[i], 12, endpoint=False)
            tau_max = np.zeros((n,))
            for t in ts:
                q, qd, qdd = f(t), fd(t), fdd(t)
                tau = arm.rne(q, qd, qdd)
                for j in range(n):
                    if np.abs(tau[j]) > np.abs(tau_max[j]):
                        tau_max[j] = tau[j]
            tau_con[i - 1] = tau_max

        output = {}
        output['time'] = t_knot[-1] - t0
        output['torque'] = np.ravel(tau_con)
        return output, False

    optProb = Optimization("Opt", optfunc)
    optProb.addVarGroup('dt', m - 1,
                        varType='c',
                        lower=np.ones((m - 1,)) * 0.05,
                        upper=np.ones((m - 1,)) * 5.0,
                        value=np.ones((m - 1,)))
    tau_ub = np.ravel(np.asarray([tau_lim for i in range(m - 1)]))
    tau_lb = -tau_ub
    optProb.addConGroup('torque', n * m - n,
                        lower=tau_lb,
                        upper=tau_ub)
    optProb.addObj('time')
    # opt = SLSQP(options={'IPRINT': -1, 'ACC': 1e-2, 'MAXIT': 50})
    # opt = CONMIN(options={'IPRINT': 0, 'ITMAX': 50, 'DABFUN': 1e-2, 'DELFUN': 1e-2})
    # opt = NSGA2(options={'PopSize': 24, 'maxGen': 10})
    opt = IPOPT(options={'tol': 1e0, 'constr_viol_tol': 1e-2, 'print_level': 2,
                         'dual_inf_tol': 1.0, 'compl_inf_tol': 1e-4,
                         'max_wall_time': 10.0 * m})
    sol = opt(optProb, sens='FD')
    # print(sol.optTime)
    dt_final = sol.xStar['dt']
    dt_final = np.abs(dt_final)
    t_knot = np.asarray([np.sum(dt_final[0:i]) for i in range(m)]) + t0
    # print(t_knot)

    f, fd, fdd = rp.cubic_spline(t_knot, q_knot, deriv=True)
    # f, fd, fdd = rp.quintic_spline(t_knot, q_knot, deriv=True, cost='minacc')
    return t_knot, f, fd, fdd


def get_dynamic_path(arm, xs, tau_lim, path_step=0.3, dev_tol=None, viz=None):
    n = arm.n
    m = len(xs)
    funcs = [lambda t: np.zeros((n,))]
    dfuncs = [lambda t: np.zeros((n,))]
    ddfuncs = [lambda t: np.zeros((n,))]

    t_knot_full = np.array([0.0])
    q0 = arm.ik(rp.transl(xs[0]), np.zeros((n,)), rep='cart', method='jt').qf
    x0 = xs[0]

    for i in range(1, m):
        if dev_tol is None:
            q_knot = get_joint_trajectory(arm, x0, xs[i], q0, p=max(int(norm(xs[i] - xs[i - 1]) / path_step), 3), viz=viz)
        else:
            q_knot = get_joint_trajectory(arm, x0, xs[i], q0, dev_tol=dev_tol, viz=viz)
        t_knot, f, fd, fdd = scale_traj_dynamic(arm, q_knot, tau_lim, t_knot_full[-1])
        t_knot_full = np.hstack([t_knot_full, t_knot[-1]])
        funcs.append(f)
        dfuncs.append(fd)
        ddfuncs.append(fdd)
        q0 = q_knot[-1]
        x0 = arm.fk(q_knot[-1])[0:3, 3]

    funcs.append(lambda t: np.zeros((n,)))
    dfuncs.append(lambda t: np.zeros((n,)))
    ddfuncs.append(lambda t: np.zeros((n,)))

    f = rp.piecewise_func(funcs, t_knot_full)
    fd = rp.piecewise_func(dfuncs, t_knot_full)
    fdd = rp.piecewise_func(ddfuncs, t_knot_full)

    return t_knot_full, f, fd, fdd


if __name__ == '__main__':
    import pyqtgraph as pg
    from PyQt5.QtWidgets import QApplication
    import time

    n = 6
    arm = rp.PlanarDyn(n=n, L=4/n)
    tau_lim = np.linspace(10, 1, n)
    xs = np.array([[3.0, 0.5, 0],
                   [2.0, 2.0, 0],
                   [-2.0, 0.5, 0],
                   [3.0, 0.5, 0]])

    viz = rp.VizScene()
    for i, x in enumerate(xs):
        c = i / len(xs)
        viz.add_marker(x, color=[0.9 - c, 0.9 - c, 0.0 + c, 1])
    viz.add_arm(arm)
    viz.update()

    t_knot, f, fd, fdd = get_dynamic_path(arm, xs, tau_lim, path_step=0.75, viz=viz)
    print(f"Total Time: {t_knot[-1]}")

    p = 500
    ts = np.linspace(t_knot[0], t_knot[-1], p)
    qs = np.asarray([f(t) for t in ts])
    qds = np.asarray([fd(t) for t in ts])
    qdds = np.asarray([fdd(t) for t in ts])
    tau = np.asarray([arm.rne(q, qd, qdd) for q, qd, qdd in zip(qs, qds, qdds)])

    app = QApplication([])
    w = pg.GraphicsLayoutWidget()
    p1 = w.addPlot(row=0, col=0, title='Joint Trajectory')
    p2 = w.addPlot(row=1, col=0, title='Joint Speed Trajectory')
    p3 = w.addPlot(row=2, col=0, title='Torque Trajectory')
    p1.addLegend()
    p2.addLegend()
    p3.addLegend()

    for i in range(n):
        p1.plot(ts, qs[:, i], pen=(i, n), name=f'q {i}')
        p2.plot(ts, qds[:, i], pen=(i, n), name=f'qd {i}')
        p3.plot(ts, tau[:, i], pen=(i, n), name=f'tau {i}')


    w.show()

    while True:
        for t in ts:
            viz.update(f(t))
            time.sleep(ts[-1] / p)

    viz.hold()
