import numpy as np
import matplotlib.pyplot as plt


def min_jerk_spline(x0, xf, v0, vf, a0, af, t0, tf):

    T = tf - t0

    c0 = x0
    c1 = v0
    c2 = a0/2
    c3 = (T**2*(-a0 + af)/2 - a0 - 6*v0 - 4*vf - 10*x0 + 10*xf)/(T**2*(13*T - 12))
    c4 = (T**2*(5*T - 3)*(a0 - af) + 14*T*(-a0 - v0 + vf) - 15*(T - 2)*(a0 + 2*v0 + 2*x0 - 2*xf))/(2*T**4*(13*T - 12))
    c5 = (-T**2*(4*T - 3)*(a0 - af) + 6*T*(a0 + v0 - vf) + 6*(2*T - 3)*(a0 + 2*v0 + 2*x0 - 2*xf))/(2*T**5*(13*T - 12))

    print(c0, c1, c2, c3, c4, c5)

    def f(x):
        if x < t0:
            return x0
        elif x > tf:
            return xf
        else:
            x = x - t0
            return c0 + c1 * x + c2 * x ** 2 + c3 * x ** 3 + c4 * x ** 4 + c5 * x ** 5

    def fd(x):
        if x < t0:
            return v0
        elif x > tf:
            return vf
        else:
            x = x - t0
            return c1 + 2 * c2 * x ** 1 + 3 * c3 * x ** 2 + 4 * c4 * x ** 3 + 5 * c5 * x ** 4

    def fdd(x):
        if x < t0:
            return a0
        elif x > tf:
            return af
        else:
            x = x - t0
            return 2 * c2 + 6 * c3 * x + 12 * c4 * x ** 2 + 20 * c5 * x ** 3

    return f, fd, fdd


def spline4(x0, xf, v0, vf, a0, t0, tf):

    T = tf - t0

    c0 = x0
    c1 = v0
    c2 = a0/2
    c3 = (-T**2*a0 - 3*T*v0 - T*vf - 4*x0 + 4*xf)/T**3
    c4 = (T**2*a0 + 4*T*v0 + 2*T*vf + 6*x0 - 6*xf)/(2*T**4)

    print(c0, c1, c2, c3, c4)

    def f(x):
        if x < t0:
            return x0
        elif x > tf:
            return xf
        else:
            x = x - t0
            return c0 + c1 * x + c2 * x ** 2 + c3 * x ** 3 + c4 * x ** 4

    def fd(x):
        if x < t0:
            return v0
        elif x > tf:
            return vf
        else:
            x = x - t0
            return c1 + 2 * c2 * x ** 1 + 3 * c3 * x ** 2 + 4 * c4 * x ** 3

    def fdd(x):
        if x < t0:
            return a0
        elif x > tf:
            return 2 * c2 + 6 * c3 * tf + 12 * c4 * tf ** 2
        else:
            x = x - t0
            return 2 * c2 + 6 * c3 * x + 12 * c4 * x ** 2

    return f, fd, fdd

t0, tf = 0, 1
x0, xf = 0, 15 / 2
v0, vf = 5, 10
a0, af = 5, 5

# f, fd, fdd = min_jerk_spline(x0, xf, v0, vf, a0, af, t0, tf)
f, fd, fdd = spline4(x0, xf, v0, vf, a0, t0, tf)

ts = np.linspace(-0.5, 1.5, 1000)
ys = []
yds = []
ydds = []

for t in ts:
    ys.append(f(t))
    yds.append(fd(t))
    ydds.append(fdd(t))

fig, ax = plt.subplots()
ax.plot(ts, ys, color=[1, 0, 0, 1])
ax.plot(ts, yds, color=[0, 1, 0, 1])
ax.plot(ts, ydds, color=[0, 0, 1, 1])
plt.show()
