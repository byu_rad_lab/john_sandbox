import sympy as sp
import numpy as np
import matplotlib.pyplot as plt

x0, xf, v0, vf, a0, af = sp.symbols('x0, xf, v0, vf, a0, af', real=True)
t, t0, tf, T = sp.symbols('t, t0, tf, T', real=True)
# T = tf - t0
t0 = 0.0
tf = T
c0, c1, c2, c3, c4, c5 = sp.symbols('c0, c1, c2, c3, c4, c5', real=True)


def f(x):
    return c0 + c1 * x + c2 * x ** 2 + c3 * x ** 3 + c4 * x ** 4 + c5 * x ** 5


def fd(x):
    return c1 + 2 * c2 * x ** 1 + 3 * c3 * x ** 2 + 4 * c4 * x ** 3 + 5 * c5 * x ** 4


def fdd(x):
    return 2 * c2 + 6 * c3 * x + 12 * c4 * x ** 2 + 20 * c5 * x ** 3


A = sp.Matrix([[T ** 3, T ** 4, T ** 5],
               [3 * T ** 2, 4 * T ** 4, 5 * T ** 5],
               [6 * T, 12 * T ** 2, 20 * T ** 3]])

B = sp.Matrix([[xf - x0 - v0 - a0 / 2],
               [vf - v0 - a0],
               [af - a0]])

x = A.inv() @ B
x.simplify()

C_minjerk = [x0, v0, a0 / 2, x[0], x[1], x[2]]

print(f"5th Order Minimum Jerk: f(x) = sum(c[i]*t^i) from i = 0 to i = 5")
for i in range(6):
    print(f"c{i} = {C_minjerk[i]}")


def f(x):
    return c0 + c1 * x + c2 * x ** 2 + c3 * x ** 3 + c4 * x ** 4


def fd(x):
    return c1 + 2 * c2 * x ** 1 + 3 * c3 * x ** 2 + 4 * c4 * x ** 3


def fdd(x):
    return 2 * c2 + 6 * c3 * x + 12 * c4 * x ** 2


expr = [f(t0) - x0,
        fd(t0) - v0,
        fdd(t0) - a0,
        f(tf) - xf,
        fd(tf) - vf]

sol = sp.solve(expr, [c0, c1, c2, c3, c4])
C_4th = [sol[c0], sol[c1], sol[c2], sol[c3], sol[c4]]

print("4th Order Spline, with Specified Initial Acceleration:")
for i in range(5):
    print(f"c{i} = {C_4th[i]}")
