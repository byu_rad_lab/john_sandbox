import RoboPy as rp
import numpy as np
from numpy import pi
from numpy.linalg import norm, pinv
import pyqtgraph as pg
from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui import QPen
from PyQt5.QtCore import Qt
import time

arm = rp.PlanarDyn(n=3, L=1.0)

q0 = np.array([0.0, pi/4, -pi/2])
qd0 = np.array([0., 0., 0.])

x0 = arm.fk(q0)[0:2, 3]
xf = np.array([1.5, -1.0])

x = x0
q = q0
qd = qd0

v = xf - x0
vhat = v / norm(v)

e = xf - x
ehat = norm(e)

w = e - e @ vhat * vhat
what = w / norm(w)

viz = rp.VizScene()
viz.add_arm(arm)
viz.add_marker(xf, color=np.array([1, 0, 0, 1]))
viz.update(q0)
time.sleep(0.01)
dt = 0.01
t = 0.0

xs = [x]
qs = [q]
ts = [t]

k_goal = 1.0
k_path = 5.0
k_damp = 2.0
k_qd = 0.0

while norm(e) > 0.1 or norm(qd) > 1.0:

    J = arm.jacob(q)[0:2, :]
    Jdag = pinv(J)
    M = arm.get_M(q)

    T_goal = Jdag @ e * k_goal
    T_path = Jdag @ w * k_path
    T_damp = -Jdag @ J @ qd
    T_qd = -k_qd * qd
    T = T_goal + T_path + T_damp + T_qd

    qdd = arm.forward_EL(q, qd, T)
    q = q + dt * qd
    qd = qd + dt * qdd

    viz.update(q)
    time.sleep(dt)

    x = arm.fk(q)[0:2, 3]

    e = xf - x
    ehat = e / norm(e)

    w = e - e @ vhat * vhat
    what = w / norm(w)

    xs.append(x)
    qs.append(q)
    ts.append(t)

    t += dt

print("Final Solution Time:", t)

xs = np.asarray(xs)
qs = np.asarray(qs)
ts = np.asarray(ts)

app = QApplication([])
window = pg.GraphicsLayoutWidget()
p1 = window.addPlot(row=0, col=0, title='Task Space Path')
p1.setAspectLocked()
p1.plot([x0[0]], [x0[1]], pen=None, symbolPen='g', symbol='x')
p1.plot([xf[0]], [xf[1]], pen=None, symbolPen='r', symbol='x')
p1.plot(xs[:, 0], xs[:, 1], pen='b')
p1.plot([x0[0], xf[0]], [x0[1], xf[1]])

p2 = window.addPlot(row=0, col=1, title='Joint Space Path')
p2.plot(ts, qs[:, 0], pen='r')
p2.plot(ts, qs[:, 1], pen='g')
p2.plot(ts, qs[:, 2], pen='b')

window.show()
app.exec()

