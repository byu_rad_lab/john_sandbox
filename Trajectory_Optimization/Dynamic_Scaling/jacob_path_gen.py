import RoboPy as rp
import numpy as np
from numpy import pi
from numpy.linalg import norm
import time
import pyqtgraph as pg
from PyQt5.QtWidgets import QApplication

from pyoptsparse import Optimization, SLSQP, IPOPT


n = 3
arm = rp.PlanarDyn(n=n, L=1.0)

x0 = np.array([2.5, 0.5, 0.0])
xf = np.array([-1.0, 1.5, 0.0])
v = xf - x0
vhat = v / norm(v)

ikmeth = 'pinv'
sol = arm.ik(rp.transl(x0), rep='cart', method=ikmeth)
q0 = sol.qf

sol = arm.ik(rp.transl(xf), q0=q0, rep='cart', method=ikmeth)
qf = sol.qf

viz = rp.VizScene()
viz.add_arm(arm)
viz.add_marker(x0, color=np.array([0.5, 0.5, 0.2, 1]))
viz.add_marker(xf, color=np.array([0.5, 0.5, 0.2, 1]))
viz.update(q0)

dt = 0.01
dev_tol = 0.2
p = 1000

count = 0
dev = 0

q_knot = np.vstack([q0, qf])
alpha = np.array([0., 1.])

qs = np.zeros((0, n))

while dev > dev_tol or count == 0:

    qs = np.zeros((0, n))
    ind = 0
    for i in range(1, len(alpha)):
        a = alpha[i] - alpha[i-1]
        if i == len(alpha) - 1:
            c = p - ind
            qnew = rp.angle_linspace(q_knot[i - 1], q_knot[i], c, endpoint=False)
        else:
            c = int(a * p)
            qnew = rp.angle_linspace(q_knot[i - 1], q_knot[i], c, endpoint=True)
        ind += c
        qs = np.vstack([qs, qnew])

    # for q in qs[0:None:int(p / 100), :]:
    #     viz.update(q)
    #     time.sleep(dt)

    xs = np.zeros((p, 3))
    ds = np.zeros((p,))
    for i in range(p):
        xs[i] = arm.fk(qs[i])[0:3, 3]
        w = xs[i] - x0
        ds[i] = norm(w - w @ vhat * vhat)

    index = np.argmax(ds)
    dev = ds[index]

    if dev > dev_tol:
        t = index / p
        index = 0
        for i, a in enumerate(alpha):
            index = i
            if t < a:
                break
        alpha = np.insert(alpha, index, t)
        viz.add_marker(x0 + t * v, color=np.array([0.5, 0.5, 0.2, 1]))
        q_knot = q_knot[0:index]
        for i in range(index, len(alpha)):
            x = x0 + alpha[i] * v
            sol = arm.ik(rp.transl(x), q0=q_knot[i-1], rep='cart', method=ikmeth)
            q_knot = np.vstack([q_knot, sol.qf])

    count += 1

m = len(alpha)

dt_init = np.ones((m - 1,)) * 5.0
tau_lim = np.array([10., 3., 1.])
tau_lb = np.ravel(np.asarray([-tau_lim] * m))
tau_ub = np.ravel(np.asarray([tau_lim] * m))

t_knot = np.asarray([np.sum(dt_init[0:i]) for i in range(m)])

qs = q_knot
f, fd, fdd = rp.quintic_spline(t_knot, qs, deriv=True)

ts = np.linspace(t_knot[0], t_knot[-1], 100)
qs = []
taus = []
for t in ts:
    q, qd, qdd = f(t), fd(t), fdd(t)
    tau = arm.rne(q, qd, qdd, g=np.array([0, 0, 0]))
    qs.append(q)
    taus.append(tau)

qs = np.asarray(qs)
taus = np.asarray(taus)

window = pg.GraphicsLayoutWidget()
p1 = window.addPlot(row=0, col=0, title='Joints')
q0 = p1.plot(ts, qs[:, 0], pen=(0, 3))
q1 = p1.plot(ts, qs[:, 1], pen=(1, 3))
q2 = p1.plot(ts, qs[:, 2], pen=(2, 3))

p2 = window.addPlot(row=0, col=1, title='Torqe')
tau0 = p2.plot(ts, taus[:, 0], pen=(0, 3))
tau1 = p2.plot(ts, taus[:, 1], pen=(1, 3))
tau2 = p2.plot(ts, taus[:, 2], pen=(2, 3))
window.show()

def optfunc(xdict):
    dt = xdict['dt']
    if np.min(dt) <= 0:
        return {'obj': 0.0, 'cubic': np.ones((n * m,)) * 1e20, 'minjerk': np.ones((n,)) * 1e20}, True
    t_knot = np.asarray([np.sum(dt[0:i]) for i in range(m)])
    t_total = np.sum(dt)

    viz.app.processEvents()

    qs = q_knot
    qds = np.zeros_like(qs)
    for i in range(1, m - 1):
        qds[i] = (qs[i+1] - qs[i]) / (t_knot[i+1] - t_knot[i])
    qdds = np.asarray([[None] * n] * m)
    # f, fd, fdd = rp.min_accel_spline(t_knot, qs, qds, qdds, deriv=True)
    f, fd, fdd = rp.cubic_spline(t_knot, q_knot, deriv=True)

    tau_knot = np.zeros((m, n))

    eps = 1e-4
    for i, t in enumerate(t_knot):
        q, qd, qdd = f(t), fd(t), fdd(t)
        qddl, qddr = fdd(t-eps), fdd(t+eps)
        taul = arm.rne(q, qd, qddl, g=np.zeros((3,)))
        taur = arm.rne(q, qd, qddr, g=np.zeros((3,)))
        tau_knot[i] = np.maximum(np.abs(taul), np.abs(taur))
        # tau_knot[i] = arm.rne(q, qd, qdd, g=np.zeros((3,)))

    ts = np.linspace(t_knot[0], t_knot[-1], 50)
    qs = []
    taus = []
    for t in ts:
        q, qd, qdd = f(t), fd(t), fdd(t)
        tau = arm.rne(q, qd, qdd, g=np.array([0, 0, 0]))
        qs.append(q)
        taus.append(tau)
    qs = np.asarray(qs)
    taus = np.asarray(taus)

    q0.setData(ts, qs[:, 0])
    q1.setData(ts, qs[:, 1])
    q2.setData(ts, qs[:, 2])

    tau0.setData(ts, taus[:, 0])
    tau1.setData(ts, taus[:, 1])
    tau2.setData(ts, taus[:, 2])

    output = {}
    output['obj'] = t_total
    output['cubic'] = np.ravel(tau_knot)
    # output['minjerk'] = tau_con
    return output, False

optProb = Optimization("Time Opt", optfunc)
optProb.addVarGroup('dt', m - 1,
                    varType='c',
                    lower=0.01,
                    upper=10.0,
                    scale=1.0,
                    value=dt_init)

optProb.addConGroup('cubic', n * m,
                    lower=tau_lb,
                    upper=tau_ub)
# optProb.addConGroup('minjerk', n,
#                     lower=-10,
#                     upper=10)
optProb.addObj('obj')

# opt = SLSQP(options={'IPRINT': 0, 'ACC': 1e-2, 'MAXIT': 100})
opt = IPOPT(options={'tol': 1e0, 'constr_viol_tol': 1e-2, 'print_level': 2,
                         'dual_inf_tol': 1.0, 'compl_inf_tol': 1e-4,
                         'max_wall_time': 10.0 * m})
sol = opt(optProb, sens='FD')
print(sol)

dt_final = sol.xStar['dt']
ts_final = np.asarray([np.sum(dt_final[0:i]) for i in range(m)])

f, fd, fdd = rp.cubic_spline(ts_final, q_knot, deriv=True)

# qds = np.zeros_like(q_knot)
# for i in range(1, m - 1):
#     qds[i] = (q_knot[i+1] - q_knot[i]) / (t_knot[i+1] - t_knot[i])
# qdds = np.asarray([[None] * n] * m)
# f, fd, fdd = rp.min_accel_spline(t_knot, q_knot, qds, qdds, deriv=True)

ts = np.arange(ts_final[0], ts_final[-1], 0.01)
qs = []
taus = []
for t in ts:
    q, qd, qdd = f(t), fd(t), fdd(t)
    tau = arm.rne(q, qd, qdd, g=np.array([0, 0, 0]))
    qs.append(q)
    taus.append(tau)
qs = np.asarray(qs)
taus = np.asarray(taus)
q0.setData(ts, qs[:, 0])
q1.setData(ts, qs[:, 1])
q2.setData(ts, qs[:, 2])

tau0.setData(ts, taus[:, 0])
tau1.setData(ts, taus[:, 1])
tau2.setData(ts, taus[:, 2])


while True:
    for q in qs:
        viz.update(q)
        time.sleep(0.01)
