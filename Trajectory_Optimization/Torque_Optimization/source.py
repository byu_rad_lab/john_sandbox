from param import *
from dataclasses import dataclass
import numpy as np
from numpy.linalg import norm


@dataclass
class TrajResult:
    ts: np.ndarray = np.array([])
    qs: np.ndarray = np.array([])
    xs: np.ndarray = np.array([])
    taus: np.ndarray = np.array([])
    q0: np.ndarray = np.array([])
    qd0: np.ndarray = np.array([])
    x0: np.ndarray = np.array([])
    xf: np.ndarray = np.array([])
    tmin: np.ndarray = np.array([])
    tmax: np.ndarray = np.array([])
    dtopt: float = 0.1
    dtsim: float = 0.05
    k: int = 2
    opt_time: float = 0.0


def q2x(q):
    if len(q.shape) == 1:
        # single joint space
        x = arm.fk(q)[0:2, 3]
        return x
    else:
        xs = np.zeros((q.shape[0], 2))
        for i, a in enumerate(q):
            xs[i] = arm.fk(a)[0:2, 3]
        return xs


def interpolate_input(tau_input, method='prev'):
    # Interpolate tau_input with p.ratio steps between each tau
    p = tau_input.shape[0]
    if p == 1:
        tau_output = np.ones((p * k, n)) * tau_input
        return tau_output
    if method == 'prev':
        tau_output = np.zeros(((p + 1) * k, n))
        for i in range(p):
            # tau_output[i:i+k] = tau_output[i:i+k] + tau_input[i]
            tau_output[i*k:(i+1)*k] = tau_output[i*k:(i+1)*k] + tau_input[i]
    else:
        tau_output = np.zeros((p * k, n))
        for i, z in enumerate(zip(tau_input[0:p-1], tau_input[1:p])):
            tc, tn = z[0], z[1]
            index = i * k
            for j in range(k):
                if method == 'mean':
                    tau = (tc + tn) / 2
                elif method == 'prev':
                    tau = tc
                elif method == 'linear':
                    tau = tc + (tn - tc) * j / k
                tau_output[index + j] = tau

    return tau_output


def forward_simulate(q0, qd0, taus, dt):
    q = q0
    qd = qd0
    qs = []
    qds = []
    for i, tau in enumerate(taus):
        qdd = arm.forward_rne(q, qd, tau)
        q = q + qd * dt
        qd = qd + qdd * dt
        q = rp.wrap_angle(q)
        qs.append(q)
        qds.append(qd)

    return np.asarray(qs), np.asarray(qds)


def path_dist(x, vhat):
    # v = xf - x0
    # vhat = v / norm(v)
    w = x - x0
    return norm(w - w @ vhat * vhat)


def path_error(xs):
    v = xf - x0
    vhat = v / norm(v)
    ds = np.zeros((xs.shape[0],))
    for i, x in enumerate(xs):
        w = x - x0
        ds[i] = norm(w - w @ vhat * vhat)
    return np.sum(ds)