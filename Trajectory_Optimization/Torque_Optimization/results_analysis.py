import pickle
from source import q2x, TrajResult
from param import arm
import RoboPy as rp
import pyqtgraph as pg
from PyQt5 import QtWidgets
import time
import numpy as np


with open('results2.traj', 'rb') as file:
    result = pickle.load(file)

print("Optimization Time: ", result.opt_time)

app = QtWidgets.QApplication([])

window = pg.GraphicsLayoutWidget()
p1 = window.addPlot(row=0, col=0, title='Task Space Path')
p1.setAspectLocked()
p1.plot(result.xs[:, 0], result.xs[:, 1])
p1.plot(result.xf[0, None], result.xf[1, None], pen=None, symbol='x')
p1.plot(result.x0[0, None], result.x0[1, None], pen=None, symbol='o')

p2 = window.addPlot(row=0, col=1, title='Joints vs Time')
p2.plot(result.ts, result.qs[:, 0], pen=(0, 3))
p2.plot(result.ts, result.qs[:, 1], pen=(1, 3))
p2.plot(result.ts, result.qs[:, 2], pen=(2, 3))

p3 = window.addPlot(row=0, col=2, title='Torques vs Time')
p3.plot(result.ts, result.taus[:, 0], pen=(0, 3))
p3.plot(result.ts, result.taus[:, 1], pen=(1, 3))
p3.plot(result.ts, result.taus[:, 2], pen=(2, 3))
window.show()

viz = rp.VizScene()
viz.add_arm(arm)
viz.add_marker(np.append(result.x0, 0), color=np.array([0, 0, 1, 1]))
viz.add_marker(np.append(result.xf, 0), color=np.array([0, 1, 0, 1]))
viz.update(result.q0)

while True:

    for q in result.qs:
        viz.update(q)
        time.sleep(result.dtsim)
