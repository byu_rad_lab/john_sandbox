from pyoptsparse import Optimization, SLSQP, IPOPT
from source import q2x, interpolate_input, forward_simulate, TrajResult, path_error, path_dist
from param import *
import numpy as np
from numpy.linalg import norm
import pickle
import RoboPy as rp
from PyQt5.QtWidgets import QApplication
import pyqtgraph as pg
from scipy.optimize import minimize
import time


opt = IPOPT(options={'tol': 1e0, 'constr_viol_tol': 1e-2, 'print_level': 2,
                         'dual_inf_tol': 1.0, 'compl_inf_tol': 1e-4,
                         'max_wall_time': 120.0})
q = q0
qd = qd0

x = q2x(q)
t = 0

tau_list = np.zeros((0, n))
qs_list = np.zeros((0, n))
qds_list = np.zeros((0, n))
ts_list = []
tstart = time.perf_counter()
opt_time = 0.0

v = (xf - x0) / norm(xf - x0)
J = arm.jacob(q)[0:2, :]
qdd0 = np.linalg.pinv(J) @ v
tau_des = arm.EL(q0, qd0, qdd0)
tau_init = np.ravel(np.asarray([tau_des for i in range(m)]))
# tau_init = np.ravel(np.random.random_sample((m, n)) * (tmax - tmin) + tmin)
# tau_init = np.zeros((m * n,))
count = 0

app = QApplication([])
window = pg.GraphicsLayoutWidget()
p1 = window.addPlot(title='Task Space Path')
p1.setAspectLocked()
p1.plot([x0[0]], [x0[1]], pen=None, symbol='o', symbolBrush=(0, 0, 255, 100))
p1.plot([xf[0]], [xf[1]], pen=None, symbol='x', symbolBrush=(0, 0, 255, 100))
point = p1.plot([x[0]], [x[1]], pen=None, symbol='d', symbolBrush=(230, 230, 0, 100))
curve = p1.plot([x0[0]], [x0[1]], pen='r')
window.show()
time.sleep(0.25)
app.processEvents()

kp = 1.0
kd = 1.0
kc = 10.0
ke = 0.0

dist_func = rp.cubic_interpolation(0, eps_end * 10, 0, 1)
plane_func = rp.cubic_interpolation(eps_end * 5, -eps_end * 5, 0, -1)

while (norm(x - xf) > eps_end or norm(qd) > eps_vel) and opt_time < 2000:

    print(f"Major Iteration: {count}, Dist: {norm(x - xf)}, Vel: {norm(qd)}")

    def func(xdict):
        tau_input = np.reshape(xdict['taus'], (m, n))
        taus = interpolate_input(tau_input, method='prev')
        qs, qds = forward_simulate(q, qd, taus, dt_sim)
        xs = q2x(qs)
        output = {}
        dist_cost = 0.0
        end_cost = norm(xs[-1] - xf)
        speed_cost = 0.0
        path_cost = 0.0
        vhat = (xf - x0) / norm(xf - x0)
        for i, x in enumerate(xs):
            d = norm(x - xf)
            k_dist = dist_func(d)
            k_plane = plane_func(norm((xf - x) @ vhat))
            # print(k_dist)
            # print(k_plane)
            # print()
            speed_cost += norm(qds[i]) * (1 - k_plane)
            path_cost += path_dist(x, vhat) * k_dist
            dist_cost += d

        output['cost'] = dist_cost * kp + speed_cost * kd + path_cost * kc + end_cost * ke
        # print(f"Objective: {output['cost']}")
        return output, False


    optProb = Optimization("Torque Optimization", func)
    optProb.addVarGroup('taus', m * n,
                        varType='c',
                        lower=tau_lb,
                        upper=tau_ub,
                        scale=1.0,
                        value=tau_init)
    optProb.addObj('cost')
    sol = opt(optProb, sens='FD')
    tau_final = np.reshape(sol.xStar['taus'], (m, n))

    # tau_init = np.ravel(tau_final)
    # tau_init = np.ravel(np.zeros((m, n)))
    start = int(p * dt_sim / dt_opt)
    tau_init = np.ravel(np.append(tau_final[start:None], np.zeros((start, n))))
    # tau_init = np.random.random_sample((m * n,)) * 2 - 1

    # v = (xf - x0) / norm(xf - x0)
    # J = arm.jacob(q)[0:2, :]
    # qdd0 = np.linalg.pinv(J) @ v
    # tau_des = arm.EL(q0, qd0, qdd0)
    # tau_init = np.ravel(np.asarray([tau_des for i in range(m)]))

    taus = interpolate_input(tau_final)[0:p]
    qs, qds = forward_simulate(q, qd, taus, dt_sim)
    q = qs[-1]
    qd = qds[-1]
    x = q2x(q)
    ts = [t + dt_sim * i for i in range(p)]
    t = ts[-1] + dt_sim
    tau_list = np.vstack([tau_list, taus])
    qs_list = np.vstack([qs_list, qs])
    qds_list = np.vstack([qds_list, qds])
    xs = q2x(qs_list)
    curve.setData(xs[:, 0], xs[:, 1], pen='r')
    point.setData([x[0]], [x[1]])
    time.sleep(0.25)
    app.processEvents()
    ts_list.append(ts)
    # opt_time += sol.optTime
    opt_time += time.perf_counter() - tstart
    tstart = time.perf_counter()
    print(f"Current Time: {opt_time}")
    count += 1

    taus = interpolate_input(tau_final)[0:r]
    qs, qds = forward_simulate(q, qd, taus, dt_sim)
    xs = q2x(qs)

    # kp *= 1.1
    # if np.min(norm(xs - xf, axis=1)) < eps_end:
    #     kd += 0.1
    #     kc = 0.0
    #     print("active")

ts = np.ravel(np.asarray(ts_list))
qs = qs_list
qds = qds_list
taus = np.reshape(np.asarray(tau_list), (count * p, n))
index = count * p

for i, z in enumerate(zip(qs, qds)):
    q, qd = z[0], z[1]
    if norm(q2x(q) - xf) < eps_end and norm(qd) < eps_vel:
        index = i
        break

ts = ts[0:index]
qs = qs[0:index]
taus = taus[0:index]
xs = q2x(qs)

result = TrajResult(ts, qs, xs, taus, q0, qd0, x0, xf, tmin, tmax, dt_opt, dt_sim, k, opt_time)

with open('results2.traj', 'wb') as file:
    pickle.dump(result, file)

print(f"Optimization Concluded:\nTotal Time: {opt_time}")

app.exec()
