import RoboPy as rp
import numpy as np
from numpy import pi


dt_opt = 0.15
dt_sim = 0.05
k = int(dt_opt / dt_sim)

n = 3
m = 10
p = 6
r = 25

arm = rp.PlanarDyn(n=n, L=1.0, joint_damping=np.ones((n,)) * 0.01)

tmin = np.array([-10, -1, -1])
tmax = np.array([10, 1, 1])

tau_lb = np.ravel(np.asarray([tmin for i in range(m)]))
tau_ub = np.ravel(np.asarray([tmax for j in range(m)]))

q0 = np.array([0.0, pi/4, -pi/2])
qd0 = np.array([0., 0., 0.])

x0 = arm.fk(q0)[0:2, 3]
print(f"Target: {x0}")
xf = np.array([1.5, 1.0])

eps_end = 1e-2
eps_vel = 1e-2
